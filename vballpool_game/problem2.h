vector<b2Body*> bodylist;

//
//rect3072
//
b2BodyDef rect3072_bdef;
rect3072_bdef.type = b2_dynamicBody;
rect3072_bdef.bullet = false;
rect3072_bdef.position.Set(-3.52008475484f,15.3193141334f);
rect3072_bdef.angle=-0.0f;
rect3072_bdef.linearDamping = 0.3f;
rect3072_bdef.angularDamping =0.3f;
b2Body* rect3072_body = m_world->CreateBody(&rect3072_bdef);

b2PolygonShape rect3072_ps;
rect3072_ps.SetAsBox(2.05522085894f, 3.16576326356f);

b2FixtureDef rect3072_fdef;
rect3072_fdef.shape = &rect3072_ps;

rect3072_fdef.density = 1.0f;
rect3072_fdef.friction = 0.2f;
rect3072_fdef.restitution = 0.4f;
rect3072_fdef.filter.groupIndex = 0;

rect3072_body->CreateFixture(&rect3072_fdef);
bodylist.push_back(rect3072_body);
uData* rect3072_ud = new uData();
rect3072_ud->name = "TITLE";
rect3072_ud->fill.Set(float(0)/255,float(255)/255,float(0)/255);
rect3072_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rect3072_ud->strokewidth=3.71699953f;
rect3072_body->SetUserData(rect3072_ud);

//
//rect3073
//
b2BodyDef rect3073_bdef;
rect3073_bdef.type = b2_dynamicBody;
rect3073_bdef.bullet = false;
rect3073_bdef.position.Set(2.07166365207f,15.553118204f);
rect3073_bdef.angle=-0.0f;
rect3073_bdef.linearDamping = 0.3f;
rect3073_bdef.angularDamping =0.3f;
b2Body* rect3073_body = m_world->CreateBody(&rect3073_bdef);

b2PolygonShape rect3073_ps;
rect3073_ps.SetAsBox(2.05711182041f, 3.99763872382f);

b2FixtureDef rect3073_fdef;
rect3073_fdef.shape = &rect3073_ps;

rect3073_fdef.density = 1.0f;
rect3073_fdef.friction = 0.2f;
rect3073_fdef.restitution = 0.4f;
rect3073_fdef.filter.groupIndex = 0;

rect3073_body->CreateFixture(&rect3073_fdef);
bodylist.push_back(rect3073_body);
uData* rect3073_ud = new uData();
rect3073_ud->name = "TITLE";
rect3073_ud->fill.Set(float(255)/255,float(0)/255,float(0)/255);
rect3073_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rect3073_ud->strokewidth=1.39247942f;
rect3073_body->SetUserData(rect3073_ud);
