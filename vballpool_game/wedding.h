vector<b2Body*> bodylist;

//
//path4259
//
b2BodyDef path4259_bdef;
path4259_bdef.type = b2_staticBody;
path4259_bdef.bullet = false;
path4259_bdef.position.Set(-9.38173558855f,21.4030867395f);
path4259_bdef.linearDamping = 0.0f;
path4259_bdef.angularDamping =0.0f;
b2Body* path4259_body = m_world->CreateBody(&path4259_bdef);

b2CircleShape path4259_s;
path4259_s.m_radius=0.318572368514f;

b2FixtureDef path4259_fdef;
path4259_fdef.shape = &path4259_s;

path4259_fdef.density = 1.0f;
path4259_fdef.friction = 0.1f;
path4259_fdef.restitution = 0.8f;
path4259_fdef.filter.groupIndex = -1;

path4259_body->CreateFixture(&path4259_fdef);
bodylist.push_back(path4259_body);
uData* path4259_ud = new uData();
path4259_ud->name = "TITLE";
path4259_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4259_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4259_ud->strokewidth=24.23800087f;
path4259_body->SetUserData(path4259_ud);

//
//path4255
//
b2BodyDef path4255_bdef;
path4255_bdef.type = b2_staticBody;
path4255_bdef.bullet = false;
path4255_bdef.position.Set(4.15298348496f,11.3507545182f);
path4255_bdef.linearDamping = 0.0f;
path4255_bdef.angularDamping =0.0f;
b2Body* path4255_body = m_world->CreateBody(&path4255_bdef);

b2CircleShape path4255_s;
path4255_s.m_radius=0.318572368514f;

b2FixtureDef path4255_fdef;
path4255_fdef.shape = &path4255_s;

path4255_fdef.density = 1.0f;
path4255_fdef.friction = 0.1f;
path4255_fdef.restitution = 0.8f;
path4255_fdef.filter.groupIndex = -1;

path4255_body->CreateFixture(&path4255_fdef);
bodylist.push_back(path4255_body);
uData* path4255_ud = new uData();
path4255_ud->name = "TITLE";
path4255_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4255_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4255_ud->strokewidth=24.23800087f;
path4255_body->SetUserData(path4255_ud);

//
//path3462
//
b2BodyDef path3462_bdef;
path3462_bdef.type = b2_staticBody;
path3462_bdef.bullet = false;
path3462_bdef.position.Set(4.15298348496f,11.9448139273f);
path3462_bdef.linearDamping = 0.0f;
path3462_bdef.angularDamping =0.0f;
b2Body* path3462_body = m_world->CreateBody(&path3462_bdef);

b2CircleShape path3462_s;
path3462_s.m_radius=0.318572368514f;

b2FixtureDef path3462_fdef;
path3462_fdef.shape = &path3462_s;

path3462_fdef.density = 1.0f;
path3462_fdef.friction = 0.1f;
path3462_fdef.restitution = 0.8f;
path3462_fdef.filter.groupIndex = -1;

path3462_body->CreateFixture(&path3462_fdef);
bodylist.push_back(path3462_body);
uData* path3462_ud = new uData();
path3462_ud->name = "TITLE";
path3462_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path3462_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3462_ud->strokewidth=24.23800087f;
path3462_body->SetUserData(path3462_ud);

//
//path3460
//
b2BodyDef path3460_bdef;
path3460_bdef.type = b2_staticBody;
path3460_bdef.bullet = false;
path3460_bdef.position.Set(4.15298348496f,12.5388733365f);
path3460_bdef.linearDamping = 0.0f;
path3460_bdef.angularDamping =0.0f;
b2Body* path3460_body = m_world->CreateBody(&path3460_bdef);

b2CircleShape path3460_s;
path3460_s.m_radius=0.318572368514f;

b2FixtureDef path3460_fdef;
path3460_fdef.shape = &path3460_s;

path3460_fdef.density = 1.0f;
path3460_fdef.friction = 0.1f;
path3460_fdef.restitution = 0.8f;
path3460_fdef.filter.groupIndex = -1;

path3460_body->CreateFixture(&path3460_fdef);
bodylist.push_back(path3460_body);
uData* path3460_ud = new uData();
path3460_ud->name = "TITLE";
path3460_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path3460_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3460_ud->strokewidth=24.23800087f;
path3460_body->SetUserData(path3460_ud);

//
//path3458
//
b2BodyDef path3458_bdef;
path3458_bdef.type = b2_staticBody;
path3458_bdef.bullet = false;
path3458_bdef.position.Set(4.15298348496f,13.2913485802f);
path3458_bdef.linearDamping = 0.0f;
path3458_bdef.angularDamping =0.0f;
b2Body* path3458_body = m_world->CreateBody(&path3458_bdef);

b2CircleShape path3458_s;
path3458_s.m_radius=0.318572368514f;

b2FixtureDef path3458_fdef;
path3458_fdef.shape = &path3458_s;

path3458_fdef.density = 1.0f;
path3458_fdef.friction = 0.1f;
path3458_fdef.restitution = 0.8f;
path3458_fdef.filter.groupIndex = -1;

path3458_body->CreateFixture(&path3458_fdef);
bodylist.push_back(path3458_body);
uData* path3458_ud = new uData();
path3458_ud->name = "TITLE";
path3458_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path3458_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3458_ud->strokewidth=24.23800087f;
path3458_body->SetUserData(path3458_ud);

//
//path3456
//
b2BodyDef path3456_bdef;
path3456_bdef.type = b2_staticBody;
path3456_bdef.bullet = false;
path3456_bdef.position.Set(4.15298348496f,14.0834277534f);
path3456_bdef.linearDamping = 0.0f;
path3456_bdef.angularDamping =0.0f;
b2Body* path3456_body = m_world->CreateBody(&path3456_bdef);

b2CircleShape path3456_s;
path3456_s.m_radius=0.318572368514f;

b2FixtureDef path3456_fdef;
path3456_fdef.shape = &path3456_s;

path3456_fdef.density = 1.0f;
path3456_fdef.friction = 0.1f;
path3456_fdef.restitution = 0.8f;
path3456_fdef.filter.groupIndex = -1;

path3456_body->CreateFixture(&path3456_fdef);
bodylist.push_back(path3456_body);
uData* path3456_ud = new uData();
path3456_ud->name = "TITLE";
path3456_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path3456_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3456_ud->strokewidth=24.23800087f;
path3456_body->SetUserData(path3456_ud);

//
//path3454
//
b2BodyDef path3454_bdef;
path3454_bdef.type = b2_staticBody;
path3454_bdef.bullet = false;
path3454_bdef.position.Set(4.11337920482f,14.915110973f);
path3454_bdef.linearDamping = 0.0f;
path3454_bdef.angularDamping =0.0f;
b2Body* path3454_body = m_world->CreateBody(&path3454_bdef);

b2CircleShape path3454_s;
path3454_s.m_radius=0.318572368514f;

b2FixtureDef path3454_fdef;
path3454_fdef.shape = &path3454_s;

path3454_fdef.density = 1.0f;
path3454_fdef.friction = 0.1f;
path3454_fdef.restitution = 0.8f;
path3454_fdef.filter.groupIndex = -1;

path3454_body->CreateFixture(&path3454_fdef);
bodylist.push_back(path3454_body);
uData* path3454_ud = new uData();
path3454_ud->name = "TITLE";
path3454_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path3454_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3454_ud->strokewidth=24.23800087f;
path3454_body->SetUserData(path3454_ud);

//
//path4220
//
b2BodyDef path4220_bdef;
path4220_bdef.type = b2_staticBody;
path4220_bdef.bullet = false;
path4220_bdef.position.Set(8.62633128919f,17.8827191032f);
path4220_bdef.linearDamping = 0.0f;
path4220_bdef.angularDamping =0.0f;
b2Body* path4220_body = m_world->CreateBody(&path4220_bdef);

b2CircleShape path4220_s;
path4220_s.m_radius=0.318572368514f;

b2FixtureDef path4220_fdef;
path4220_fdef.shape = &path4220_s;

path4220_fdef.density = 1.0f;
path4220_fdef.friction = 0.1f;
path4220_fdef.restitution = 0.8f;
path4220_fdef.filter.groupIndex = -1;

path4220_body->CreateFixture(&path4220_fdef);
bodylist.push_back(path4220_body);
uData* path4220_ud = new uData();
path4220_ud->name = "TITLE";
path4220_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4220_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4220_ud->strokewidth=24.23800087f;
path4220_body->SetUserData(path4220_ud);

//
//path3316
//
b2BodyDef path3316_bdef;
path3316_bdef.type = b2_staticBody;
path3316_bdef.bullet = false;
path3316_bdef.position.Set(8.66593556933f,18.6747983933f);
path3316_bdef.linearDamping = 0.0f;
path3316_bdef.angularDamping =0.0f;
b2Body* path3316_body = m_world->CreateBody(&path3316_bdef);

b2CircleShape path3316_s;
path3316_s.m_radius=0.318572368514f;

b2FixtureDef path3316_fdef;
path3316_fdef.shape = &path3316_s;

path3316_fdef.density = 1.0f;
path3316_fdef.friction = 0.1f;
path3316_fdef.restitution = 0.8f;
path3316_fdef.filter.groupIndex = -1;

path3316_body->CreateFixture(&path3316_fdef);
bodylist.push_back(path3316_body);
uData* path3316_ud = new uData();
path3316_ud->name = "TITLE";
path3316_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3316_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3316_ud->strokewidth=24.23800087f;
path3316_body->SetUserData(path3316_ud);

//
//path3314
//
b2BodyDef path3314_bdef;
path3314_bdef.type = b2_staticBody;
path3314_bdef.bullet = false;
path3314_bdef.position.Set(8.66593556933f,19.3876695907f);
path3314_bdef.linearDamping = 0.0f;
path3314_bdef.angularDamping =0.0f;
b2Body* path3314_body = m_world->CreateBody(&path3314_bdef);

b2CircleShape path3314_s;
path3314_s.m_radius=0.318572368514f;

b2FixtureDef path3314_fdef;
path3314_fdef.shape = &path3314_s;

path3314_fdef.density = 1.0f;
path3314_fdef.friction = 0.1f;
path3314_fdef.restitution = 0.8f;
path3314_fdef.filter.groupIndex = -1;

path3314_body->CreateFixture(&path3314_fdef);
bodylist.push_back(path3314_body);
uData* path3314_ud = new uData();
path3314_ud->name = "TITLE";
path3314_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3314_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3314_ud->strokewidth=24.23800087f;
path3314_body->SetUserData(path3314_ud);

//
//path3312
//
b2BodyDef path3312_bdef;
path3312_bdef.type = b2_staticBody;
path3312_bdef.bullet = false;
path3312_bdef.position.Set(9.10157914381f,20.1797487639f);
path3312_bdef.linearDamping = 0.0f;
path3312_bdef.angularDamping =0.0f;
b2Body* path3312_body = m_world->CreateBody(&path3312_bdef);

b2CircleShape path3312_s;
path3312_s.m_radius=0.318572368514f;

b2FixtureDef path3312_fdef;
path3312_fdef.shape = &path3312_s;

path3312_fdef.density = 1.0f;
path3312_fdef.friction = 0.1f;
path3312_fdef.restitution = 0.8f;
path3312_fdef.filter.groupIndex = -1;

path3312_body->CreateFixture(&path3312_fdef);
bodylist.push_back(path3312_body);
uData* path3312_ud = new uData();
path3312_ud->name = "TITLE";
path3312_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3312_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3312_ud->strokewidth=24.23800087f;
path3312_body->SetUserData(path3312_ud);

//
//path3310
//
b2BodyDef path3310_bdef;
path3310_bdef.type = b2_staticBody;
path3310_bdef.bullet = false;
path3310_bdef.position.Set(9.41841104689f,20.773808173f);
path3310_bdef.linearDamping = 0.0f;
path3310_bdef.angularDamping =0.0f;
b2Body* path3310_body = m_world->CreateBody(&path3310_bdef);

b2CircleShape path3310_s;
path3310_s.m_radius=0.318572368514f;

b2FixtureDef path3310_fdef;
path3310_fdef.shape = &path3310_s;

path3310_fdef.density = 1.0f;
path3310_fdef.friction = 0.1f;
path3310_fdef.restitution = 0.8f;
path3310_fdef.filter.groupIndex = -1;

path3310_body->CreateFixture(&path3310_fdef);
bodylist.push_back(path3310_body);
uData* path3310_ud = new uData();
path3310_ud->name = "TITLE";
path3310_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3310_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3310_ud->strokewidth=24.23800087f;
path3310_body->SetUserData(path3310_ud);

//
//path3308
//
b2BodyDef path3308_bdef;
path3308_bdef.type = b2_staticBody;
path3308_bdef.bullet = false;
path3308_bdef.position.Set(9.8936589015f,21.4470755579f);
path3308_bdef.linearDamping = 0.0f;
path3308_bdef.angularDamping =0.0f;
b2Body* path3308_body = m_world->CreateBody(&path3308_bdef);

b2CircleShape path3308_s;
path3308_s.m_radius=0.318572368514f;

b2FixtureDef path3308_fdef;
path3308_fdef.shape = &path3308_s;

path3308_fdef.density = 1.0f;
path3308_fdef.friction = 0.1f;
path3308_fdef.restitution = 0.8f;
path3308_fdef.filter.groupIndex = -1;

path3308_body->CreateFixture(&path3308_fdef);
bodylist.push_back(path3308_body);
uData* path3308_ud = new uData();
path3308_ud->name = "TITLE";
path3308_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3308_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3308_ud->strokewidth=24.23800087f;
path3308_body->SetUserData(path3308_ud);

//
//path3306
//
b2BodyDef path3306_bdef;
path3306_bdef.type = b2_staticBody;
path3306_bdef.bullet = false;
path3306_bdef.position.Set(8.11148032344f,20.0609369756f);
path3306_bdef.linearDamping = 0.0f;
path3306_bdef.angularDamping =0.0f;
b2Body* path3306_body = m_world->CreateBody(&path3306_bdef);

b2CircleShape path3306_s;
path3306_s.m_radius=0.318572368514f;

b2FixtureDef path3306_fdef;
path3306_fdef.shape = &path3306_s;

path3306_fdef.density = 1.0f;
path3306_fdef.friction = 0.1f;
path3306_fdef.restitution = 0.8f;
path3306_fdef.filter.groupIndex = -1;

path3306_body->CreateFixture(&path3306_fdef);
bodylist.push_back(path3306_body);
uData* path3306_ud = new uData();
path3306_ud->name = "TITLE";
path3306_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3306_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3306_ud->strokewidth=24.23800087f;
path3306_body->SetUserData(path3306_ud);

//
//path3304
//
b2BodyDef path3304_bdef;
path3304_bdef.type = b2_staticBody;
path3304_bdef.bullet = false;
path3304_bdef.position.Set(7.71544102908f,20.8134122193f);
path3304_bdef.linearDamping = 0.0f;
path3304_bdef.angularDamping =0.0f;
b2Body* path3304_body = m_world->CreateBody(&path3304_bdef);

b2CircleShape path3304_s;
path3304_s.m_radius=0.318572368514f;

b2FixtureDef path3304_fdef;
path3304_fdef.shape = &path3304_s;

path3304_fdef.density = 1.0f;
path3304_fdef.friction = 0.1f;
path3304_fdef.restitution = 0.8f;
path3304_fdef.filter.groupIndex = -1;

path3304_body->CreateFixture(&path3304_fdef);
bodylist.push_back(path3304_body);
uData* path3304_ud = new uData();
path3304_ud->name = "TITLE";
path3304_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3304_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3304_ud->strokewidth=24.23800087f;
path3304_body->SetUserData(path3304_ud);

//
//path3302
//
b2BodyDef path3302_bdef;
path3302_bdef.type = b2_staticBody;
path3302_bdef.bullet = false;
path3302_bdef.position.Set(7.2797974546f,21.5262834168f);
path3302_bdef.linearDamping = 0.0f;
path3302_bdef.angularDamping =0.0f;
b2Body* path3302_body = m_world->CreateBody(&path3302_bdef);

b2CircleShape path3302_s;
path3302_s.m_radius=0.318572368514f;

b2FixtureDef path3302_fdef;
path3302_fdef.shape = &path3302_s;

path3302_fdef.density = 1.0f;
path3302_fdef.friction = 0.1f;
path3302_fdef.restitution = 0.8f;
path3302_fdef.filter.groupIndex = -1;

path3302_body->CreateFixture(&path3302_fdef);
bodylist.push_back(path3302_body);
uData* path3302_ud = new uData();
path3302_ud->name = "TITLE";
path3302_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3302_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3302_ud->strokewidth=24.23800087f;
path3302_body->SetUserData(path3302_ud);

//
//path4218
//
b2BodyDef path4218_bdef;
path4218_bdef.type = b2_staticBody;
path4218_bdef.bullet = false;
path4218_bdef.position.Set(-8.26184194929f,14.9878875054f);
path4218_bdef.linearDamping = 0.0f;
path4218_bdef.angularDamping =0.0f;
b2Body* path4218_body = m_world->CreateBody(&path4218_bdef);

b2CircleShape path4218_s;
path4218_s.m_radius=0.318572368514f;

b2FixtureDef path4218_fdef;
path4218_fdef.shape = &path4218_s;

path4218_fdef.density = 1.0f;
path4218_fdef.friction = 0.1f;
path4218_fdef.restitution = 0.8f;
path4218_fdef.filter.groupIndex = -1;

path4218_body->CreateFixture(&path4218_fdef);
bodylist.push_back(path4218_body);
uData* path4218_ud = new uData();
path4218_ud->name = "TITLE";
path4218_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4218_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4218_ud->strokewidth=24.23800087f;
path4218_body->SetUserData(path4218_ud);

//
//path3366
//
b2BodyDef path3366_bdef;
path3366_bdef.type = b2_staticBody;
path3366_bdef.bullet = false;
path3366_bdef.position.Set(-8.26184194929f,15.0274915517f);
path3366_bdef.linearDamping = 0.0f;
path3366_bdef.angularDamping =0.0f;
b2Body* path3366_body = m_world->CreateBody(&path3366_bdef);

b2CircleShape path3366_s;
path3366_s.m_radius=0.318572368514f;

b2FixtureDef path3366_fdef;
path3366_fdef.shape = &path3366_s;

path3366_fdef.density = 1.0f;
path3366_fdef.friction = 0.1f;
path3366_fdef.restitution = 0.8f;
path3366_fdef.filter.groupIndex = -1;

path3366_body->CreateFixture(&path3366_fdef);
bodylist.push_back(path3366_body);
uData* path3366_ud = new uData();
path3366_ud->name = "TITLE";
path3366_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3366_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3366_ud->strokewidth=24.23800087f;
path3366_body->SetUserData(path3366_ud);

//
//path3364
//
b2BodyDef path3364_bdef;
path3364_bdef.type = b2_staticBody;
path3364_bdef.bullet = false;
path3364_bdef.position.Set(-8.42025778393f,14.5126400015f);
path3364_bdef.linearDamping = 0.0f;
path3364_bdef.angularDamping =0.0f;
b2Body* path3364_body = m_world->CreateBody(&path3364_bdef);

b2CircleShape path3364_s;
path3364_s.m_radius=0.318572368514f;

b2FixtureDef path3364_fdef;
path3364_fdef.shape = &path3364_s;

path3364_fdef.density = 1.0f;
path3364_fdef.friction = 0.1f;
path3364_fdef.restitution = 0.8f;
path3364_fdef.filter.groupIndex = -1;

path3364_body->CreateFixture(&path3364_fdef);
bodylist.push_back(path3364_body);
uData* path3364_ud = new uData();
path3364_ud->name = "TITLE";
path3364_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3364_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3364_ud->strokewidth=24.23800087f;
path3364_body->SetUserData(path3364_ud);

//
//path3362
//
b2BodyDef path3362_bdef;
path3362_bdef.type = b2_staticBody;
path3362_bdef.bullet = false;
path3362_bdef.position.Set(-8.6182776649f,13.8789766629f);
path3362_bdef.linearDamping = 0.0f;
path3362_bdef.angularDamping =0.0f;
b2Body* path3362_body = m_world->CreateBody(&path3362_bdef);

b2CircleShape path3362_s;
path3362_s.m_radius=0.318572368514f;

b2FixtureDef path3362_fdef;
path3362_fdef.shape = &path3362_s;

path3362_fdef.density = 1.0f;
path3362_fdef.friction = 0.1f;
path3362_fdef.restitution = 0.8f;
path3362_fdef.filter.groupIndex = -1;

path3362_body->CreateFixture(&path3362_fdef);
bodylist.push_back(path3362_body);
uData* path3362_ud = new uData();
path3362_ud->name = "TITLE";
path3362_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3362_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3362_ud->strokewidth=24.23800087f;
path3362_body->SetUserData(path3362_ud);

//
//path3360
//
b2BodyDef path3360_bdef;
path3360_bdef.type = b2_staticBody;
path3360_bdef.bullet = false;
path3360_bdef.position.Set(-8.97471326361f,13.3641251127f);
path3360_bdef.linearDamping = 0.0f;
path3360_bdef.angularDamping =0.0f;
b2Body* path3360_body = m_world->CreateBody(&path3360_bdef);

b2CircleShape path3360_s;
path3360_s.m_radius=0.318572368514f;

b2FixtureDef path3360_fdef;
path3360_fdef.shape = &path3360_s;

path3360_fdef.density = 1.0f;
path3360_fdef.friction = 0.1f;
path3360_fdef.restitution = 0.8f;
path3360_fdef.filter.groupIndex = -1;

path3360_body->CreateFixture(&path3360_fdef);
bodylist.push_back(path3360_body);
uData* path3360_ud = new uData();
path3360_ud->name = "TITLE";
path3360_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3360_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3360_ud->strokewidth=24.23800087f;
path3360_body->SetUserData(path3360_ud);

//
//path3358
//
b2BodyDef path3358_bdef;
path3358_bdef.type = b2_staticBody;
path3358_bdef.bullet = false;
path3358_bdef.position.Set(-9.21233707402f,12.6116498689f);
path3358_bdef.linearDamping = 0.0f;
path3358_bdef.angularDamping =0.0f;
b2Body* path3358_body = m_world->CreateBody(&path3358_bdef);

b2CircleShape path3358_s;
path3358_s.m_radius=0.318572368514f;

b2FixtureDef path3358_fdef;
path3358_fdef.shape = &path3358_s;

path3358_fdef.density = 1.0f;
path3358_fdef.friction = 0.1f;
path3358_fdef.restitution = 0.8f;
path3358_fdef.filter.groupIndex = -1;

path3358_body->CreateFixture(&path3358_fdef);
bodylist.push_back(path3358_body);
uData* path3358_ud = new uData();
path3358_ud->name = "TITLE";
path3358_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3358_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3358_ud->strokewidth=24.23800087f;
path3358_body->SetUserData(path3358_ud);

//
//path3356
//
b2BodyDef path3356_bdef;
path3356_bdef.type = b2_staticBody;
path3356_bdef.bullet = false;
path3356_bdef.position.Set(-9.37075290866f,11.8591746252f);
path3356_bdef.linearDamping = 0.0f;
path3356_bdef.angularDamping =0.0f;
b2Body* path3356_body = m_world->CreateBody(&path3356_bdef);

b2CircleShape path3356_s;
path3356_s.m_radius=0.318572368514f;

b2FixtureDef path3356_fdef;
path3356_fdef.shape = &path3356_s;

path3356_fdef.density = 1.0f;
path3356_fdef.friction = 0.1f;
path3356_fdef.restitution = 0.8f;
path3356_fdef.filter.groupIndex = -1;

path3356_body->CreateFixture(&path3356_fdef);
bodylist.push_back(path3356_body);
uData* path3356_ud = new uData();
path3356_ud->name = "TITLE";
path3356_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3356_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3356_ud->strokewidth=24.23800087f;
path3356_body->SetUserData(path3356_ud);

//
//path3354
//
b2BodyDef path3354_bdef;
path3354_bdef.type = b2_staticBody;
path3354_bdef.bullet = false;
path3354_bdef.position.Set(-9.80639648315f,11.2255112866f);
path3354_bdef.linearDamping = 0.0f;
path3354_bdef.angularDamping =0.0f;
b2Body* path3354_body = m_world->CreateBody(&path3354_bdef);

b2CircleShape path3354_s;
path3354_s.m_radius=0.318572368514f;

b2FixtureDef path3354_fdef;
path3354_fdef.shape = &path3354_s;

path3354_fdef.density = 1.0f;
path3354_fdef.friction = 0.1f;
path3354_fdef.restitution = 0.8f;
path3354_fdef.filter.groupIndex = -1;

path3354_body->CreateFixture(&path3354_fdef);
bodylist.push_back(path3354_body);
uData* path3354_ud = new uData();
path3354_ud->name = "TITLE";
path3354_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3354_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3354_ud->strokewidth=24.23800087f;
path3354_body->SetUserData(path3354_ud);

//
//path3352
//
b2BodyDef path3352_bdef;
path3352_bdef.type = b2_staticBody;
path3352_bdef.bullet = false;
path3352_bdef.position.Set(-10.0440201767f,11.7007587905f);
path3352_bdef.linearDamping = 0.0f;
path3352_bdef.angularDamping =0.0f;
b2Body* path3352_body = m_world->CreateBody(&path3352_bdef);

b2CircleShape path3352_s;
path3352_s.m_radius=0.318572368514f;

b2FixtureDef path3352_fdef;
path3352_fdef.shape = &path3352_s;

path3352_fdef.density = 1.0f;
path3352_fdef.friction = 0.1f;
path3352_fdef.restitution = 0.8f;
path3352_fdef.filter.groupIndex = -1;

path3352_body->CreateFixture(&path3352_fdef);
bodylist.push_back(path3352_body);
uData* path3352_ud = new uData();
path3352_ud->name = "TITLE";
path3352_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3352_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3352_ud->strokewidth=24.23800087f;
path3352_body->SetUserData(path3352_ud);

//
//path3350
//
b2BodyDef path3350_bdef;
path3350_bdef.type = b2_staticBody;
path3350_bdef.bullet = false;
path3350_bdef.position.Set(-10.2024360113f,12.2552142702f);
path3350_bdef.linearDamping = 0.0f;
path3350_bdef.angularDamping =0.0f;
b2Body* path3350_body = m_world->CreateBody(&path3350_bdef);

b2CircleShape path3350_s;
path3350_s.m_radius=0.318572368514f;

b2FixtureDef path3350_fdef;
path3350_fdef.shape = &path3350_s;

path3350_fdef.density = 1.0f;
path3350_fdef.friction = 0.1f;
path3350_fdef.restitution = 0.8f;
path3350_fdef.filter.groupIndex = -1;

path3350_body->CreateFixture(&path3350_fdef);
bodylist.push_back(path3350_body);
uData* path3350_ud = new uData();
path3350_ud->name = "TITLE";
path3350_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3350_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3350_ud->strokewidth=24.23800087f;
path3350_body->SetUserData(path3350_ud);

//
//path3348
//
b2BodyDef path3348_bdef;
path3348_bdef.type = b2_staticBody;
path3348_bdef.bullet = false;
path3348_bdef.position.Set(-10.5192676806f,12.9284815382f);
path3348_bdef.linearDamping = 0.0f;
path3348_bdef.angularDamping =0.0f;
b2Body* path3348_body = m_world->CreateBody(&path3348_bdef);

b2CircleShape path3348_s;
path3348_s.m_radius=0.318572368514f;

b2FixtureDef path3348_fdef;
path3348_fdef.shape = &path3348_s;

path3348_fdef.density = 1.0f;
path3348_fdef.friction = 0.1f;
path3348_fdef.restitution = 0.8f;
path3348_fdef.filter.groupIndex = -1;

path3348_body->CreateFixture(&path3348_fdef);
bodylist.push_back(path3348_body);
uData* path3348_ud = new uData();
path3348_ud->name = "TITLE";
path3348_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3348_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3348_ud->strokewidth=24.23800087f;
path3348_body->SetUserData(path3348_ud);

//
//path3346
//
b2BodyDef path3346_bdef;
path3346_bdef.type = b2_staticBody;
path3346_bdef.bullet = false;
path3346_bdef.position.Set(-10.7172875615f,13.6017489231f);
path3346_bdef.linearDamping = 0.0f;
path3346_bdef.angularDamping =0.0f;
b2Body* path3346_body = m_world->CreateBody(&path3346_bdef);

b2CircleShape path3346_s;
path3346_s.m_radius=0.318572368514f;

b2FixtureDef path3346_fdef;
path3346_fdef.shape = &path3346_s;

path3346_fdef.density = 1.0f;
path3346_fdef.friction = 0.1f;
path3346_fdef.restitution = 0.8f;
path3346_fdef.filter.groupIndex = -1;

path3346_body->CreateFixture(&path3346_fdef);
bodylist.push_back(path3346_body);
uData* path3346_ud = new uData();
path3346_ud->name = "TITLE";
path3346_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3346_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3346_ud->strokewidth=24.23800087f;
path3346_body->SetUserData(path3346_ud);

//
//path3344
//
b2BodyDef path3344_bdef;
path3344_bdef.type = b2_staticBody;
path3344_bdef.bullet = false;
path3344_bdef.position.Set(-10.7172875615f,14.3938280963f);
path3344_bdef.linearDamping = 0.0f;
path3344_bdef.angularDamping =0.0f;
b2Body* path3344_body = m_world->CreateBody(&path3344_bdef);

b2CircleShape path3344_s;
path3344_s.m_radius=0.318572368514f;

b2FixtureDef path3344_fdef;
path3344_fdef.shape = &path3344_s;

path3344_fdef.density = 1.0f;
path3344_fdef.friction = 0.1f;
path3344_fdef.restitution = 0.8f;
path3344_fdef.filter.groupIndex = -1;

path3344_body->CreateFixture(&path3344_fdef);
bodylist.push_back(path3344_body);
uData* path3344_ud = new uData();
path3344_ud->name = "TITLE";
path3344_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3344_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3344_ud->strokewidth=24.23800087f;
path3344_body->SetUserData(path3344_ud);

//
//path3342
//
b2BodyDef path3342_bdef;
path3342_bdef.type = b2_staticBody;
path3342_bdef.bullet = false;
path3342_bdef.position.Set(-10.9549112551f,15.0670955396f);
path3342_bdef.linearDamping = 0.0f;
path3342_bdef.angularDamping =0.0f;
b2Body* path3342_body = m_world->CreateBody(&path3342_bdef);

b2CircleShape path3342_s;
path3342_s.m_radius=0.318572368514f;

b2FixtureDef path3342_fdef;
path3342_fdef.shape = &path3342_s;

path3342_fdef.density = 1.0f;
path3342_fdef.friction = 0.1f;
path3342_fdef.restitution = 0.8f;
path3342_fdef.filter.groupIndex = -1;

path3342_body->CreateFixture(&path3342_fdef);
bodylist.push_back(path3342_body);
uData* path3342_ud = new uData();
path3342_ud->name = "TITLE";
path3342_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3342_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3342_ud->strokewidth=24.23800087f;
path3342_body->SetUserData(path3342_ud);

//
//path3340
//
b2BodyDef path3340_bdef;
path3340_bdef.type = b2_staticBody;
path3340_bdef.bullet = false;
path3340_bdef.position.Set(-11.2717429243f,14.275016308f);
path3340_bdef.linearDamping = 0.0f;
path3340_bdef.angularDamping =0.0f;
b2Body* path3340_body = m_world->CreateBody(&path3340_bdef);

b2CircleShape path3340_s;
path3340_s.m_radius=0.318572368514f;

b2FixtureDef path3340_fdef;
path3340_fdef.shape = &path3340_s;

path3340_fdef.density = 1.0f;
path3340_fdef.friction = 0.1f;
path3340_fdef.restitution = 0.8f;
path3340_fdef.filter.groupIndex = -1;

path3340_body->CreateFixture(&path3340_fdef);
bodylist.push_back(path3340_body);
uData* path3340_ud = new uData();
path3340_ud->name = "TITLE";
path3340_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3340_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3340_ud->strokewidth=24.23800087f;
path3340_body->SetUserData(path3340_ud);

//
//path3338
//
b2BodyDef path3338_bdef;
path3338_bdef.type = b2_staticBody;
path3338_bdef.bullet = false;
path3338_bdef.position.Set(-11.5093667347f,13.7997686872f);
path3338_bdef.linearDamping = 0.0f;
path3338_bdef.angularDamping =0.0f;
b2Body* path3338_body = m_world->CreateBody(&path3338_bdef);

b2CircleShape path3338_s;
path3338_s.m_radius=0.318572368514f;

b2FixtureDef path3338_fdef;
path3338_fdef.shape = &path3338_s;

path3338_fdef.density = 1.0f;
path3338_fdef.friction = 0.1f;
path3338_fdef.restitution = 0.8f;
path3338_fdef.filter.groupIndex = -1;

path3338_body->CreateFixture(&path3338_fdef);
bodylist.push_back(path3338_body);
uData* path3338_ud = new uData();
path3338_ud->name = "TITLE";
path3338_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3338_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3338_ud->strokewidth=24.23800087f;
path3338_body->SetUserData(path3338_ud);

//
//path3336
//
b2BodyDef path3336_bdef;
path3336_bdef.type = b2_staticBody;
path3336_bdef.bullet = false;
path3336_bdef.position.Set(-11.7469905452f,13.2453133244f);
path3336_bdef.linearDamping = 0.0f;
path3336_bdef.angularDamping =0.0f;
b2Body* path3336_body = m_world->CreateBody(&path3336_bdef);

b2CircleShape path3336_s;
path3336_s.m_radius=0.318572368514f;

b2FixtureDef path3336_fdef;
path3336_fdef.shape = &path3336_s;

path3336_fdef.density = 1.0f;
path3336_fdef.friction = 0.1f;
path3336_fdef.restitution = 0.8f;
path3336_fdef.filter.groupIndex = -1;

path3336_body->CreateFixture(&path3336_fdef);
bodylist.push_back(path3336_body);
uData* path3336_ud = new uData();
path3336_ud->name = "TITLE";
path3336_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3336_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3336_ud->strokewidth=24.23800087f;
path3336_body->SetUserData(path3336_ud);

//
//path3334
//
b2BodyDef path3334_bdef;
path3334_bdef.type = b2_staticBody;
path3334_bdef.bullet = false;
path3334_bdef.position.Set(-12.0638222144f,12.4532340343f);
path3334_bdef.linearDamping = 0.0f;
path3334_bdef.angularDamping =0.0f;
b2Body* path3334_body = m_world->CreateBody(&path3334_bdef);

b2CircleShape path3334_s;
path3334_s.m_radius=0.318572368514f;

b2FixtureDef path3334_fdef;
path3334_fdef.shape = &path3334_s;

path3334_fdef.density = 1.0f;
path3334_fdef.friction = 0.1f;
path3334_fdef.restitution = 0.8f;
path3334_fdef.filter.groupIndex = -1;

path3334_body->CreateFixture(&path3334_fdef);
bodylist.push_back(path3334_body);
uData* path3334_ud = new uData();
path3334_ud->name = "TITLE";
path3334_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3334_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3334_ud->strokewidth=24.23800087f;
path3334_body->SetUserData(path3334_ud);

//
//path3332
//
b2BodyDef path3332_bdef;
path3332_bdef.type = b2_staticBody;
path3332_bdef.bullet = false;
path3332_bdef.position.Set(-12.1826341196f,11.8987786715f);
path3332_bdef.linearDamping = 0.0f;
path3332_bdef.angularDamping =0.0f;
b2Body* path3332_body = m_world->CreateBody(&path3332_bdef);

b2CircleShape path3332_s;
path3332_s.m_radius=0.318572368514f;

b2FixtureDef path3332_fdef;
path3332_fdef.shape = &path3332_s;

path3332_fdef.density = 1.0f;
path3332_fdef.friction = 0.1f;
path3332_fdef.restitution = 0.8f;
path3332_fdef.filter.groupIndex = -1;

path3332_body->CreateFixture(&path3332_fdef);
bodylist.push_back(path3332_body);
uData* path3332_ud = new uData();
path3332_ud->name = "TITLE";
path3332_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3332_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3332_ud->strokewidth=24.23800087f;
path3332_body->SetUserData(path3332_ud);

//
//path3330
//
b2BodyDef path3330_bdef;
path3330_bdef.type = b2_staticBody;
path3330_bdef.bullet = false;
path3330_bdef.position.Set(-12.6974855296f,11.265115216f);
path3330_bdef.linearDamping = 0.0f;
path3330_bdef.angularDamping =0.0f;
b2Body* path3330_body = m_world->CreateBody(&path3330_bdef);

b2CircleShape path3330_s;
path3330_s.m_radius=0.318572368514f;

b2FixtureDef path3330_fdef;
path3330_fdef.shape = &path3330_s;

path3330_fdef.density = 1.0f;
path3330_fdef.friction = 0.1f;
path3330_fdef.restitution = 0.8f;
path3330_fdef.filter.groupIndex = -1;

path3330_body->CreateFixture(&path3330_fdef);
bodylist.push_back(path3330_body);
uData* path3330_ud = new uData();
path3330_ud->name = "TITLE";
path3330_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3330_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3330_ud->strokewidth=24.23800087f;
path3330_body->SetUserData(path3330_ud);

//
//path3328
//
b2BodyDef path3328_bdef;
path3328_bdef.type = b2_staticBody;
path3328_bdef.bullet = false;
path3328_bdef.position.Set(-12.8559013759f,11.7007587905f);
path3328_bdef.linearDamping = 0.0f;
path3328_bdef.angularDamping =0.0f;
b2Body* path3328_body = m_world->CreateBody(&path3328_bdef);

b2CircleShape path3328_s;
path3328_s.m_radius=0.318572368514f;

b2FixtureDef path3328_fdef;
path3328_fdef.shape = &path3328_s;

path3328_fdef.density = 1.0f;
path3328_fdef.friction = 0.1f;
path3328_fdef.restitution = 0.8f;
path3328_fdef.filter.groupIndex = -1;

path3328_body->CreateFixture(&path3328_fdef);
bodylist.push_back(path3328_body);
uData* path3328_ud = new uData();
path3328_ud->name = "TITLE";
path3328_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3328_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3328_ud->strokewidth=24.23800087f;
path3328_body->SetUserData(path3328_ud);

//
//path3326
//
b2BodyDef path3326_bdef;
path3326_bdef.type = b2_staticBody;
path3326_bdef.bullet = false;
path3326_bdef.position.Set(-13.0935251396f,12.4136301048f);
path3326_bdef.linearDamping = 0.0f;
path3326_bdef.angularDamping =0.0f;
b2Body* path3326_body = m_world->CreateBody(&path3326_bdef);

b2CircleShape path3326_s;
path3326_s.m_radius=0.318572368514f;

b2FixtureDef path3326_fdef;
path3326_fdef.shape = &path3326_s;

path3326_fdef.density = 1.0f;
path3326_fdef.friction = 0.1f;
path3326_fdef.restitution = 0.8f;
path3326_fdef.filter.groupIndex = -1;

path3326_body->CreateFixture(&path3326_fdef);
bodylist.push_back(path3326_body);
uData* path3326_ud = new uData();
path3326_ud->name = "TITLE";
path3326_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3326_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3326_ud->strokewidth=24.23800087f;
path3326_body->SetUserData(path3326_ud);

//
//path3324
//
b2BodyDef path3324_bdef;
path3324_bdef.type = b2_staticBody;
path3324_bdef.bullet = false;
path3324_bdef.position.Set(-13.2915449387f,13.0472934434f);
path3324_bdef.linearDamping = 0.0f;
path3324_bdef.angularDamping =0.0f;
b2Body* path3324_body = m_world->CreateBody(&path3324_bdef);

b2CircleShape path3324_s;
path3324_s.m_radius=0.318572368514f;

b2FixtureDef path3324_fdef;
path3324_fdef.shape = &path3324_s;

path3324_fdef.density = 1.0f;
path3324_fdef.friction = 0.1f;
path3324_fdef.restitution = 0.8f;
path3324_fdef.filter.groupIndex = -1;

path3324_body->CreateFixture(&path3324_fdef);
bodylist.push_back(path3324_body);
uData* path3324_ud = new uData();
path3324_ud->name = "TITLE";
path3324_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3324_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3324_ud->strokewidth=24.23800087f;
path3324_body->SetUserData(path3324_ud);

//
//path3322
//
b2BodyDef path3322_bdef;
path3322_bdef.type = b2_staticBody;
path3322_bdef.bullet = false;
path3322_bdef.position.Set(-13.5291687024f,13.6809568989f);
path3322_bdef.linearDamping = 0.0f;
path3322_bdef.angularDamping =0.0f;
b2Body* path3322_body = m_world->CreateBody(&path3322_bdef);

b2CircleShape path3322_s;
path3322_s.m_radius=0.318572368514f;

b2FixtureDef path3322_fdef;
path3322_fdef.shape = &path3322_s;

path3322_fdef.density = 1.0f;
path3322_fdef.friction = 0.1f;
path3322_fdef.restitution = 0.8f;
path3322_fdef.filter.groupIndex = -1;

path3322_body->CreateFixture(&path3322_fdef);
bodylist.push_back(path3322_body);
uData* path3322_ud = new uData();
path3322_ud->name = "TITLE";
path3322_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3322_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3322_ud->strokewidth=24.23800087f;
path3322_body->SetUserData(path3322_ud);

//
//path3320
//
b2BodyDef path3320_bdef;
path3320_bdef.type = b2_staticBody;
path3320_bdef.bullet = false;
path3320_bdef.position.Set(-13.7271885015f,14.275016308f);
path3320_bdef.linearDamping = 0.0f;
path3320_bdef.angularDamping =0.0f;
b2Body* path3320_body = m_world->CreateBody(&path3320_bdef);

b2CircleShape path3320_s;
path3320_s.m_radius=0.318572368514f;

b2FixtureDef path3320_fdef;
path3320_fdef.shape = &path3320_s;

path3320_fdef.density = 1.0f;
path3320_fdef.friction = 0.1f;
path3320_fdef.restitution = 0.8f;
path3320_fdef.filter.groupIndex = -1;

path3320_body->CreateFixture(&path3320_fdef);
bodylist.push_back(path3320_body);
uData* path3320_ud = new uData();
path3320_ud->name = "TITLE";
path3320_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3320_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3320_ud->strokewidth=24.23800087f;
path3320_body->SetUserData(path3320_ud);

//
//path3318
//
b2BodyDef path3318_bdef;
path3318_bdef.type = b2_staticBody;
path3318_bdef.bullet = false;
path3318_bdef.position.Set(-13.9648122652f,14.9878875054f);
path3318_bdef.linearDamping = 0.0f;
path3318_bdef.angularDamping =0.0f;
b2Body* path3318_body = m_world->CreateBody(&path3318_bdef);

b2CircleShape path3318_s;
path3318_s.m_radius=0.318572368514f;

b2FixtureDef path3318_fdef;
path3318_fdef.shape = &path3318_s;

path3318_fdef.density = 1.0f;
path3318_fdef.friction = 0.1f;
path3318_fdef.restitution = 0.8f;
path3318_fdef.filter.groupIndex = -1;

path3318_body->CreateFixture(&path3318_fdef);
bodylist.push_back(path3318_body);
uData* path3318_ud = new uData();
path3318_ud->name = "TITLE";
path3318_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path3318_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path3318_ud->strokewidth=24.23800087f;
path3318_body->SetUserData(path3318_ud);

//
//path4184
//
b2BodyDef path4184_bdef;
path4184_bdef.type = b2_staticBody;
path4184_bdef.bullet = false;
path4184_bdef.position.Set(12.8651757903f,12.9554647962f);
path4184_bdef.linearDamping = 0.0f;
path4184_bdef.angularDamping =0.0f;
b2Body* path4184_body = m_world->CreateBody(&path4184_bdef);

b2CircleShape path4184_s;
path4184_s.m_radius=0.318572368514f;

b2FixtureDef path4184_fdef;
path4184_fdef.shape = &path4184_s;

path4184_fdef.density = 1.0f;
path4184_fdef.friction = 0.1f;
path4184_fdef.restitution = 0.8f;
path4184_fdef.filter.groupIndex = -1;

path4184_body->CreateFixture(&path4184_fdef);
bodylist.push_back(path4184_body);
uData* path4184_ud = new uData();
path4184_ud->name = "TITLE";
path4184_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4184_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4184_ud->strokewidth=24.23800087f;
path4184_body->SetUserData(path4184_ud);

//
//path3526
//
b2BodyDef path3526_bdef;
path3526_bdef.type = b2_staticBody;
path3526_bdef.bullet = false;
path3526_bdef.position.Set(12.8651757903f,12.9554647962f);
path3526_bdef.linearDamping = 0.0f;
path3526_bdef.angularDamping =0.0f;
b2Body* path3526_body = m_world->CreateBody(&path3526_bdef);

b2CircleShape path3526_s;
path3526_s.m_radius=0.318572368514f;

b2FixtureDef path3526_fdef;
path3526_fdef.shape = &path3526_s;

path3526_fdef.density = 1.0f;
path3526_fdef.friction = 0.1f;
path3526_fdef.restitution = 0.8f;
path3526_fdef.filter.groupIndex = -1;

path3526_body->CreateFixture(&path3526_fdef);
bodylist.push_back(path3526_body);
uData* path3526_ud = new uData();
path3526_ud->name = "TITLE";
path3526_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3526_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3526_ud->strokewidth=24.23800087f;
path3526_body->SetUserData(path3526_ud);

//
//path3524
//
b2BodyDef path3524_bdef;
path3524_bdef.type = b2_staticBody;
path3524_bdef.bullet = false;
path3524_bdef.position.Set(13.657254379f,12.8762569373f);
path3524_bdef.linearDamping = 0.0f;
path3524_bdef.angularDamping =0.0f;
b2Body* path3524_body = m_world->CreateBody(&path3524_bdef);

b2CircleShape path3524_s;
path3524_s.m_radius=0.318572368514f;

b2FixtureDef path3524_fdef;
path3524_fdef.shape = &path3524_s;

path3524_fdef.density = 1.0f;
path3524_fdef.friction = 0.1f;
path3524_fdef.restitution = 0.8f;
path3524_fdef.filter.groupIndex = -1;

path3524_body->CreateFixture(&path3524_fdef);
bodylist.push_back(path3524_body);
uData* path3524_ud = new uData();
path3524_ud->name = "TITLE";
path3524_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3524_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3524_ud->strokewidth=24.23800087f;
path3524_body->SetUserData(path3524_ud);

//
//path3522
//
b2BodyDef path3522_bdef;
path3522_bdef.type = b2_staticBody;
path3522_bdef.bullet = false;
path3522_bdef.position.Set(14.4889372478f,12.9950688425f);
path3522_bdef.linearDamping = 0.0f;
path3522_bdef.angularDamping =0.0f;
b2Body* path3522_body = m_world->CreateBody(&path3522_bdef);

b2CircleShape path3522_s;
path3522_s.m_radius=0.318572368514f;

b2FixtureDef path3522_fdef;
path3522_fdef.shape = &path3522_s;

path3522_fdef.density = 1.0f;
path3522_fdef.friction = 0.1f;
path3522_fdef.restitution = 0.8f;
path3522_fdef.filter.groupIndex = -1;

path3522_body->CreateFixture(&path3522_fdef);
bodylist.push_back(path3522_body);
uData* path3522_ud = new uData();
path3522_ud->name = "TITLE";
path3522_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3522_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3522_ud->strokewidth=24.23800087f;
path3522_body->SetUserData(path3522_ud);

//
//path3520
//
b2BodyDef path3520_bdef;
path3520_bdef.type = b2_staticBody;
path3520_bdef.bullet = false;
path3520_bdef.position.Set(14.251313905f,12.3218014576f);
path3520_bdef.linearDamping = 0.0f;
path3520_bdef.angularDamping =0.0f;
b2Body* path3520_body = m_world->CreateBody(&path3520_bdef);

b2CircleShape path3520_s;
path3520_s.m_radius=0.318572368514f;

b2FixtureDef path3520_fdef;
path3520_fdef.shape = &path3520_s;

path3520_fdef.density = 1.0f;
path3520_fdef.friction = 0.1f;
path3520_fdef.restitution = 0.8f;
path3520_fdef.filter.groupIndex = -1;

path3520_body->CreateFixture(&path3520_fdef);
bodylist.push_back(path3520_body);
uData* path3520_ud = new uData();
path3520_ud->name = "TITLE";
path3520_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3520_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3520_ud->strokewidth=24.23800087f;
path3520_body->SetUserData(path3520_ud);

//
//path3518
//
b2BodyDef path3518_bdef;
path3518_bdef.type = b2_staticBody;
path3518_bdef.bullet = false;
path3518_bdef.position.Set(13.5780469877f,11.6089301433f);
path3518_bdef.linearDamping = 0.0f;
path3518_bdef.angularDamping =0.0f;
b2Body* path3518_body = m_world->CreateBody(&path3518_bdef);

b2CircleShape path3518_s;
path3518_s.m_radius=0.318572368514f;

b2FixtureDef path3518_fdef;
path3518_fdef.shape = &path3518_s;

path3518_fdef.density = 1.0f;
path3518_fdef.friction = 0.1f;
path3518_fdef.restitution = 0.8f;
path3518_fdef.filter.groupIndex = -1;

path3518_body->CreateFixture(&path3518_fdef);
bodylist.push_back(path3518_body);
uData* path3518_ud = new uData();
path3518_ud->name = "TITLE";
path3518_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3518_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3518_ud->strokewidth=24.23800087f;
path3518_body->SetUserData(path3518_ud);

//
//path3516
//
b2BodyDef path3516_bdef;
path3516_bdef.type = b2_staticBody;
path3516_bdef.bullet = false;
path3516_bdef.position.Set(12.7463629499f,11.2128906151f);
path3516_bdef.linearDamping = 0.0f;
path3516_bdef.angularDamping =0.0f;
b2Body* path3516_body = m_world->CreateBody(&path3516_bdef);

b2CircleShape path3516_s;
path3516_s.m_radius=0.318572368514f;

b2FixtureDef path3516_fdef;
path3516_fdef.shape = &path3516_s;

path3516_fdef.density = 1.0f;
path3516_fdef.friction = 0.1f;
path3516_fdef.restitution = 0.8f;
path3516_fdef.filter.groupIndex = -1;

path3516_body->CreateFixture(&path3516_fdef);
bodylist.push_back(path3516_body);
uData* path3516_ud = new uData();
path3516_ud->name = "TITLE";
path3516_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3516_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3516_ud->strokewidth=24.23800087f;
path3516_body->SetUserData(path3516_ud);

//
//path3514
//
b2BodyDef path3514_bdef;
path3514_bdef.type = b2_staticBody;
path3514_bdef.bullet = false;
path3514_bdef.position.Set(11.8750769699f,11.4109103792f);
path3514_bdef.linearDamping = 0.0f;
path3514_bdef.angularDamping =0.0f;
b2Body* path3514_body = m_world->CreateBody(&path3514_bdef);

b2CircleShape path3514_s;
path3514_s.m_radius=0.318572368514f;

b2FixtureDef path3514_fdef;
path3514_fdef.shape = &path3514_s;

path3514_fdef.density = 1.0f;
path3514_fdef.friction = 0.1f;
path3514_fdef.restitution = 0.8f;
path3514_fdef.filter.groupIndex = -1;

path3514_body->CreateFixture(&path3514_fdef);
bodylist.push_back(path3514_body);
uData* path3514_ud = new uData();
path3514_ud->name = "TITLE";
path3514_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3514_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3514_ud->strokewidth=24.23800087f;
path3514_body->SetUserData(path3514_ud);

//
//path3512
//
b2BodyDef path3512_bdef;
path3512_bdef.type = b2_staticBody;
path3512_bdef.bullet = false;
path3512_bdef.position.Set(11.2018088836f,11.688138119f);
path3512_bdef.linearDamping = 0.0f;
path3512_bdef.angularDamping =0.0f;
b2Body* path3512_body = m_world->CreateBody(&path3512_bdef);

b2CircleShape path3512_s;
path3512_s.m_radius=0.318572368514f;

b2FixtureDef path3512_fdef;
path3512_fdef.shape = &path3512_s;

path3512_fdef.density = 1.0f;
path3512_fdef.friction = 0.1f;
path3512_fdef.restitution = 0.8f;
path3512_fdef.filter.groupIndex = -1;

path3512_body->CreateFixture(&path3512_fdef);
bodylist.push_back(path3512_body);
uData* path3512_ud = new uData();
path3512_ud->name = "TITLE";
path3512_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3512_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3512_ud->strokewidth=24.23800087f;
path3512_body->SetUserData(path3512_ud);

//
//path3510
//
b2BodyDef path3510_bdef;
path3510_bdef.type = b2_staticBody;
path3510_bdef.bullet = false;
path3510_bdef.position.Set(10.8057695893f,12.361405387f);
path3510_bdef.linearDamping = 0.0f;
path3510_bdef.angularDamping =0.0f;
b2Body* path3510_body = m_world->CreateBody(&path3510_bdef);

b2CircleShape path3510_s;
path3510_s.m_radius=0.318572368514f;

b2FixtureDef path3510_fdef;
path3510_fdef.shape = &path3510_s;

path3510_fdef.density = 1.0f;
path3510_fdef.friction = 0.1f;
path3510_fdef.restitution = 0.8f;
path3510_fdef.filter.groupIndex = -1;

path3510_body->CreateFixture(&path3510_fdef);
bodylist.push_back(path3510_body);
uData* path3510_ud = new uData();
path3510_ud->name = "TITLE";
path3510_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3510_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3510_ud->strokewidth=24.23800087f;
path3510_body->SetUserData(path3510_ud);

//
//path3508
//
b2BodyDef path3508_bdef;
path3508_bdef.type = b2_staticBody;
path3508_bdef.bullet = false;
path3508_bdef.position.Set(10.6077493576f,13.0742767014f);
path3508_bdef.linearDamping = 0.0f;
path3508_bdef.angularDamping =0.0f;
b2Body* path3508_body = m_world->CreateBody(&path3508_bdef);

b2CircleShape path3508_s;
path3508_s.m_radius=0.318572368514f;

b2FixtureDef path3508_fdef;
path3508_fdef.shape = &path3508_s;

path3508_fdef.density = 1.0f;
path3508_fdef.friction = 0.1f;
path3508_fdef.restitution = 0.8f;
path3508_fdef.filter.groupIndex = -1;

path3508_body->CreateFixture(&path3508_fdef);
bodylist.push_back(path3508_body);
uData* path3508_ud = new uData();
path3508_ud->name = "TITLE";
path3508_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3508_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3508_ud->strokewidth=24.23800087f;
path3508_body->SetUserData(path3508_ud);

//
//path3506
//
b2BodyDef path3506_bdef;
path3506_bdef.type = b2_staticBody;
path3506_bdef.bullet = false;
path3506_bdef.position.Set(10.6473536377f,13.7871480157f);
path3506_bdef.linearDamping = 0.0f;
path3506_bdef.angularDamping =0.0f;
b2Body* path3506_body = m_world->CreateBody(&path3506_bdef);

b2CircleShape path3506_s;
path3506_s.m_radius=0.318572368514f;

b2FixtureDef path3506_fdef;
path3506_fdef.shape = &path3506_s;

path3506_fdef.density = 1.0f;
path3506_fdef.friction = 0.1f;
path3506_fdef.restitution = 0.8f;
path3506_fdef.filter.groupIndex = -1;

path3506_body->CreateFixture(&path3506_fdef);
bodylist.push_back(path3506_body);
uData* path3506_ud = new uData();
path3506_ud->name = "TITLE";
path3506_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3506_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3506_ud->strokewidth=24.23800087f;
path3506_body->SetUserData(path3506_ud);

//
//path3504
//
b2BodyDef path3504_bdef;
path3504_bdef.type = b2_staticBody;
path3504_bdef.bullet = false;
path3504_bdef.position.Set(11.2018088836f,14.3812074248f);
path3504_bdef.linearDamping = 0.0f;
path3504_bdef.angularDamping =0.0f;
b2Body* path3504_body = m_world->CreateBody(&path3504_bdef);

b2CircleShape path3504_s;
path3504_s.m_radius=0.318572368514f;

b2FixtureDef path3504_fdef;
path3504_fdef.shape = &path3504_s;

path3504_fdef.density = 1.0f;
path3504_fdef.friction = 0.1f;
path3504_fdef.restitution = 0.8f;
path3504_fdef.filter.groupIndex = -1;

path3504_body->CreateFixture(&path3504_fdef);
bodylist.push_back(path3504_body);
uData* path3504_ud = new uData();
path3504_ud->name = "TITLE";
path3504_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3504_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3504_ud->strokewidth=24.23800087f;
path3504_body->SetUserData(path3504_ud);

//
//path3502
//
b2BodyDef path3502_bdef;
path3502_bdef.type = b2_staticBody;
path3502_bdef.bullet = false;
path3502_bdef.position.Set(11.597848178f,14.6980390941f);
path3502_bdef.linearDamping = 0.0f;
path3502_bdef.angularDamping =0.0f;
b2Body* path3502_body = m_world->CreateBody(&path3502_bdef);

b2CircleShape path3502_s;
path3502_s.m_radius=0.318572368514f;

b2FixtureDef path3502_fdef;
path3502_fdef.shape = &path3502_s;

path3502_fdef.density = 1.0f;
path3502_fdef.friction = 0.1f;
path3502_fdef.restitution = 0.8f;
path3502_fdef.filter.groupIndex = -1;

path3502_body->CreateFixture(&path3502_fdef);
bodylist.push_back(path3502_body);
uData* path3502_ud = new uData();
path3502_ud->name = "TITLE";
path3502_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3502_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3502_ud->strokewidth=24.23800087f;
path3502_body->SetUserData(path3502_ud);

//
//path3500
//
b2BodyDef path3500_bdef;
path3500_bdef.type = b2_staticBody;
path3500_bdef.bullet = false;
path3500_bdef.position.Set(12.4295322158f,14.9356629045f);
path3500_bdef.linearDamping = 0.0f;
path3500_bdef.angularDamping =0.0f;
b2Body* path3500_body = m_world->CreateBody(&path3500_bdef);

b2CircleShape path3500_s;
path3500_s.m_radius=0.318572368514f;

b2FixtureDef path3500_fdef;
path3500_fdef.shape = &path3500_s;

path3500_fdef.density = 1.0f;
path3500_fdef.friction = 0.1f;
path3500_fdef.restitution = 0.8f;
path3500_fdef.filter.groupIndex = -1;

path3500_body->CreateFixture(&path3500_fdef);
bodylist.push_back(path3500_body);
uData* path3500_ud = new uData();
path3500_ud->name = "TITLE";
path3500_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3500_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3500_ud->strokewidth=24.23800087f;
path3500_body->SetUserData(path3500_ud);

//
//path3498
//
b2BodyDef path3498_bdef;
path3498_bdef.type = b2_staticBody;
path3498_bdef.bullet = false;
path3498_bdef.position.Set(13.2216108045f,14.8168509993f);
path3498_bdef.linearDamping = 0.0f;
path3498_bdef.angularDamping =0.0f;
b2Body* path3498_body = m_world->CreateBody(&path3498_bdef);

b2CircleShape path3498_s;
path3498_s.m_radius=0.318572368514f;

b2FixtureDef path3498_fdef;
path3498_fdef.shape = &path3498_s;

path3498_fdef.density = 1.0f;
path3498_fdef.friction = 0.1f;
path3498_fdef.restitution = 0.8f;
path3498_fdef.filter.groupIndex = -1;

path3498_body->CreateFixture(&path3498_fdef);
bodylist.push_back(path3498_body);
uData* path3498_ud = new uData();
path3498_ud->name = "TITLE";
path3498_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3498_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3498_ud->strokewidth=24.23800087f;
path3498_body->SetUserData(path3498_ud);

//
//path3496
//
b2BodyDef path3496_bdef;
path3496_bdef.type = b2_staticBody;
path3496_bdef.bullet = false;
path3496_bdef.position.Set(13.8948788908f,14.3416034954f);
path3496_bdef.linearDamping = 0.0f;
path3496_bdef.angularDamping =0.0f;
b2Body* path3496_body = m_world->CreateBody(&path3496_bdef);

b2CircleShape path3496_s;
path3496_s.m_radius=0.318572368514f;

b2FixtureDef path3496_fdef;
path3496_fdef.shape = &path3496_s;

path3496_fdef.density = 1.0f;
path3496_fdef.friction = 0.1f;
path3496_fdef.restitution = 0.8f;
path3496_fdef.filter.groupIndex = -1;

path3496_body->CreateFixture(&path3496_fdef);
bodylist.push_back(path3496_body);
uData* path3496_ud = new uData();
path3496_ud->name = "TITLE";
path3496_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3496_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3496_ud->strokewidth=24.23800087f;
path3496_body->SetUserData(path3496_ud);

//
//path4180
//
b2BodyDef path4180_bdef;
path4180_bdef.type = b2_staticBody;
path4180_bdef.bullet = false;
path4180_bdef.position.Set(0.020784955482f,14.2652680837f);
path4180_bdef.linearDamping = 0.0f;
path4180_bdef.angularDamping =0.0f;
b2Body* path4180_body = m_world->CreateBody(&path4180_bdef);

b2CircleShape path4180_s;
path4180_s.m_radius=0.318572368514f;

b2FixtureDef path4180_fdef;
path4180_fdef.shape = &path4180_s;

path4180_fdef.density = 1.0f;
path4180_fdef.friction = 0.1f;
path4180_fdef.restitution = 0.8f;
path4180_fdef.filter.groupIndex = -1;

path4180_body->CreateFixture(&path4180_fdef);
bodylist.push_back(path4180_body);
uData* path4180_ud = new uData();
path4180_ud->name = "TITLE";
path4180_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4180_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4180_ud->strokewidth=24.23800087f;
path4180_body->SetUserData(path4180_ud);

//
//path3452
//
b2BodyDef path3452_bdef;
path3452_bdef.type = b2_staticBody;
path3452_bdef.bullet = false;
path3452_bdef.position.Set(0.020784955482f,13.5920006988f);
path3452_bdef.linearDamping = 0.0f;
path3452_bdef.angularDamping =0.0f;
b2Body* path3452_body = m_world->CreateBody(&path3452_bdef);

b2CircleShape path3452_s;
path3452_s.m_radius=0.318572368514f;

b2FixtureDef path3452_fdef;
path3452_fdef.shape = &path3452_s;

path3452_fdef.density = 1.0f;
path3452_fdef.friction = 0.1f;
path3452_fdef.restitution = 0.8f;
path3452_fdef.filter.groupIndex = -1;

path3452_body->CreateFixture(&path3452_fdef);
bodylist.push_back(path3452_body);
uData* path3452_ud = new uData();
path3452_ud->name = "TITLE";
path3452_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3452_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3452_ud->strokewidth=24.23800087f;
path3452_body->SetUserData(path3452_ud);

//
//path3450
//
b2BodyDef path3450_bdef;
path3450_bdef.type = b2_staticBody;
path3450_bdef.bullet = false;
path3450_bdef.position.Set(0.020784955482f,12.8791293844f);
path3450_bdef.linearDamping = 0.0f;
path3450_bdef.angularDamping =0.0f;
b2Body* path3450_body = m_world->CreateBody(&path3450_bdef);

b2CircleShape path3450_s;
path3450_s.m_radius=0.318572368514f;

b2FixtureDef path3450_fdef;
path3450_fdef.shape = &path3450_s;

path3450_fdef.density = 1.0f;
path3450_fdef.friction = 0.1f;
path3450_fdef.restitution = 0.8f;
path3450_fdef.filter.groupIndex = -1;

path3450_body->CreateFixture(&path3450_fdef);
bodylist.push_back(path3450_body);
uData* path3450_ud = new uData();
path3450_ud->name = "TITLE";
path3450_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3450_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3450_ud->strokewidth=24.23800087f;
path3450_body->SetUserData(path3450_ud);

//
//path3448
//
b2BodyDef path3448_bdef;
path3448_bdef.type = b2_staticBody;
path3448_bdef.bullet = false;
path3448_bdef.position.Set(0.020784955482f,12.0474462818f);
path3448_bdef.linearDamping = 0.0f;
path3448_bdef.angularDamping =0.0f;
b2Body* path3448_body = m_world->CreateBody(&path3448_bdef);

b2CircleShape path3448_s;
path3448_s.m_radius=0.318572368514f;

b2FixtureDef path3448_fdef;
path3448_fdef.shape = &path3448_s;

path3448_fdef.density = 1.0f;
path3448_fdef.friction = 0.1f;
path3448_fdef.restitution = 0.8f;
path3448_fdef.filter.groupIndex = -1;

path3448_body->CreateFixture(&path3448_fdef);
bodylist.push_back(path3448_body);
uData* path3448_ud = new uData();
path3448_ud->name = "TITLE";
path3448_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3448_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3448_ud->strokewidth=24.23800087f;
path3448_body->SetUserData(path3448_ud);

//
//path3446
//
b2BodyDef path3446_bdef;
path3446_bdef.type = b2_staticBody;
path3446_bdef.bullet = false;
path3446_bdef.position.Set(0.0603880666228f,11.2157630623f);
path3446_bdef.linearDamping = 0.0f;
path3446_bdef.angularDamping =0.0f;
b2Body* path3446_body = m_world->CreateBody(&path3446_bdef);

b2CircleShape path3446_s;
path3446_s.m_radius=0.318572368514f;

b2FixtureDef path3446_fdef;
path3446_fdef.shape = &path3446_s;

path3446_fdef.density = 1.0f;
path3446_fdef.friction = 0.1f;
path3446_fdef.restitution = 0.8f;
path3446_fdef.filter.groupIndex = -1;

path3446_body->CreateFixture(&path3446_fdef);
bodylist.push_back(path3446_body);
uData* path3446_ud = new uData();
path3446_ud->name = "TITLE";
path3446_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3446_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3446_ud->strokewidth=24.23800087f;
path3446_body->SetUserData(path3446_ud);

//
//path3444
//
b2BodyDef path3444_bdef;
path3444_bdef.type = b2_staticBody;
path3444_bdef.bullet = false;
path3444_bdef.position.Set(0.852467824317f,11.2157630623f);
path3444_bdef.linearDamping = 0.0f;
path3444_bdef.angularDamping =0.0f;
b2Body* path3444_body = m_world->CreateBody(&path3444_bdef);

b2CircleShape path3444_s;
path3444_s.m_radius=0.318572368514f;

b2FixtureDef path3444_fdef;
path3444_fdef.shape = &path3444_s;

path3444_fdef.density = 1.0f;
path3444_fdef.friction = 0.1f;
path3444_fdef.restitution = 0.8f;
path3444_fdef.filter.groupIndex = -1;

path3444_body->CreateFixture(&path3444_fdef);
bodylist.push_back(path3444_body);
uData* path3444_ud = new uData();
path3444_ud->name = "TITLE";
path3444_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3444_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3444_ud->strokewidth=24.23800087f;
path3444_body->SetUserData(path3444_ud);

//
//path3442
//
b2BodyDef path3442_bdef;
path3442_bdef.type = b2_staticBody;
path3442_bdef.bullet = false;
path3442_bdef.position.Set(1.60494330188f,11.4533868727f);
path3442_bdef.linearDamping = 0.0f;
path3442_bdef.angularDamping =0.0f;
b2Body* path3442_body = m_world->CreateBody(&path3442_bdef);

b2CircleShape path3442_s;
path3442_s.m_radius=0.318572368514f;

b2FixtureDef path3442_fdef;
path3442_fdef.shape = &path3442_s;

path3442_fdef.density = 1.0f;
path3442_fdef.friction = 0.1f;
path3442_fdef.restitution = 0.8f;
path3442_fdef.filter.groupIndex = -1;

path3442_body->CreateFixture(&path3442_fdef);
bodylist.push_back(path3442_body);
uData* path3442_ud = new uData();
path3442_ud->name = "TITLE";
path3442_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3442_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3442_ud->strokewidth=24.23800087f;
path3442_body->SetUserData(path3442_ud);

//
//path3440
//
b2BodyDef path3440_bdef;
path3440_bdef.type = b2_staticBody;
path3440_bdef.bullet = false;
path3440_bdef.position.Set(2.23860593904f,11.7306146125f);
path3440_bdef.linearDamping = 0.0f;
path3440_bdef.angularDamping =0.0f;
b2Body* path3440_body = m_world->CreateBody(&path3440_bdef);

b2CircleShape path3440_s;
path3440_s.m_radius=0.318572368514f;

b2FixtureDef path3440_fdef;
path3440_fdef.shape = &path3440_s;

path3440_fdef.density = 1.0f;
path3440_fdef.friction = 0.1f;
path3440_fdef.restitution = 0.8f;
path3440_fdef.filter.groupIndex = -1;

path3440_body->CreateFixture(&path3440_fdef);
bodylist.push_back(path3440_body);
uData* path3440_ud = new uData();
path3440_ud->name = "TITLE";
path3440_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3440_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3440_ud->strokewidth=24.23800087f;
path3440_body->SetUserData(path3440_ud);

//
//path3438
//
b2BodyDef path3438_bdef;
path3438_bdef.type = b2_staticBody;
path3438_bdef.bullet = false;
path3438_bdef.position.Set(2.67424951353f,12.4038818805f);
path3438_bdef.linearDamping = 0.0f;
path3438_bdef.angularDamping =0.0f;
b2Body* path3438_body = m_world->CreateBody(&path3438_bdef);

b2CircleShape path3438_s;
path3438_s.m_radius=0.318572368514f;

b2FixtureDef path3438_fdef;
path3438_fdef.shape = &path3438_s;

path3438_fdef.density = 1.0f;
path3438_fdef.friction = 0.1f;
path3438_fdef.restitution = 0.8f;
path3438_fdef.filter.groupIndex = -1;

path3438_body->CreateFixture(&path3438_fdef);
bodylist.push_back(path3438_body);
uData* path3438_ud = new uData();
path3438_ud->name = "TITLE";
path3438_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3438_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3438_ud->strokewidth=24.23800087f;
path3438_body->SetUserData(path3438_ud);

//
//path3436
//
b2BodyDef path3436_bdef;
path3436_bdef.type = b2_staticBody;
path3436_bdef.bullet = false;
path3436_bdef.position.Set(2.67424951353f,12.9979412896f);
path3436_bdef.linearDamping = 0.0f;
path3436_bdef.angularDamping =0.0f;
b2Body* path3436_body = m_world->CreateBody(&path3436_bdef);

b2CircleShape path3436_s;
path3436_s.m_radius=0.318572368514f;

b2FixtureDef path3436_fdef;
path3436_fdef.shape = &path3436_s;

path3436_fdef.density = 1.0f;
path3436_fdef.friction = 0.1f;
path3436_fdef.restitution = 0.8f;
path3436_fdef.filter.groupIndex = -1;

path3436_body->CreateFixture(&path3436_fdef);
bodylist.push_back(path3436_body);
uData* path3436_ud = new uData();
path3436_ud->name = "TITLE";
path3436_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3436_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3436_ud->strokewidth=24.23800087f;
path3436_body->SetUserData(path3436_ud);

//
//path3434
//
b2BodyDef path3434_bdef;
path3434_bdef.type = b2_staticBody;
path3434_bdef.bullet = false;
path3434_bdef.position.Set(2.55543784212f,13.6712086745f);
path3434_bdef.linearDamping = 0.0f;
path3434_bdef.angularDamping =0.0f;
b2Body* path3434_body = m_world->CreateBody(&path3434_bdef);

b2CircleShape path3434_s;
path3434_s.m_radius=0.318572368514f;

b2FixtureDef path3434_fdef;
path3434_fdef.shape = &path3434_s;

path3434_fdef.density = 1.0f;
path3434_fdef.friction = 0.1f;
path3434_fdef.restitution = 0.8f;
path3434_fdef.filter.groupIndex = -1;

path3434_body->CreateFixture(&path3434_fdef);
bodylist.push_back(path3434_body);
uData* path3434_ud = new uData();
path3434_ud->name = "TITLE";
path3434_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3434_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3434_ud->strokewidth=24.23800087f;
path3434_body->SetUserData(path3434_ud);

//
//path3432
//
b2BodyDef path3432_bdef;
path3432_bdef.type = b2_staticBody;
path3432_bdef.bullet = false;
path3432_bdef.position.Set(2.1990028279f,14.3048720131f);
path3432_bdef.linearDamping = 0.0f;
path3432_bdef.angularDamping =0.0f;
b2Body* path3432_body = m_world->CreateBody(&path3432_bdef);

b2CircleShape path3432_s;
path3432_s.m_radius=0.318572368514f;

b2FixtureDef path3432_fdef;
path3432_fdef.shape = &path3432_s;

path3432_fdef.density = 1.0f;
path3432_fdef.friction = 0.1f;
path3432_fdef.restitution = 0.8f;
path3432_fdef.filter.groupIndex = -1;

path3432_body->CreateFixture(&path3432_fdef);
bodylist.push_back(path3432_body);
uData* path3432_ud = new uData();
path3432_ud->name = "TITLE";
path3432_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3432_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3432_ud->strokewidth=24.23800087f;
path3432_body->SetUserData(path3432_ud);

//
//path3430
//
b2BodyDef path3430_bdef;
path3430_bdef.type = b2_staticBody;
path3430_bdef.bullet = false;
path3430_bdef.position.Set(1.60494330188f,14.8593274928f);
path3430_bdef.linearDamping = 0.0f;
path3430_bdef.angularDamping =0.0f;
b2Body* path3430_body = m_world->CreateBody(&path3430_bdef);

b2CircleShape path3430_s;
path3430_s.m_radius=0.318572368514f;

b2FixtureDef path3430_fdef;
path3430_fdef.shape = &path3430_s;

path3430_fdef.density = 1.0f;
path3430_fdef.friction = 0.1f;
path3430_fdef.restitution = 0.8f;
path3430_fdef.filter.groupIndex = -1;

path3430_body->CreateFixture(&path3430_fdef);
bodylist.push_back(path3430_body);
uData* path3430_ud = new uData();
path3430_ud->name = "TITLE";
path3430_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3430_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3430_ud->strokewidth=24.23800087f;
path3430_body->SetUserData(path3430_ud);

//
//path3428
//
b2BodyDef path3428_bdef;
path3428_bdef.type = b2_staticBody;
path3428_bdef.bullet = false;
path3428_bdef.position.Set(0.733656152911f,14.9781392811f);
path3428_bdef.linearDamping = 0.0f;
path3428_bdef.angularDamping =0.0f;
b2Body* path3428_body = m_world->CreateBody(&path3428_bdef);

b2CircleShape path3428_s;
path3428_s.m_radius=0.318572368514f;

b2FixtureDef path3428_fdef;
path3428_fdef.shape = &path3428_s;

path3428_fdef.density = 1.0f;
path3428_fdef.friction = 0.1f;
path3428_fdef.restitution = 0.8f;
path3428_fdef.filter.groupIndex = -1;

path3428_body->CreateFixture(&path3428_fdef);
bodylist.push_back(path3428_body);
uData* path3428_ud = new uData();
path3428_ud->name = "TITLE";
path3428_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3428_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3428_ud->strokewidth=24.23800087f;
path3428_body->SetUserData(path3428_ud);

//
//path3426
//
b2BodyDef path3426_bdef;
path3426_bdef.type = b2_staticBody;
path3426_bdef.bullet = false;
path3426_bdef.position.Set(-0.0980278849161f,15.0177433274f);
path3426_bdef.linearDamping = 0.0f;
path3426_bdef.angularDamping =0.0f;
b2Body* path3426_body = m_world->CreateBody(&path3426_bdef);

b2CircleShape path3426_s;
path3426_s.m_radius=0.318572368514f;

b2FixtureDef path3426_fdef;
path3426_fdef.shape = &path3426_s;

path3426_fdef.density = 1.0f;
path3426_fdef.friction = 0.1f;
path3426_fdef.restitution = 0.8f;
path3426_fdef.filter.groupIndex = -1;

path3426_body->CreateFixture(&path3426_fdef);
bodylist.push_back(path3426_body);
uData* path3426_ud = new uData();
path3426_ud->name = "TITLE";
path3426_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3426_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3426_ud->strokewidth=24.23800087f;
path3426_body->SetUserData(path3426_ud);

//
//path3424
//
b2BodyDef path3424_bdef;
path3424_bdef.type = b2_staticBody;
path3424_bdef.bullet = false;
path3424_bdef.position.Set(-3.30594839025f,15.0573473153f);
path3424_bdef.linearDamping = 0.0f;
path3424_bdef.angularDamping =0.0f;
b2Body* path3424_body = m_world->CreateBody(&path3424_bdef);

b2CircleShape path3424_s;
path3424_s.m_radius=0.318572368514f;

b2FixtureDef path3424_fdef;
path3424_fdef.shape = &path3424_s;

path3424_fdef.density = 1.0f;
path3424_fdef.friction = 0.1f;
path3424_fdef.restitution = 0.8f;
path3424_fdef.filter.groupIndex = -1;

path3424_body->CreateFixture(&path3424_fdef);
bodylist.push_back(path3424_body);
uData* path3424_ud = new uData();
path3424_ud->name = "TITLE";
path3424_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3424_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3424_ud->strokewidth=24.23800087f;
path3424_body->SetUserData(path3424_ud);

//
//path3422
//
b2BodyDef path3422_bdef;
path3422_bdef.type = b2_staticBody;
path3422_bdef.bullet = false;
path3422_bdef.position.Set(-2.59307707592f,14.8989314222f);
path3422_bdef.linearDamping = 0.0f;
path3422_bdef.angularDamping =0.0f;
b2Body* path3422_body = m_world->CreateBody(&path3422_bdef);

b2CircleShape path3422_s;
path3422_s.m_radius=0.318572368514f;

b2FixtureDef path3422_fdef;
path3422_fdef.shape = &path3422_s;

path3422_fdef.density = 1.0f;
path3422_fdef.friction = 0.1f;
path3422_fdef.restitution = 0.8f;
path3422_fdef.filter.groupIndex = -1;

path3422_body->CreateFixture(&path3422_fdef);
bodylist.push_back(path3422_body);
uData* path3422_ud = new uData();
path3422_ud->name = "TITLE";
path3422_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3422_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3422_ud->strokewidth=24.23800087f;
path3422_body->SetUserData(path3422_ud);

//
//path3420
//
b2BodyDef path3420_bdef;
path3420_bdef.type = b2_staticBody;
path3420_bdef.bullet = false;
path3420_bdef.position.Set(-1.80099848721f,14.4236839183f);
path3420_bdef.linearDamping = 0.0f;
path3420_bdef.angularDamping =0.0f;
b2Body* path3420_body = m_world->CreateBody(&path3420_bdef);

b2CircleShape path3420_s;
path3420_s.m_radius=0.318572368514f;

b2FixtureDef path3420_fdef;
path3420_fdef.shape = &path3420_s;

path3420_fdef.density = 1.0f;
path3420_fdef.friction = 0.1f;
path3420_fdef.restitution = 0.8f;
path3420_fdef.filter.groupIndex = -1;

path3420_body->CreateFixture(&path3420_fdef);
bodylist.push_back(path3420_body);
uData* path3420_ud = new uData();
path3420_ud->name = "TITLE";
path3420_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3420_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3420_ud->strokewidth=24.23800087f;
path3420_body->SetUserData(path3420_ud);

//
//path3418
//
b2BodyDef path3418_bdef;
path3418_bdef.type = b2_staticBody;
path3418_bdef.bullet = false;
path3418_bdef.position.Set(-1.52377027977f,13.9088324849f);
path3418_bdef.linearDamping = 0.0f;
path3418_bdef.angularDamping =0.0f;
b2Body* path3418_body = m_world->CreateBody(&path3418_bdef);

b2CircleShape path3418_s;
path3418_s.m_radius=0.318572368514f;

b2FixtureDef path3418_fdef;
path3418_fdef.shape = &path3418_s;

path3418_fdef.density = 1.0f;
path3418_fdef.friction = 0.1f;
path3418_fdef.restitution = 0.8f;
path3418_fdef.filter.groupIndex = -1;

path3418_body->CreateFixture(&path3418_fdef);
bodylist.push_back(path3418_body);
uData* path3418_ud = new uData();
path3418_ud->name = "TITLE";
path3418_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3418_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3418_ud->strokewidth=24.23800087f;
path3418_body->SetUserData(path3418_ud);

//
//path3416
//
b2BodyDef path3416_bdef;
path3416_bdef.type = b2_staticBody;
path3416_bdef.bullet = false;
path3416_bdef.position.Set(-1.36535432823f,13.1959611706f);
path3416_bdef.linearDamping = 0.0f;
path3416_bdef.angularDamping =0.0f;
b2Body* path3416_body = m_world->CreateBody(&path3416_bdef);

b2CircleShape path3416_s;
path3416_s.m_radius=0.318572368514f;

b2FixtureDef path3416_fdef;
path3416_fdef.shape = &path3416_s;

path3416_fdef.density = 1.0f;
path3416_fdef.friction = 0.1f;
path3416_fdef.restitution = 0.8f;
path3416_fdef.filter.groupIndex = -1;

path3416_body->CreateFixture(&path3416_fdef);
bodylist.push_back(path3416_body);
uData* path3416_ud = new uData();
path3416_ud->name = "TITLE";
path3416_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3416_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3416_ud->strokewidth=24.23800087f;
path3416_body->SetUserData(path3416_ud);

//
//path3414
//
b2BodyDef path3414_bdef;
path3414_bdef.type = b2_staticBody;
path3414_bdef.bullet = false;
path3414_bdef.position.Set(-1.52377027977f,12.5226937857f);
path3414_bdef.linearDamping = 0.0f;
path3414_bdef.angularDamping =0.0f;
b2Body* path3414_body = m_world->CreateBody(&path3414_bdef);

b2CircleShape path3414_s;
path3414_s.m_radius=0.318572368514f;

b2FixtureDef path3414_fdef;
path3414_fdef.shape = &path3414_s;

path3414_fdef.density = 1.0f;
path3414_fdef.friction = 0.1f;
path3414_fdef.restitution = 0.8f;
path3414_fdef.filter.groupIndex = -1;

path3414_body->CreateFixture(&path3414_fdef);
bodylist.push_back(path3414_body);
uData* path3414_ud = new uData();
path3414_ud->name = "TITLE";
path3414_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3414_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3414_ud->strokewidth=24.23800087f;
path3414_body->SetUserData(path3414_ud);

//
//path3412
//
b2BodyDef path3412_bdef;
path3412_bdef.type = b2_staticBody;
path3412_bdef.bullet = false;
path3412_bdef.position.Set(-1.91980969102f,12.0078423524f);
path3412_bdef.linearDamping = 0.0f;
path3412_bdef.angularDamping =0.0f;
b2Body* path3412_body = m_world->CreateBody(&path3412_bdef);

b2CircleShape path3412_s;
path3412_s.m_radius=0.318572368514f;

b2FixtureDef path3412_fdef;
path3412_fdef.shape = &path3412_s;

path3412_fdef.density = 1.0f;
path3412_fdef.friction = 0.1f;
path3412_fdef.restitution = 0.8f;
path3412_fdef.filter.groupIndex = -1;

path3412_body->CreateFixture(&path3412_fdef);
bodylist.push_back(path3412_body);
uData* path3412_ud = new uData();
path3412_ud->name = "TITLE";
path3412_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3412_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3412_ud->strokewidth=24.23800087f;
path3412_body->SetUserData(path3412_ud);

//
//path3410
//
b2BodyDef path3410_bdef;
path3410_bdef.type = b2_staticBody;
path3410_bdef.bullet = false;
path3410_bdef.position.Set(-2.63268100535f,11.4533868727f);
path3410_bdef.linearDamping = 0.0f;
path3410_bdef.angularDamping =0.0f;
b2Body* path3410_body = m_world->CreateBody(&path3410_bdef);

b2CircleShape path3410_s;
path3410_s.m_radius=0.318572368514f;

b2FixtureDef path3410_fdef;
path3410_fdef.shape = &path3410_s;

path3410_fdef.density = 1.0f;
path3410_fdef.friction = 0.1f;
path3410_fdef.restitution = 0.8f;
path3410_fdef.filter.groupIndex = -1;

path3410_body->CreateFixture(&path3410_fdef);
bodylist.push_back(path3410_body);
uData* path3410_ud = new uData();
path3410_ud->name = "TITLE";
path3410_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3410_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3410_ud->strokewidth=24.23800087f;
path3410_body->SetUserData(path3410_ud);

//
//path3408
//
b2BodyDef path3408_bdef;
path3408_bdef.type = b2_staticBody;
path3408_bdef.bullet = false;
path3408_bdef.position.Set(-3.22674041448f,11.4137829432f);
path3408_bdef.linearDamping = 0.0f;
path3408_bdef.angularDamping =0.0f;
b2Body* path3408_body = m_world->CreateBody(&path3408_bdef);

b2CircleShape path3408_s;
path3408_s.m_radius=0.318572368514f;

b2FixtureDef path3408_fdef;
path3408_fdef.shape = &path3408_s;

path3408_fdef.density = 1.0f;
path3408_fdef.friction = 0.1f;
path3408_fdef.restitution = 0.8f;
path3408_fdef.filter.groupIndex = -1;

path3408_body->CreateFixture(&path3408_fdef);
bodylist.push_back(path3408_body);
uData* path3408_ud = new uData();
path3408_ud->name = "TITLE";
path3408_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3408_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3408_ud->strokewidth=24.23800087f;
path3408_body->SetUserData(path3408_ud);

//
//path3406
//
b2BodyDef path3406_bdef;
path3406_bdef.type = b2_staticBody;
path3406_bdef.bullet = false;
path3406_bdef.position.Set(-4.09802756344f,11.4137829432f);
path3406_bdef.linearDamping = 0.0f;
path3406_bdef.angularDamping =0.0f;
b2Body* path3406_body = m_world->CreateBody(&path3406_bdef);

b2CircleShape path3406_s;
path3406_s.m_radius=0.318572368514f;

b2FixtureDef path3406_fdef;
path3406_fdef.shape = &path3406_s;

path3406_fdef.density = 1.0f;
path3406_fdef.friction = 0.1f;
path3406_fdef.restitution = 0.8f;
path3406_fdef.filter.groupIndex = -1;

path3406_body->CreateFixture(&path3406_fdef);
bodylist.push_back(path3406_body);
uData* path3406_ud = new uData();
path3406_ud->name = "TITLE";
path3406_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3406_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3406_ud->strokewidth=24.23800087f;
path3406_body->SetUserData(path3406_ud);

//
//path3404
//
b2BodyDef path3404_bdef;
path3404_bdef.type = b2_staticBody;
path3404_bdef.bullet = false;
path3404_bdef.position.Set(-4.09802756344f,11.9682384229f);
path3404_bdef.linearDamping = 0.0f;
path3404_bdef.angularDamping =0.0f;
b2Body* path3404_body = m_world->CreateBody(&path3404_bdef);

b2CircleShape path3404_s;
path3404_s.m_radius=0.318572368514f;

b2FixtureDef path3404_fdef;
path3404_fdef.shape = &path3404_s;

path3404_fdef.density = 1.0f;
path3404_fdef.friction = 0.1f;
path3404_fdef.restitution = 0.8f;
path3404_fdef.filter.groupIndex = -1;

path3404_body->CreateFixture(&path3404_fdef);
bodylist.push_back(path3404_body);
uData* path3404_ud = new uData();
path3404_ud->name = "TITLE";
path3404_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3404_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3404_ud->strokewidth=24.23800087f;
path3404_body->SetUserData(path3404_ud);

//
//path3402
//
b2BodyDef path3402_bdef;
path3402_bdef.type = b2_staticBody;
path3402_bdef.bullet = false;
path3402_bdef.position.Set(-4.09802756344f,12.7207136667f);
path3402_bdef.linearDamping = 0.0f;
path3402_bdef.angularDamping =0.0f;
b2Body* path3402_body = m_world->CreateBody(&path3402_bdef);

b2CircleShape path3402_s;
path3402_s.m_radius=0.318572368514f;

b2FixtureDef path3402_fdef;
path3402_fdef.shape = &path3402_s;

path3402_fdef.density = 1.0f;
path3402_fdef.friction = 0.1f;
path3402_fdef.restitution = 0.8f;
path3402_fdef.filter.groupIndex = -1;

path3402_body->CreateFixture(&path3402_fdef);
bodylist.push_back(path3402_body);
uData* path3402_ud = new uData();
path3402_ud->name = "TITLE";
path3402_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3402_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3402_ud->strokewidth=24.23800087f;
path3402_body->SetUserData(path3402_ud);

//
//path3400
//
b2BodyDef path3400_bdef;
path3400_bdef.type = b2_staticBody;
path3400_bdef.bullet = false;
path3400_bdef.position.Set(-4.09802756344f,13.5523967693f);
path3400_bdef.linearDamping = 0.0f;
path3400_bdef.angularDamping =0.0f;
b2Body* path3400_body = m_world->CreateBody(&path3400_bdef);

b2CircleShape path3400_s;
path3400_s.m_radius=0.318572368514f;

b2FixtureDef path3400_fdef;
path3400_fdef.shape = &path3400_s;

path3400_fdef.density = 1.0f;
path3400_fdef.friction = 0.1f;
path3400_fdef.restitution = 0.8f;
path3400_fdef.filter.groupIndex = -1;

path3400_body->CreateFixture(&path3400_fdef);
bodylist.push_back(path3400_body);
uData* path3400_ud = new uData();
path3400_ud->name = "TITLE";
path3400_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3400_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3400_ud->strokewidth=24.23800087f;
path3400_body->SetUserData(path3400_ud);

//
//path3398
//
b2BodyDef path3398_bdef;
path3398_bdef.type = b2_staticBody;
path3398_bdef.bullet = false;
path3398_bdef.position.Set(-4.09802756344f,14.106852249f);
path3398_bdef.linearDamping = 0.0f;
path3398_bdef.angularDamping =0.0f;
b2Body* path3398_body = m_world->CreateBody(&path3398_bdef);

b2CircleShape path3398_s;
path3398_s.m_radius=0.318572368514f;

b2FixtureDef path3398_fdef;
path3398_fdef.shape = &path3398_s;

path3398_fdef.density = 1.0f;
path3398_fdef.friction = 0.1f;
path3398_fdef.restitution = 0.8f;
path3398_fdef.filter.groupIndex = -1;

path3398_body->CreateFixture(&path3398_fdef);
bodylist.push_back(path3398_body);
uData* path3398_ud = new uData();
path3398_ud->name = "TITLE";
path3398_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3398_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3398_ud->strokewidth=24.23800087f;
path3398_body->SetUserData(path3398_ud);

//
//path3396
//
b2BodyDef path3396_bdef;
path3396_bdef.type = b2_staticBody;
path3396_bdef.bullet = false;
path3396_bdef.position.Set(-4.09802756344f,14.8593274928f);
path3396_bdef.linearDamping = 0.0f;
path3396_bdef.angularDamping =0.0f;
b2Body* path3396_body = m_world->CreateBody(&path3396_bdef);

b2CircleShape path3396_s;
path3396_s.m_radius=0.318572368514f;

b2FixtureDef path3396_fdef;
path3396_fdef.shape = &path3396_s;

path3396_fdef.density = 1.0f;
path3396_fdef.friction = 0.1f;
path3396_fdef.restitution = 0.8f;
path3396_fdef.filter.groupIndex = -1;

path3396_body->CreateFixture(&path3396_fdef);
bodylist.push_back(path3396_body);
uData* path3396_ud = new uData();
path3396_ud->name = "TITLE";
path3396_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path3396_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3396_ud->strokewidth=24.23800087f;
path3396_body->SetUserData(path3396_ud);

//
//path4178
//
b2BodyDef path4178_bdef;
path4178_bdef.type = b2_staticBody;
path4178_bdef.bullet = false;
path4178_bdef.position.Set(0.298012578427f,2.03391091791f);
path4178_bdef.linearDamping = 0.0f;
path4178_bdef.angularDamping =0.0f;
b2Body* path4178_body = m_world->CreateBody(&path4178_bdef);

b2CircleShape path4178_s;
path4178_s.m_radius=0.318572368514f;

b2FixtureDef path4178_fdef;
path4178_fdef.shape = &path4178_s;

path4178_fdef.density = 1.0f;
path4178_fdef.friction = 0.1f;
path4178_fdef.restitution = 0.8f;
path4178_fdef.filter.groupIndex = -1;

path4178_body->CreateFixture(&path4178_fdef);
bodylist.push_back(path4178_body);
uData* path4178_ud = new uData();
path4178_ud->name = "TITLE";
path4178_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path4178_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4178_ud->strokewidth=24.23800087f;
path4178_body->SetUserData(path4178_ud);

//
//path3628
//
b2BodyDef path3628_bdef;
path3628_bdef.type = b2_staticBody;
path3628_bdef.bullet = false;
path3628_bdef.position.Set(0.971278326731f,2.66757472407f);
path3628_bdef.linearDamping = 0.0f;
path3628_bdef.angularDamping =0.0f;
b2Body* path3628_body = m_world->CreateBody(&path3628_bdef);

b2CircleShape path3628_s;
path3628_s.m_radius=0.318572368514f;

b2FixtureDef path3628_fdef;
path3628_fdef.shape = &path3628_s;

path3628_fdef.density = 1.0f;
path3628_fdef.friction = 0.1f;
path3628_fdef.restitution = 0.8f;
path3628_fdef.filter.groupIndex = -1;

path3628_body->CreateFixture(&path3628_fdef);
bodylist.push_back(path3628_body);
uData* path3628_ud = new uData();
path3628_ud->name = "TITLE";
path3628_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3628_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3628_ud->strokewidth=24.23800087f;
path3628_body->SetUserData(path3628_ud);

//
//path3626
//
b2BodyDef path3626_bdef;
path3626_bdef.type = b2_staticBody;
path3626_bdef.bullet = false;
path3626_bdef.position.Set(1.60494213289f,3.26163425009f);
path3626_bdef.linearDamping = 0.0f;
path3626_bdef.angularDamping =0.0f;
b2Body* path3626_body = m_world->CreateBody(&path3626_bdef);

b2CircleShape path3626_s;
path3626_s.m_radius=0.318572368514f;

b2FixtureDef path3626_fdef;
path3626_fdef.shape = &path3626_s;

path3626_fdef.density = 1.0f;
path3626_fdef.friction = 0.1f;
path3626_fdef.restitution = 0.8f;
path3626_fdef.filter.groupIndex = -1;

path3626_body->CreateFixture(&path3626_fdef);
bodylist.push_back(path3626_body);
uData* path3626_ud = new uData();
path3626_ud->name = "TITLE";
path3626_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3626_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3626_ud->strokewidth=24.23800087f;
path3626_body->SetUserData(path3626_ud);

//
//path3624
//
b2BodyDef path3624_bdef;
path3624_bdef.type = b2_staticBody;
path3624_bdef.bullet = false;
path3624_bdef.position.Set(2.15939737878f,3.73688093572f);
path3624_bdef.linearDamping = 0.0f;
path3624_bdef.angularDamping =0.0f;
b2Body* path3624_body = m_world->CreateBody(&path3624_bdef);

b2CircleShape path3624_s;
path3624_s.m_radius=0.318572368514f;

b2FixtureDef path3624_fdef;
path3624_fdef.shape = &path3624_s;

path3624_fdef.density = 1.0f;
path3624_fdef.friction = 0.1f;
path3624_fdef.restitution = 0.8f;
path3624_fdef.filter.groupIndex = -1;

path3624_body->CreateFixture(&path3624_fdef);
bodylist.push_back(path3624_body);
uData* path3624_ud = new uData();
path3624_ud->name = "TITLE";
path3624_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3624_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3624_ud->strokewidth=24.23800087000000000f;
path3624_body->SetUserData(path3624_ud);

//
//path3622
//
b2BodyDef path3622_bdef;
path3622_bdef.type = b2_staticBody;
path3622_bdef.bullet = false;
path3622_bdef.position.Set(2.63464523339f,4.410149022f);
path3622_bdef.linearDamping = 0.0f;
path3622_bdef.angularDamping =0.0f;
b2Body* path3622_body = m_world->CreateBody(&path3622_bdef);

b2CircleShape path3622_s;
path3622_s.m_radius=0.318572368514f;

b2FixtureDef path3622_fdef;
path3622_fdef.shape = &path3622_s;

path3622_fdef.density = 1.0f;
path3622_fdef.friction = 0.1f;
path3622_fdef.restitution = 0.8f;
path3622_fdef.filter.groupIndex = -1;

path3622_body->CreateFixture(&path3622_fdef);
bodylist.push_back(path3622_body);
uData* path3622_ud = new uData();
path3622_ud->name = "TITLE";
path3622_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3622_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3622_ud->strokewidth=24.23800087000000000f;
path3622_body->SetUserData(path3622_ud);

//
//path3620
//
b2BodyDef path3620_bdef;
path3620_bdef.type = b2_staticBody;
path3620_bdef.bullet = false;
path3620_bdef.position.Set(3.07028880788f,5.0834160562f);
path3620_bdef.linearDamping = 0.0f;
path3620_bdef.angularDamping =0.0f;
b2Body* path3620_body = m_world->CreateBody(&path3620_bdef);

b2CircleShape path3620_s;
path3620_s.m_radius=0.318572368514f;

b2FixtureDef path3620_fdef;
path3620_fdef.shape = &path3620_s;

path3620_fdef.density = 1.0f;
path3620_fdef.friction = 0.1f;
path3620_fdef.restitution = 0.8f;
path3620_fdef.filter.groupIndex = -1;

path3620_body->CreateFixture(&path3620_fdef);
bodylist.push_back(path3620_body);
uData* path3620_ud = new uData();
path3620_ud->name = "TITLE";
path3620_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3620_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3620_ud->strokewidth=24.23800087000000000f;
path3620_body->SetUserData(path3620_ud);

//
//path3618
//
b2BodyDef path3618_bdef;
path3618_bdef.type = b2_staticBody;
path3618_bdef.bullet = false;
path3618_bdef.position.Set(3.4267238221f,5.71707951166f);
path3618_bdef.linearDamping = 0.0f;
path3618_bdef.angularDamping =0.0f;
b2Body* path3618_body = m_world->CreateBody(&path3618_bdef);

b2CircleShape path3618_s;
path3618_s.m_radius=0.318572368514f;

b2FixtureDef path3618_fdef;
path3618_fdef.shape = &path3618_s;

path3618_fdef.density = 1.0f;
path3618_fdef.friction = 0.1f;
path3618_fdef.restitution = 0.8f;
path3618_fdef.filter.groupIndex = -1;

path3618_body->CreateFixture(&path3618_fdef);
bodylist.push_back(path3618_body);
uData* path3618_ud = new uData();
path3618_ud->name = "TITLE";
path3618_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3618_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3618_ud->strokewidth=24.23800087000000000f;
path3618_body->SetUserData(path3618_ud);

//
//path3616
//
b2BodyDef path3616_bdef;
path3616_bdef.type = b2_staticBody;
path3616_bdef.bullet = false;
path3616_bdef.position.Set(3.6643483339f,6.50915868486f);
path3616_bdef.linearDamping = 0.0f;
path3616_bdef.angularDamping =0.0f;
b2Body* path3616_body = m_world->CreateBody(&path3616_bdef);

b2CircleShape path3616_s;
path3616_s.m_radius=0.318572368514f;

b2FixtureDef path3616_fdef;
path3616_fdef.shape = &path3616_s;

path3616_fdef.density = 1.0f;
path3616_fdef.friction = 0.1f;
path3616_fdef.restitution = 0.8f;
path3616_fdef.filter.groupIndex = -1;

path3616_body->CreateFixture(&path3616_fdef);
bodylist.push_back(path3616_body);
uData* path3616_ud = new uData();
path3616_ud->name = "TITLE";
path3616_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3616_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3616_ud->strokewidth=24.23800087000000000f;
path3616_body->SetUserData(path3616_ud);

//
//path3614
//
b2BodyDef path3614_bdef;
path3614_bdef.type = b2_staticBody;
path3614_bdef.bullet = false;
path3614_bdef.position.Set(3.70395261403f,7.34084178749f);
path3614_bdef.linearDamping = 0.0f;
path3614_bdef.angularDamping =0.0f;
b2Body* path3614_body = m_world->CreateBody(&path3614_bdef);

b2CircleShape path3614_s;
path3614_s.m_radius=0.318572368514f;

b2FixtureDef path3614_fdef;
path3614_fdef.shape = &path3614_s;

path3614_fdef.density = 1.0f;
path3614_fdef.friction = 0.1f;
path3614_fdef.restitution = 0.8f;
path3614_fdef.filter.groupIndex = -1;

path3614_body->CreateFixture(&path3614_fdef);
bodylist.push_back(path3614_body);
uData* path3614_ud = new uData();
path3614_ud->name = "TITLE";
path3614_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3614_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3614_ud->strokewidth=24.23800087000000000f;
path3614_body->SetUserData(path3614_ud);

//
//path3612
//
b2BodyDef path3612_bdef;
path3612_bdef.type = b2_staticBody;
path3612_bdef.bullet = false;
path3612_bdef.position.Set(3.38712071095f,8.13292107759f);
path3612_bdef.linearDamping = 0.0f;
path3612_bdef.angularDamping =0.0f;
b2Body* path3612_body = m_world->CreateBody(&path3612_bdef);

b2CircleShape path3612_s;
path3612_s.m_radius=0.318572368514f;

b2FixtureDef path3612_fdef;
path3612_fdef.shape = &path3612_s;

path3612_fdef.density = 1.0f;
path3612_fdef.friction = 0.1f;
path3612_fdef.restitution = 0.8f;
path3612_fdef.filter.groupIndex = -1;

path3612_body->CreateFixture(&path3612_fdef);
bodylist.push_back(path3612_body);
uData* path3612_ud = new uData();
path3612_ud->name = "TITLE";
path3612_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3612_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3612_ud->strokewidth=24.23800087000000000f;
path3612_body->SetUserData(path3612_ud);

//
//path3610
//
b2BodyDef path3610_bdef;
path3610_bdef.type = b2_staticBody;
path3610_bdef.bullet = false;
path3610_bdef.position.Set(2.91187285634f,8.76658441615f);
path3610_bdef.linearDamping = 0.0f;
path3610_bdef.angularDamping =0.0f;
b2Body* path3610_body = m_world->CreateBody(&path3610_bdef);

b2CircleShape path3610_s;
path3610_s.m_radius=0.318572368514f;

b2FixtureDef path3610_fdef;
path3610_fdef.shape = &path3610_s;

path3610_fdef.density = 1.0f;
path3610_fdef.friction = 0.1f;
path3610_fdef.restitution = 0.8f;
path3610_fdef.filter.groupIndex = -1;

path3610_body->CreateFixture(&path3610_fdef);
bodylist.push_back(path3610_body);
uData* path3610_ud = new uData();
path3610_ud->name = "TITLE";
path3610_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3610_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3610_ud->strokewidth=24.23800087000000000f;
path3610_body->SetUserData(path3610_ud);

//
//path3608
//
b2BodyDef path3608_bdef;
path3608_bdef.type = b2_staticBody;
path3608_bdef.bullet = false;
path3608_bdef.position.Set(2.15939737878f,8.92500025079f);
path3608_bdef.linearDamping = 0.0f;
path3608_bdef.angularDamping =0.0f;
b2Body* path3608_body = m_world->CreateBody(&path3608_bdef);

b2CircleShape path3608_s;
path3608_s.m_radius=0.318572368514f;

b2FixtureDef path3608_fdef;
path3608_fdef.shape = &path3608_s;

path3608_fdef.density = 1.0f;
path3608_fdef.friction = 0.1f;
path3608_fdef.restitution = 0.8f;
path3608_fdef.filter.groupIndex = -1;

path3608_body->CreateFixture(&path3608_fdef);
bodylist.push_back(path3608_body);
uData* path3608_ud = new uData();
path3608_ud->name = "TITLE";
path3608_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3608_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3608_ud->strokewidth=24.23800087000000000f;
path3608_body->SetUserData(path3608_ud);

//
//path3606
//
b2BodyDef path3606_bdef;
path3606_bdef.type = b2_staticBody;
path3606_bdef.bullet = false;
path3606_bdef.position.Set(1.36731879007f,8.88539632135f);
path3606_bdef.linearDamping = 0.0f;
path3606_bdef.angularDamping =0.0f;
b2Body* path3606_body = m_world->CreateBody(&path3606_bdef);

b2CircleShape path3606_s;
path3606_s.m_radius=0.318572368514f;

b2FixtureDef path3606_fdef;
path3606_fdef.shape = &path3606_s;

path3606_fdef.density = 1.0f;
path3606_fdef.friction = 0.1f;
path3606_fdef.restitution = 0.8f;
path3606_fdef.filter.groupIndex = -1;

path3606_body->CreateFixture(&path3606_fdef);
bodylist.push_back(path3606_body);
uData* path3606_ud = new uData();
path3606_ud->name = "TITLE";
path3606_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3606_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3606_ud->strokewidth=24.23800087000000000f;
path3606_body->SetUserData(path3606_ud);

//
//path3604
//
b2BodyDef path3604_bdef;
path3604_bdef.type = b2_staticBody;
path3604_bdef.bullet = false;
path3604_bdef.position.Set(0.57523903238f,8.64777251094f);
path3604_bdef.linearDamping = 0.0f;
path3604_bdef.angularDamping =0.0f;
b2Body* path3604_body = m_world->CreateBody(&path3604_bdef);

b2CircleShape path3604_s;
path3604_s.m_radius=0.318572368514f;

b2FixtureDef path3604_fdef;
path3604_fdef.shape = &path3604_s;

path3604_fdef.density = 1.0f;
path3604_fdef.friction = 0.1f;
path3604_fdef.restitution = 0.8f;
path3604_fdef.filter.groupIndex = -1;

path3604_body->CreateFixture(&path3604_fdef);
bodylist.push_back(path3604_body);
uData* path3604_ud = new uData();
path3604_ud->name = "TITLE";
path3604_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3604_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3604_ud->strokewidth=24.23800087000000000f;
path3604_body->SetUserData(path3604_ud);

//
//path3600
//
b2BodyDef path3600_bdef;
path3600_bdef.type = b2_staticBody;
path3600_bdef.bullet = false;
path3600_bdef.position.Set(-0.0584236047834f,8.13292107759f);
path3600_bdef.linearDamping = 0.0f;
path3600_bdef.angularDamping =0.0f;
b2Body* path3600_body = m_world->CreateBody(&path3600_bdef);

b2CircleShape path3600_s;
path3600_s.m_radius=0.318572368514f;

b2FixtureDef path3600_fdef;
path3600_fdef.shape = &path3600_s;

path3600_fdef.density = 1.0f;
path3600_fdef.friction = 0.1f;
path3600_fdef.restitution = 0.8f;
path3600_fdef.filter.groupIndex = -1;

path3600_body->CreateFixture(&path3600_fdef);
bodylist.push_back(path3600_body);
uData* path3600_ud = new uData();
path3600_ud->name = "TITLE";
path3600_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3600_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3600_ud->strokewidth=24.23800087000000000f;
path3600_body->SetUserData(path3600_ud);

//
//path3598
//
b2BodyDef path3598_bdef;
path3598_bdef.type = b2_staticBody;
path3598_bdef.bullet = false;
path3598_bdef.position.Set(-0.494067179267f,7.45965369269f);
path3598_bdef.linearDamping = 0.0f;
path3598_bdef.angularDamping =0.0f;
b2Body* path3598_body = m_world->CreateBody(&path3598_bdef);

b2CircleShape path3598_s;
path3598_s.m_radius=0.318572368514f;

b2FixtureDef path3598_fdef;
path3598_fdef.shape = &path3598_s;

path3598_fdef.density = 1.0f;
path3598_fdef.friction = 0.1f;
path3598_fdef.restitution = 0.8f;
path3598_fdef.filter.groupIndex = -1;

path3598_body->CreateFixture(&path3598_fdef);
bodylist.push_back(path3598_body);
uData* path3598_ud = new uData();
path3598_ud->name = "TITLE";
path3598_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3598_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3598_ud->strokewidth=24.23800087000000000f;
path3598_body->SetUserData(path3598_ud);

//
//path3596
//
b2BodyDef path3596_bdef;
path3596_bdef.type = b2_staticBody;
path3596_bdef.bullet = false;
path3596_bdef.position.Set(-0.810899082345f,8.13292107759f);
path3596_bdef.linearDamping = 0.0f;
path3596_bdef.angularDamping =0.0f;
b2Body* path3596_body = m_world->CreateBody(&path3596_bdef);

b2CircleShape path3596_s;
path3596_s.m_radius=0.318572368514f;

b2FixtureDef path3596_fdef;
path3596_fdef.shape = &path3596_s;

path3596_fdef.density = 1.0f;
path3596_fdef.friction = 0.1f;
path3596_fdef.restitution = 0.8f;
path3596_fdef.filter.groupIndex = -1;

path3596_body->CreateFixture(&path3596_fdef);
bodylist.push_back(path3596_body);
uData* path3596_ud = new uData();
path3596_ud->name = "TITLE";
path3596_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3596_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3596_ud->strokewidth=24.23800087000000000f;
path3596_body->SetUserData(path3596_ud);

//
//path3594
//
b2BodyDef path3594_bdef;
path3594_bdef.type = b2_staticBody;
path3594_bdef.bullet = false;
path3594_bdef.position.Set(-1.56337455991f,8.68737655728f);
path3594_bdef.linearDamping = 0.0f;
path3594_bdef.angularDamping =0.0f;
b2Body* path3594_body = m_world->CreateBody(&path3594_bdef);

b2CircleShape path3594_s;
path3594_s.m_radius=0.318572368514f;

b2FixtureDef path3594_fdef;
path3594_fdef.shape = &path3594_s;

path3594_fdef.density = 1.0f;
path3594_fdef.friction = 0.1f;
path3594_fdef.restitution = 0.8f;
path3594_fdef.filter.groupIndex = -1;

path3594_body->CreateFixture(&path3594_fdef);
bodylist.push_back(path3594_body);
uData* path3594_ud = new uData();
path3594_ud->name = "TITLE";
path3594_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3594_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3594_ud->strokewidth=24.23800087000000000f;
path3594_body->SetUserData(path3594_ud);

//
//path3592
//
b2BodyDef path3592_bdef;
path3592_bdef.type = b2_staticBody;
path3592_bdef.bullet = false;
path3592_bdef.position.Set(-2.31584992057f,8.92500025079f);
path3592_bdef.linearDamping = 0.0f;
path3592_bdef.angularDamping =0.0f;
b2Body* path3592_body = m_world->CreateBody(&path3592_bdef);

b2CircleShape path3592_s;
path3592_s.m_radius=0.318572368514f;

b2FixtureDef path3592_fdef;
path3592_fdef.shape = &path3592_s;

path3592_fdef.density = 1.0f;
path3592_fdef.friction = 0.1f;
path3592_fdef.restitution = 0.8f;
path3592_fdef.filter.groupIndex = -1;

path3592_body->CreateFixture(&path3592_fdef);
bodylist.push_back(path3592_body);
uData* path3592_ud = new uData();
path3592_ud->name = "TITLE";
path3592_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3592_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3592_ud->strokewidth=24.23800087000000000f;
path3592_body->SetUserData(path3592_ud);

//
//path3590
//
b2BodyDef path3590_bdef;
path3590_bdef.type = b2_staticBody;
path3590_bdef.bullet = false;
path3590_bdef.position.Set(-3.10792909377f,8.84579239192f);
path3590_bdef.linearDamping = 0.0f;
path3590_bdef.angularDamping =0.0f;
b2Body* path3590_body = m_world->CreateBody(&path3590_bdef);

b2CircleShape path3590_s;
path3590_s.m_radius=0.318572368514f;

b2FixtureDef path3590_fdef;
path3590_fdef.shape = &path3590_s;

path3590_fdef.density = 1.0f;
path3590_fdef.friction = 0.1f;
path3590_fdef.restitution = 0.8f;
path3590_fdef.filter.groupIndex = -1;

path3590_body->CreateFixture(&path3590_fdef);
bodylist.push_back(path3590_body);
uData* path3590_ud = new uData();
path3590_ud->name = "TITLE";
path3590_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3590_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3590_ud->strokewidth=24.23800087000000000f;
path3590_body->SetUserData(path3590_ud);

//
//path3588
//
b2BodyDef path3588_bdef;
path3588_bdef.type = b2_staticBody;
path3588_bdef.bullet = false;
path3588_bdef.position.Set(-4.01882017217f,8.60816858151f);
path3588_bdef.linearDamping = 0.0f;
path3588_bdef.angularDamping =0.0f;
b2Body* path3588_body = m_world->CreateBody(&path3588_bdef);

b2CircleShape path3588_s;
path3588_s.m_radius=0.318572368514f;

b2FixtureDef path3588_fdef;
path3588_fdef.shape = &path3588_s;

path3588_fdef.density = 1.0f;
path3588_fdef.friction = 0.1f;
path3588_fdef.restitution = 0.8f;
path3588_fdef.filter.groupIndex = -1;

path3588_body->CreateFixture(&path3588_fdef);
bodylist.push_back(path3588_body);
uData* path3588_ud = new uData();
path3588_ud->name = "TITLE";
path3588_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3588_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3588_ud->strokewidth=24.23800087000000000f;
path3588_body->SetUserData(path3588_ud);

//
//path3586
//
b2BodyDef path3586_bdef;
path3586_bdef.type = b2_staticBody;
path3586_bdef.bullet = false;
path3586_bdef.position.Set(-4.77129541593f,7.93490119661f);
path3586_bdef.linearDamping = 0.0f;
path3586_bdef.angularDamping =0.0f;
b2Body* path3586_body = m_world->CreateBody(&path3586_bdef);

b2CircleShape path3586_s;
path3586_s.m_radius=0.318572368514f;

b2FixtureDef path3586_fdef;
path3586_fdef.shape = &path3586_s;

path3586_fdef.density = 1.0f;
path3586_fdef.friction = 0.1f;
path3586_fdef.restitution = 0.8f;
path3586_fdef.filter.groupIndex = -1;

path3586_body->CreateFixture(&path3586_fdef);
bodylist.push_back(path3586_body);
uData* path3586_ud = new uData();
path3586_ud->name = "TITLE";
path3586_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3586_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3586_ud->strokewidth=24.23800087000000000f;
path3586_body->SetUserData(path3586_ud);

//
//path3584
//
b2BodyDef path3584_bdef;
path3584_bdef.type = b2_staticBody;
path3584_bdef.bullet = false;
path3584_bdef.position.Set(-4.7316914865f,7.14282202341f);
path3584_bdef.linearDamping = 0.0f;
path3584_bdef.angularDamping =0.0f;
b2Body* path3584_body = m_world->CreateBody(&path3584_bdef);

b2CircleShape path3584_s;
path3584_s.m_radius=0.318572368514f;

b2FixtureDef path3584_fdef;
path3584_fdef.shape = &path3584_s;

path3584_fdef.density = 1.0f;
path3584_fdef.friction = 0.1f;
path3584_fdef.restitution = 0.8f;
path3584_fdef.filter.groupIndex = -1;

path3584_body->CreateFixture(&path3584_fdef);
bodylist.push_back(path3584_body);
uData* path3584_ud = new uData();
path3584_ud->name = "TITLE";
path3584_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3584_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3584_ud->strokewidth=24.23800087000000000f;
path3584_body->SetUserData(path3584_ud);

//
//path3582
//
b2BodyDef path3582_bdef;
path3582_bdef.type = b2_staticBody;
path3582_bdef.bullet = false;
path3582_bdef.position.Set(-4.49406767609f,6.23193094501f);
path3582_bdef.linearDamping = 0.0f;
path3582_bdef.angularDamping =0.0f;
b2Body* path3582_body = m_world->CreateBody(&path3582_bdef);

b2CircleShape path3582_s;
path3582_s.m_radius=0.318572368514f;

b2FixtureDef path3582_fdef;
path3582_fdef.shape = &path3582_s;

path3582_fdef.density = 1.0f;
path3582_fdef.friction = 0.1f;
path3582_fdef.restitution = 0.8f;
path3582_fdef.filter.groupIndex = -1;

path3582_body->CreateFixture(&path3582_fdef);
bodylist.push_back(path3582_body);
uData* path3582_ud = new uData();
path3582_ud->name = "TITLE";
path3582_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3582_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3582_ud->strokewidth=24.23800087000000000f;
path3582_body->SetUserData(path3582_ud);

//
//path3580
//
b2BodyDef path3580_bdef;
path3580_bdef.type = b2_staticBody;
path3580_bdef.bullet = false;
path3580_bdef.position.Set(-4.0584241016f,5.40024772548f);
path3580_bdef.linearDamping = 0.0f;
path3580_bdef.angularDamping =0.0f;
b2Body* path3580_body = m_world->CreateBody(&path3580_bdef);

b2CircleShape path3580_s;
path3580_s.m_radius=0.318572368514f;

b2FixtureDef path3580_fdef;
path3580_fdef.shape = &path3580_s;

path3580_fdef.density = 1.0f;
path3580_fdef.friction = 0.1f;
path3580_fdef.restitution = 0.8f;
path3580_fdef.filter.groupIndex = -1;

path3580_body->CreateFixture(&path3580_fdef);
bodylist.push_back(path3580_body);
uData* path3580_ud = new uData();
path3580_ud->name = "TITLE";
path3580_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3580_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3580_ud->strokewidth=24.23800087000000000f;
path3580_body->SetUserData(path3580_ud);

//
//path3578
//
b2BodyDef path3578_bdef;
path3578_bdef.type = b2_staticBody;
path3578_bdef.bullet = false;
path3578_bdef.position.Set(-3.46436480938f,4.56856462285f);
path3578_bdef.linearDamping = 0.0f;
path3578_bdef.angularDamping =0.0f;
b2Body* path3578_body = m_world->CreateBody(&path3578_bdef);

b2CircleShape path3578_s;
path3578_s.m_radius=0.318572368514f;

b2FixtureDef path3578_fdef;
path3578_fdef.shape = &path3578_s;

path3578_fdef.density = 1.0f;
path3578_fdef.friction = 0.1f;
path3578_fdef.restitution = 0.8f;
path3578_fdef.filter.groupIndex = -1;

path3578_body->CreateFixture(&path3578_fdef);
bodylist.push_back(path3578_body);
uData* path3578_ud = new uData();
path3578_ud->name = "TITLE";
path3578_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3578_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3578_ud->strokewidth=24.23800087000000000f;
path3578_body->SetUserData(path3578_ud);

//
//path3576
//
b2BodyDef path3576_bdef;
path3576_bdef.type = b2_staticBody;
path3576_bdef.bullet = false;
path3576_bdef.position.Set(-3.0287212349f,3.97450544752f);
path3576_bdef.linearDamping = 0.0f;
path3576_bdef.angularDamping =0.0f;
b2Body* path3576_body = m_world->CreateBody(&path3576_bdef);

b2CircleShape path3576_s;
path3576_s.m_radius=0.318572368514f;

b2FixtureDef path3576_fdef;
path3576_fdef.shape = &path3576_s;

path3576_fdef.density = 1.0f;
path3576_fdef.friction = 0.1f;
path3576_fdef.restitution = 0.8f;
path3576_fdef.filter.groupIndex = -1;

path3576_body->CreateFixture(&path3576_fdef);
bodylist.push_back(path3576_body);
uData* path3576_ud = new uData();
path3576_ud->name = "TITLE";
path3576_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3576_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3576_ud->strokewidth=24.23800087000000000f;
path3576_body->SetUserData(path3576_ud);

//
//path3574
//
b2BodyDef path3574_bdef;
path3574_bdef.type = b2_staticBody;
path3574_bdef.bullet = false;
path3574_bdef.position.Set(-2.47426575521f,3.42005020163f);
path3574_bdef.linearDamping = 0.0f;
path3574_bdef.angularDamping =0.0f;
b2Body* path3574_body = m_world->CreateBody(&path3574_bdef);

b2CircleShape path3574_s;
path3574_s.m_radius=0.318572368514f;

b2FixtureDef path3574_fdef;
path3574_fdef.shape = &path3574_s;

path3574_fdef.density = 1.0f;
path3574_fdef.friction = 0.1f;
path3574_fdef.restitution = 0.8f;
path3574_fdef.filter.groupIndex = -1;

path3574_body->CreateFixture(&path3574_fdef);
bodylist.push_back(path3574_body);
uData* path3574_ud = new uData();
path3574_ud->name = "TITLE";
path3574_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3574_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3574_ud->strokewidth=24.23800087000000000f;
path3574_body->SetUserData(path3574_ud);

//
//path3572
//
b2BodyDef path3572_bdef;
path3572_bdef.type = b2_staticBody;
path3572_bdef.bullet = false;
path3572_bdef.position.Set(-1.76139479158f,2.74678211534f);
path3572_bdef.linearDamping = 0.0f;
path3572_bdef.angularDamping =0.0f;
b2Body* path3572_body = m_world->CreateBody(&path3572_bdef);

b2CircleShape path3572_s;
path3572_s.m_radius=0.318572368514f;

b2FixtureDef path3572_fdef;
path3572_fdef.shape = &path3572_s;

path3572_fdef.density = 1.0f;
path3572_fdef.friction = 0.1f;
path3572_fdef.restitution = 0.8f;
path3572_fdef.filter.groupIndex = -1;

path3572_body->CreateFixture(&path3572_fdef);
bodylist.push_back(path3572_body);
uData* path3572_ud = new uData();
path3572_ud->name = "TITLE";
path3572_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3572_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3572_ud->strokewidth=24.23800087000000000f;
path3572_body->SetUserData(path3572_ud);

//
//path3570
//
b2BodyDef path3570_bdef;
path3570_bdef.type = b2_staticBody;
path3570_bdef.bullet = false;
path3570_bdef.position.Set(-1.08812670529f,2.11311947818f);
path3570_bdef.linearDamping = 0.0f;
path3570_bdef.angularDamping =0.0f;
b2Body* path3570_body = m_world->CreateBody(&path3570_bdef);

b2CircleShape path3570_s;
path3570_s.m_radius=0.318572368514f;

b2FixtureDef path3570_fdef;
path3570_fdef.shape = &path3570_s;

path3570_fdef.density = 1.0f;
path3570_fdef.friction = 0.1f;
path3570_fdef.restitution = 0.8f;
path3570_fdef.filter.groupIndex = -1;

path3570_body->CreateFixture(&path3570_fdef);
bodylist.push_back(path3570_body);
uData* path3570_ud = new uData();
path3570_ud->name = "TITLE";
path3570_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3570_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3570_ud->strokewidth=24.23800087000000000f;
path3570_body->SetUserData(path3570_ud);

//
//path3568
//
b2BodyDef path3568_bdef;
path3568_bdef.type = b2_staticBody;
path3568_bdef.bullet = false;
path3568_bdef.position.Set(-0.454464068127f,1.51905995216f);
path3568_bdef.linearDamping = 0.0f;
path3568_bdef.angularDamping =0.0f;
b2Body* path3568_body = m_world->CreateBody(&path3568_bdef);

b2CircleShape path3568_s;
path3568_s.m_radius=0.318572368514f;

b2FixtureDef path3568_fdef;
path3568_fdef.shape = &path3568_s;

path3568_fdef.density = 1.0f;
path3568_fdef.friction = 0.1f;
path3568_fdef.restitution = 0.8f;
path3568_fdef.filter.groupIndex = -1;

path3568_body->CreateFixture(&path3568_fdef);
bodylist.push_back(path3568_body);
uData* path3568_ud = new uData();
path3568_ud->name = "TITLE";
path3568_ud->fill.Set(float(255)/255,float(42)/255,float(127)/255);
path3568_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path3568_ud->strokewidth=24.23800087000000000f;
path3568_body->SetUserData(path3568_ud);

//
//path4156
//
b2BodyDef path4156_bdef;
path4156_bdef.type = b2_staticBody;
path4156_bdef.bullet = false;
path4156_bdef.position.Set(-5.35891157492f,11.3167319411f);
path4156_bdef.linearDamping = 0.0f;
path4156_bdef.angularDamping =0.0f;
b2Body* path4156_body = m_world->CreateBody(&path4156_bdef);

b2CircleShape path4156_s;
path4156_s.m_radius=0.318572368514f;

b2FixtureDef path4156_fdef;
path4156_fdef.shape = &path4156_s;

path4156_fdef.density = 1.0f;
path4156_fdef.friction = 0.1f;
path4156_fdef.restitution = 0.8f;
path4156_fdef.filter.groupIndex = -1;

path4156_body->CreateFixture(&path4156_fdef);
bodylist.push_back(path4156_body);
uData* path4156_ud = new uData();
path4156_ud->name = "TITLE";
path4156_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4156_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4156_ud->strokewidth=24.23800087f;
path4156_body->SetUserData(path4156_ud);

//
//path3388
//
b2BodyDef path3388_bdef;
path3388_bdef.type = b2_staticBody;
path3388_bdef.bullet = false;
path3388_bdef.position.Set(-5.35891157492f,11.3167319411f);
path3388_bdef.linearDamping = 0.0f;
path3388_bdef.angularDamping =0.0f;
b2Body* path3388_body = m_world->CreateBody(&path3388_bdef);

b2CircleShape path3388_s;
path3388_s.m_radius=0.318572368514f;

b2FixtureDef path3388_fdef;
path3388_fdef.shape = &path3388_s;

path3388_fdef.density = 1.0f;
path3388_fdef.friction = 0.1f;
path3388_fdef.restitution = 0.8f;
path3388_fdef.filter.groupIndex = -1;

path3388_body->CreateFixture(&path3388_fdef);
bodylist.push_back(path3388_body);
uData* path3388_ud = new uData();
path3388_ud->name = "TITLE";
path3388_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3388_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3388_ud->strokewidth=24.23800087f;
path3388_body->SetUserData(path3388_ud);

//
//path3386
//
b2BodyDef path3386_bdef;
path3386_bdef.type = b2_staticBody;
path3386_bdef.bullet = false;
path3386_bdef.position.Set(-6.19059479446f,11.3167319411f);
path3386_bdef.linearDamping = 0.0f;
path3386_bdef.angularDamping =0.0f;
b2Body* path3386_body = m_world->CreateBody(&path3386_bdef);

b2CircleShape path3386_s;
path3386_s.m_radius=0.318572368514f;

b2FixtureDef path3386_fdef;
path3386_fdef.shape = &path3386_s;

path3386_fdef.density = 1.0f;
path3386_fdef.friction = 0.1f;
path3386_fdef.restitution = 0.8f;
path3386_fdef.filter.groupIndex = -1;

path3386_body->CreateFixture(&path3386_fdef);
bodylist.push_back(path3386_body);
uData* path3386_ud = new uData();
path3386_ud->name = "TITLE";
path3386_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3386_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3386_ud->strokewidth=24.23800087f;
path3386_body->SetUserData(path3386_ud);

//
//path3384
//
b2BodyDef path3384_bdef;
path3384_bdef.type = b2_staticBody;
path3384_bdef.bullet = false;
path3384_bdef.position.Set(-6.98267396765f,11.3167319411f);
path3384_bdef.linearDamping = 0.0f;
path3384_bdef.angularDamping =0.0f;
b2Body* path3384_body = m_world->CreateBody(&path3384_bdef);

b2CircleShape path3384_s;
path3384_s.m_radius=0.318572368514f;

b2FixtureDef path3384_fdef;
path3384_fdef.shape = &path3384_s;

path3384_fdef.density = 1.0f;
path3384_fdef.friction = 0.1f;
path3384_fdef.restitution = 0.8f;
path3384_fdef.filter.groupIndex = -1;

path3384_body->CreateFixture(&path3384_fdef);
bodylist.push_back(path3384_body);
uData* path3384_ud = new uData();
path3384_ud->name = "TITLE";
path3384_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3384_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3384_ud->strokewidth=24.23800087f;
path3384_body->SetUserData(path3384_ud);

//
//path3382
//
b2BodyDef path3382_bdef;
path3382_bdef.type = b2_staticBody;
path3382_bdef.bullet = false;
path3382_bdef.position.Set(-7.02227789709f,11.9899992091f);
path3382_bdef.linearDamping = 0.0f;
path3382_bdef.angularDamping =0.0f;
b2Body* path3382_body = m_world->CreateBody(&path3382_bdef);

b2CircleShape path3382_s;
path3382_s.m_radius=0.318572368514f;

b2FixtureDef path3382_fdef;
path3382_fdef.shape = &path3382_s;

path3382_fdef.density = 1.0f;
path3382_fdef.friction = 0.1f;
path3382_fdef.restitution = 0.8f;
path3382_fdef.filter.groupIndex = -1;

path3382_body->CreateFixture(&path3382_fdef);
bodylist.push_back(path3382_body);
uData* path3382_ud = new uData();
path3382_ud->name = "TITLE";
path3382_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3382_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3382_ud->strokewidth=24.23800087f;
path3382_body->SetUserData(path3382_ud);

//
//path3380
//
b2BodyDef path3380_bdef;
path3380_bdef.type = b2_staticBody;
path3380_bdef.bullet = false;
path3380_bdef.position.Set(-5.47772348013f,13.1385140979f);
path3380_bdef.linearDamping = 0.0f;
path3380_bdef.angularDamping =0.0f;
b2Body* path3380_body = m_world->CreateBody(&path3380_bdef);

b2CircleShape path3380_s;
path3380_s.m_radius=0.318572368514f;

b2FixtureDef path3380_fdef;
path3380_fdef.shape = &path3380_s;

path3380_fdef.density = 1.0f;
path3380_fdef.friction = 0.1f;
path3380_fdef.restitution = 0.8f;
path3380_fdef.filter.groupIndex = -1;

path3380_body->CreateFixture(&path3380_fdef);
bodylist.push_back(path3380_body);
uData* path3380_ud = new uData();
path3380_ud->name = "TITLE";
path3380_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3380_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3380_ud->strokewidth=24.23800087f;
path3380_body->SetUserData(path3380_ud);

//
//path3394
//
b2BodyDef path3394_bdef;
path3394_bdef.type = b2_staticBody;
path3394_bdef.bullet = false;
path3394_bdef.position.Set(-5.47772348013f,13.1385140979f);
path3394_bdef.linearDamping = 0.0f;
path3394_bdef.angularDamping =0.0f;
b2Body* path3394_body = m_world->CreateBody(&path3394_bdef);

b2CircleShape path3394_s;
path3394_s.m_radius=0.318572368514f;

b2FixtureDef path3394_fdef;
path3394_fdef.shape = &path3394_s;

path3394_fdef.density = 1.0f;
path3394_fdef.friction = 0.1f;
path3394_fdef.restitution = 0.8f;
path3394_fdef.filter.groupIndex = -1;

path3394_body->CreateFixture(&path3394_fdef);
bodylist.push_back(path3394_body);
uData* path3394_ud = new uData();
path3394_ud->name = "TITLE";
path3394_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3394_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3394_ud->strokewidth=24.23800087f;
path3394_body->SetUserData(path3394_ud);

//
//path3392
//
b2BodyDef path3392_bdef;
path3392_bdef.type = b2_staticBody;
path3392_bdef.bullet = false;
path3392_bdef.position.Set(-6.23019872389f,13.1781180274f);
path3392_bdef.linearDamping = 0.0f;
path3392_bdef.angularDamping =0.0f;
b2Body* path3392_body = m_world->CreateBody(&path3392_bdef);

b2CircleShape path3392_s;
path3392_s.m_radius=0.318572368514f;

b2FixtureDef path3392_fdef;
path3392_fdef.shape = &path3392_s;

path3392_fdef.density = 1.0f;
path3392_fdef.friction = 0.1f;
path3392_fdef.restitution = 0.8f;
path3392_fdef.filter.groupIndex = -1;

path3392_body->CreateFixture(&path3392_fdef);
bodylist.push_back(path3392_body);
uData* path3392_ud = new uData();
path3392_ud->name = "TITLE";
path3392_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3392_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3392_ud->strokewidth=24.23800087f;
path3392_body->SetUserData(path3392_ud);

//
//path3390
//
b2BodyDef path3390_bdef;
path3390_bdef.type = b2_staticBody;
path3390_bdef.bullet = false;
path3390_bdef.position.Set(-7.02227789709f,12.8612863581f);
path3390_bdef.linearDamping = 0.0f;
path3390_bdef.angularDamping =0.0f;
b2Body* path3390_body = m_world->CreateBody(&path3390_bdef);

b2CircleShape path3390_s;
path3390_s.m_radius=0.318572368514f;

b2FixtureDef path3390_fdef;
path3390_fdef.shape = &path3390_s;

path3390_fdef.density = 1.0f;
path3390_fdef.friction = 0.1f;
path3390_fdef.restitution = 0.8f;
path3390_fdef.filter.groupIndex = -1;

path3390_body->CreateFixture(&path3390_fdef);
bodylist.push_back(path3390_body);
uData* path3390_ud = new uData();
path3390_ud->name = "TITLE";
path3390_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3390_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3390_ud->strokewidth=24.23800087f;
path3390_body->SetUserData(path3390_ud);

//
//path3378
//
b2BodyDef path3378_bdef;
path3378_bdef.type = b2_staticBody;
path3378_bdef.bullet = false;
path3378_bdef.position.Set(-7.02227789709f,13.534553743f);
path3378_bdef.linearDamping = 0.0f;
path3378_bdef.angularDamping =0.0f;
b2Body* path3378_body = m_world->CreateBody(&path3378_bdef);

b2CircleShape path3378_s;
path3378_s.m_radius=0.318572368514f;

b2FixtureDef path3378_fdef;
path3378_fdef.shape = &path3378_s;

path3378_fdef.density = 1.0f;
path3378_fdef.friction = 0.1f;
path3378_fdef.restitution = 0.8f;
path3378_fdef.filter.groupIndex = -1;

path3378_body->CreateFixture(&path3378_fdef);
bodylist.push_back(path3378_body);
uData* path3378_ud = new uData();
path3378_ud->name = "TITLE";
path3378_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3378_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3378_ud->strokewidth=24.23800087f;
path3378_body->SetUserData(path3378_ud);

//
//path3376
//
b2BodyDef path3376_bdef;
path3376_bdef.type = b2_staticBody;
path3376_bdef.bullet = false;
path3376_bdef.position.Set(-7.02227789709f,14.1682170815f);
path3376_bdef.linearDamping = 0.0f;
path3376_bdef.angularDamping =0.0f;
b2Body* path3376_body = m_world->CreateBody(&path3376_bdef);

b2CircleShape path3376_s;
path3376_s.m_radius=0.318572368514f;

b2FixtureDef path3376_fdef;
path3376_fdef.shape = &path3376_s;

path3376_fdef.density = 1.0f;
path3376_fdef.friction = 0.1f;
path3376_fdef.restitution = 0.8f;
path3376_fdef.filter.groupIndex = -1;

path3376_body->CreateFixture(&path3376_fdef);
bodylist.push_back(path3376_body);
uData* path3376_ud = new uData();
path3376_ud->name = "TITLE";
path3376_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3376_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3376_ud->strokewidth=24.23800087f;
path3376_body->SetUserData(path3376_ud);

//
//path3374
//
b2BodyDef path3374_bdef;
path3374_bdef.type = b2_staticBody;
path3374_bdef.bullet = false;
path3374_bdef.position.Set(-7.02227789709f,14.9206923253f);
path3374_bdef.linearDamping = 0.0f;
path3374_bdef.angularDamping =0.0f;
b2Body* path3374_body = m_world->CreateBody(&path3374_bdef);

b2CircleShape path3374_s;
path3374_s.m_radius=0.318572368514f;

b2FixtureDef path3374_fdef;
path3374_fdef.shape = &path3374_s;

path3374_fdef.density = 1.0f;
path3374_fdef.friction = 0.1f;
path3374_fdef.restitution = 0.8f;
path3374_fdef.filter.groupIndex = -1;

path3374_body->CreateFixture(&path3374_fdef);
bodylist.push_back(path3374_body);
uData* path3374_ud = new uData();
path3374_ud->name = "TITLE";
path3374_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3374_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3374_ud->strokewidth=24.23800087f;
path3374_body->SetUserData(path3374_ud);

//
//path3372
//
b2BodyDef path3372_bdef;
path3372_bdef.type = b2_staticBody;
path3372_bdef.bullet = false;
path3372_bdef.position.Set(-7.02227789709f,14.9206923253f);
path3372_bdef.linearDamping = 0.0f;
path3372_bdef.angularDamping =0.0f;
b2Body* path3372_body = m_world->CreateBody(&path3372_bdef);

b2CircleShape path3372_s;
path3372_s.m_radius=0.318572368514f;

b2FixtureDef path3372_fdef;
path3372_fdef.shape = &path3372_s;

path3372_fdef.density = 1.0f;
path3372_fdef.friction = 0.1f;
path3372_fdef.restitution = 0.8f;
path3372_fdef.filter.groupIndex = -1;

path3372_body->CreateFixture(&path3372_fdef);
bodylist.push_back(path3372_body);
uData* path3372_ud = new uData();
path3372_ud->name = "TITLE";
path3372_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3372_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3372_ud->strokewidth=24.23800087f;
path3372_body->SetUserData(path3372_ud);

//
//path3370
//
b2BodyDef path3370_bdef;
path3370_bdef.type = b2_staticBody;
path3370_bdef.bullet = false;
path3370_bdef.position.Set(-6.15099074812f,14.9206923253f);
path3370_bdef.linearDamping = 0.0f;
path3370_bdef.angularDamping =0.0f;
b2Body* path3370_body = m_world->CreateBody(&path3370_bdef);

b2CircleShape path3370_s;
path3370_s.m_radius=0.318572368514f;

b2FixtureDef path3370_fdef;
path3370_fdef.shape = &path3370_s;

path3370_fdef.density = 1.0f;
path3370_fdef.friction = 0.1f;
path3370_fdef.restitution = 0.8f;
path3370_fdef.filter.groupIndex = -1;

path3370_body->CreateFixture(&path3370_fdef);
bodylist.push_back(path3370_body);
uData* path3370_ud = new uData();
path3370_ud->name = "TITLE";
path3370_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3370_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3370_ud->strokewidth=24.23800087f;
path3370_body->SetUserData(path3370_ud);

//
//path3368
//
b2BodyDef path3368_bdef;
path3368_bdef.type = b2_staticBody;
path3368_bdef.bullet = false;
path3368_bdef.position.Set(-5.35891157492f,14.9206923253f);
path3368_bdef.linearDamping = 0.0f;
path3368_bdef.angularDamping =0.0f;
b2Body* path3368_body = m_world->CreateBody(&path3368_bdef);

b2CircleShape path3368_s;
path3368_s.m_radius=0.318572368514f;

b2FixtureDef path3368_fdef;
path3368_fdef.shape = &path3368_s;

path3368_fdef.density = 1.0f;
path3368_fdef.friction = 0.1f;
path3368_fdef.restitution = 0.8f;
path3368_fdef.filter.groupIndex = -1;

path3368_body->CreateFixture(&path3368_fdef);
bodylist.push_back(path3368_body);
uData* path3368_ud = new uData();
path3368_ud->name = "TITLE";
path3368_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3368_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3368_ud->strokewidth=24.23800087f;
path3368_body->SetUserData(path3368_ud);

//
//path4154
//
b2BodyDef path4154_bdef;
path4154_bdef.type = b2_staticBody;
path4154_bdef.bullet = false;
path4154_bdef.position.Set(3.68140977221f,17.7426701202f);
path4154_bdef.linearDamping = 0.0f;
path4154_bdef.angularDamping =0.0f;
b2Body* path4154_body = m_world->CreateBody(&path4154_bdef);

b2CircleShape path4154_s;
path4154_s.m_radius=0.318572368514f;

b2FixtureDef path4154_fdef;
path4154_fdef.shape = &path4154_s;

path4154_fdef.density = 1.0f;
path4154_fdef.friction = 0.1f;
path4154_fdef.restitution = 0.8f;
path4154_fdef.filter.groupIndex = -1;

path4154_body->CreateFixture(&path4154_fdef);
bodylist.push_back(path4154_body);
uData* path4154_ud = new uData();
path4154_ud->name = "TITLE";
path4154_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4154_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4154_ud->strokewidth=24.23800087f;
path4154_body->SetUserData(path4154_ud);

//
//path3300
//
b2BodyDef path3300_bdef;
path3300_bdef.type = b2_staticBody;
path3300_bdef.bullet = false;
path3300_bdef.position.Set(3.64180666107f,18.3763335757f);
path3300_bdef.linearDamping = 0.0f;
path3300_bdef.angularDamping =0.0f;
b2Body* path3300_body = m_world->CreateBody(&path3300_bdef);

b2CircleShape path3300_s;
path3300_s.m_radius=0.318572368514f;

b2FixtureDef path3300_fdef;
path3300_fdef.shape = &path3300_s;

path3300_fdef.density = 1.0f;
path3300_fdef.friction = 0.1f;
path3300_fdef.restitution = 0.8f;
path3300_fdef.filter.groupIndex = -1;

path3300_body->CreateFixture(&path3300_fdef);
bodylist.push_back(path3300_body);
uData* path3300_ud = new uData();
path3300_ud->name = "TITLE";
path3300_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3300_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3300_ud->strokewidth=24.23800087f;
path3300_body->SetUserData(path3300_ud);

//
//path3298
//
b2BodyDef path3298_bdef;
path3298_bdef.type = b2_staticBody;
path3298_bdef.bullet = false;
path3298_bdef.position.Set(3.64180666107f,19.2476207246f);
path3298_bdef.linearDamping = 0.0f;
path3298_bdef.angularDamping =0.0f;
b2Body* path3298_body = m_world->CreateBody(&path3298_bdef);

b2CircleShape path3298_s;
path3298_s.m_radius=0.318572368514f;

b2FixtureDef path3298_fdef;
path3298_fdef.shape = &path3298_s;

path3298_fdef.density = 1.0f;
path3298_fdef.friction = 0.1f;
path3298_fdef.restitution = 0.8f;
path3298_fdef.filter.groupIndex = -1;

path3298_body->CreateFixture(&path3298_fdef);
bodylist.push_back(path3298_body);
uData* path3298_ud = new uData();
path3298_ud->name = "TITLE";
path3298_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3298_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3298_ud->strokewidth=24.23800087f;
path3298_body->SetUserData(path3298_ud);

//
//path3296
//
b2BodyDef path3296_bdef;
path3296_bdef.type = b2_staticBody;
path3296_bdef.bullet = false;
path3296_bdef.position.Set(3.68140977221f,19.8812840632f);
path3296_bdef.linearDamping = 0.0f;
path3296_bdef.angularDamping =0.0f;
b2Body* path3296_body = m_world->CreateBody(&path3296_bdef);

b2CircleShape path3296_s;
path3296_s.m_radius=0.318572368514f;

b2FixtureDef path3296_fdef;
path3296_fdef.shape = &path3296_s;

path3296_fdef.density = 1.0f;
path3296_fdef.friction = 0.1f;
path3296_fdef.restitution = 0.8f;
path3296_fdef.filter.groupIndex = -1;

path3296_body->CreateFixture(&path3296_fdef);
bodylist.push_back(path3296_body);
uData* path3296_ud = new uData();
path3296_ud->name = "TITLE";
path3296_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3296_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3296_ud->strokewidth=24.23800087f;
path3296_body->SetUserData(path3296_ud);

//
//path3294
//
b2BodyDef path3294_bdef;
path3294_bdef.type = b2_staticBody;
path3294_bdef.bullet = false;
path3294_bdef.position.Set(3.64180666107f,20.633759307f);
path3294_bdef.linearDamping = 0.0f;
path3294_bdef.angularDamping =0.0f;
b2Body* path3294_body = m_world->CreateBody(&path3294_bdef);

b2CircleShape path3294_s;
path3294_s.m_radius=0.318572368514f;

b2FixtureDef path3294_fdef;
path3294_fdef.shape = &path3294_s;

path3294_fdef.density = 1.0f;
path3294_fdef.friction = 0.1f;
path3294_fdef.restitution = 0.8f;
path3294_fdef.filter.groupIndex = -1;

path3294_body->CreateFixture(&path3294_fdef);
bodylist.push_back(path3294_body);
uData* path3294_ud = new uData();
path3294_ud->name = "TITLE";
path3294_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3294_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3294_ud->strokewidth=24.23800087f;
path3294_body->SetUserData(path3294_ud);

//
//path3292
//
b2BodyDef path3292_bdef;
path3292_bdef.type = b2_staticBody;
path3292_bdef.bullet = false;
path3292_bdef.position.Set(4.43388524977f,19.2872246541f);
path3292_bdef.linearDamping = 0.0f;
path3292_bdef.angularDamping =0.0f;
b2Body* path3292_body = m_world->CreateBody(&path3292_bdef);

b2CircleShape path3292_s;
path3292_s.m_radius=0.318572368514f;

b2FixtureDef path3292_fdef;
path3292_fdef.shape = &path3292_s;

path3292_fdef.density = 1.0f;
path3292_fdef.friction = 0.1f;
path3292_fdef.restitution = 0.8f;
path3292_fdef.filter.groupIndex = -1;

path3292_body->CreateFixture(&path3292_fdef);
bodylist.push_back(path3292_body);
uData* path3292_ud = new uData();
path3292_ud->name = "TITLE";
path3292_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3292_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3292_ud->strokewidth=24.23800087f;
path3292_body->SetUserData(path3292_ud);

//
//path3290
//
b2BodyDef path3290_bdef;
path3290_bdef.type = b2_staticBody;
path3290_bdef.bullet = false;
path3290_bdef.position.Set(5.1467564472f,19.2872246541f);
path3290_bdef.linearDamping = 0.0f;
path3290_bdef.angularDamping =0.0f;
b2Body* path3290_body = m_world->CreateBody(&path3290_bdef);

b2CircleShape path3290_s;
path3290_s.m_radius=0.318572368514f;

b2FixtureDef path3290_fdef;
path3290_fdef.shape = &path3290_s;

path3290_fdef.density = 1.0f;
path3290_fdef.friction = 0.1f;
path3290_fdef.restitution = 0.8f;
path3290_fdef.filter.groupIndex = -1;

path3290_body->CreateFixture(&path3290_fdef);
bodylist.push_back(path3290_body);
uData* path3290_ud = new uData();
path3290_ud->name = "TITLE";
path3290_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3290_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3290_ud->strokewidth=24.23800087f;
path3290_body->SetUserData(path3290_ud);

//
//path3288
//
b2BodyDef path3288_bdef;
path3288_bdef.type = b2_staticBody;
path3288_bdef.bullet = false;
path3288_bdef.position.Set(5.66160858195f,19.5248483476f);
path3288_bdef.linearDamping = 0.0f;
path3288_bdef.angularDamping =0.0f;
b2Body* path3288_body = m_world->CreateBody(&path3288_bdef);

b2CircleShape path3288_s;
path3288_s.m_radius=0.318572368514f;

b2FixtureDef path3288_fdef;
path3288_fdef.shape = &path3288_s;

path3288_fdef.density = 1.0f;
path3288_fdef.friction = 0.1f;
path3288_fdef.restitution = 0.8f;
path3288_fdef.filter.groupIndex = -1;

path3288_body->CreateFixture(&path3288_fdef);
bodylist.push_back(path3288_body);
uData* path3288_ud = new uData();
path3288_ud->name = "TITLE";
path3288_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3288_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3288_ud->strokewidth=24.23800087f;
path3288_body->SetUserData(path3288_ud);

//
//path3286
//
b2BodyDef path3286_bdef;
path3286_bdef.type = b2_staticBody;
path3286_bdef.bullet = false;
path3286_bdef.position.Set(5.82002453349f,20.0396998978f);
path3286_bdef.linearDamping = 0.0f;
path3286_bdef.angularDamping =0.0f;
b2Body* path3286_body = m_world->CreateBody(&path3286_bdef);

b2CircleShape path3286_s;
path3286_s.m_radius=0.318572368514f;

b2FixtureDef path3286_fdef;
path3286_fdef.shape = &path3286_s;

path3286_fdef.density = 1.0f;
path3286_fdef.friction = 0.1f;
path3286_fdef.restitution = 0.8f;
path3286_fdef.filter.groupIndex = -1;

path3286_body->CreateFixture(&path3286_fdef);
bodylist.push_back(path3286_body);
uData* path3286_ud = new uData();
path3286_ud->name = "TITLE";
path3286_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3286_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3286_ud->strokewidth=24.23800087f;
path3286_body->SetUserData(path3286_ud);

//
//path3284
//
b2BodyDef path3284_bdef;
path3284_bdef.type = b2_staticBody;
path3284_bdef.bullet = false;
path3284_bdef.position.Set(5.58240002168f,20.8713830005f);
path3284_bdef.linearDamping = 0.0f;
path3284_bdef.angularDamping =0.0f;
b2Body* path3284_body = m_world->CreateBody(&path3284_bdef);

b2CircleShape path3284_s;
path3284_s.m_radius=0.318572368514f;

b2FixtureDef path3284_fdef;
path3284_fdef.shape = &path3284_s;

path3284_fdef.density = 1.0f;
path3284_fdef.friction = 0.1f;
path3284_fdef.restitution = 0.8f;
path3284_fdef.filter.groupIndex = -1;

path3284_body->CreateFixture(&path3284_fdef);
bodylist.push_back(path3284_body);
uData* path3284_ud = new uData();
path3284_ud->name = "TITLE";
path3284_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3284_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3284_ud->strokewidth=24.23800087f;
path3284_body->SetUserData(path3284_ud);

//
//path3282
//
b2BodyDef path3282_bdef;
path3282_bdef.type = b2_staticBody;
path3282_bdef.bullet = false;
path3282_bdef.position.Set(4.98834049566f,21.4258384802f);
path3282_bdef.linearDamping = 0.0f;
path3282_bdef.angularDamping =0.0f;
b2Body* path3282_body = m_world->CreateBody(&path3282_bdef);

b2CircleShape path3282_s;
path3282_s.m_radius=0.318572368514f;

b2FixtureDef path3282_fdef;
path3282_fdef.shape = &path3282_s;

path3282_fdef.density = 1.0f;
path3282_fdef.friction = 0.1f;
path3282_fdef.restitution = 0.8f;
path3282_fdef.filter.groupIndex = -1;

path3282_body->CreateFixture(&path3282_fdef);
bodylist.push_back(path3282_body);
uData* path3282_ud = new uData();
path3282_ud->name = "TITLE";
path3282_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3282_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3282_ud->strokewidth=24.23800087f;
path3282_body->SetUserData(path3282_ud);

//
//path3280
//
b2BodyDef path3280_bdef;
path3280_bdef.type = b2_staticBody;
path3280_bdef.bullet = false;
path3280_bdef.position.Set(4.43388524977f,21.505046339f);
path3280_bdef.linearDamping = 0.0f;
path3280_bdef.angularDamping =0.0f;
b2Body* path3280_body = m_world->CreateBody(&path3280_bdef);

b2CircleShape path3280_s;
path3280_s.m_radius=0.318572368514f;

b2FixtureDef path3280_fdef;
path3280_fdef.shape = &path3280_s;

path3280_fdef.density = 1.0f;
path3280_fdef.friction = 0.1f;
path3280_fdef.restitution = 0.8f;
path3280_fdef.filter.groupIndex = -1;

path3280_body->CreateFixture(&path3280_fdef);
bodylist.push_back(path3280_body);
uData* path3280_ud = new uData();
path3280_ud->name = "TITLE";
path3280_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3280_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3280_ud->strokewidth=24.23800087f;
path3280_body->SetUserData(path3280_ud);

//
//path3278
//
b2BodyDef path3278_bdef;
path3278_bdef.type = b2_staticBody;
path3278_bdef.bullet = false;
path3278_bdef.position.Set(3.5625981008f,21.505046339f);
path3278_bdef.linearDamping = 0.0f;
path3278_bdef.angularDamping =0.0f;
b2Body* path3278_body = m_world->CreateBody(&path3278_bdef);

b2CircleShape path3278_s;
path3278_s.m_radius=0.318572368514f;

b2FixtureDef path3278_fdef;
path3278_fdef.shape = &path3278_s;

path3278_fdef.density = 1.0f;
path3278_fdef.friction = 0.1f;
path3278_fdef.restitution = 0.8f;
path3278_fdef.filter.groupIndex = -1;

path3278_body->CreateFixture(&path3278_fdef);
bodylist.push_back(path3278_body);
uData* path3278_ud = new uData();
path3278_ud->name = "TITLE";
path3278_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path3278_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path3278_ud->strokewidth=24.23800087f;
path3278_body->SetUserData(path3278_ud);

//
//path4138
//
b2BodyDef path4138_bdef;
path4138_bdef.type = b2_staticBody;
path4138_bdef.bullet = false;
path4138_bdef.position.Set(0.108003448965f,17.7928921209f);
path4138_bdef.linearDamping = 0.0f;
path4138_bdef.angularDamping =0.0f;
b2Body* path4138_body = m_world->CreateBody(&path4138_bdef);

b2CircleShape path4138_s;
path4138_s.m_radius=0.318572368514f;

b2FixtureDef path4138_fdef;
path4138_fdef.shape = &path4138_s;

path4138_fdef.density = 1.0f;
path4138_fdef.friction = 0.1f;
path4138_fdef.restitution = 0.8f;
path4138_fdef.filter.groupIndex = -1;

path4138_body->CreateFixture(&path4138_fdef);
bodylist.push_back(path4138_body);
uData* path4138_ud = new uData();
path4138_ud->name = "TITLE";
path4138_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path4138_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4138_ud->strokewidth=24.23800087f;
path4138_body->SetUserData(path4138_ud);

//
//path3276
//
b2BodyDef path3276_bdef;
path3276_bdef.type = b2_staticBody;
path3276_bdef.bullet = false;
path3276_bdef.position.Set(0.108003448965f,18.4661593889f);
path3276_bdef.linearDamping = 0.0f;
path3276_bdef.angularDamping =0.0f;
b2Body* path3276_body = m_world->CreateBody(&path3276_bdef);

b2CircleShape path3276_s;
path3276_s.m_radius=0.318572368514f;

b2FixtureDef path3276_fdef;
path3276_fdef.shape = &path3276_s;

path3276_fdef.density = 1.0f;
path3276_fdef.friction = 0.1f;
path3276_fdef.restitution = 0.8f;
path3276_fdef.filter.groupIndex = -1;

path3276_body->CreateFixture(&path3276_fdef);
bodylist.push_back(path3276_body);
uData* path3276_ud = new uData();
path3276_ud->name = "TITLE";
path3276_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3276_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3276_ud->strokewidth=24.23800087f;
path3276_body->SetUserData(path3276_ud);

//
//path3274
//
b2BodyDef path3274_bdef;
path3274_bdef.type = b2_staticBody;
path3274_bdef.bullet = false;
path3274_bdef.position.Set(0.147606560105f,19.0998227275f);
path3274_bdef.linearDamping = 0.0f;
path3274_bdef.angularDamping =0.0f;
b2Body* path3274_body = m_world->CreateBody(&path3274_bdef);

b2CircleShape path3274_s;
path3274_s.m_radius=0.318572368514f;

b2FixtureDef path3274_fdef;
path3274_fdef.shape = &path3274_s;

path3274_fdef.density = 1.0f;
path3274_fdef.friction = 0.1f;
path3274_fdef.restitution = 0.8f;
path3274_fdef.filter.groupIndex = -1;

path3274_body->CreateFixture(&path3274_fdef);
bodylist.push_back(path3274_body);
uData* path3274_ud = new uData();
path3274_ud->name = "TITLE";
path3274_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3274_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3274_ud->strokewidth=24.23800087f;
path3274_body->SetUserData(path3274_ud);

//
//path3272
//
b2BodyDef path3272_bdef;
path3272_bdef.type = b2_staticBody;
path3272_bdef.bullet = false;
path3272_bdef.position.Set(0.147606560105f,19.7730901124f);
path3272_bdef.linearDamping = 0.0f;
path3272_bdef.angularDamping =0.0f;
b2Body* path3272_body = m_world->CreateBody(&path3272_bdef);

b2CircleShape path3272_s;
path3272_s.m_radius=0.318572368514f;

b2FixtureDef path3272_fdef;
path3272_fdef.shape = &path3272_s;

path3272_fdef.density = 1.0f;
path3272_fdef.friction = 0.1f;
path3272_fdef.restitution = 0.8f;
path3272_fdef.filter.groupIndex = -1;

path3272_body->CreateFixture(&path3272_fdef);
bodylist.push_back(path3272_body);
uData* path3272_ud = new uData();
path3272_ud->name = "TITLE";
path3272_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3272_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3272_ud->strokewidth=24.23800087f;
path3272_body->SetUserData(path3272_ud);

//
//path3270
//
b2BodyDef path3270_bdef;
path3270_bdef.type = b2_staticBody;
path3270_bdef.bullet = false;
path3270_bdef.position.Set(0.187210840238f,20.6443771444f);
path3270_bdef.linearDamping = 0.0f;
path3270_bdef.angularDamping =0.0f;
b2Body* path3270_body = m_world->CreateBody(&path3270_bdef);

b2CircleShape path3270_s;
path3270_s.m_radius=0.318572368514f;

b2FixtureDef path3270_fdef;
path3270_fdef.shape = &path3270_s;

path3270_fdef.density = 1.0f;
path3270_fdef.friction = 0.1f;
path3270_fdef.restitution = 0.8f;
path3270_fdef.filter.groupIndex = -1;

path3270_body->CreateFixture(&path3270_fdef);
bodylist.push_back(path3270_body);
uData* path3270_ud = new uData();
path3270_ud->name = "TITLE";
path3270_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3270_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3270_ud->strokewidth=24.23800087f;
path3270_body->SetUserData(path3270_ud);

//
//path3268
//
b2BodyDef path3268_bdef;
path3268_bdef.type = b2_staticBody;
path3268_bdef.bullet = false;
path3268_bdef.position.Set(0.9396863178f,19.3770504673f);
path3268_bdef.linearDamping = 0.0f;
path3268_bdef.angularDamping =0.0f;
b2Body* path3268_body = m_world->CreateBody(&path3268_bdef);

b2CircleShape path3268_s;
path3268_s.m_radius=0.318572368514f;

b2FixtureDef path3268_fdef;
path3268_fdef.shape = &path3268_s;

path3268_fdef.density = 1.0f;
path3268_fdef.friction = 0.1f;
path3268_fdef.restitution = 0.8f;
path3268_fdef.filter.groupIndex = -1;

path3268_body->CreateFixture(&path3268_fdef);
bodylist.push_back(path3268_body);
uData* path3268_ud = new uData();
path3268_ud->name = "TITLE";
path3268_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3268_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3268_ud->strokewidth=24.23800087f;
path3268_body->SetUserData(path3268_ud);

//
//path3266
//
b2BodyDef path3266_bdef;
path3266_bdef.type = b2_staticBody;
path3266_bdef.bullet = false;
path3266_bdef.position.Set(1.49414156369f,19.3770504673f);
path3266_bdef.linearDamping = 0.0f;
path3266_bdef.angularDamping =0.0f;
b2Body* path3266_body = m_world->CreateBody(&path3266_bdef);

b2CircleShape path3266_s;
path3266_s.m_radius=0.318572368514f;

b2FixtureDef path3266_fdef;
path3266_fdef.shape = &path3266_s;

path3266_fdef.density = 1.0f;
path3266_fdef.friction = 0.1f;
path3266_fdef.restitution = 0.8f;
path3266_fdef.filter.groupIndex = -1;

path3266_body->CreateFixture(&path3266_fdef);
bodylist.push_back(path3266_body);
uData* path3266_ud = new uData();
path3266_ud->name = "TITLE";
path3266_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3266_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3266_ud->strokewidth=24.23800087f;
path3266_body->SetUserData(path3266_ud);

//
//path3264
//
b2BodyDef path3264_bdef;
path3264_bdef.type = b2_staticBody;
path3264_bdef.bullet = false;
path3264_bdef.position.Set(2.16740848099f,19.535466302f);
path3264_bdef.linearDamping = 0.0f;
path3264_bdef.angularDamping =0.0f;
b2Body* path3264_body = m_world->CreateBody(&path3264_bdef);

b2CircleShape path3264_s;
path3264_s.m_radius=0.318572368514f;

b2FixtureDef path3264_fdef;
path3264_fdef.shape = &path3264_s;

path3264_fdef.density = 1.0f;
path3264_fdef.friction = 0.1f;
path3264_fdef.restitution = 0.8f;
path3264_fdef.filter.groupIndex = -1;

path3264_body->CreateFixture(&path3264_fdef);
bodylist.push_back(path3264_body);
uData* path3264_ud = new uData();
path3264_ud->name = "TITLE";
path3264_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3264_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3264_ud->strokewidth=24.23800087f;
path3264_body->SetUserData(path3264_ud);

//
//path3262
//
b2BodyDef path3262_bdef;
path3262_bdef.type = b2_staticBody;
path3262_bdef.bullet = false;
path3262_bdef.position.Set(2.28622132138f,20.2483376163f);
path3262_bdef.linearDamping = 0.0f;
path3262_bdef.angularDamping =0.0f;
b2Body* path3262_body = m_world->CreateBody(&path3262_bdef);

b2CircleShape path3262_s;
path3262_s.m_radius=0.318572368514f;

b2FixtureDef path3262_fdef;
path3262_fdef.shape = &path3262_s;

path3262_fdef.density = 1.0f;
path3262_fdef.friction = 0.1f;
path3262_fdef.restitution = 0.8f;
path3262_fdef.filter.groupIndex = -1;

path3262_body->CreateFixture(&path3262_fdef);
bodylist.push_back(path3262_body);
uData* path3262_ud = new uData();
path3262_ud->name = "TITLE";
path3262_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3262_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3262_ud->strokewidth=24.23800087f;
path3262_body->SetUserData(path3262_ud);

//
//path3260
//
b2BodyDef path3260_bdef;
path3260_bdef.type = b2_staticBody;
path3260_bdef.bullet = false;
path3260_bdef.position.Set(2.20701276112f,20.8820009548f);
path3260_bdef.linearDamping = 0.0f;
path3260_bdef.angularDamping =0.0f;
b2Body* path3260_body = m_world->CreateBody(&path3260_bdef);

b2CircleShape path3260_s;
path3260_s.m_radius=0.318572368514f;

b2FixtureDef path3260_fdef;
path3260_fdef.shape = &path3260_s;

path3260_fdef.density = 1.0f;
path3260_fdef.friction = 0.1f;
path3260_fdef.restitution = 0.8f;
path3260_fdef.filter.groupIndex = -1;

path3260_body->CreateFixture(&path3260_fdef);
bodylist.push_back(path3260_body);
uData* path3260_ud = new uData();
path3260_ud->name = "TITLE";
path3260_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3260_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3260_ud->strokewidth=24.23800087f;
path3260_body->SetUserData(path3260_ud);

//
//path3258
//
b2BodyDef path3258_bdef;
path3258_bdef.type = b2_staticBody;
path3258_bdef.bullet = false;
path3258_bdef.position.Set(1.7317649065f,21.3572484588f);
path3258_bdef.linearDamping = 0.0f;
path3258_bdef.angularDamping =0.0f;
b2Body* path3258_body = m_world->CreateBody(&path3258_bdef);

b2CircleShape path3258_s;
path3258_s.m_radius=0.318572368514f;

b2FixtureDef path3258_fdef;
path3258_fdef.shape = &path3258_s;

path3258_fdef.density = 1.0f;
path3258_fdef.friction = 0.1f;
path3258_fdef.restitution = 0.8f;
path3258_fdef.filter.groupIndex = -1;

path3258_body->CreateFixture(&path3258_fdef);
bodylist.push_back(path3258_body);
uData* path3258_ud = new uData();
path3258_ud->name = "TITLE";
path3258_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3258_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3258_ud->strokewidth=24.23800087f;
path3258_body->SetUserData(path3258_ud);

//
//path3256
//
b2BodyDef path3256_bdef;
path3256_bdef.type = b2_staticBody;
path3256_bdef.bullet = false;
path3256_bdef.position.Set(0.900082037667f,21.476060364f);
path3256_bdef.linearDamping = 0.0f;
path3256_bdef.angularDamping =0.0f;
b2Body* path3256_body = m_world->CreateBody(&path3256_bdef);

b2CircleShape path3256_s;
path3256_s.m_radius=0.318572368514f;

b2FixtureDef path3256_fdef;
path3256_fdef.shape = &path3256_s;

path3256_fdef.density = 1.0f;
path3256_fdef.friction = 0.1f;
path3256_fdef.restitution = 0.8f;
path3256_fdef.filter.groupIndex = -1;

path3256_body->CreateFixture(&path3256_fdef);
bodylist.push_back(path3256_body);
uData* path3256_ud = new uData();
path3256_ud->name = "TITLE";
path3256_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3256_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3256_ud->strokewidth=24.23800087f;
path3256_body->SetUserData(path3256_ud);

//
//path3254
//
b2BodyDef path3254_bdef;
path3254_bdef.type = b2_staticBody;
path3254_bdef.bullet = false;
path3254_bdef.position.Set(0.108003448965f,21.5156642934f);
path3254_bdef.linearDamping = 0.0f;
path3254_bdef.angularDamping =0.0f;
b2Body* path3254_body = m_world->CreateBody(&path3254_bdef);

b2CircleShape path3254_s;
path3254_s.m_radius=0.318572368514f;

b2FixtureDef path3254_fdef;
path3254_fdef.shape = &path3254_s;

path3254_fdef.density = 1.0f;
path3254_fdef.friction = 0.1f;
path3254_fdef.restitution = 0.8f;
path3254_fdef.filter.groupIndex = -1;

path3254_body->CreateFixture(&path3254_fdef);
bodylist.push_back(path3254_body);
uData* path3254_ud = new uData();
path3254_ud->name = "TITLE";
path3254_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path3254_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3254_ud->strokewidth=24.23800087f;
path3254_body->SetUserData(path3254_ud);

//
//path3252
//
b2BodyDef path3252_bdef;
path3252_bdef.type = b2_staticBody;
path3252_bdef.bullet = false;
path3252_bdef.position.Set(-1.20223435287f,17.7088101499f);
path3252_bdef.linearDamping = 0.0f;
path3252_bdef.angularDamping =0.0f;
b2Body* path3252_body = m_world->CreateBody(&path3252_bdef);

b2CircleShape path3252_s;
path3252_s.m_radius=0.318572368514f;

b2FixtureDef path3252_fdef;
path3252_fdef.shape = &path3252_s;

path3252_fdef.density = 1.0f;
path3252_fdef.friction = 0.1f;
path3252_fdef.restitution = 0.8f;
path3252_fdef.filter.groupIndex = -1;

path3252_body->CreateFixture(&path3252_fdef);
bodylist.push_back(path3252_body);
uData* path3252_ud = new uData();
path3252_ud->name = "TITLE";
path3252_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3252_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3252_ud->strokewidth=24.23800087f;
path3252_body->SetUserData(path3252_ud);

//
//path3250
//
b2BodyDef path3250_bdef;
path3250_bdef.type = b2_staticBody;
path3250_bdef.bullet = false;
path3250_bdef.position.Set(-1.43985769568f,18.2236615832f);
path3250_bdef.linearDamping = 0.0f;
path3250_bdef.angularDamping =0.0f;
b2Body* path3250_body = m_world->CreateBody(&path3250_bdef);

b2CircleShape path3250_s;
path3250_s.m_radius=0.318572368514f;

b2FixtureDef path3250_fdef;
path3250_fdef.shape = &path3250_s;

path3250_fdef.density = 1.0f;
path3250_fdef.friction = 0.1f;
path3250_fdef.restitution = 0.8f;
path3250_fdef.filter.groupIndex = -1;

path3250_body->CreateFixture(&path3250_fdef);
bodylist.push_back(path3250_body);
uData* path3250_ud = new uData();
path3250_ud->name = "TITLE";
path3250_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3250_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3250_ud->strokewidth=24.23800087f;
path3250_body->SetUserData(path3250_ud);

//
//path3248
//
b2BodyDef path3248_bdef;
path3248_bdef.type = b2_staticBody;
path3248_bdef.bullet = false;
path3248_bdef.position.Set(-1.67748220749f,18.8573249218f);
path3248_bdef.linearDamping = 0.0f;
path3248_bdef.angularDamping =0.0f;
b2Body* path3248_body = m_world->CreateBody(&path3248_bdef);

b2CircleShape path3248_s;
path3248_s.m_radius=0.318572368514f;

b2FixtureDef path3248_fdef;
path3248_fdef.shape = &path3248_s;

path3248_fdef.density = 1.0f;
path3248_fdef.friction = 0.1f;
path3248_fdef.restitution = 0.8f;
path3248_fdef.filter.groupIndex = -1;

path3248_body->CreateFixture(&path3248_fdef);
bodylist.push_back(path3248_body);
uData* path3248_ud = new uData();
path3248_ud->name = "TITLE";
path3248_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3248_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3248_ud->strokewidth=24.23800087f;
path3248_body->SetUserData(path3248_ud);

//
//path3246
//
b2BodyDef path3246_bdef;
path3246_bdef.type = b2_staticBody;
path3246_bdef.bullet = false;
path3246_bdef.position.Set(-1.83589734073f,19.4909883773f);
path3246_bdef.linearDamping = 0.0f;
path3246_bdef.angularDamping =0.0f;
b2Body* path3246_body = m_world->CreateBody(&path3246_bdef);

b2CircleShape path3246_s;
path3246_s.m_radius=0.318572368514f;

b2FixtureDef path3246_fdef;
path3246_fdef.shape = &path3246_s;

path3246_fdef.density = 1.0f;
path3246_fdef.friction = 0.1f;
path3246_fdef.restitution = 0.8f;
path3246_fdef.filter.groupIndex = -1;

path3246_body->CreateFixture(&path3246_fdef);
bodylist.push_back(path3246_body);
uData* path3246_ud = new uData();
path3246_ud->name = "TITLE";
path3246_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3246_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3246_ud->strokewidth=24.23800087f;
path3246_body->SetUserData(path3246_ud);

//
//path3244
//
b2BodyDef path3244_bdef;
path3244_bdef.type = b2_staticBody;
path3244_bdef.bullet = false;
path3244_bdef.position.Set(-2.11312508057f,20.2830675505f);
path3244_bdef.linearDamping = 0.0f;
path3244_bdef.angularDamping =0.0f;
b2Body* path3244_body = m_world->CreateBody(&path3244_bdef);

b2CircleShape path3244_s;
path3244_s.m_radius=0.318572368514f;

b2FixtureDef path3244_fdef;
path3244_fdef.shape = &path3244_s;

path3244_fdef.density = 1.0f;
path3244_fdef.friction = 0.1f;
path3244_fdef.restitution = 0.8f;
path3244_fdef.filter.groupIndex = -1;

path3244_body->CreateFixture(&path3244_fdef);
bodylist.push_back(path3244_body);
uData* path3244_ud = new uData();
path3244_ud->name = "TITLE";
path3244_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3244_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3244_ud->strokewidth=24.23800087f;
path3244_body->SetUserData(path3244_ud);

//
//path3242
//
b2BodyDef path3242_bdef;
path3242_bdef.type = b2_staticBody;
path3242_bdef.bullet = false;
path3242_bdef.position.Set(-2.27154091521f,20.916730889f);
path3242_bdef.linearDamping = 0.0f;
path3242_bdef.angularDamping =0.0f;
b2Body* path3242_body = m_world->CreateBody(&path3242_bdef);

b2CircleShape path3242_s;
path3242_s.m_radius=0.318572368514f;

b2FixtureDef path3242_fdef;
path3242_fdef.shape = &path3242_s;

path3242_fdef.density = 1.0f;
path3242_fdef.friction = 0.1f;
path3242_fdef.restitution = 0.8f;
path3242_fdef.filter.groupIndex = -1;

path3242_body->CreateFixture(&path3242_fdef);
bodylist.push_back(path3242_body);
uData* path3242_ud = new uData();
path3242_ud->name = "TITLE";
path3242_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3242_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3242_ud->strokewidth=24.23800087f;
path3242_body->SetUserData(path3242_ud);

//
//path3240
//
b2BodyDef path3240_bdef;
path3240_bdef.type = b2_staticBody;
path3240_bdef.bullet = false;
path3240_bdef.position.Set(-2.50916472562f,21.5107902981f);
path3240_bdef.linearDamping = 0.0f;
path3240_bdef.angularDamping =0.0f;
b2Body* path3240_body = m_world->CreateBody(&path3240_bdef);

b2CircleShape path3240_s;
path3240_s.m_radius=0.318572368514f;

b2FixtureDef path3240_fdef;
path3240_fdef.shape = &path3240_s;

path3240_fdef.density = 1.0f;
path3240_fdef.friction = 0.1f;
path3240_fdef.restitution = 0.8f;
path3240_fdef.filter.groupIndex = -1;

path3240_body->CreateFixture(&path3240_fdef);
bodylist.push_back(path3240_body);
uData* path3240_ud = new uData();
path3240_ud->name = "TITLE";
path3240_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3240_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3240_ud->strokewidth=24.23800087f;
path3240_body->SetUserData(path3240_ud);

//
//path3238
//
b2BodyDef path3238_bdef;
path3238_bdef.type = b2_staticBody;
path3238_bdef.bullet = false;
path3238_bdef.position.Set(-2.27154091521f,18.2236615832f);
path3238_bdef.linearDamping = 0.0f;
path3238_bdef.angularDamping =0.0f;
b2Body* path3238_body = m_world->CreateBody(&path3238_bdef);

b2CircleShape path3238_s;
path3238_s.m_radius=0.318572368514f;

b2FixtureDef path3238_fdef;
path3238_fdef.shape = &path3238_s;

path3238_fdef.density = 1.0f;
path3238_fdef.friction = 0.1f;
path3238_fdef.restitution = 0.8f;
path3238_fdef.filter.groupIndex = -1;

path3238_body->CreateFixture(&path3238_fdef);
bodylist.push_back(path3238_body);
uData* path3238_ud = new uData();
path3238_ud->name = "TITLE";
path3238_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3238_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3238_ud->strokewidth=24.23800087f;
path3238_body->SetUserData(path3238_ud);

//
//path3236
//
b2BodyDef path3236_bdef;
path3236_bdef.type = b2_staticBody;
path3236_bdef.bullet = false;
path3236_bdef.position.Set(-2.90520425377f,18.2632655127f);
path3236_bdef.linearDamping = 0.0f;
path3236_bdef.angularDamping =0.0f;
b2Body* path3236_body = m_world->CreateBody(&path3236_bdef);

b2CircleShape path3236_s;
path3236_s.m_radius=0.318572368514f;

b2FixtureDef path3236_fdef;
path3236_fdef.shape = &path3236_s;

path3236_fdef.density = 1.0f;
path3236_fdef.friction = 0.1f;
path3236_fdef.restitution = 0.8f;
path3236_fdef.filter.groupIndex = -1;

path3236_body->CreateFixture(&path3236_fdef);
bodylist.push_back(path3236_body);
uData* path3236_ud = new uData();
path3236_ud->name = "TITLE";
path3236_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3236_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3236_ud->strokewidth=24.23800087f;
path3236_body->SetUserData(path3236_ud);

//
//path3234
//
b2BodyDef path3234_bdef;
path3234_bdef.type = b2_staticBody;
path3234_bdef.bullet = false;
path3234_bdef.position.Set(-3.69728354387f,18.2236615832f);
path3234_bdef.linearDamping = 0.0f;
path3234_bdef.angularDamping =0.0f;
b2Body* path3234_body = m_world->CreateBody(&path3234_bdef);

b2CircleShape path3234_s;
path3234_s.m_radius=0.318572368514f;

b2FixtureDef path3234_fdef;
path3234_fdef.shape = &path3234_s;

path3234_fdef.density = 1.0f;
path3234_fdef.friction = 0.1f;
path3234_fdef.restitution = 0.8f;
path3234_fdef.filter.groupIndex = -1;

path3234_body->CreateFixture(&path3234_fdef);
bodylist.push_back(path3234_body);
uData* path3234_ud = new uData();
path3234_ud->name = "TITLE";
path3234_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3234_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3234_ud->strokewidth=24.23800087f;
path3234_body->SetUserData(path3234_ud);

//
//path3232
//
b2BodyDef path3232_bdef;
path3232_bdef.type = b2_staticBody;
path3232_bdef.bullet = false;
path3232_bdef.position.Set(-4.64777855171f,17.7484140793f);
path3232_bdef.linearDamping = 0.0f;
path3232_bdef.angularDamping =0.0f;
b2Body* path3232_body = m_world->CreateBody(&path3232_bdef);

b2CircleShape path3232_s;
path3232_s.m_radius=0.318572368514f;

b2FixtureDef path3232_fdef;
path3232_fdef.shape = &path3232_s;

path3232_fdef.density = 1.0f;
path3232_fdef.friction = 0.1f;
path3232_fdef.restitution = 0.8f;
path3232_fdef.filter.groupIndex = -1;

path3232_body->CreateFixture(&path3232_fdef);
bodylist.push_back(path3232_body);
uData* path3232_ud = new uData();
path3232_ud->name = "TITLE";
path3232_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3232_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3232_ud->strokewidth=24.23800087f;
path3232_body->SetUserData(path3232_ud);

//
//path3230
//
b2BodyDef path3230_bdef;
path3230_bdef.type = b2_staticBody;
path3230_bdef.bullet = false;
path3230_bdef.position.Set(-4.48936271707f,18.4612853937f);
path3230_bdef.linearDamping = 0.0f;
path3230_bdef.angularDamping =0.0f;
b2Body* path3230_body = m_world->CreateBody(&path3230_bdef);

b2CircleShape path3230_s;
path3230_s.m_radius=0.318572368514f;

b2FixtureDef path3230_fdef;
path3230_fdef.shape = &path3230_s;

path3230_fdef.density = 1.0f;
path3230_fdef.friction = 0.1f;
path3230_fdef.restitution = 0.8f;
path3230_fdef.filter.groupIndex = -1;

path3230_body->CreateFixture(&path3230_fdef);
bodylist.push_back(path3230_body);
uData* path3230_ud = new uData();
path3230_ud->name = "TITLE";
path3230_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3230_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3230_ud->strokewidth=24.23800087f;
path3230_body->SetUserData(path3230_ud);

//
//path3228
//
b2BodyDef path3228_bdef;
path3228_bdef.type = b2_staticBody;
path3228_bdef.bullet = false;
path3228_bdef.position.Set(-4.21213497722f,19.0157407564f);
path3228_bdef.linearDamping = 0.0f;
path3228_bdef.angularDamping =0.0f;
b2Body* path3228_body = m_world->CreateBody(&path3228_bdef);

b2CircleShape path3228_s;
path3228_s.m_radius=0.318572368514f;

b2FixtureDef path3228_fdef;
path3228_fdef.shape = &path3228_s;

path3228_fdef.density = 1.0f;
path3228_fdef.friction = 0.1f;
path3228_fdef.restitution = 0.8f;
path3228_fdef.filter.groupIndex = -1;

path3228_body->CreateFixture(&path3228_fdef);
bodylist.push_back(path3228_body);
uData* path3228_ud = new uData();
path3228_ud->name = "TITLE";
path3228_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3228_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3228_ud->strokewidth=24.23800087f;
path3228_body->SetUserData(path3228_ud);

//
//path3226
//
b2BodyDef path3226_bdef;
path3226_bdef.type = b2_staticBody;
path3226_bdef.bullet = false;
path3226_bdef.position.Set(-4.01411521315f,19.5701962361f);
path3226_bdef.linearDamping = 0.0f;
path3226_bdef.angularDamping =0.0f;
b2Body* path3226_body = m_world->CreateBody(&path3226_bdef);

b2CircleShape path3226_s;
path3226_s.m_radius=0.318572368514f;

b2FixtureDef path3226_fdef;
path3226_fdef.shape = &path3226_s;

path3226_fdef.density = 1.0f;
path3226_fdef.friction = 0.1f;
path3226_fdef.restitution = 0.8f;
path3226_fdef.filter.groupIndex = -1;

path3226_body->CreateFixture(&path3226_fdef);
bodylist.push_back(path3226_body);
uData* path3226_ud = new uData();
path3226_ud->name = "TITLE";
path3226_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3226_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3226_ud->strokewidth=24.23800087f;
path3226_body->SetUserData(path3226_ud);

//
//path3224
//
b2BodyDef path3224_bdef;
path3224_bdef.type = b2_staticBody;
path3224_bdef.bullet = false;
path3224_bdef.position.Set(-3.7368874733f,20.2434635041f);
path3224_bdef.linearDamping = 0.0f;
path3224_bdef.angularDamping =0.0f;
b2Body* path3224_body = m_world->CreateBody(&path3224_bdef);

b2CircleShape path3224_s;
path3224_s.m_radius=0.318572368514f;

b2FixtureDef path3224_fdef;
path3224_fdef.shape = &path3224_s;

path3224_fdef.density = 1.0f;
path3224_fdef.friction = 0.1f;
path3224_fdef.restitution = 0.8f;
path3224_fdef.filter.groupIndex = -1;

path3224_body->CreateFixture(&path3224_fdef);
bodylist.push_back(path3224_body);
uData* path3224_ud = new uData();
path3224_ud->name = "TITLE";
path3224_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3224_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3224_ud->strokewidth=24.23800087f;
path3224_body->SetUserData(path3224_ud);

//
//path3222
//
b2BodyDef path3222_bdef;
path3222_bdef.type = b2_staticBody;
path3222_bdef.bullet = false;
path3222_bdef.position.Set(-3.53886759233f,20.8771269596f);
path3222_bdef.linearDamping = 0.0f;
path3222_bdef.angularDamping =0.0f;
b2Body* path3222_body = m_world->CreateBody(&path3222_bdef);

b2CircleShape path3222_s;
path3222_s.m_radius=0.318572368514f;

b2FixtureDef path3222_fdef;
path3222_fdef.shape = &path3222_s;

path3222_fdef.density = 1.0f;
path3222_fdef.friction = 0.1f;
path3222_fdef.restitution = 0.8f;
path3222_fdef.filter.groupIndex = -1;

path3222_body->CreateFixture(&path3222_fdef);
bodylist.push_back(path3222_body);
uData* path3222_ud = new uData();
path3222_ud->name = "TITLE";
path3222_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3222_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3222_ud->strokewidth=24.23800087f;
path3222_body->SetUserData(path3222_ud);

//
//path3220
//
b2BodyDef path3220_bdef;
path3220_bdef.type = b2_staticBody;
path3220_bdef.bullet = false;
path3220_bdef.position.Set(-3.34084782826f,21.5107902981f);
path3220_bdef.linearDamping = 0.0f;
path3220_bdef.angularDamping =0.0f;
b2Body* path3220_body = m_world->CreateBody(&path3220_bdef);

b2CircleShape path3220_s;
path3220_s.m_radius=0.318572368514f;

b2FixtureDef path3220_fdef;
path3220_fdef.shape = &path3220_s;

path3220_fdef.density = 1.0f;
path3220_fdef.friction = 0.1f;
path3220_fdef.restitution = 0.8f;
path3220_fdef.filter.groupIndex = -1;

path3220_body->CreateFixture(&path3220_fdef);
bodylist.push_back(path3220_body);
uData* path3220_ud = new uData();
path3220_ud->name = "TITLE";
path3220_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path3220_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path3220_ud->strokewidth=24.23800087f;
path3220_body->SetUserData(path3220_ud);

//
//path4078
//
b2BodyDef path4078_bdef;
path4078_bdef.type = b2_staticBody;
path4078_bdef.bullet = false;
path4078_bdef.position.Set(9.12700355092f,14.8744737715f);
path4078_bdef.linearDamping = 0.0f;
path4078_bdef.angularDamping =0.0f;
b2Body* path4078_body = m_world->CreateBody(&path4078_bdef);

b2CircleShape path4078_s;
path4078_s.m_radius=0.318572368514f;

b2FixtureDef path4078_fdef;
path4078_fdef.shape = &path4078_s;

path4078_fdef.density = 1.0f;
path4078_fdef.friction = 0.1f;
path4078_fdef.restitution = 0.8f;
path4078_fdef.filter.groupIndex = -1;

path4078_body->CreateFixture(&path4078_fdef);
bodylist.push_back(path4078_body);
uData* path4078_ud = new uData();
path4078_ud->name = "TITLE";
path4078_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4078_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4078_ud->strokewidth=24.23800087f;
path4078_body->SetUserData(path4078_ud);

//
//path3494
//
b2BodyDef path3494_bdef;
path3494_bdef.type = b2_staticBody;
path3494_bdef.bullet = false;
path3494_bdef.position.Set(9.12700355092f,14.2408104329f);
path3494_bdef.linearDamping = 0.0f;
path3494_bdef.angularDamping =0.0f;
b2Body* path3494_body = m_world->CreateBody(&path3494_bdef);

b2CircleShape path3494_s;
path3494_s.m_radius=0.318572368514f;

b2FixtureDef path3494_fdef;
path3494_fdef.shape = &path3494_s;

path3494_fdef.density = 1.0f;
path3494_fdef.friction = 0.1f;
path3494_fdef.restitution = 0.8f;
path3494_fdef.filter.groupIndex = -1;

path3494_body->CreateFixture(&path3494_fdef);
bodylist.push_back(path3494_body);
uData* path3494_ud = new uData();
path3494_ud->name = "TITLE";
path3494_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3494_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3494_ud->strokewidth=24.23800087f;
path3494_body->SetUserData(path3494_ud);

//
//path3492
//
b2BodyDef path3492_bdef;
path3492_bdef.type = b2_staticBody;
path3492_bdef.bullet = false;
path3492_bdef.position.Set(9.12700355092f,13.6071470944f);
path3492_bdef.linearDamping = 0.0f;
path3492_bdef.angularDamping =0.0f;
b2Body* path3492_body = m_world->CreateBody(&path3492_bdef);

b2CircleShape path3492_s;
path3492_s.m_radius=0.318572368514f;

b2FixtureDef path3492_fdef;
path3492_fdef.shape = &path3492_s;

path3492_fdef.density = 1.0f;
path3492_fdef.friction = 0.1f;
path3492_fdef.restitution = 0.8f;
path3492_fdef.filter.groupIndex = -1;

path3492_body->CreateFixture(&path3492_fdef);
bodylist.push_back(path3492_body);
uData* path3492_ud = new uData();
path3492_ud->name = "TITLE";
path3492_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3492_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3492_ud->strokewidth=24.23800087f;
path3492_body->SetUserData(path3492_ud);

//
//path3490
//
b2BodyDef path3490_bdef;
path3490_bdef.type = b2_staticBody;
path3490_bdef.bullet = false;
path3490_bdef.position.Set(9.12700355092f,12.8546718506f);
path3490_bdef.linearDamping = 0.0f;
path3490_bdef.angularDamping =0.0f;
b2Body* path3490_body = m_world->CreateBody(&path3490_bdef);

b2CircleShape path3490_s;
path3490_s.m_radius=0.318572368514f;

b2FixtureDef path3490_fdef;
path3490_fdef.shape = &path3490_s;

path3490_fdef.density = 1.0f;
path3490_fdef.friction = 0.1f;
path3490_fdef.restitution = 0.8f;
path3490_fdef.filter.groupIndex = -1;

path3490_body->CreateFixture(&path3490_fdef);
bodylist.push_back(path3490_body);
uData* path3490_ud = new uData();
path3490_ud->name = "TITLE";
path3490_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3490_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3490_ud->strokewidth=24.23800087f;
path3490_body->SetUserData(path3490_ud);

//
//path3488
//
b2BodyDef path3488_bdef;
path3488_bdef.type = b2_staticBody;
path3488_bdef.bullet = false;
path3488_bdef.position.Set(9.12700355092f,12.1418005363f);
path3488_bdef.linearDamping = 0.0f;
path3488_bdef.angularDamping =0.0f;
b2Body* path3488_body = m_world->CreateBody(&path3488_bdef);

b2CircleShape path3488_s;
path3488_s.m_radius=0.318572368514f;

b2FixtureDef path3488_fdef;
path3488_fdef.shape = &path3488_s;

path3488_fdef.density = 1.0f;
path3488_fdef.friction = 0.1f;
path3488_fdef.restitution = 0.8f;
path3488_fdef.filter.groupIndex = -1;

path3488_body->CreateFixture(&path3488_fdef);
bodylist.push_back(path3488_body);
uData* path3488_ud = new uData();
path3488_ud->name = "TITLE";
path3488_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3488_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3488_ud->strokewidth=24.23800087f;
path3488_body->SetUserData(path3488_ud);

//
//path3486
//
b2BodyDef path3486_bdef;
path3486_bdef.type = b2_staticBody;
path3486_bdef.bullet = false;
path3486_bdef.position.Set(9.12700355092f,11.5081371977f);
path3486_bdef.linearDamping = 0.0f;
path3486_bdef.angularDamping =0.0f;
b2Body* path3486_body = m_world->CreateBody(&path3486_bdef);

b2CircleShape path3486_s;
path3486_s.m_radius=0.318572368514f;

b2FixtureDef path3486_fdef;
path3486_fdef.shape = &path3486_s;

path3486_fdef.density = 1.0f;
path3486_fdef.friction = 0.1f;
path3486_fdef.restitution = 0.8f;
path3486_fdef.filter.groupIndex = -1;

path3486_body->CreateFixture(&path3486_fdef);
bodylist.push_back(path3486_body);
uData* path3486_ud = new uData();
path3486_ud->name = "TITLE";
path3486_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3486_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3486_ud->strokewidth=24.23800087f;
path3486_body->SetUserData(path3486_ud);

//
//path3484
//
b2BodyDef path3484_bdef;
path3484_bdef.type = b2_staticBody;
path3484_bdef.bullet = false;
path3484_bdef.position.Set(8.5329440249f,11.4685331514f);
path3484_bdef.linearDamping = 0.0f;
path3484_bdef.angularDamping =0.0f;
b2Body* path3484_body = m_world->CreateBody(&path3484_bdef);

b2CircleShape path3484_s;
path3484_s.m_radius=0.318572368514f;

b2FixtureDef path3484_fdef;
path3484_fdef.shape = &path3484_s;

path3484_fdef.density = 1.0f;
path3484_fdef.friction = 0.1f;
path3484_fdef.restitution = 0.8f;
path3484_fdef.filter.groupIndex = -1;

path3484_body->CreateFixture(&path3484_fdef);
bodylist.push_back(path3484_body);
uData* path3484_ud = new uData();
path3484_ud->name = "TITLE";
path3484_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3484_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3484_ud->strokewidth=24.23800087f;
path3484_body->SetUserData(path3484_ud);

//
//path3482
//
b2BodyDef path3482_bdef;
path3482_bdef.type = b2_staticBody;
path3482_bdef.bullet = false;
path3482_bdef.position.Set(8.09730045042f,12.2606124415f);
path3482_bdef.linearDamping = 0.0f;
path3482_bdef.angularDamping =0.0f;
b2Body* path3482_body = m_world->CreateBody(&path3482_bdef);

b2CircleShape path3482_s;
path3482_s.m_radius=0.318572368514f;

b2FixtureDef path3482_fdef;
path3482_fdef.shape = &path3482_s;

path3482_fdef.density = 1.0f;
path3482_fdef.friction = 0.1f;
path3482_fdef.restitution = 0.8f;
path3482_fdef.filter.groupIndex = -1;

path3482_body->CreateFixture(&path3482_fdef);
bodylist.push_back(path3482_body);
uData* path3482_ud = new uData();
path3482_ud->name = "TITLE";
path3482_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3482_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3482_ud->strokewidth=24.23800087f;
path3482_body->SetUserData(path3482_ud);

//
//path3480
//
b2BodyDef path3480_bdef;
path3480_bdef.type = b2_staticBody;
path3480_bdef.bullet = false;
path3480_bdef.position.Set(7.46363664426f,13.0130876853f);
path3480_bdef.linearDamping = 0.0f;
path3480_bdef.angularDamping =0.0f;
b2Body* path3480_body = m_world->CreateBody(&path3480_bdef);

b2CircleShape path3480_s;
path3480_s.m_radius=0.318572368514f;

b2FixtureDef path3480_fdef;
path3480_fdef.shape = &path3480_s;

path3480_fdef.density = 1.0f;
path3480_fdef.friction = 0.1f;
path3480_fdef.restitution = 0.8f;
path3480_fdef.filter.groupIndex = -1;

path3480_body->CreateFixture(&path3480_fdef);
bodylist.push_back(path3480_body);
uData* path3480_ud = new uData();
path3480_ud->name = "TITLE";
path3480_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3480_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3480_ud->strokewidth=24.23800087f;
path3480_body->SetUserData(path3480_ud);

//
//path3478
//
b2BodyDef path3478_bdef;
path3478_bdef.type = b2_staticBody;
path3478_bdef.bullet = false;
path3478_bdef.position.Set(7.02799306978f,13.7259589996f);
path3478_bdef.linearDamping = 0.0f;
path3478_bdef.angularDamping =0.0f;
b2Body* path3478_body = m_world->CreateBody(&path3478_bdef);

b2CircleShape path3478_s;
path3478_s.m_radius=0.318572368514f;

b2FixtureDef path3478_fdef;
path3478_fdef.shape = &path3478_s;

path3478_fdef.density = 1.0f;
path3478_fdef.friction = 0.1f;
path3478_fdef.restitution = 0.8f;
path3478_fdef.filter.groupIndex = -1;

path3478_body->CreateFixture(&path3478_fdef);
bodylist.push_back(path3478_body);
uData* path3478_ud = new uData();
path3478_ud->name = "TITLE";
path3478_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3478_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3478_ud->strokewidth=24.23800087f;
path3478_body->SetUserData(path3478_ud);

//
//path3476
//
b2BodyDef path3476_bdef;
path3476_bdef.type = b2_staticBody;
path3476_bdef.bullet = false;
path3476_bdef.position.Set(6.7111611667f,14.438830197f);
path3476_bdef.linearDamping = 0.0f;
path3476_bdef.angularDamping =0.0f;
b2Body* path3476_body = m_world->CreateBody(&path3476_bdef);

b2CircleShape path3476_s;
path3476_s.m_radius=0.318572368514f;

b2FixtureDef path3476_fdef;
path3476_fdef.shape = &path3476_s;

path3476_fdef.density = 1.0f;
path3476_fdef.friction = 0.1f;
path3476_fdef.restitution = 0.8f;
path3476_fdef.filter.groupIndex = -1;

path3476_body->CreateFixture(&path3476_fdef);
bodylist.push_back(path3476_body);
uData* path3476_ud = new uData();
path3476_ud->name = "TITLE";
path3476_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3476_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3476_ud->strokewidth=24.23800087f;
path3476_body->SetUserData(path3476_ud);

//
//path3474
//
b2BodyDef path3474_bdef;
path3474_bdef.type = b2_staticBody;
path3474_bdef.bullet = false;
path3474_bdef.position.Set(5.87947829786f,14.8744737715f);
path3474_bdef.linearDamping = 0.0f;
path3474_bdef.angularDamping =0.0f;
b2Body* path3474_body = m_world->CreateBody(&path3474_bdef);

b2CircleShape path3474_s;
path3474_s.m_radius=0.318572368514f;

b2FixtureDef path3474_fdef;
path3474_fdef.shape = &path3474_s;

path3474_fdef.density = 1.0f;
path3474_fdef.friction = 0.1f;
path3474_fdef.restitution = 0.8f;
path3474_fdef.filter.groupIndex = -1;

path3474_body->CreateFixture(&path3474_fdef);
bodylist.push_back(path3474_body);
uData* path3474_ud = new uData();
path3474_ud->name = "TITLE";
path3474_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3474_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3474_ud->strokewidth=24.23800087f;
path3474_body->SetUserData(path3474_ud);

//
//path3472
//
b2BodyDef path3472_bdef;
path3472_bdef.type = b2_staticBody;
path3472_bdef.bullet = false;
path3472_bdef.position.Set(5.87947829786f,14.2012065035f);
path3472_bdef.linearDamping = 0.0f;
path3472_bdef.angularDamping =0.0f;
b2Body* path3472_body = m_world->CreateBody(&path3472_bdef);

b2CircleShape path3472_s;
path3472_s.m_radius=0.318572368514f;

b2FixtureDef path3472_fdef;
path3472_fdef.shape = &path3472_s;

path3472_fdef.density = 1.0f;
path3472_fdef.friction = 0.1f;
path3472_fdef.restitution = 0.8f;
path3472_fdef.filter.groupIndex = -1;

path3472_body->CreateFixture(&path3472_fdef);
bodylist.push_back(path3472_body);
uData* path3472_ud = new uData();
path3472_ud->name = "TITLE";
path3472_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3472_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3472_ud->strokewidth=24.23800087f;
path3472_body->SetUserData(path3472_ud);

//
//path3470
//
b2BodyDef path3470_bdef;
path3470_bdef.type = b2_staticBody;
path3470_bdef.bullet = false;
path3470_bdef.position.Set(5.87947829786f,13.4487312597f);
path3470_bdef.linearDamping = 0.0f;
path3470_bdef.angularDamping =0.0f;
b2Body* path3470_body = m_world->CreateBody(&path3470_bdef);

b2CircleShape path3470_s;
path3470_s.m_radius=0.318572368514f;

b2FixtureDef path3470_fdef;
path3470_fdef.shape = &path3470_s;

path3470_fdef.density = 1.0f;
path3470_fdef.friction = 0.1f;
path3470_fdef.restitution = 0.8f;
path3470_fdef.filter.groupIndex = -1;

path3470_body->CreateFixture(&path3470_fdef);
bodylist.push_back(path3470_body);
uData* path3470_ud = new uData();
path3470_ud->name = "TITLE";
path3470_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3470_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3470_ud->strokewidth=24.23800087f;
path3470_body->SetUserData(path3470_ud);

//
//path3468
//
b2BodyDef path3468_bdef;
path3468_bdef.type = b2_staticBody;
path3468_bdef.bullet = false;
path3468_bdef.position.Set(5.87947829786f,12.7358599454f);
path3468_bdef.linearDamping = 0.0f;
path3468_bdef.angularDamping =0.0f;
b2Body* path3468_body = m_world->CreateBody(&path3468_bdef);

b2CircleShape path3468_s;
path3468_s.m_radius=0.318572368514f;

b2FixtureDef path3468_fdef;
path3468_fdef.shape = &path3468_s;

path3468_fdef.density = 1.0f;
path3468_fdef.friction = 0.1f;
path3468_fdef.restitution = 0.8f;
path3468_fdef.filter.groupIndex = -1;

path3468_body->CreateFixture(&path3468_fdef);
bodylist.push_back(path3468_body);
uData* path3468_ud = new uData();
path3468_ud->name = "TITLE";
path3468_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3468_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3468_ud->strokewidth=24.23800087f;
path3468_body->SetUserData(path3468_ud);

//
//path3466
//
b2BodyDef path3466_bdef;
path3466_bdef.type = b2_staticBody;
path3466_bdef.bullet = false;
path3466_bdef.position.Set(5.87947829786f,11.9041767259f);
path3466_bdef.linearDamping = 0.0f;
path3466_bdef.angularDamping =0.0f;
b2Body* path3466_body = m_world->CreateBody(&path3466_bdef);

b2CircleShape path3466_s;
path3466_s.m_radius=0.318572368514f;

b2FixtureDef path3466_fdef;
path3466_fdef.shape = &path3466_s;

path3466_fdef.density = 1.0f;
path3466_fdef.friction = 0.1f;
path3466_fdef.restitution = 0.8f;
path3466_fdef.filter.groupIndex = -1;

path3466_body->CreateFixture(&path3466_fdef);
bodylist.push_back(path3466_body);
uData* path3466_ud = new uData();
path3466_ud->name = "TITLE";
path3466_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3466_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3466_ud->strokewidth=24.23800087f;
path3466_body->SetUserData(path3466_ud);

//
//path3464
//
b2BodyDef path3464_bdef;
path3464_bdef.type = b2_staticBody;
path3464_bdef.bullet = false;
path3464_bdef.position.Set(5.83987401773f,11.2705133873f);
path3464_bdef.linearDamping = 0.0f;
path3464_bdef.angularDamping =0.0f;
b2Body* path3464_body = m_world->CreateBody(&path3464_bdef);

b2CircleShape path3464_s;
path3464_s.m_radius=0.318572368514f;

b2FixtureDef path3464_fdef;
path3464_fdef.shape = &path3464_s;

path3464_fdef.density = 1.0f;
path3464_fdef.friction = 0.1f;
path3464_fdef.restitution = 0.8f;
path3464_fdef.filter.groupIndex = -1;

path3464_body->CreateFixture(&path3464_fdef);
bodylist.push_back(path3464_body);
uData* path3464_ud = new uData();
path3464_ud->name = "TITLE";
path3464_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3464_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3464_ud->strokewidth=24.23800087f;
path3464_body->SetUserData(path3464_ud);

//
//path3968
//
b2BodyDef path3968_bdef;
path3968_bdef.type = b2_staticBody;
path3968_bdef.bullet = false;
path3968_bdef.position.Set(-3.0292767399f,5.20288458421f);
path3968_bdef.linearDamping = 0.0f;
path3968_bdef.angularDamping =0.0f;
b2Body* path3968_body = m_world->CreateBody(&path3968_bdef);

b2CircleShape path3968_s;
path3968_s.m_radius=0.318572368514f;

b2FixtureDef path3968_fdef;
path3968_fdef.shape = &path3968_s;

path3968_fdef.density = 1.0f;
path3968_fdef.friction = 0.1f;
path3968_fdef.restitution = 0.8f;
path3968_fdef.filter.groupIndex = -1;

path3968_body->CreateFixture(&path3968_fdef);
bodylist.push_back(path3968_body);
uData* path3968_ud = new uData();
path3968_ud->name = "TITLE";
path3968_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3968_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3968_ud->strokewidth=24.23800087f;
path3968_body->SetUserData(path3968_ud);

//
//path3966
//
b2BodyDef path3966_bdef;
path3966_bdef.type = b2_staticBody;
path3966_bdef.bullet = false;
path3966_bdef.position.Set(-3.06508296501f,6.09803904294f);
path3966_bdef.linearDamping = 0.0f;
path3966_bdef.angularDamping =0.0f;
b2Body* path3966_body = m_world->CreateBody(&path3966_bdef);

b2CircleShape path3966_s;
path3966_s.m_radius=0.318572368514f;

b2FixtureDef path3966_fdef;
path3966_fdef.shape = &path3966_s;

path3966_fdef.density = 1.0f;
path3966_fdef.friction = 0.1f;
path3966_fdef.restitution = 0.8f;
path3966_fdef.filter.groupIndex = -1;

path3966_body->CreateFixture(&path3966_fdef);
bodylist.push_back(path3966_body);
uData* path3966_ud = new uData();
path3966_ud->name = "TITLE";
path3966_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3966_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3966_ud->strokewidth=24.23800087f;
path3966_body->SetUserData(path3966_ud);

//
//path3964
//
b2BodyDef path3964_bdef;
path3964_bdef.type = b2_staticBody;
path3964_bdef.bullet = false;
path3964_bdef.position.Set(-3.06508296501f,6.95738797796f);
path3964_bdef.linearDamping = 0.0f;
path3964_bdef.angularDamping =0.0f;
b2Body* path3964_body = m_world->CreateBody(&path3964_bdef);

b2CircleShape path3964_s;
path3964_s.m_radius=0.318572368514f;

b2FixtureDef path3964_fdef;
path3964_fdef.shape = &path3964_s;

path3964_fdef.density = 1.0f;
path3964_fdef.friction = 0.1f;
path3964_fdef.restitution = 0.8f;
path3964_fdef.filter.groupIndex = -1;

path3964_body->CreateFixture(&path3964_fdef);
bodylist.push_back(path3964_body);
uData* path3964_ud = new uData();
path3964_ud->name = "TITLE";
path3964_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3964_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3964_ud->strokewidth=24.23800087f;
path3964_body->SetUserData(path3964_ud);

//
//path4050
//
b2BodyDef path4050_bdef;
path4050_bdef.type = b2_staticBody;
path4050_bdef.bullet = false;
path4050_bdef.position.Set(4.44281401071f,-9.66921640238f);
path4050_bdef.linearDamping = 0.0f;
path4050_bdef.angularDamping =0.0f;
b2Body* path4050_body = m_world->CreateBody(&path4050_bdef);

b2CircleShape path4050_s;
path4050_s.m_radius=0.318572368514f;

b2FixtureDef path4050_fdef;
path4050_fdef.shape = &path4050_s;

path4050_fdef.density = 1.0f;
path4050_fdef.friction = 0.1f;
path4050_fdef.restitution = 0.8f;
path4050_fdef.filter.groupIndex = -1;

path4050_body->CreateFixture(&path4050_fdef);
bodylist.push_back(path4050_body);
uData* path4050_ud = new uData();
path4050_ud->name = "TITLE";
path4050_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4050_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4050_ud->strokewidth=24.23800087f;
path4050_body->SetUserData(path4050_ud);

//
//path4048
//
b2BodyDef path4048_bdef;
path4048_bdef.type = b2_staticBody;
path4048_bdef.bullet = false;
path4048_bdef.position.Set(4.44281401071f,-8.97099618174f);
path4048_bdef.linearDamping = 0.0f;
path4048_bdef.angularDamping =0.0f;
b2Body* path4048_body = m_world->CreateBody(&path4048_bdef);

b2CircleShape path4048_s;
path4048_s.m_radius=0.318572368514f;

b2FixtureDef path4048_fdef;
path4048_fdef.shape = &path4048_s;

path4048_fdef.density = 1.0f;
path4048_fdef.friction = 0.1f;
path4048_fdef.restitution = 0.8f;
path4048_fdef.filter.groupIndex = -1;

path4048_body->CreateFixture(&path4048_fdef);
bodylist.push_back(path4048_body);
uData* path4048_ud = new uData();
path4048_ud->name = "TITLE";
path4048_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4048_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4048_ud->strokewidth=24.23800087f;
path4048_body->SetUserData(path4048_ud);

//
//path4046
//
b2BodyDef path4046_bdef;
path4046_bdef.type = b2_staticBody;
path4046_bdef.bullet = false;
path4046_bdef.position.Set(4.42491089815f,-8.32648412978f);
path4046_bdef.linearDamping = 0.0f;
path4046_bdef.angularDamping =0.0f;
b2Body* path4046_body = m_world->CreateBody(&path4046_bdef);

b2CircleShape path4046_s;
path4046_s.m_radius=0.318572368514f;

b2FixtureDef path4046_fdef;
path4046_fdef.shape = &path4046_s;

path4046_fdef.density = 1.0f;
path4046_fdef.friction = 0.1f;
path4046_fdef.restitution = 0.8f;
path4046_fdef.filter.groupIndex = -1;

path4046_body->CreateFixture(&path4046_fdef);
bodylist.push_back(path4046_body);
uData* path4046_ud = new uData();
path4046_ud->name = "TITLE";
path4046_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4046_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4046_ud->strokewidth=24.23800087f;
path4046_body->SetUserData(path4046_ud);

//
//path4044
//
b2BodyDef path4044_bdef;
path4044_bdef.type = b2_staticBody;
path4044_bdef.bullet = false;
path4044_bdef.position.Set(5.40958091965f,-7.52084406483f);
path4044_bdef.linearDamping = 0.0f;
path4044_bdef.angularDamping =0.0f;
b2Body* path4044_body = m_world->CreateBody(&path4044_bdef);

b2CircleShape path4044_s;
path4044_s.m_radius=0.318572368514f;

b2FixtureDef path4044_fdef;
path4044_fdef.shape = &path4044_s;

path4044_fdef.density = 1.0f;
path4044_fdef.friction = 0.1f;
path4044_fdef.restitution = 0.8f;
path4044_fdef.filter.groupIndex = -1;

path4044_body->CreateFixture(&path4044_fdef);
bodylist.push_back(path4044_body);
uData* path4044_ud = new uData();
path4044_ud->name = "TITLE";
path4044_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4044_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4044_ud->strokewidth=24.23800087f;
path4044_body->SetUserData(path4044_ud);

//
//path4042
//
b2BodyDef path4042_bdef;
path4042_bdef.type = b2_staticBody;
path4042_bdef.bullet = false;
path4042_bdef.position.Set(4.46071712326f,-7.48503783972f);
path4042_bdef.linearDamping = 0.0f;
path4042_bdef.angularDamping =0.0f;
b2Body* path4042_body = m_world->CreateBody(&path4042_bdef);

b2CircleShape path4042_s;
path4042_s.m_radius=0.318572368514f;

b2FixtureDef path4042_fdef;
path4042_fdef.shape = &path4042_s;

path4042_fdef.density = 1.0f;
path4042_fdef.friction = 0.1f;
path4042_fdef.restitution = 0.8f;
path4042_fdef.filter.groupIndex = -1;

path4042_body->CreateFixture(&path4042_fdef);
bodylist.push_back(path4042_body);
uData* path4042_ud = new uData();
path4042_ud->name = "TITLE";
path4042_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4042_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4042_ud->strokewidth=24.23800087f;
path4042_body->SetUserData(path4042_ud);

//
//path4040
//
b2BodyDef path4040_bdef;
path4040_bdef.type = b2_staticBody;
path4040_bdef.bullet = false;
path4040_bdef.position.Set(3.49394904532f,-7.52084406483f);
path4040_bdef.linearDamping = 0.0f;
path4040_bdef.angularDamping =0.0f;
b2Body* path4040_body = m_world->CreateBody(&path4040_bdef);

b2CircleShape path4040_s;
path4040_s.m_radius=0.318572368514f;

b2FixtureDef path4040_fdef;
path4040_fdef.shape = &path4040_s;

path4040_fdef.density = 1.0f;
path4040_fdef.friction = 0.1f;
path4040_fdef.restitution = 0.8f;
path4040_fdef.filter.groupIndex = -1;

path4040_body->CreateFixture(&path4040_fdef);
bodylist.push_back(path4040_body);
uData* path4040_ud = new uData();
path4040_ud->name = "TITLE";
path4040_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4040_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4040_ud->strokewidth=24.23800087f;
path4040_body->SetUserData(path4040_ud);

//
//path4038
//
b2BodyDef path4038_bdef;
path4038_bdef.type = b2_staticBody;
path4038_bdef.bullet = false;
path4038_bdef.position.Set(0.844290725243f,-9.59760395216f);
path4038_bdef.linearDamping = 0.0f;
path4038_bdef.angularDamping =0.0f;
b2Body* path4038_body = m_world->CreateBody(&path4038_bdef);

b2CircleShape path4038_s;
path4038_s.m_radius=0.318572368514f;

b2FixtureDef path4038_fdef;
path4038_fdef.shape = &path4038_s;

path4038_fdef.density = 1.0f;
path4038_fdef.friction = 0.1f;
path4038_fdef.restitution = 0.8f;
path4038_fdef.filter.groupIndex = -1;

path4038_body->CreateFixture(&path4038_fdef);
bodylist.push_back(path4038_body);
uData* path4038_ud = new uData();
path4038_ud->name = "TITLE";
path4038_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4038_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4038_ud->strokewidth=24.23800087f;
path4038_body->SetUserData(path4038_ud);

//
//path4036
//
b2BodyDef path4036_bdef;
path4036_bdef.type = b2_staticBody;
path4036_bdef.bullet = false;
path4036_bdef.position.Set(1.57831717098f,-9.59760395216f);
path4036_bdef.linearDamping = 0.0f;
path4036_bdef.angularDamping =0.0f;
b2Body* path4036_body = m_world->CreateBody(&path4036_bdef);

b2CircleShape path4036_s;
path4036_s.m_radius=0.318572368514f;

b2FixtureDef path4036_fdef;
path4036_fdef.shape = &path4036_s;

path4036_fdef.density = 1.0f;
path4036_fdef.friction = 0.1f;
path4036_fdef.restitution = 0.8f;
path4036_fdef.filter.groupIndex = -1;

path4036_body->CreateFixture(&path4036_fdef);
bodylist.push_back(path4036_body);
uData* path4036_ud = new uData();
path4036_ud->name = "TITLE";
path4036_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4036_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4036_ud->strokewidth=24.23800087f;
path4036_body->SetUserData(path4036_ud);

//
//path4034
//
b2BodyDef path4034_bdef;
path4034_bdef.type = b2_staticBody;
path4034_bdef.bullet = false;
path4034_bdef.position.Set(2.09750743506f,-9.13212419473f);
path4034_bdef.linearDamping = 0.0f;
path4034_bdef.angularDamping =0.0f;
b2Body* path4034_body = m_world->CreateBody(&path4034_bdef);

b2CircleShape path4034_s;
path4034_s.m_radius=0.318572368514f;

b2FixtureDef path4034_fdef;
path4034_fdef.shape = &path4034_s;

path4034_fdef.density = 1.0f;
path4034_fdef.friction = 0.1f;
path4034_fdef.restitution = 0.8f;
path4034_fdef.filter.groupIndex = -1;

path4034_body->CreateFixture(&path4034_fdef);
bodylist.push_back(path4034_body);
uData* path4034_ud = new uData();
path4034_ud->name = "TITLE";
path4034_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4034_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4034_ud->strokewidth=24.23800087f;
path4034_body->SetUserData(path4034_ud);

//
//path4032
//
b2BodyDef path4032_bdef;
path4032_bdef.type = b2_staticBody;
path4032_bdef.bullet = false;
path4032_bdef.position.Set(1.4529953831f,-8.61293393065f);
path4032_bdef.linearDamping = 0.0f;
path4032_bdef.angularDamping =0.0f;
b2Body* path4032_body = m_world->CreateBody(&path4032_bdef);

b2CircleShape path4032_s;
path4032_s.m_radius=0.318572368514f;

b2FixtureDef path4032_fdef;
path4032_fdef.shape = &path4032_s;

path4032_fdef.density = 1.0f;
path4032_fdef.friction = 0.1f;
path4032_fdef.restitution = 0.8f;
path4032_fdef.filter.groupIndex = -1;

path4032_body->CreateFixture(&path4032_fdef);
bodylist.push_back(path4032_body);
uData* path4032_ud = new uData();
path4032_ud->name = "TITLE";
path4032_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4032_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4032_ud->strokewidth=24.23800087f;
path4032_body->SetUserData(path4032_ud);

//
//path4030
//
b2BodyDef path4030_bdef;
path4030_bdef.type = b2_staticBody;
path4030_bdef.bullet = false;
path4030_bdef.position.Set(0.772678275025f,-8.14745300424f);
path4030_bdef.linearDamping = 0.0f;
path4030_bdef.angularDamping =0.0f;
b2Body* path4030_body = m_world->CreateBody(&path4030_bdef);

b2CircleShape path4030_s;
path4030_s.m_radius=0.318572368514f;

b2FixtureDef path4030_fdef;
path4030_fdef.shape = &path4030_s;

path4030_fdef.density = 1.0f;
path4030_fdef.friction = 0.1f;
path4030_fdef.restitution = 0.8f;
path4030_fdef.filter.groupIndex = -1;

path4030_body->CreateFixture(&path4030_fdef);
bodylist.push_back(path4030_body);
uData* path4030_ud = new uData();
path4030_ud->name = "TITLE";
path4030_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4030_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4030_ud->strokewidth=24.23800087f;
path4030_body->SetUserData(path4030_ud);

//
//path4028
//
b2BodyDef path4028_bdef;
path4028_bdef.type = b2_staticBody;
path4028_bdef.bullet = false;
path4028_bdef.position.Set(1.27396425756f,-7.55665028994f);
path4028_bdef.linearDamping = 0.0f;
path4028_bdef.angularDamping =0.0f;
b2Body* path4028_body = m_world->CreateBody(&path4028_bdef);

b2CircleShape path4028_s;
path4028_s.m_radius=0.318572368514f;

b2FixtureDef path4028_fdef;
path4028_fdef.shape = &path4028_s;

path4028_fdef.density = 1.0f;
path4028_fdef.friction = 0.1f;
path4028_fdef.restitution = 0.8f;
path4028_fdef.filter.groupIndex = -1;

path4028_body->CreateFixture(&path4028_fdef);
bodylist.push_back(path4028_body);
uData* path4028_ud = new uData();
path4028_ud->name = "TITLE";
path4028_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4028_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4028_ud->strokewidth=24.23800087f;
path4028_body->SetUserData(path4028_ud);

//
//path4026
//
b2BodyDef path4026_bdef;
path4026_bdef.type = b2_staticBody;
path4026_bdef.bullet = false;
path4026_bdef.position.Set(2.06170120995f,-7.62826274016f);
path4026_bdef.linearDamping = 0.0f;
path4026_bdef.angularDamping =0.0f;
b2Body* path4026_body = m_world->CreateBody(&path4026_bdef);

b2CircleShape path4026_s;
path4026_s.m_radius=0.318572368514f;

b2FixtureDef path4026_fdef;
path4026_fdef.shape = &path4026_s;

path4026_fdef.density = 1.0f;
path4026_fdef.friction = 0.1f;
path4026_fdef.restitution = 0.8f;
path4026_fdef.filter.groupIndex = -1;

path4026_body->CreateFixture(&path4026_fdef);
bodylist.push_back(path4026_body);
uData* path4026_ud = new uData();
path4026_ud->name = "TITLE";
path4026_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4026_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4026_ud->strokewidth=24.23800087f;
path4026_body->SetUserData(path4026_ud);

//
//path4012
//
b2BodyDef path4012_bdef;
path4012_bdef.type = b2_staticBody;
path4012_bdef.bullet = false;
path4012_bdef.position.Set(-0.838600685884f,-9.54389461449f);
path4012_bdef.linearDamping = 0.0f;
path4012_bdef.angularDamping =0.0f;
b2Body* path4012_body = m_world->CreateBody(&path4012_bdef);

b2CircleShape path4012_s;
path4012_s.m_radius=0.318572368514f;

b2FixtureDef path4012_fdef;
path4012_fdef.shape = &path4012_s;

path4012_fdef.density = 1.0f;
path4012_fdef.friction = 0.1f;
path4012_fdef.restitution = 0.8f;
path4012_fdef.filter.groupIndex = -1;

path4012_body->CreateFixture(&path4012_fdef);
bodylist.push_back(path4012_body);
uData* path4012_ud = new uData();
path4012_ud->name = "TITLE";
path4012_ud->fill.Set(float(245)/255,float(0)/255,float(4)/255);
path4012_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4012_ud->strokewidth=24.23800087f;
path4012_body->SetUserData(path4012_ud);

//
//path4010
//
b2BodyDef path4010_bdef;
path4010_bdef.type = b2_staticBody;
path4010_bdef.bullet = false;
path4010_bdef.position.Set(-0.82069757333f,-8.5950308181f);
path4010_bdef.linearDamping = 0.0f;
path4010_bdef.angularDamping =0.0f;
b2Body* path4010_body = m_world->CreateBody(&path4010_bdef);

b2CircleShape path4010_s;
path4010_s.m_radius=0.318572368514f;

b2FixtureDef path4010_fdef;
path4010_fdef.shape = &path4010_s;

path4010_fdef.density = 1.0f;
path4010_fdef.friction = 0.1f;
path4010_fdef.restitution = 0.8f;
path4010_fdef.filter.groupIndex = -1;

path4010_body->CreateFixture(&path4010_fdef);
bodylist.push_back(path4010_body);
uData* path4010_ud = new uData();
path4010_ud->name = "TITLE";
path4010_ud->fill.Set(float(245)/255,float(0)/255,float(4)/255);
path4010_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4010_ud->strokewidth=24.23800087f;
path4010_body->SetUserData(path4010_ud);

//
//path4006
//
b2BodyDef path4006_bdef;
path4006_bdef.type = b2_staticBody;
path4006_bdef.bullet = false;
path4006_bdef.position.Set(1.80456037663f,5.1670783591f);
path4006_bdef.linearDamping = 0.0f;
path4006_bdef.angularDamping =0.0f;
b2Body* path4006_body = m_world->CreateBody(&path4006_bdef);

b2CircleShape path4006_s;
path4006_s.m_radius=0.318572368514f;

b2FixtureDef path4006_fdef;
path4006_fdef.shape = &path4006_s;

path4006_fdef.density = 1.0f;
path4006_fdef.friction = 0.1f;
path4006_fdef.restitution = 0.8f;
path4006_fdef.filter.groupIndex = -1;

path4006_body->CreateFixture(&path4006_fdef);
bodylist.push_back(path4006_body);
uData* path4006_ud = new uData();
path4006_ud->name = "TITLE";
path4006_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path4006_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4006_ud->strokewidth=24.23800087f;
path4006_body->SetUserData(path4006_ud);

//
//path4004
//
b2BodyDef path4004_bdef;
path4004_bdef.type = b2_staticBody;
path4004_bdef.bullet = false;
path4004_bdef.position.Set(1.46440240709f,6.06223281784f);
path4004_bdef.linearDamping = 0.0f;
path4004_bdef.angularDamping =0.0f;
b2Body* path4004_body = m_world->CreateBody(&path4004_bdef);

b2CircleShape path4004_s;
path4004_s.m_radius=0.318572368514f;

b2FixtureDef path4004_fdef;
path4004_fdef.shape = &path4004_s;

path4004_fdef.density = 1.0f;
path4004_fdef.friction = 0.1f;
path4004_fdef.restitution = 0.8f;
path4004_fdef.filter.groupIndex = -1;

path4004_body->CreateFixture(&path4004_fdef);
bodylist.push_back(path4004_body);
uData* path4004_ud = new uData();
path4004_ud->name = "TITLE";
path4004_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path4004_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4004_ud->strokewidth=24.23800087f;
path4004_body->SetUserData(path4004_ud);

//
//path4002
//
b2BodyDef path4002_bdef;
path4002_bdef.type = b2_staticBody;
path4002_bdef.bullet = false;
path4002_bdef.position.Set(0.909405917898f,6.99319420307f);
path4002_bdef.linearDamping = 0.0f;
path4002_bdef.angularDamping =0.0f;
b2Body* path4002_body = m_world->CreateBody(&path4002_bdef);

b2CircleShape path4002_s;
path4002_s.m_radius=0.318572368514f;

b2FixtureDef path4002_fdef;
path4002_fdef.shape = &path4002_s;

path4002_fdef.density = 1.0f;
path4002_fdef.friction = 0.1f;
path4002_fdef.restitution = 0.8f;
path4002_fdef.filter.groupIndex = -1;

path4002_body->CreateFixture(&path4002_fdef);
bodylist.push_back(path4002_body);
uData* path4002_ud = new uData();
path4002_ud->name = "TITLE";
path4002_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path4002_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4002_ud->strokewidth=24.23800087f;
path4002_body->SetUserData(path4002_ud);

//
//path4000
//
b2BodyDef path4000_bdef;
path4000_bdef.type = b2_staticBody;
path4000_bdef.bullet = false;
path4000_bdef.position.Set(0.479731216591f,6.1159421555f);
path4000_bdef.linearDamping = 0.0f;
path4000_bdef.angularDamping =0.0f;
b2Body* path4000_body = m_world->CreateBody(&path4000_bdef);

b2CircleShape path4000_s;
path4000_s.m_radius=0.318572368514f;

b2FixtureDef path4000_fdef;
path4000_fdef.shape = &path4000_s;

path4000_fdef.density = 1.0f;
path4000_fdef.friction = 0.1f;
path4000_fdef.restitution = 0.8f;
path4000_fdef.filter.groupIndex = -1;

path4000_body->CreateFixture(&path4000_fdef);
bodylist.push_back(path4000_body);
uData* path4000_ud = new uData();
path4000_ud->name = "TITLE";
path4000_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path4000_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4000_ud->strokewidth=24.23800087f;
path4000_body->SetUserData(path4000_ud);

//
//path3998
//
b2BodyDef path3998_bdef;
path3998_bdef.type = b2_staticBody;
path3998_bdef.bullet = false;
path3998_bdef.position.Set(0.121668965502f,5.23869080932f);
path3998_bdef.linearDamping = 0.0f;
path3998_bdef.angularDamping =0.0f;
b2Body* path3998_body = m_world->CreateBody(&path3998_bdef);

b2CircleShape path3998_s;
path3998_s.m_radius=0.318572368514f;

b2FixtureDef path3998_fdef;
path3998_fdef.shape = &path3998_s;

path3998_fdef.density = 1.0f;
path3998_fdef.friction = 0.1f;
path3998_fdef.restitution = 0.8f;
path3998_fdef.filter.groupIndex = -1;

path3998_body->CreateFixture(&path3998_fdef);
bodylist.push_back(path3998_body);
uData* path3998_ud = new uData();
path3998_ud->name = "TITLE";
path3998_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path3998_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3998_ud->strokewidth=24.23800087f;
path3998_body->SetUserData(path3998_ud);

//
//path3996
//
b2BodyDef path3996_bdef;
path3996_bdef.type = b2_staticBody;
path3996_bdef.bullet = false;
path3996_bdef.position.Set(-1.31057886986f,6.99319420307f);
path3996_bdef.linearDamping = 0.0f;
path3996_bdef.angularDamping =0.0f;
b2Body* path3996_body = m_world->CreateBody(&path3996_bdef);

b2CircleShape path3996_s;
path3996_s.m_radius=0.318572368514f;

b2FixtureDef path3996_fdef;
path3996_fdef.shape = &path3996_s;

path3996_fdef.density = 1.0f;
path3996_fdef.friction = 0.1f;
path3996_fdef.restitution = 0.8f;
path3996_fdef.filter.groupIndex = -1;

path3996_body->CreateFixture(&path3996_fdef);
bodylist.push_back(path3996_body);
uData* path3996_ud = new uData();
path3996_ud->name = "TITLE";
path3996_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3996_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3996_ud->strokewidth=24.23800087f;
path3996_body->SetUserData(path3996_ud);

//
//path3994
//
b2BodyDef path3994_bdef;
path3994_bdef.type = b2_staticBody;
path3994_bdef.bullet = false;
path3994_bdef.position.Set(-1.31057886986f,6.06223281784f);
path3994_bdef.linearDamping = 0.0f;
path3994_bdef.angularDamping =0.0f;
b2Body* path3994_body = m_world->CreateBody(&path3994_bdef);

b2CircleShape path3994_s;
path3994_s.m_radius=0.318572368514f;

b2FixtureDef path3994_fdef;
path3994_fdef.shape = &path3994_s;

path3994_fdef.density = 1.0f;
path3994_fdef.friction = 0.1f;
path3994_fdef.restitution = 0.8f;
path3994_fdef.filter.groupIndex = -1;

path3994_body->CreateFixture(&path3994_fdef);
bodylist.push_back(path3994_body);
uData* path3994_ud = new uData();
path3994_ud->name = "TITLE";
path3994_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3994_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3994_ud->strokewidth=24.23800087f;
path3994_body->SetUserData(path3994_ud);

//
//path3992
//
b2BodyDef path3992_bdef;
path3992_bdef.type = b2_staticBody;
path3992_bdef.bullet = false;
path3992_bdef.position.Set(-1.31057886986f,5.20288458421f);
path3992_bdef.linearDamping = 0.0f;
path3992_bdef.angularDamping =0.0f;
b2Body* path3992_body = m_world->CreateBody(&path3992_bdef);

b2CircleShape path3992_s;
path3992_s.m_radius=0.318572368514f;

b2FixtureDef path3992_fdef;
path3992_fdef.shape = &path3992_s;

path3992_fdef.density = 1.0f;
path3992_fdef.friction = 0.1f;
path3992_fdef.restitution = 0.8f;
path3992_fdef.filter.groupIndex = -1;

path3992_body->CreateFixture(&path3992_fdef);
bodylist.push_back(path3992_body);
uData* path3992_ud = new uData();
path3992_ud->name = "TITLE";
path3992_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3992_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3992_ud->strokewidth=24.23800087f;
path3992_body->SetUserData(path3992_ud);

//
//path3988
//
b2BodyDef path3988_bdef;
path3988_bdef.type = b2_staticBody;
path3988_bdef.bullet = false;
path3988_bdef.position.Set(-2.23205972994f,6.12878353267f);
path3988_bdef.linearDamping = 0.0f;
path3988_bdef.angularDamping =0.0f;
b2Body* path3988_body = m_world->CreateBody(&path3988_bdef);

b2CircleShape path3988_s;
path3988_s.m_radius=0.318572368514f;

b2FixtureDef path3988_fdef;
path3988_fdef.shape = &path3988_s;

path3988_fdef.density = 1.0f;
path3988_fdef.friction = 0.1f;
path3988_fdef.restitution = 0.8f;
path3988_fdef.filter.groupIndex = -1;

path3988_body->CreateFixture(&path3988_fdef);
bodylist.push_back(path3988_body);
uData* path3988_ud = new uData();
path3988_ud->name = "TITLE";
path3988_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3988_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3988_ud->strokewidth=24.23800087f;
path3988_body->SetUserData(path3988_ud);

//
//path3986
//
b2BodyDef path3986_bdef;
path3986_bdef.type = b2_staticBody;
path3986_bdef.bullet = false;
path3986_bdef.position.Set(-3.06508296501f,6.95738797796f);
path3986_bdef.linearDamping = 0.0f;
path3986_bdef.angularDamping =0.0f;
b2Body* path3986_body = m_world->CreateBody(&path3986_bdef);

b2CircleShape path3986_s;
path3986_s.m_radius=0.318572368514f;

b2FixtureDef path3986_fdef;
path3986_fdef.shape = &path3986_s;

path3986_fdef.density = 1.0f;
path3986_fdef.friction = 0.1f;
path3986_fdef.restitution = 0.8f;
path3986_fdef.filter.groupIndex = -1;

path3986_body->CreateFixture(&path3986_fdef);
bodylist.push_back(path3986_body);
uData* path3986_ud = new uData();
path3986_ud->name = "TITLE";
path3986_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3986_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3986_ud->strokewidth=24.23800087f;
path3986_body->SetUserData(path3986_ud);

//
//path3208
//
b2BodyDef path3208_bdef;
path3208_bdef.type = b2_staticBody;
path3208_bdef.bullet = false;
path3208_bdef.position.Set(-6.53066193332f,17.8827187525f);
path3208_bdef.linearDamping = 0.0f;
path3208_bdef.angularDamping =0.0f;
b2Body* path3208_body = m_world->CreateBody(&path3208_bdef);

b2CircleShape path3208_s;
path3208_s.m_radius=0.318572368514f;

b2FixtureDef path3208_fdef;
path3208_fdef.shape = &path3208_s;

path3208_fdef.density = 1.0f;
path3208_fdef.friction = 0.1f;
path3208_fdef.restitution = 0.8f;
path3208_fdef.filter.groupIndex = -1;

path3208_body->CreateFixture(&path3208_fdef);
bodylist.push_back(path3208_body);
uData* path3208_ud = new uData();
path3208_ud->name = "TITLE";
path3208_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3208_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3208_ud->strokewidth=24.23800087f;
path3208_body->SetUserData(path3208_ud);

//
//path3206
//
b2BodyDef path3206_bdef;
path3206_bdef.type = b2_staticBody;
path3206_bdef.bullet = false;
path3206_bdef.position.Set(-6.53066193332f,18.4767782785f);
path3206_bdef.linearDamping = 0.0f;
path3206_bdef.angularDamping =0.0f;
b2Body* path3206_body = m_world->CreateBody(&path3206_bdef);

b2CircleShape path3206_s;
path3206_s.m_radius=0.318572368514f;

b2FixtureDef path3206_fdef;
path3206_fdef.shape = &path3206_s;

path3206_fdef.density = 1.0f;
path3206_fdef.friction = 0.1f;
path3206_fdef.restitution = 0.8f;
path3206_fdef.filter.groupIndex = -1;

path3206_body->CreateFixture(&path3206_fdef);
bodylist.push_back(path3206_body);
uData* path3206_ud = new uData();
path3206_ud->name = "TITLE";
path3206_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3206_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3206_ud->strokewidth=24.23800087f;
path3206_body->SetUserData(path3206_ud);

//
//path3204
//
b2BodyDef path3204_bdef;
path3204_bdef.type = b2_staticBody;
path3204_bdef.bullet = false;
path3204_bdef.position.Set(-6.53066193332f,19.1896494759f);
path3204_bdef.linearDamping = 0.0f;
path3204_bdef.angularDamping =0.0f;
b2Body* path3204_body = m_world->CreateBody(&path3204_bdef);

b2CircleShape path3204_s;
path3204_s.m_radius=0.318572368514f;

b2FixtureDef path3204_fdef;
path3204_fdef.shape = &path3204_s;

path3204_fdef.density = 1.0f;
path3204_fdef.friction = 0.1f;
path3204_fdef.restitution = 0.8f;
path3204_fdef.filter.groupIndex = -1;

path3204_body->CreateFixture(&path3204_fdef);
bodylist.push_back(path3204_body);
uData* path3204_ud = new uData();
path3204_ud->name = "TITLE";
path3204_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3204_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3204_ud->strokewidth=24.23800087f;
path3204_body->SetUserData(path3204_ud);

//
//path3202
//
b2BodyDef path3202_bdef;
path3202_bdef.type = b2_staticBody;
path3202_bdef.bullet = false;
path3202_bdef.position.Set(-6.53066193332f,19.9025206733f);
path3202_bdef.linearDamping = 0.0f;
path3202_bdef.angularDamping =0.0f;
b2Body* path3202_body = m_world->CreateBody(&path3202_bdef);

b2CircleShape path3202_s;
path3202_s.m_radius=0.318572368514f;

b2FixtureDef path3202_fdef;
path3202_fdef.shape = &path3202_s;

path3202_fdef.density = 1.0f;
path3202_fdef.friction = 0.1f;
path3202_fdef.restitution = 0.8f;
path3202_fdef.filter.groupIndex = -1;

path3202_body->CreateFixture(&path3202_fdef);
bodylist.push_back(path3202_body);
uData* path3202_ud = new uData();
path3202_ud->name = "TITLE";
path3202_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3202_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3202_ud->strokewidth=24.23800087f;
path3202_body->SetUserData(path3202_ud);

//
//path3200
//
b2BodyDef path3200_bdef;
path3200_bdef.type = b2_staticBody;
path3200_bdef.bullet = false;
path3200_bdef.position.Set(-6.53066193332f,20.7342047112f);
path3200_bdef.linearDamping = 0.0f;
path3200_bdef.angularDamping =0.0f;
b2Body* path3200_body = m_world->CreateBody(&path3200_bdef);

b2CircleShape path3200_s;
path3200_s.m_radius=0.318572368514f;

b2FixtureDef path3200_fdef;
path3200_fdef.shape = &path3200_s;

path3200_fdef.density = 1.0f;
path3200_fdef.friction = 0.1f;
path3200_fdef.restitution = 0.8f;
path3200_fdef.filter.groupIndex = -1;

path3200_body->CreateFixture(&path3200_fdef);
bodylist.push_back(path3200_body);
uData* path3200_ud = new uData();
path3200_ud->name = "TITLE";
path3200_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3200_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3200_ud->strokewidth=24.23800087f;
path3200_body->SetUserData(path3200_ud);

//
//path3198
//
b2BodyDef path3198_bdef;
path3198_bdef.type = b2_staticBody;
path3198_bdef.bullet = false;
path3198_bdef.position.Set(-6.53066193332f,21.4074716285f);
path3198_bdef.linearDamping = 0.0f;
path3198_bdef.angularDamping =0.0f;
b2Body* path3198_body = m_world->CreateBody(&path3198_bdef);

b2CircleShape path3198_s;
path3198_s.m_radius=0.318572368514f;

b2FixtureDef path3198_fdef;
path3198_fdef.shape = &path3198_s;

path3198_fdef.density = 1.0f;
path3198_fdef.friction = 0.1f;
path3198_fdef.restitution = 0.8f;
path3198_fdef.filter.groupIndex = -1;

path3198_body->CreateFixture(&path3198_fdef);
bodylist.push_back(path3198_body);
uData* path3198_ud = new uData();
path3198_ud->name = "TITLE";
path3198_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3198_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3198_ud->strokewidth=24.23800087f;
path3198_body->SetUserData(path3198_ud);

//
//path3196
//
b2BodyDef path3196_bdef;
path3196_bdef.type = b2_staticBody;
path3196_bdef.bullet = false;
path3196_bdef.position.Set(-7.12472134244f,19.5856899393f);
path3196_bdef.linearDamping = 0.0f;
path3196_bdef.angularDamping =0.0f;
b2Body* path3196_body = m_world->CreateBody(&path3196_bdef);

b2CircleShape path3196_s;
path3196_s.m_radius=0.318572368514f;

b2FixtureDef path3196_fdef;
path3196_fdef.shape = &path3196_s;

path3196_fdef.density = 1.0f;
path3196_fdef.friction = 0.1f;
path3196_fdef.restitution = 0.8f;
path3196_fdef.filter.groupIndex = -1;

path3196_body->CreateFixture(&path3196_fdef);
bodylist.push_back(path3196_body);
uData* path3196_ud = new uData();
path3196_ud->name = "TITLE";
path3196_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3196_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3196_ud->strokewidth=24.23800087f;
path3196_body->SetUserData(path3196_ud);

//
//path3194
//
b2BodyDef path3194_bdef;
path3194_bdef.type = b2_staticBody;
path3194_bdef.bullet = false;
path3194_bdef.position.Set(-7.79798861044f,19.5856899393f);
path3194_bdef.linearDamping = 0.0f;
path3194_bdef.angularDamping =0.0f;
b2Body* path3194_body = m_world->CreateBody(&path3194_bdef);

b2CircleShape path3194_s;
path3194_s.m_radius=0.318572368514f;

b2FixtureDef path3194_fdef;
path3194_fdef.shape = &path3194_s;

path3194_fdef.density = 1.0f;
path3194_fdef.friction = 0.1f;
path3194_fdef.restitution = 0.8f;
path3194_fdef.filter.groupIndex = -1;

path3194_body->CreateFixture(&path3194_fdef);
bodylist.push_back(path3194_body);
uData* path3194_ud = new uData();
path3194_ud->name = "TITLE";
path3194_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3194_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3194_ud->strokewidth=24.23800087f;
path3194_body->SetUserData(path3194_ud);

//
//path3192
//
b2BodyDef path3192_bdef;
path3192_bdef.type = b2_staticBody;
path3192_bdef.bullet = false;
path3192_bdef.position.Set(-8.62967182997f,19.5856899393f);
path3192_bdef.linearDamping = 0.0f;
path3192_bdef.angularDamping =0.0f;
b2Body* path3192_body = m_world->CreateBody(&path3192_bdef);

b2CircleShape path3192_s;
path3192_s.m_radius=0.318572368514f;

b2FixtureDef path3192_fdef;
path3192_fdef.shape = &path3192_s;

path3192_fdef.density = 1.0f;
path3192_fdef.friction = 0.1f;
path3192_fdef.restitution = 0.8f;
path3192_fdef.filter.groupIndex = -1;

path3192_body->CreateFixture(&path3192_fdef);
bodylist.push_back(path3192_body);
uData* path3192_ud = new uData();
path3192_ud->name = "TITLE";
path3192_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3192_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3192_ud->strokewidth=24.23800087f;
path3192_body->SetUserData(path3192_ud);

//
//path3190
//
b2BodyDef path3190_bdef;
path3190_bdef.type = b2_staticBody;
path3190_bdef.bullet = false;
path3190_bdef.position.Set(-9.42175100317f,17.7639082501f);
path3190_bdef.linearDamping = 0.0f;
path3190_bdef.angularDamping =0.0f;
b2Body* path3190_body = m_world->CreateBody(&path3190_bdef);

b2CircleShape path3190_s;
path3190_s.m_radius=0.318572368514f;

b2FixtureDef path3190_fdef;
path3190_fdef.shape = &path3190_s;

path3190_fdef.density = 1.0f;
path3190_fdef.friction = 0.1f;
path3190_fdef.restitution = 0.8f;
path3190_fdef.filter.groupIndex = -1;

path3190_body->CreateFixture(&path3190_fdef);
bodylist.push_back(path3190_body);
uData* path3190_ud = new uData();
path3190_ud->name = "TITLE";
path3190_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3190_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3190_ud->strokewidth=24.23800087f;
path3190_body->SetUserData(path3190_ud);

//
//path3188
//
b2BodyDef path3188_bdef;
path3188_bdef.type = b2_staticBody;
path3188_bdef.bullet = false;
path3188_bdef.position.Set(-9.38214707373f,18.5163825586f);
path3188_bdef.linearDamping = 0.0f;
path3188_bdef.angularDamping =0.0f;
b2Body* path3188_body = m_world->CreateBody(&path3188_bdef);

b2CircleShape path3188_s;
path3188_s.m_radius=0.318572368514f;

b2FixtureDef path3188_fdef;
path3188_fdef.shape = &path3188_s;

path3188_fdef.density = 1.0f;
path3188_fdef.friction = 0.1f;
path3188_fdef.restitution = 0.8f;
path3188_fdef.filter.groupIndex = -1;

path3188_body->CreateFixture(&path3188_fdef);
bodylist.push_back(path3188_body);
uData* path3188_ud = new uData();
path3188_ud->name = "TITLE";
path3188_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3188_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3188_ud->strokewidth=24.23800087f;
path3188_body->SetUserData(path3188_ud);

//
//path3186
//
b2BodyDef path3186_bdef;
path3186_bdef.type = b2_staticBody;
path3186_bdef.bullet = false;
path3186_bdef.position.Set(-9.42175100317f,19.2292537561f);
path3186_bdef.linearDamping = 0.0f;
path3186_bdef.angularDamping =0.0f;
b2Body* path3186_body = m_world->CreateBody(&path3186_bdef);

b2CircleShape path3186_s;
path3186_s.m_radius=0.318572368514f;

b2FixtureDef path3186_fdef;
path3186_fdef.shape = &path3186_s;

path3186_fdef.density = 1.0f;
path3186_fdef.friction = 0.1f;
path3186_fdef.restitution = 0.8f;
path3186_fdef.filter.groupIndex = -1;

path3186_body->CreateFixture(&path3186_fdef);
bodylist.push_back(path3186_body);
uData* path3186_ud = new uData();
path3186_ud->name = "TITLE";
path3186_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3186_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3186_ud->strokewidth=24.23800087f;
path3186_body->SetUserData(path3186_ud);

//
//path3184
//
b2BodyDef path3184_bdef;
path3184_bdef.type = b2_staticBody;
path3184_bdef.bullet = false;
path3184_bdef.position.Set(-9.38214707373f,20.0213335137f);
path3184_bdef.linearDamping = 0.0f;
path3184_bdef.angularDamping =0.0f;
b2Body* path3184_body = m_world->CreateBody(&path3184_bdef);

b2CircleShape path3184_s;
path3184_s.m_radius=0.318572368514f;

b2FixtureDef path3184_fdef;
path3184_fdef.shape = &path3184_s;

path3184_fdef.density = 1.0f;
path3184_fdef.friction = 0.1f;
path3184_fdef.restitution = 0.8f;
path3184_fdef.filter.groupIndex = -1;

path3184_body->CreateFixture(&path3184_fdef);
bodylist.push_back(path3184_body);
uData* path3184_ud = new uData();
path3184_ud->name = "TITLE";
path3184_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3184_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3184_ud->strokewidth=24.23800087f;
path3184_body->SetUserData(path3184_ud);

//
//path3182
//
b2BodyDef path3182_bdef;
path3182_bdef.type = b2_staticBody;
path3182_bdef.bullet = false;
path3182_bdef.position.Set(-9.38214707373f,20.7342047112f);
path3182_bdef.linearDamping = 0.0f;
path3182_bdef.angularDamping =0.0f;
b2Body* path3182_body = m_world->CreateBody(&path3182_bdef);

b2CircleShape path3182_s;
path3182_s.m_radius=0.318572368514f;

b2FixtureDef path3182_fdef;
path3182_fdef.shape = &path3182_s;

path3182_fdef.density = 1.0f;
path3182_fdef.friction = 0.1f;
path3182_fdef.restitution = 0.8f;
path3182_fdef.filter.groupIndex = -1;

path3182_body->CreateFixture(&path3182_fdef);
bodylist.push_back(path3182_body);
uData* path3182_ud = new uData();
path3182_ud->name = "TITLE";
path3182_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path3182_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path3182_ud->strokewidth=24.23800087f;
path3182_body->SetUserData(path3182_ud);
