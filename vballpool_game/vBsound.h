#pragma once

#include "windows.h"
#include "mmsystem.h"
#pragma comment(lib, "winmm.lib")

#include <iostream>
#include <string>
using namespace std;


#define MAXSND 8

class vBsound
{
public:
	vBsound(void);
	~vBsound(void);
	bool st_flg; //再生が開始されたかどうかを示すフラグ
	MCI_STATUS_PARMS mciStatus;
	MCIDEVICEID id;
	MCIDEVICEID id_se;
	int sndnum;
	string fnames[MAXSND];

	void playBGM();
	void playSE(string alias);
	void continueBGM();
};

