vector<b2Body*> bodylist;

//
//path4031
//
b2BodyDef path4031_bdef;
path4031_bdef.type = b2_staticBody;
path4031_bdef.bullet = false;
path4031_bdef.position.Set(4.17023313098f,18.2830246338f);
path4031_bdef.linearDamping = 0.0f;
path4031_bdef.angularDamping =0.0f;
b2Body* path4031_body = m_world->CreateBody(&path4031_bdef);

b2CircleShape path4031_s;
path4031_s.m_radius=0.318572368514f;

b2FixtureDef path4031_fdef;
path4031_fdef.shape = &path4031_s;

path4031_fdef.density = 1.0f;
path4031_fdef.friction = 0.1f;
path4031_fdef.restitution = 0.8f;
path4031_fdef.filter.groupIndex = -1;

path4031_body->CreateFixture(&path4031_fdef);
bodylist.push_back(path4031_body);
uData* path4031_ud = new uData();
path4031_ud->name = "TITLE";
path4031_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4031_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4031_ud->strokewidth=24.23800087f;
path4031_body->SetUserData(path4031_ud);

//
//path4161
//
b2BodyDef path4161_bdef;
path4161_bdef.type = b2_staticBody;
path4161_bdef.bullet = false;
path4161_bdef.position.Set(3.86228089117f,17.6883561799f);
path4161_bdef.linearDamping = 0.0f;
path4161_bdef.angularDamping =0.0f;
b2Body* path4161_body = m_world->CreateBody(&path4161_bdef);

b2CircleShape path4161_s;
path4161_s.m_radius=0.318572368514f;

b2FixtureDef path4161_fdef;
path4161_fdef.shape = &path4161_s;

path4161_fdef.density = 1.0f;
path4161_fdef.friction = 0.1f;
path4161_fdef.restitution = 0.8f;
path4161_fdef.filter.groupIndex = -1;

path4161_body->CreateFixture(&path4161_fdef);
bodylist.push_back(path4161_body);
uData* path4161_ud = new uData();
path4161_ud->name = "TITLE";
path4161_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4161_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4161_ud->strokewidth=24.23800087f;
path4161_body->SetUserData(path4161_ud);

//
//path4189
//
b2BodyDef path4189_bdef;
path4189_bdef.type = b2_staticBody;
path4189_bdef.bullet = false;
path4189_bdef.position.Set(4.8179973187f,18.803359458f);
path4189_bdef.linearDamping = 0.0f;
path4189_bdef.angularDamping =0.0f;
b2Body* path4189_body = m_world->CreateBody(&path4189_bdef);

b2CircleShape path4189_s;
path4189_s.m_radius=0.318572368514f;

b2FixtureDef path4189_fdef;
path4189_fdef.shape = &path4189_s;

path4189_fdef.density = 1.0f;
path4189_fdef.friction = 0.1f;
path4189_fdef.restitution = 0.8f;
path4189_fdef.filter.groupIndex = -1;

path4189_body->CreateFixture(&path4189_fdef);
bodylist.push_back(path4189_body);
uData* path4189_ud = new uData();
path4189_ud->name = "TITLE";
path4189_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4189_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4189_ud->strokewidth=24.23800087f;
path4189_body->SetUserData(path4189_ud);

//
//path4187
//
b2BodyDef path4187_bdef;
path4187_bdef.type = b2_staticBody;
path4187_bdef.bullet = false;
path4187_bdef.position.Set(14.8723327246f,14.8779025419f);
path4187_bdef.linearDamping = 0.0f;
path4187_bdef.angularDamping =0.0f;
b2Body* path4187_body = m_world->CreateBody(&path4187_bdef);

b2CircleShape path4187_s;
path4187_s.m_radius=0.318572368514f;

b2FixtureDef path4187_fdef;
path4187_fdef.shape = &path4187_s;

path4187_fdef.density = 1.0f;
path4187_fdef.friction = 0.1f;
path4187_fdef.restitution = 0.8f;
path4187_fdef.filter.groupIndex = -1;

path4187_body->CreateFixture(&path4187_fdef);
bodylist.push_back(path4187_body);
uData* path4187_ud = new uData();
path4187_ud->name = "TITLE";
path4187_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4187_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4187_ud->strokewidth=24.23800087f;
path4187_body->SetUserData(path4187_ud);

//
//path4320
//
b2BodyDef path4320_bdef;
path4320_bdef.type = b2_staticBody;
path4320_bdef.bullet = false;
path4320_bdef.position.Set(14.8723327246f,14.8779025419f);
path4320_bdef.linearDamping = 0.0f;
path4320_bdef.angularDamping =0.0f;
b2Body* path4320_body = m_world->CreateBody(&path4320_bdef);

b2CircleShape path4320_s;
path4320_s.m_radius=0.318572368514f;

b2FixtureDef path4320_fdef;
path4320_fdef.shape = &path4320_s;

path4320_fdef.density = 1.0f;
path4320_fdef.friction = 0.1f;
path4320_fdef.restitution = 0.8f;
path4320_fdef.filter.groupIndex = -1;

path4320_body->CreateFixture(&path4320_fdef);
bodylist.push_back(path4320_body);
uData* path4320_ud = new uData();
path4320_ud->name = "TITLE";
path4320_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4320_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4320_ud->strokewidth=24.23800087f;
path4320_body->SetUserData(path4320_ud);

//
//path4318
//
b2BodyDef path4318_bdef;
path4318_bdef.type = b2_staticBody;
path4318_bdef.bullet = false;
path4318_bdef.position.Set(14.1251761607f,14.8497078576f);
path4318_bdef.linearDamping = 0.0f;
path4318_bdef.angularDamping =0.0f;
b2Body* path4318_body = m_world->CreateBody(&path4318_bdef);

b2CircleShape path4318_s;
path4318_s.m_radius=0.318572368514f;

b2FixtureDef path4318_fdef;
path4318_fdef.shape = &path4318_s;

path4318_fdef.density = 1.0f;
path4318_fdef.friction = 0.1f;
path4318_fdef.restitution = 0.8f;
path4318_fdef.filter.groupIndex = -1;

path4318_body->CreateFixture(&path4318_fdef);
bodylist.push_back(path4318_body);
uData* path4318_ud = new uData();
path4318_ud->name = "TITLE";
path4318_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4318_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4318_ud->strokewidth=24.23800087f;
path4318_body->SetUserData(path4318_ud);

//
//path4316
//
b2BodyDef path4316_bdef;
path4316_bdef.type = b2_staticBody;
path4316_bdef.bullet = false;
path4316_bdef.position.Set(13.335725466f,14.9060972263f);
path4316_bdef.linearDamping = 0.0f;
path4316_bdef.angularDamping =0.0f;
b2Body* path4316_body = m_world->CreateBody(&path4316_bdef);

b2CircleShape path4316_s;
path4316_s.m_radius=0.318572368514f;

b2FixtureDef path4316_fdef;
path4316_fdef.shape = &path4316_s;

path4316_fdef.density = 1.0f;
path4316_fdef.friction = 0.1f;
path4316_fdef.restitution = 0.8f;
path4316_fdef.filter.groupIndex = -1;

path4316_body->CreateFixture(&path4316_fdef);
bodylist.push_back(path4316_body);
uData* path4316_ud = new uData();
path4316_ud->name = "TITLE";
path4316_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4316_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4316_ud->strokewidth=24.23800087f;
path4316_body->SetUserData(path4316_ud);

//
//path4314
//
b2BodyDef path4314_bdef;
path4314_bdef.type = b2_staticBody;
path4314_bdef.bullet = false;
path4314_bdef.position.Set(13.335725466f,15.7378387669f);
path4314_bdef.linearDamping = 0.0f;
path4314_bdef.angularDamping =0.0f;
b2Body* path4314_body = m_world->CreateBody(&path4314_bdef);

b2CircleShape path4314_s;
path4314_s.m_radius=0.318572368514f;

b2FixtureDef path4314_fdef;
path4314_fdef.shape = &path4314_s;

path4314_fdef.density = 1.0f;
path4314_fdef.friction = 0.1f;
path4314_fdef.restitution = 0.8f;
path4314_fdef.filter.groupIndex = -1;

path4314_body->CreateFixture(&path4314_fdef);
bodylist.push_back(path4314_body);
uData* path4314_ud = new uData();
path4314_ud->name = "TITLE";
path4314_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4314_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4314_ud->strokewidth=24.23800087f;
path4314_body->SetUserData(path4314_ud);

//
//path4312
//
b2BodyDef path4312_bdef;
path4312_bdef.type = b2_staticBody;
path4312_bdef.bullet = false;
path4312_bdef.position.Set(13.3780184278f,16.3722178792f);
path4312_bdef.linearDamping = 0.0f;
path4312_bdef.angularDamping =0.0f;
b2Body* path4312_body = m_world->CreateBody(&path4312_bdef);

b2CircleShape path4312_s;
path4312_s.m_radius=0.318572368514f;

b2FixtureDef path4312_fdef;
path4312_fdef.shape = &path4312_s;

path4312_fdef.density = 1.0f;
path4312_fdef.friction = 0.1f;
path4312_fdef.restitution = 0.8f;
path4312_fdef.filter.groupIndex = -1;

path4312_body->CreateFixture(&path4312_fdef);
bodylist.push_back(path4312_body);
uData* path4312_ud = new uData();
path4312_ud->name = "TITLE";
path4312_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4312_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4312_ud->strokewidth=24.23800087f;
path4312_body->SetUserData(path4312_ud);

//
//path4310
//
b2BodyDef path4310_bdef;
path4310_bdef.type = b2_staticBody;
path4310_bdef.bullet = false;
path4310_bdef.position.Set(13.4062121769f,17.3308352422f);
path4310_bdef.linearDamping = 0.0f;
path4310_bdef.angularDamping =0.0f;
b2Body* path4310_body = m_world->CreateBody(&path4310_bdef);

b2CircleShape path4310_s;
path4310_s.m_radius=0.318572368514f;

b2FixtureDef path4310_fdef;
path4310_fdef.shape = &path4310_s;

path4310_fdef.density = 1.0f;
path4310_fdef.friction = 0.1f;
path4310_fdef.restitution = 0.8f;
path4310_fdef.filter.groupIndex = -1;

path4310_body->CreateFixture(&path4310_fdef);
bodylist.push_back(path4310_body);
uData* path4310_ud = new uData();
path4310_ud->name = "TITLE";
path4310_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4310_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4310_ud->strokewidth=24.23800087f;
path4310_body->SetUserData(path4310_ud);

//
//path3934
//
b2BodyDef path3934_bdef;
path3934_bdef.type = b2_staticBody;
path3934_bdef.bullet = false;
path3934_bdef.position.Set(-13.2629238061f,18.9872455242f);
path3934_bdef.linearDamping = 0.0f;
path3934_bdef.angularDamping =0.0f;
b2Body* path3934_body = m_world->CreateBody(&path3934_bdef);

b2CircleShape path3934_s;
path3934_s.m_radius=0.318572368514f;

b2FixtureDef path3934_fdef;
path3934_fdef.shape = &path3934_s;

path3934_fdef.density = 1.0f;
path3934_fdef.friction = 0.1f;
path3934_fdef.restitution = 0.8f;
path3934_fdef.filter.groupIndex = -1;

path3934_body->CreateFixture(&path3934_fdef);
bodylist.push_back(path3934_body);
uData* path3934_ud = new uData();
path3934_ud->name = "TITLE";
path3934_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path3934_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path3934_ud->strokewidth=24.23800087f;
path3934_body->SetUserData(path3934_ud);

//
//path3916
//
b2BodyDef path3916_bdef;
path3916_bdef.type = b2_staticBody;
path3916_bdef.bullet = false;
path3916_bdef.position.Set(13.4485039697f,18.0920903641f);
path3916_bdef.linearDamping = 0.0f;
path3916_bdef.angularDamping =0.0f;
b2Body* path3916_body = m_world->CreateBody(&path3916_bdef);

b2CircleShape path3916_s;
path3916_s.m_radius=0.318572368514f;

b2FixtureDef path3916_fdef;
path3916_fdef.shape = &path3916_s;

path3916_fdef.density = 1.0f;
path3916_fdef.friction = 0.1f;
path3916_fdef.restitution = 0.8f;
path3916_fdef.filter.groupIndex = -1;

path3916_body->CreateFixture(&path3916_fdef);
bodylist.push_back(path3916_body);
uData* path3916_ud = new uData();
path3916_ud->name = "TITLE";
path3916_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path3916_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path3916_ud->strokewidth=24.23800087f;
path3916_body->SetUserData(path3916_ud);

//
//path4306
//
b2BodyDef path4306_bdef;
path4306_bdef.type = b2_staticBody;
path4306_bdef.bullet = false;
path4306_bdef.position.Set(13.4203102205f,18.9943185104f);
path4306_bdef.linearDamping = 0.0f;
path4306_bdef.angularDamping =0.0f;
b2Body* path4306_body = m_world->CreateBody(&path4306_bdef);

b2CircleShape path4306_s;
path4306_s.m_radius=0.318572368514f;

b2FixtureDef path4306_fdef;
path4306_fdef.shape = &path4306_s;

path4306_fdef.density = 1.0f;
path4306_fdef.friction = 0.1f;
path4306_fdef.restitution = 0.8f;
path4306_fdef.filter.groupIndex = -1;

path4306_body->CreateFixture(&path4306_fdef);
bodylist.push_back(path4306_body);
uData* path4306_ud = new uData();
path4306_ud->name = "TITLE";
path4306_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4306_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4306_ud->strokewidth=24.23800087f;
path4306_body->SetUserData(path4306_ud);

//
//path4304
//
b2BodyDef path4304_bdef;
path4304_bdef.type = b2_staticBody;
path4304_bdef.bullet = false;
path4304_bdef.position.Set(10.9955718071f,18.7828588452f);
path4304_bdef.linearDamping = 0.0f;
path4304_bdef.angularDamping =0.0f;
b2Body* path4304_body = m_world->CreateBody(&path4304_bdef);

b2CircleShape path4304_s;
path4304_s.m_radius=0.318572368514f;

b2FixtureDef path4304_fdef;
path4304_fdef.shape = &path4304_s;

path4304_fdef.density = 1.0f;
path4304_fdef.friction = 0.1f;
path4304_fdef.restitution = 0.8f;
path4304_fdef.filter.groupIndex = -1;

path4304_body->CreateFixture(&path4304_fdef);
bodylist.push_back(path4304_body);
uData* path4304_ud = new uData();
path4304_ud->name = "TITLE";
path4304_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4304_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4304_ud->strokewidth=24.23800087f;
path4304_body->SetUserData(path4304_ud);

//
//path4302
//
b2BodyDef path4302_bdef;
path4302_bdef.type = b2_staticBody;
path4302_bdef.bullet = false;
path4302_bdef.position.Set(11.6158529928f,18.2753554618f);
path4302_bdef.linearDamping = 0.0f;
path4302_bdef.angularDamping =0.0f;
b2Body* path4302_body = m_world->CreateBody(&path4302_bdef);

b2CircleShape path4302_s;
path4302_s.m_radius=0.318572368514f;

b2FixtureDef path4302_fdef;
path4302_fdef.shape = &path4302_s;

path4302_fdef.density = 1.0f;
path4302_fdef.friction = 0.1f;
path4302_fdef.restitution = 0.8f;
path4302_fdef.filter.groupIndex = -1;

path4302_body->CreateFixture(&path4302_fdef);
bodylist.push_back(path4302_body);
uData* path4302_ud = new uData();
path4302_ud->name = "TITLE";
path4302_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4302_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4302_ud->strokewidth=24.23800087f;
path4302_body->SetUserData(path4302_ud);

//
//path4300
//
b2BodyDef path4300_bdef;
path4300_bdef.type = b2_staticBody;
path4300_bdef.bullet = false;
path4300_bdef.position.Set(12.241323334f,16.9237040023f);
path4300_bdef.linearDamping = 0.0f;
path4300_bdef.angularDamping =0.0f;
b2Body* path4300_body = m_world->CreateBody(&path4300_bdef);

b2CircleShape path4300_s;
path4300_s.m_radius=0.318572368514f;

b2FixtureDef path4300_fdef;
path4300_fdef.shape = &path4300_s;

path4300_fdef.density = 1.0f;
path4300_fdef.friction = 0.1f;
path4300_fdef.restitution = 0.8f;
path4300_fdef.filter.groupIndex = -1;

path4300_body->CreateFixture(&path4300_fdef);
bodylist.push_back(path4300_body);
uData* path4300_ud = new uData();
path4300_ud->name = "TITLE";
path4300_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4300_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4300_ud->strokewidth=24.23800087f;
path4300_body->SetUserData(path4300_ud);

//
//path3954
//
b2BodyDef path3954_bdef;
path3954_bdef.type = b2_staticBody;
path3954_bdef.bullet = false;
path3954_bdef.position.Set(12.0387732583f,17.6326306817f);
path3954_bdef.linearDamping = 0.0f;
path3954_bdef.angularDamping =0.0f;
b2Body* path3954_body = m_world->CreateBody(&path3954_bdef);

b2CircleShape path3954_s;
path3954_s.m_radius=0.318572368514f;

b2FixtureDef path3954_fdef;
path3954_fdef.shape = &path3954_s;

path3954_fdef.density = 1.0f;
path3954_fdef.friction = 0.1f;
path3954_fdef.restitution = 0.8f;
path3954_fdef.filter.groupIndex = -1;

path3954_body->CreateFixture(&path3954_fdef);
bodylist.push_back(path3954_body);
uData* path3954_ud = new uData();
path3954_ud->name = "TITLE";
path3954_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path3954_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path3954_ud->strokewidth=24.23800087f;
path3954_body->SetUserData(path3954_ud);

//
//path4296
//
b2BodyDef path4296_bdef;
path4296_bdef.type = b2_staticBody;
path4296_bdef.bullet = false;
path4296_bdef.position.Set(12.0246752148f,16.1466608719f);
path4296_bdef.linearDamping = 0.0f;
path4296_bdef.angularDamping =0.0f;
b2Body* path4296_body = m_world->CreateBody(&path4296_bdef);

b2CircleShape path4296_s;
path4296_s.m_radius=0.318572368514f;

b2FixtureDef path4296_fdef;
path4296_fdef.shape = &path4296_s;

path4296_fdef.density = 1.0f;
path4296_fdef.friction = 0.1f;
path4296_fdef.restitution = 0.8f;
path4296_fdef.filter.groupIndex = -1;

path4296_body->CreateFixture(&path4296_fdef);
bodylist.push_back(path4296_body);
uData* path4296_ud = new uData();
path4296_ud->name = "TITLE";
path4296_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4296_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4296_ud->strokewidth=24.23800087f;
path4296_body->SetUserData(path4296_ud);

//
//path4294
//
b2BodyDef path4294_bdef;
path4294_bdef.type = b2_staticBody;
path4294_bdef.bullet = false;
path4294_bdef.position.Set(11.6863397037f,15.4981844173f);
path4294_bdef.linearDamping = 0.0f;
path4294_bdef.angularDamping =0.0f;
b2Body* path4294_body = m_world->CreateBody(&path4294_bdef);

b2CircleShape path4294_s;
path4294_s.m_radius=0.318572368514f;

b2FixtureDef path4294_fdef;
path4294_fdef.shape = &path4294_s;

path4294_fdef.density = 1.0f;
path4294_fdef.friction = 0.1f;
path4294_fdef.restitution = 0.8f;
path4294_fdef.filter.groupIndex = -1;

path4294_body->CreateFixture(&path4294_fdef);
bodylist.push_back(path4294_body);
uData* path4294_ud = new uData();
path4294_ud->name = "TITLE";
path4294_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4294_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4294_ud->strokewidth=24.23800087f;
path4294_body->SetUserData(path4294_ud);

//
//path4292
//
b2BodyDef path4292_bdef;
path4292_bdef.type = b2_staticBody;
path4292_bdef.bullet = false;
path4292_bdef.position.Set(11.2070319399f,15.0893623006f);
path4292_bdef.linearDamping = 0.0f;
path4292_bdef.angularDamping =0.0f;
b2Body* path4292_body = m_world->CreateBody(&path4292_bdef);

b2CircleShape path4292_s;
path4292_s.m_radius=0.318572368514f;

b2FixtureDef path4292_fdef;
path4292_fdef.shape = &path4292_s;

path4292_fdef.density = 1.0f;
path4292_fdef.friction = 0.1f;
path4292_fdef.restitution = 0.8f;
path4292_fdef.filter.groupIndex = -1;

path4292_body->CreateFixture(&path4292_fdef);
bodylist.push_back(path4292_body);
uData* path4292_ud = new uData();
path4292_ud->name = "TITLE";
path4292_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4292_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4292_ud->strokewidth=24.23800087f;
path4292_body->SetUserData(path4292_ud);

//
//path4290
//
b2BodyDef path4290_bdef;
path4290_bdef.type = b2_staticBody;
path4290_bdef.bullet = false;
path4290_bdef.position.Set(10.4316792888f,14.8074159479f);
path4290_bdef.linearDamping = 0.0f;
path4290_bdef.angularDamping =0.0f;
b2Body* path4290_body = m_world->CreateBody(&path4290_bdef);

b2CircleShape path4290_s;
path4290_s.m_radius=0.318572368514f;

b2FixtureDef path4290_fdef;
path4290_fdef.shape = &path4290_s;

path4290_fdef.density = 1.0f;
path4290_fdef.friction = 0.1f;
path4290_fdef.restitution = 0.8f;
path4290_fdef.filter.groupIndex = -1;

path4290_body->CreateFixture(&path4290_fdef);
bodylist.push_back(path4290_body);
uData* path4290_ud = new uData();
path4290_ud->name = "TITLE";
path4290_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4290_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4290_ud->strokewidth=24.23800087f;
path4290_body->SetUserData(path4290_ud);

//
//path4288
//
b2BodyDef path4288_bdef;
path4288_bdef.type = b2_staticBody;
path4288_bdef.bullet = false;
path4288_bdef.position.Set(9.69861843045f,14.8779025419f);
path4288_bdef.linearDamping = 0.0f;
path4288_bdef.angularDamping =0.0f;
b2Body* path4288_body = m_world->CreateBody(&path4288_bdef);

b2CircleShape path4288_s;
path4288_s.m_radius=0.318572368514f;

b2FixtureDef path4288_fdef;
path4288_fdef.shape = &path4288_s;

path4288_fdef.density = 1.0f;
path4288_fdef.friction = 0.1f;
path4288_fdef.restitution = 0.8f;
path4288_fdef.filter.groupIndex = -1;

path4288_body->CreateFixture(&path4288_fdef);
bodylist.push_back(path4288_body);
uData* path4288_ud = new uData();
path4288_ud->name = "TITLE";
path4288_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4288_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4288_ud->strokewidth=24.23800087f;
path4288_body->SetUserData(path4288_ud);

//
//path4286
//
b2BodyDef path4286_bdef;
path4286_bdef.type = b2_staticBody;
path4286_bdef.bullet = false;
path4286_bdef.position.Set(9.07833724477f,15.4981844173f);
path4286_bdef.linearDamping = 0.0f;
path4286_bdef.angularDamping =0.0f;
b2Body* path4286_body = m_world->CreateBody(&path4286_bdef);

b2CircleShape path4286_s;
path4286_s.m_radius=0.318572368514f;

b2FixtureDef path4286_fdef;
path4286_fdef.shape = &path4286_s;

path4286_fdef.density = 1.0f;
path4286_fdef.friction = 0.1f;
path4286_fdef.restitution = 0.8f;
path4286_fdef.filter.groupIndex = -1;

path4286_body->CreateFixture(&path4286_fdef);
bodylist.push_back(path4286_body);
uData* path4286_ud = new uData();
path4286_ud->name = "TITLE";
path4286_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4286_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4286_ud->strokewidth=24.23800087f;
path4286_body->SetUserData(path4286_ud);

//
//path4284
//
b2BodyDef path4284_bdef;
path4284_bdef.type = b2_staticBody;
path4284_bdef.bullet = false;
path4284_bdef.position.Set(8.69770994101f,16.1325635297f);
path4284_bdef.linearDamping = 0.0f;
path4284_bdef.angularDamping =0.0f;
b2Body* path4284_body = m_world->CreateBody(&path4284_bdef);

b2CircleShape path4284_s;
path4284_s.m_radius=0.318572368514f;

b2FixtureDef path4284_fdef;
path4284_fdef.shape = &path4284_s;

path4284_fdef.density = 1.0f;
path4284_fdef.friction = 0.1f;
path4284_fdef.restitution = 0.8f;
path4284_fdef.filter.groupIndex = -1;

path4284_body->CreateFixture(&path4284_fdef);
bodylist.push_back(path4284_body);
uData* path4284_ud = new uData();
path4284_ud->name = "TITLE";
path4284_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4284_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4284_ud->strokewidth=24.23800087f;
path4284_body->SetUserData(path4284_ud);

//
//path4282
//
b2BodyDef path4282_bdef;
path4282_bdef.type = b2_staticBody;
path4282_bdef.bullet = false;
path4282_bdef.position.Set(8.59902831193f,16.8092346686f);
path4282_bdef.linearDamping = 0.0f;
path4282_bdef.angularDamping =0.0f;
b2Body* path4282_body = m_world->CreateBody(&path4282_bdef);

b2CircleShape path4282_s;
path4282_s.m_radius=0.318572368514f;

b2FixtureDef path4282_fdef;
path4282_fdef.shape = &path4282_s;

path4282_fdef.density = 1.0f;
path4282_fdef.friction = 0.1f;
path4282_fdef.restitution = 0.8f;
path4282_fdef.filter.groupIndex = -1;

path4282_body->CreateFixture(&path4282_fdef);
bodylist.push_back(path4282_body);
uData* path4282_ud = new uData();
path4282_ud->name = "TITLE";
path4282_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4282_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4282_ud->strokewidth=24.23800087f;
path4282_body->SetUserData(path4282_ud);

//
//path4280
//
b2BodyDef path4280_bdef;
path4280_bdef.type = b2_staticBody;
path4280_bdef.bullet = false;
path4280_bdef.position.Set(8.69770994101f,17.6268788903f);
path4280_bdef.linearDamping = 0.0f;
path4280_bdef.angularDamping =0.0f;
b2Body* path4280_body = m_world->CreateBody(&path4280_bdef);

b2CircleShape path4280_s;
path4280_s.m_radius=0.318572368514f;

b2FixtureDef path4280_fdef;
path4280_fdef.shape = &path4280_s;

path4280_fdef.density = 1.0f;
path4280_fdef.friction = 0.1f;
path4280_fdef.restitution = 0.8f;
path4280_fdef.filter.groupIndex = -1;

path4280_body->CreateFixture(&path4280_fdef);
bodylist.push_back(path4280_body);
uData* path4280_ud = new uData();
path4280_ud->name = "TITLE";
path4280_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4280_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4280_ud->strokewidth=24.23800087f;
path4280_body->SetUserData(path4280_ud);

//
//path4278
//
b2BodyDef path4278_bdef;
path4278_bdef.type = b2_staticBody;
path4278_bdef.bullet = false;
path4278_bdef.position.Set(9.06423920119f,18.2471608943f);
path4278_bdef.linearDamping = 0.0f;
path4278_bdef.angularDamping =0.0f;
b2Body* path4278_body = m_world->CreateBody(&path4278_bdef);

b2CircleShape path4278_s;
path4278_s.m_radius=0.318572368514f;

b2FixtureDef path4278_fdef;
path4278_fdef.shape = &path4278_s;

path4278_fdef.density = 1.0f;
path4278_fdef.friction = 0.1f;
path4278_fdef.restitution = 0.8f;
path4278_fdef.filter.groupIndex = -1;

path4278_body->CreateFixture(&path4278_fdef);
bodylist.push_back(path4278_body);
uData* path4278_ud = new uData();
path4278_ud->name = "TITLE";
path4278_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4278_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4278_ud->strokewidth=24.23800087f;
path4278_body->SetUserData(path4278_ud);

//
//path4276
//
b2BodyDef path4276_bdef;
path4276_bdef.type = b2_staticBody;
path4276_bdef.bullet = false;
path4276_bdef.position.Set(9.67042351229f,18.7123722512f);
path4276_bdef.linearDamping = 0.0f;
path4276_bdef.angularDamping =0.0f;
b2Body* path4276_body = m_world->CreateBody(&path4276_bdef);

b2CircleShape path4276_s;
path4276_s.m_radius=0.318572368514f;

b2FixtureDef path4276_fdef;
path4276_fdef.shape = &path4276_s;

path4276_fdef.density = 1.0f;
path4276_fdef.friction = 0.1f;
path4276_fdef.restitution = 0.8f;
path4276_fdef.filter.groupIndex = -1;

path4276_body->CreateFixture(&path4276_fdef);
bodylist.push_back(path4276_body);
uData* path4276_ud = new uData();
path4276_ud->name = "TITLE";
path4276_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4276_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4276_ud->strokewidth=24.23800087f;
path4276_body->SetUserData(path4276_ud);

//
//path4272
//
b2BodyDef path4272_bdef;
path4272_bdef.type = b2_staticBody;
path4272_bdef.bullet = false;
path4272_bdef.position.Set(10.3329976597f,18.9661239429f);
path4272_bdef.linearDamping = 0.0f;
path4272_bdef.angularDamping =0.0f;
b2Body* path4272_body = m_world->CreateBody(&path4272_bdef);

b2CircleShape path4272_s;
path4272_s.m_radius=0.318572368514f;

b2FixtureDef path4272_fdef;
path4272_fdef.shape = &path4272_s;

path4272_fdef.density = 1.0f;
path4272_fdef.friction = 0.1f;
path4272_fdef.restitution = 0.8f;
path4272_fdef.filter.groupIndex = -1;

path4272_body->CreateFixture(&path4272_fdef);
bodylist.push_back(path4272_body);
uData* path4272_ud = new uData();
path4272_ud->name = "TITLE";
path4272_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4272_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4272_ud->strokewidth=24.23800087f;
path4272_body->SetUserData(path4272_ud);

//
//path4270
//
b2BodyDef path4270_bdef;
path4270_bdef.type = b2_staticBody;
path4270_bdef.bullet = false;
path4270_bdef.position.Set(5.49761887653f,18.9520266007f);
path4270_bdef.linearDamping = 0.0f;
path4270_bdef.angularDamping =0.0f;
b2Body* path4270_body = m_world->CreateBody(&path4270_bdef);

b2CircleShape path4270_s;
path4270_s.m_radius=0.318572368514f;

b2FixtureDef path4270_fdef;
path4270_fdef.shape = &path4270_s;

path4270_fdef.density = 1.0f;
path4270_fdef.friction = 0.1f;
path4270_fdef.restitution = 0.8f;
path4270_fdef.filter.groupIndex = -1;

path4270_body->CreateFixture(&path4270_fdef);
bodylist.push_back(path4270_body);
uData* path4270_ud = new uData();
path4270_ud->name = "TITLE";
path4270_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4270_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4270_ud->strokewidth=24.23800087f;
path4270_body->SetUserData(path4270_ud);

//
//path4185
//
b2BodyDef path4185_bdef;
path4185_bdef.type = b2_staticBody;
path4185_bdef.bullet = false;
path4185_bdef.position.Set(6.27281137572f,18.7502640749f);
path4185_bdef.linearDamping = 0.0f;
path4185_bdef.angularDamping =0.0f;
b2Body* path4185_body = m_world->CreateBody(&path4185_bdef);

b2CircleShape path4185_s;
path4185_s.m_radius=0.318572368514f;

b2FixtureDef path4185_fdef;
path4185_fdef.shape = &path4185_s;

path4185_fdef.density = 1.0f;
path4185_fdef.friction = 0.1f;
path4185_fdef.restitution = 0.8f;
path4185_fdef.filter.groupIndex = -1;

path4185_body->CreateFixture(&path4185_fdef);
bodylist.push_back(path4185_body);
uData* path4185_ud = new uData();
path4185_ud->name = "TITLE";
path4185_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4185_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4185_ud->strokewidth=24.23800087f;
path4185_body->SetUserData(path4185_ud);

//
//path4183
//
b2BodyDef path4183_bdef;
path4183_bdef.type = b2_staticBody;
path4183_bdef.bullet = false;
path4183_bdef.position.Set(6.80376520635f,18.2193101274f);
path4183_bdef.linearDamping = 0.0f;
path4183_bdef.angularDamping =0.0f;
b2Body* path4183_body = m_world->CreateBody(&path4183_bdef);

b2CircleShape path4183_s;
path4183_s.m_radius=0.318572368514f;

b2FixtureDef path4183_fdef;
path4183_fdef.shape = &path4183_s;

path4183_fdef.density = 1.0f;
path4183_fdef.friction = 0.1f;
path4183_fdef.restitution = 0.8f;
path4183_fdef.filter.groupIndex = -1;

path4183_body->CreateFixture(&path4183_fdef);
bodylist.push_back(path4183_body);
uData* path4183_ud = new uData();
path4183_ud->name = "TITLE";
path4183_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4183_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4183_ud->strokewidth=24.23800087f;
path4183_body->SetUserData(path4183_ud);

//
//path4181
//
b2BodyDef path4181_bdef;
path4181_bdef.type = b2_staticBody;
path4181_bdef.bullet = false;
path4181_bdef.position.Set(7.15419510864f,17.6883561799f);
path4181_bdef.linearDamping = 0.0f;
path4181_bdef.angularDamping =0.0f;
b2Body* path4181_body = m_world->CreateBody(&path4181_bdef);

b2CircleShape path4181_s;
path4181_s.m_radius=0.318572368514f;

b2FixtureDef path4181_fdef;
path4181_fdef.shape = &path4181_s;

path4181_fdef.density = 1.0f;
path4181_fdef.friction = 0.1f;
path4181_fdef.restitution = 0.8f;
path4181_fdef.filter.groupIndex = -1;

path4181_body->CreateFixture(&path4181_fdef);
bodylist.push_back(path4181_body);
uData* path4181_ud = new uData();
path4181_ud->name = "TITLE";
path4181_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4181_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4181_ud->strokewidth=24.23800087f;
path4181_body->SetUserData(path4181_ud);

//
//path4179
//
b2BodyDef path4179_bdef;
path4179_bdef.type = b2_staticBody;
path4179_bdef.bullet = false;
path4179_bdef.position.Set(7.29224254348f,16.8494489288f);
path4179_bdef.linearDamping = 0.0f;
path4179_bdef.angularDamping =0.0f;
b2Body* path4179_body = m_world->CreateBody(&path4179_bdef);

b2CircleShape path4179_s;
path4179_s.m_radius=0.318572368514f;

b2FixtureDef path4179_fdef;
path4179_fdef.shape = &path4179_s;

path4179_fdef.density = 1.0f;
path4179_fdef.friction = 0.1f;
path4179_fdef.restitution = 0.8f;
path4179_fdef.filter.groupIndex = -1;

path4179_body->CreateFixture(&path4179_fdef);
bodylist.push_back(path4179_body);
uData* path4179_ud = new uData();
path4179_ud->name = "TITLE";
path4179_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4179_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4179_ud->strokewidth=24.23800087f;
path4179_body->SetUserData(path4179_ud);

//
//path4177
//
b2BodyDef path4177_bdef;
path4177_bdef.type = b2_staticBody;
path4177_bdef.bullet = false;
path4177_bdef.position.Set(7.18605247875f,16.1804469619f);
path4177_bdef.linearDamping = 0.0f;
path4177_bdef.angularDamping =0.0f;
b2Body* path4177_body = m_world->CreateBody(&path4177_bdef);

b2CircleShape path4177_s;
path4177_s.m_radius=0.318572368514f;

b2FixtureDef path4177_fdef;
path4177_fdef.shape = &path4177_s;

path4177_fdef.density = 1.0f;
path4177_fdef.friction = 0.1f;
path4177_fdef.restitution = 0.8f;
path4177_fdef.filter.groupIndex = -1;

path4177_body->CreateFixture(&path4177_fdef);
bodylist.push_back(path4177_body);
uData* path4177_ud = new uData();
path4177_ud->name = "TITLE";
path4177_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4177_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4177_ud->strokewidth=24.23800087f;
path4177_body->SetUserData(path4177_ud);

//
//path4175
//
b2BodyDef path4175_bdef;
path4175_bdef.type = b2_staticBody;
path4175_bdef.bullet = false;
path4175_bdef.position.Set(6.79314608298f,15.511444995f);
path4175_bdef.linearDamping = 0.0f;
path4175_bdef.angularDamping =0.0f;
b2Body* path4175_body = m_world->CreateBody(&path4175_bdef);

b2CircleShape path4175_s;
path4175_s.m_radius=0.318572368514f;

b2FixtureDef path4175_fdef;
path4175_fdef.shape = &path4175_s;

path4175_fdef.density = 1.0f;
path4175_fdef.friction = 0.1f;
path4175_fdef.restitution = 0.8f;
path4175_fdef.filter.groupIndex = -1;

path4175_body->CreateFixture(&path4175_fdef);
bodylist.push_back(path4175_body);
uData* path4175_ud = new uData();
path4175_ud->name = "TITLE";
path4175_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4175_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4175_ud->strokewidth=24.23800087f;
path4175_body->SetUserData(path4175_ud);

//
//path4173
//
b2BodyDef path4173_bdef;
path4173_bdef.type = b2_staticBody;
path4173_bdef.bullet = false;
path4173_bdef.position.Set(6.27281137572f,15.0335864423f);
path4173_bdef.linearDamping = 0.0f;
path4173_bdef.angularDamping =0.0f;
b2Body* path4173_body = m_world->CreateBody(&path4173_bdef);

b2CircleShape path4173_s;
path4173_s.m_radius=0.318572368514f;

b2FixtureDef path4173_fdef;
path4173_fdef.shape = &path4173_s;

path4173_fdef.density = 1.0f;
path4173_fdef.friction = 0.1f;
path4173_fdef.restitution = 0.8f;
path4173_fdef.filter.groupIndex = -1;

path4173_body->CreateFixture(&path4173_fdef);
bodylist.push_back(path4173_body);
uData* path4173_ud = new uData();
path4173_ud->name = "TITLE";
path4173_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4173_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4173_ud->strokewidth=24.23800087f;
path4173_body->SetUserData(path4173_ud);

//
//path4171
//
b2BodyDef path4171_bdef;
path4171_bdef.type = b2_staticBody;
path4171_bdef.bullet = false;
path4171_bdef.position.Set(5.54009537002f,14.8105857866f);
path4171_bdef.linearDamping = 0.0f;
path4171_bdef.angularDamping =0.0f;
b2Body* path4171_body = m_world->CreateBody(&path4171_bdef);

b2CircleShape path4171_s;
path4171_s.m_radius=0.318572368514f;

b2FixtureDef path4171_fdef;
path4171_fdef.shape = &path4171_s;

path4171_fdef.density = 1.0f;
path4171_fdef.friction = 0.1f;
path4171_fdef.restitution = 0.8f;
path4171_fdef.filter.groupIndex = -1;

path4171_body->CreateFixture(&path4171_fdef);
bodylist.push_back(path4171_body);
uData* path4171_ud = new uData();
path4171_ud->name = "TITLE";
path4171_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4171_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4171_ud->strokewidth=24.23800087f;
path4171_body->SetUserData(path4171_ud);

//
//path4169
//
b2BodyDef path4169_bdef;
path4169_bdef.type = b2_staticBody;
path4169_bdef.bullet = false;
path4169_bdef.position.Set(4.75428257846f,15.0654436721f);
path4169_bdef.linearDamping = 0.0f;
path4169_bdef.angularDamping =0.0f;
b2Body* path4169_body = m_world->CreateBody(&path4169_bdef);

b2CircleShape path4169_s;
path4169_s.m_radius=0.318572368514f;

b2FixtureDef path4169_fdef;
path4169_fdef.shape = &path4169_s;

path4169_fdef.density = 1.0f;
path4169_fdef.friction = 0.1f;
path4169_fdef.restitution = 0.8f;
path4169_fdef.filter.groupIndex = -1;

path4169_body->CreateFixture(&path4169_fdef);
bodylist.push_back(path4169_body);
uData* path4169_ud = new uData();
path4169_ud->name = "TITLE";
path4169_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4169_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4169_ud->strokewidth=24.23800087f;
path4169_body->SetUserData(path4169_ud);

//
//path4167
//
b2BodyDef path4167_bdef;
path4167_bdef.type = b2_staticBody;
path4167_bdef.bullet = false;
path4167_bdef.position.Set(4.26580524133f,15.458349612f);
path4167_bdef.linearDamping = 0.0f;
path4167_bdef.angularDamping =0.0f;
b2Body* path4167_body = m_world->CreateBody(&path4167_bdef);

b2CircleShape path4167_s;
path4167_s.m_radius=0.318572368514f;

b2FixtureDef path4167_fdef;
path4167_fdef.shape = &path4167_s;

path4167_fdef.density = 1.0f;
path4167_fdef.friction = 0.1f;
path4167_fdef.restitution = 0.8f;
path4167_fdef.filter.groupIndex = -1;

path4167_body->CreateFixture(&path4167_fdef);
bodylist.push_back(path4167_body);
uData* path4167_ud = new uData();
path4167_ud->name = "TITLE";
path4167_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4167_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4167_ud->strokewidth=24.23800087f;
path4167_body->SetUserData(path4167_ud);

//
//path4165
//
b2BodyDef path4165_bdef;
path4165_bdef.type = b2_staticBody;
path4165_bdef.bullet = false;
path4165_bdef.position.Set(3.83042352105f,16.0954943256f);
path4165_bdef.linearDamping = 0.0f;
path4165_bdef.angularDamping =0.0f;
b2Body* path4165_body = m_world->CreateBody(&path4165_bdef);

b2CircleShape path4165_s;
path4165_s.m_radius=0.318572368514f;

b2FixtureDef path4165_fdef;
path4165_fdef.shape = &path4165_s;

path4165_fdef.density = 1.0f;
path4165_fdef.friction = 0.1f;
path4165_fdef.restitution = 0.8f;
path4165_fdef.filter.groupIndex = -1;

path4165_body->CreateFixture(&path4165_fdef);
bodylist.push_back(path4165_body);
uData* path4165_ud = new uData();
path4165_ud->name = "TITLE";
path4165_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4165_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4165_ud->strokewidth=24.23800087f;
path4165_body->SetUserData(path4165_ud);

//
//path4163
//
b2BodyDef path4163_bdef;
path4163_bdef.type = b2_staticBody;
path4163_bdef.bullet = false;
path4163_bdef.position.Set(3.74547053408f,16.881306182f);
path4163_bdef.linearDamping = 0.0f;
path4163_bdef.angularDamping =0.0f;
b2Body* path4163_body = m_world->CreateBody(&path4163_bdef);

b2CircleShape path4163_s;
path4163_s.m_radius=0.318572368514f;

b2FixtureDef path4163_fdef;
path4163_fdef.shape = &path4163_s;

path4163_fdef.density = 1.0f;
path4163_fdef.friction = 0.1f;
path4163_fdef.restitution = 0.8f;
path4163_fdef.filter.groupIndex = -1;

path4163_body->CreateFixture(&path4163_fdef);
bodylist.push_back(path4163_body);
uData* path4163_ud = new uData();
path4163_ud->name = "TITLE";
path4163_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4163_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4163_ud->strokewidth=24.23800087f;
path4163_body->SetUserData(path4163_ud);

//
//path4157
//
b2BodyDef path4157_bdef;
path4157_bdef.type = b2_staticBody;
path4157_bdef.bullet = false;
path4157_bdef.position.Set(1.23936793917f,16.5521147602f);
path4157_bdef.linearDamping = 0.0f;
path4157_bdef.angularDamping =0.0f;
b2Body* path4157_body = m_world->CreateBody(&path4157_bdef);

b2CircleShape path4157_s;
path4157_s.m_radius=0.318572368514f;

b2FixtureDef path4157_fdef;
path4157_fdef.shape = &path4157_s;

path4157_fdef.density = 1.0f;
path4157_fdef.friction = 0.1f;
path4157_fdef.restitution = 0.8f;
path4157_fdef.filter.groupIndex = -1;

path4157_body->CreateFixture(&path4157_fdef);
bodylist.push_back(path4157_body);
uData* path4157_ud = new uData();
path4157_ud->name = "TITLE";
path4157_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4157_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4157_ud->strokewidth=24.23800087f;
path4157_body->SetUserData(path4157_ud);

//
//path4155
//
b2BodyDef path4155_bdef;
path4155_bdef.type = b2_staticBody;
path4155_bdef.bullet = false;
path4155_bdef.position.Set(1.86589271116f,16.5733528901f);
path4155_bdef.linearDamping = 0.0f;
path4155_bdef.angularDamping =0.0f;
b2Body* path4155_body = m_world->CreateBody(&path4155_bdef);

b2CircleShape path4155_s;
path4155_s.m_radius=0.318572368514f;

b2FixtureDef path4155_fdef;
path4155_fdef.shape = &path4155_s;

path4155_fdef.density = 1.0f;
path4155_fdef.friction = 0.1f;
path4155_fdef.restitution = 0.8f;
path4155_fdef.filter.groupIndex = -1;

path4155_body->CreateFixture(&path4155_fdef);
bodylist.push_back(path4155_body);
uData* path4155_ud = new uData();
path4155_ud->name = "TITLE";
path4155_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4155_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4155_ud->strokewidth=24.23800087f;
path4155_body->SetUserData(path4155_ud);

//
//path4153
//
b2BodyDef path4153_bdef;
path4153_bdef.type = b2_staticBody;
path4153_bdef.bullet = false;
path4153_bdef.position.Set(2.43932303527f,16.9662588183f);
path4153_bdef.linearDamping = 0.0f;
path4153_bdef.angularDamping =0.0f;
b2Body* path4153_body = m_world->CreateBody(&path4153_bdef);

b2CircleShape path4153_s;
path4153_s.m_radius=0.318572368514f;

b2FixtureDef path4153_fdef;
path4153_fdef.shape = &path4153_s;

path4153_fdef.density = 1.0f;
path4153_fdef.friction = 0.1f;
path4153_fdef.restitution = 0.8f;
path4153_fdef.filter.groupIndex = -1;

path4153_body->CreateFixture(&path4153_fdef);
bodylist.push_back(path4153_body);
uData* path4153_ud = new uData();
path4153_ud->name = "TITLE";
path4153_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4153_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4153_ud->strokewidth=24.23800087f;
path4153_body->SetUserData(path4153_ud);

//
//path4151
//
b2BodyDef path4151_bdef;
path4151_bdef.type = b2_staticBody;
path4151_bdef.bullet = false;
path4151_bdef.position.Set(2.51365689887f,17.6883561799f);
path4151_bdef.linearDamping = 0.0f;
path4151_bdef.angularDamping =0.0f;
b2Body* path4151_body = m_world->CreateBody(&path4151_bdef);

b2CircleShape path4151_s;
path4151_s.m_radius=0.318572368514f;

b2FixtureDef path4151_fdef;
path4151_fdef.shape = &path4151_s;

path4151_fdef.density = 1.0f;
path4151_fdef.friction = 0.1f;
path4151_fdef.restitution = 0.8f;
path4151_fdef.filter.groupIndex = -1;

path4151_body->CreateFixture(&path4151_fdef);
bodylist.push_back(path4151_body);
uData* path4151_ud = new uData();
path4151_ud->name = "TITLE";
path4151_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4151_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4151_ud->strokewidth=24.23800087f;
path4151_body->SetUserData(path4151_ud);

//
//path4149
//
b2BodyDef path4149_bdef;
path4149_bdef.type = b2_staticBody;
path4149_bdef.bullet = false;
path4149_bdef.position.Set(2.37560946403f,18.4104535298f);
path4149_bdef.linearDamping = 0.0f;
path4149_bdef.angularDamping =0.0f;
b2Body* path4149_body = m_world->CreateBody(&path4149_bdef);

b2CircleShape path4149_s;
path4149_s.m_radius=0.318572368514f;

b2FixtureDef path4149_fdef;
path4149_fdef.shape = &path4149_s;

path4149_fdef.density = 1.0f;
path4149_fdef.friction = 0.1f;
path4149_fdef.restitution = 0.8f;
path4149_fdef.filter.groupIndex = -1;

path4149_body->CreateFixture(&path4149_fdef);
bodylist.push_back(path4149_body);
uData* path4149_ud = new uData();
path4149_ud->name = "TITLE";
path4149_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4149_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4149_ud->strokewidth=24.23800087f;
path4149_body->SetUserData(path4149_ud);

//
//path4147
//
b2BodyDef path4147_bdef;
path4147_bdef.type = b2_staticBody;
path4147_bdef.bullet = false;
path4147_bdef.position.Set(1.85527358778f,18.8564548411f);
path4147_bdef.linearDamping = 0.0f;
path4147_bdef.angularDamping =0.0f;
b2Body* path4147_body = m_world->CreateBody(&path4147_bdef);

b2CircleShape path4147_s;
path4147_s.m_radius=0.318572368514f;

b2FixtureDef path4147_fdef;
path4147_fdef.shape = &path4147_s;

path4147_fdef.density = 1.0f;
path4147_fdef.friction = 0.1f;
path4147_fdef.restitution = 0.8f;
path4147_fdef.filter.groupIndex = -1;

path4147_body->CreateFixture(&path4147_fdef);
bodylist.push_back(path4147_body);
uData* path4147_ud = new uData();
path4147_ud->name = "TITLE";
path4147_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4147_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4147_ud->strokewidth=24.23800087f;
path4147_body->SetUserData(path4147_ud);

//
//path4145
//
b2BodyDef path4145_bdef;
path4145_bdef.type = b2_staticBody;
path4145_bdef.bullet = false;
path4145_bdef.position.Set(1.18627232231f,18.9307884709f);
path4145_bdef.linearDamping = 0.0f;
path4145_bdef.angularDamping =0.0f;
b2Body* path4145_body = m_world->CreateBody(&path4145_bdef);

b2CircleShape path4145_s;
path4145_s.m_radius=0.318572368514f;

b2FixtureDef path4145_fdef;
path4145_fdef.shape = &path4145_s;

path4145_fdef.density = 1.0f;
path4145_fdef.friction = 0.1f;
path4145_fdef.restitution = 0.8f;
path4145_fdef.filter.groupIndex = -1;

path4145_body->CreateFixture(&path4145_fdef);
bodylist.push_back(path4145_body);
uData* path4145_ud = new uData();
path4145_ud->name = "TITLE";
path4145_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4145_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4145_ud->strokewidth=24.23800087f;
path4145_body->SetUserData(path4145_ud);

//
//path4143
//
b2BodyDef path4143_bdef;
path4143_bdef.type = b2_staticBody;
path4143_bdef.bullet = false;
path4143_bdef.position.Set(0.496031641107f,18.8989312176f);
path4143_bdef.linearDamping = 0.0f;
path4143_bdef.angularDamping =0.0f;
b2Body* path4143_body = m_world->CreateBody(&path4143_bdef);

b2CircleShape path4143_s;
path4143_s.m_radius=0.318572368514f;

b2FixtureDef path4143_fdef;
path4143_fdef.shape = &path4143_s;

path4143_fdef.density = 1.0f;
path4143_fdef.friction = 0.1f;
path4143_fdef.restitution = 0.8f;
path4143_fdef.filter.groupIndex = -1;

path4143_body->CreateFixture(&path4143_fdef);
bodylist.push_back(path4143_body);
uData* path4143_ud = new uData();
path4143_ud->name = "TITLE";
path4143_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4143_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4143_ud->strokewidth=24.23800087f;
path4143_body->SetUserData(path4143_ud);

//
//path4141
//
b2BodyDef path4141_bdef;
path4141_bdef.type = b2_staticBody;
path4141_bdef.bullet = false;
path4141_bdef.position.Set(0.496031641107f,18.1131193613f);
path4141_bdef.linearDamping = 0.0f;
path4141_bdef.angularDamping =0.0f;
b2Body* path4141_body = m_world->CreateBody(&path4141_bdef);

b2CircleShape path4141_s;
path4141_s.m_radius=0.318572368514f;

b2FixtureDef path4141_fdef;
path4141_fdef.shape = &path4141_s;

path4141_fdef.density = 1.0f;
path4141_fdef.friction = 0.1f;
path4141_fdef.restitution = 0.8f;
path4141_fdef.filter.groupIndex = -1;

path4141_body->CreateFixture(&path4141_fdef);
bodylist.push_back(path4141_body);
uData* path4141_ud = new uData();
path4141_ud->name = "TITLE";
path4141_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4141_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4141_ud->strokewidth=24.23800087f;
path4141_body->SetUserData(path4141_ud);

//
//path4139
//
b2BodyDef path4139_bdef;
path4139_bdef.type = b2_staticBody;
path4139_bdef.bullet = false;
path4139_bdef.position.Set(0.527889011223f,17.3060693283f);
path4139_bdef.linearDamping = 0.0f;
path4139_bdef.angularDamping =0.0f;
b2Body* path4139_body = m_world->CreateBody(&path4139_bdef);

b2CircleShape path4139_s;
path4139_s.m_radius=0.318572368514f;

b2FixtureDef path4139_fdef;
path4139_fdef.shape = &path4139_s;

path4139_fdef.density = 1.0f;
path4139_fdef.friction = 0.1f;
path4139_fdef.restitution = 0.8f;
path4139_fdef.filter.groupIndex = -1;

path4139_body->CreateFixture(&path4139_fdef);
bodylist.push_back(path4139_body);
uData* path4139_ud = new uData();
path4139_ud->name = "TITLE";
path4139_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4139_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4139_ud->strokewidth=24.23800087f;
path4139_body->SetUserData(path4139_ud);

//
//path4137
//
b2BodyDef path4137_bdef;
path4137_bdef.type = b2_staticBody;
path4137_bdef.bullet = false;
path4137_bdef.position.Set(0.549127257967f,16.520257507f);
path4137_bdef.linearDamping = 0.0f;
path4137_bdef.angularDamping =0.0f;
b2Body* path4137_body = m_world->CreateBody(&path4137_bdef);

b2CircleShape path4137_s;
path4137_s.m_radius=0.318572368514f;

b2FixtureDef path4137_fdef;
path4137_fdef.shape = &path4137_s;

path4137_fdef.density = 1.0f;
path4137_fdef.friction = 0.1f;
path4137_fdef.restitution = 0.8f;
path4137_fdef.filter.groupIndex = -1;

path4137_body->CreateFixture(&path4137_fdef);
bodylist.push_back(path4137_body);
uData* path4137_ud = new uData();
path4137_ud->name = "TITLE";
path4137_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4137_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4137_ud->strokewidth=24.23800087f;
path4137_body->SetUserData(path4137_ud);

//
//path4135
//
b2BodyDef path4135_bdef;
path4135_bdef.type = b2_staticBody;
path4135_bdef.bullet = false;
path4135_bdef.position.Set(0.506650764479f,15.7450646571f);
path4135_bdef.linearDamping = 0.0f;
path4135_bdef.angularDamping =0.0f;
b2Body* path4135_body = m_world->CreateBody(&path4135_bdef);

b2CircleShape path4135_s;
path4135_s.m_radius=0.318572368514f;

b2FixtureDef path4135_fdef;
path4135_fdef.shape = &path4135_s;

path4135_fdef.density = 1.0f;
path4135_fdef.friction = 0.1f;
path4135_fdef.restitution = 0.8f;
path4135_fdef.filter.groupIndex = -1;

path4135_body->CreateFixture(&path4135_fdef);
bodylist.push_back(path4135_body);
uData* path4135_ud = new uData();
path4135_ud->name = "TITLE";
path4135_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4135_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4135_ud->strokewidth=24.23800087f;
path4135_body->SetUserData(path4135_ud);

//
//path4133
//
b2BodyDef path4133_bdef;
path4133_bdef.type = b2_staticBody;
path4133_bdef.bullet = false;
path4133_bdef.position.Set(0.549127257967f,14.9061574294f);
path4133_bdef.linearDamping = 0.0f;
path4133_bdef.angularDamping =0.0f;
b2Body* path4133_body = m_world->CreateBody(&path4133_bdef);

b2CircleShape path4133_s;
path4133_s.m_radius=0.318572368514f;

b2FixtureDef path4133_fdef;
path4133_fdef.shape = &path4133_s;

path4133_fdef.density = 1.0f;
path4133_fdef.friction = 0.1f;
path4133_fdef.restitution = 0.8f;
path4133_fdef.filter.groupIndex = -1;

path4133_body->CreateFixture(&path4133_fdef);
bodylist.push_back(path4133_body);
uData* path4133_ud = new uData();
path4133_ud->name = "TITLE";
path4133_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4133_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4133_ud->strokewidth=24.23800087f;
path4133_body->SetUserData(path4133_ud);

//
//path4268
//
b2BodyDef path4268_bdef;
path4268_bdef.type = b2_staticBody;
path4268_bdef.bullet = false;
path4268_bdef.position.Set(-0.725161701732f,14.9273956761f);
path4268_bdef.linearDamping = 0.0f;
path4268_bdef.angularDamping =0.0f;
b2Body* path4268_body = m_world->CreateBody(&path4268_bdef);

b2CircleShape path4268_s;
path4268_s.m_radius=0.318572368514f;

b2FixtureDef path4268_fdef;
path4268_fdef.shape = &path4268_s;

path4268_fdef.density = 1.0f;
path4268_fdef.friction = 0.1f;
path4268_fdef.restitution = 0.8f;
path4268_fdef.filter.groupIndex = -1;

path4268_body->CreateFixture(&path4268_fdef);
bodylist.push_back(path4268_body);
uData* path4268_ud = new uData();
path4268_ud->name = "TITLE";
path4268_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4268_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4268_ud->strokewidth=24.23800087f;
path4268_body->SetUserData(path4268_ud);

//
//path4266
//
b2BodyDef path4266_bdef;
path4266_bdef.type = b2_staticBody;
path4266_bdef.bullet = false;
path4266_bdef.position.Set(-1.48973507755f,14.9273956761f);
path4266_bdef.linearDamping = 0.0f;
path4266_bdef.angularDamping =0.0f;
b2Body* path4266_body = m_world->CreateBody(&path4266_bdef);

b2CircleShape path4266_s;
path4266_s.m_radius=0.318572368514f;

b2FixtureDef path4266_fdef;
path4266_fdef.shape = &path4266_s;

path4266_fdef.density = 1.0f;
path4266_fdef.friction = 0.1f;
path4266_fdef.restitution = 0.8f;
path4266_fdef.filter.groupIndex = -1;

path4266_body->CreateFixture(&path4266_fdef);
bodylist.push_back(path4266_body);
uData* path4266_ud = new uData();
path4266_ud->name = "TITLE";
path4266_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4266_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4266_ud->strokewidth=24.23800087f;
path4266_body->SetUserData(path4266_ud);

//
//path4264
//
b2BodyDef path4264_bdef;
path4264_bdef.type = b2_staticBody;
path4264_bdef.bullet = false;
path4264_bdef.position.Set(-2.19059464833f,14.8530620463f);
path4264_bdef.linearDamping = 0.0f;
path4264_bdef.angularDamping =0.0f;
b2Body* path4264_body = m_world->CreateBody(&path4264_bdef);

b2CircleShape path4264_s;
path4264_s.m_radius=0.318572368514f;

b2FixtureDef path4264_fdef;
path4264_fdef.shape = &path4264_s;

path4264_fdef.density = 1.0f;
path4264_fdef.friction = 0.1f;
path4264_fdef.restitution = 0.8f;
path4264_fdef.filter.groupIndex = -1;

path4264_body->CreateFixture(&path4264_fdef);
bodylist.push_back(path4264_body);
uData* path4264_ud = new uData();
path4264_ud->name = "TITLE";
path4264_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4264_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4264_ud->strokewidth=24.23800087f;
path4264_body->SetUserData(path4264_ud);

//
//path4262
//
b2BodyDef path4262_bdef;
path4262_bdef.type = b2_staticBody;
path4262_bdef.bullet = false;
path4262_bdef.position.Set(-2.21183277818f,15.6601120209f);
path4262_bdef.linearDamping = 0.0f;
path4262_bdef.angularDamping =0.0f;
b2Body* path4262_body = m_world->CreateBody(&path4262_bdef);

b2CircleShape path4262_s;
path4262_s.m_radius=0.318572368514f;

b2FixtureDef path4262_fdef;
path4262_fdef.shape = &path4262_s;

path4262_fdef.density = 1.0f;
path4262_fdef.friction = 0.1f;
path4262_fdef.restitution = 0.8f;
path4262_fdef.filter.groupIndex = -1;

path4262_body->CreateFixture(&path4262_fdef);
bodylist.push_back(path4262_body);
uData* path4262_ud = new uData();
path4262_ud->name = "TITLE";
path4262_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4262_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4262_ud->strokewidth=24.23800087f;
path4262_body->SetUserData(path4262_ud);

//
//path4260
//
b2BodyDef path4260_bdef;
path4260_bdef.type = b2_staticBody;
path4260_bdef.bullet = false;
path4260_bdef.position.Set(-2.17997552496f,16.4777811304f);
path4260_bdef.linearDamping = 0.0f;
path4260_bdef.angularDamping =0.0f;
b2Body* path4260_body = m_world->CreateBody(&path4260_bdef);

b2CircleShape path4260_s;
path4260_s.m_radius=0.318572368514f;

b2FixtureDef path4260_fdef;
path4260_fdef.shape = &path4260_s;

path4260_fdef.density = 1.0f;
path4260_fdef.friction = 0.1f;
path4260_fdef.restitution = 0.8f;
path4260_fdef.filter.groupIndex = -1;

path4260_body->CreateFixture(&path4260_fdef);
bodylist.push_back(path4260_body);
uData* path4260_ud = new uData();
path4260_ud->name = "TITLE";
path4260_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4260_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4260_ud->strokewidth=24.23800087f;
path4260_body->SetUserData(path4260_ud);

//
//path4258
//
b2BodyDef path4258_bdef;
path4258_bdef.type = b2_staticBody;
path4258_bdef.bullet = false;
path4258_bdef.position.Set(-2.17997552496f,17.2529738284f);
path4258_bdef.linearDamping = 0.0f;
path4258_bdef.angularDamping =0.0f;
b2Body* path4258_body = m_world->CreateBody(&path4258_bdef);

b2CircleShape path4258_s;
path4258_s.m_radius=0.318572368514f;

b2FixtureDef path4258_fdef;
path4258_fdef.shape = &path4258_s;

path4258_fdef.density = 1.0f;
path4258_fdef.friction = 0.1f;
path4258_fdef.restitution = 0.8f;
path4258_fdef.filter.groupIndex = -1;

path4258_body->CreateFixture(&path4258_fdef);
bodylist.push_back(path4258_body);
uData* path4258_ud = new uData();
path4258_ud->name = "TITLE";
path4258_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4258_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4258_ud->strokewidth=24.23800087f;
path4258_body->SetUserData(path4258_ud);

//
//path4256
//
b2BodyDef path4256_bdef;
path4256_bdef.type = b2_staticBody;
path4256_bdef.bullet = false;
path4256_bdef.position.Set(-2.19059464833f,18.0494048548f);
path4256_bdef.linearDamping = 0.0f;
path4256_bdef.angularDamping =0.0f;
b2Body* path4256_body = m_world->CreateBody(&path4256_bdef);

b2CircleShape path4256_s;
path4256_s.m_radius=0.318572368514f;

b2FixtureDef path4256_fdef;
path4256_fdef.shape = &path4256_s;

path4256_fdef.density = 1.0f;
path4256_fdef.friction = 0.1f;
path4256_fdef.restitution = 0.8f;
path4256_fdef.filter.groupIndex = -1;

path4256_body->CreateFixture(&path4256_fdef);
bodylist.push_back(path4256_body);
uData* path4256_ud = new uData();
path4256_ud->name = "TITLE";
path4256_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4256_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4256_ud->strokewidth=24.23800087f;
path4256_body->SetUserData(path4256_ud);

//
//path4254
//
b2BodyDef path4254_bdef;
path4254_bdef.type = b2_staticBody;
path4254_bdef.bullet = false;
path4254_bdef.position.Set(-2.17997552496f,18.9307884709f);
path4254_bdef.linearDamping = 0.0f;
path4254_bdef.angularDamping =0.0f;
b2Body* path4254_body = m_world->CreateBody(&path4254_bdef);

b2CircleShape path4254_s;
path4254_s.m_radius=0.318572368514f;

b2FixtureDef path4254_fdef;
path4254_fdef.shape = &path4254_s;

path4254_fdef.density = 1.0f;
path4254_fdef.friction = 0.1f;
path4254_fdef.restitution = 0.8f;
path4254_fdef.filter.groupIndex = -1;

path4254_body->CreateFixture(&path4254_fdef);
bodylist.push_back(path4254_body);
uData* path4254_ud = new uData();
path4254_ud->name = "TITLE";
path4254_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4254_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4254_ud->strokewidth=24.23800087f;
path4254_body->SetUserData(path4254_ud);

//
//path4115
//
b2BodyDef path4115_bdef;
path4115_bdef.type = b2_staticBody;
path4115_bdef.bullet = false;
path4115_bdef.position.Set(-3.3586933095f,14.9167765528f);
path4115_bdef.linearDamping = 0.0f;
path4115_bdef.angularDamping =0.0f;
b2Body* path4115_body = m_world->CreateBody(&path4115_bdef);

b2CircleShape path4115_s;
path4115_s.m_radius=0.318572368514f;

b2FixtureDef path4115_fdef;
path4115_fdef.shape = &path4115_s;

path4115_fdef.density = 1.0f;
path4115_fdef.friction = 0.1f;
path4115_fdef.restitution = 0.8f;
path4115_fdef.filter.groupIndex = -1;

path4115_body->CreateFixture(&path4115_fdef);
bodylist.push_back(path4115_body);
uData* path4115_ud = new uData();
path4115_ud->name = "TITLE";
path4115_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4115_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4115_ud->strokewidth=24.23800087f;
path4115_body->SetUserData(path4115_ud);

//
//path4113
//
b2BodyDef path4113_bdef;
path4113_bdef.type = b2_staticBody;
path4113_bdef.bullet = false;
path4113_bdef.position.Set(-4.11264791265f,14.8743001762f);
path4113_bdef.linearDamping = 0.0f;
path4113_bdef.angularDamping =0.0f;
b2Body* path4113_body = m_world->CreateBody(&path4113_bdef);

b2CircleShape path4113_s;
path4113_s.m_radius=0.318572368514f;

b2FixtureDef path4113_fdef;
path4113_fdef.shape = &path4113_s;

path4113_fdef.density = 1.0f;
path4113_fdef.friction = 0.1f;
path4113_fdef.restitution = 0.8f;
path4113_fdef.filter.groupIndex = -1;

path4113_body->CreateFixture(&path4113_fdef);
bodylist.push_back(path4113_body);
uData* path4113_ud = new uData();
path4113_ud->name = "TITLE";
path4113_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4113_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4113_ud->strokewidth=24.23800087f;
path4113_body->SetUserData(path4113_ud);

//
//path4111
//
b2BodyDef path4111_bdef;
path4111_bdef.type = b2_staticBody;
path4111_bdef.bullet = false;
path4111_bdef.position.Set(-4.84536426905f,14.8636810528f);
path4111_bdef.linearDamping = 0.0f;
path4111_bdef.angularDamping =0.0f;
b2Body* path4111_body = m_world->CreateBody(&path4111_bdef);

b2CircleShape path4111_s;
path4111_s.m_radius=0.318572368514f;

b2FixtureDef path4111_fdef;
path4111_fdef.shape = &path4111_s;

path4111_fdef.density = 1.0f;
path4111_fdef.friction = 0.1f;
path4111_fdef.restitution = 0.8f;
path4111_fdef.filter.groupIndex = -1;

path4111_body->CreateFixture(&path4111_fdef);
bodylist.push_back(path4111_body);
uData* path4111_ud = new uData();
path4111_ud->name = "TITLE";
path4111_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4111_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4111_ud->strokewidth=24.23800087f;
path4111_body->SetUserData(path4111_ud);

//
//path4109
//
b2BodyDef path4109_bdef;
path4109_bdef.type = b2_staticBody;
path4109_bdef.bullet = false;
path4109_bdef.position.Set(-4.83474526258f,15.6707311442f);
path4109_bdef.linearDamping = 0.0f;
path4109_bdef.angularDamping =0.0f;
b2Body* path4109_body = m_world->CreateBody(&path4109_bdef);

b2CircleShape path4109_s;
path4109_s.m_radius=0.318572368514f;

b2FixtureDef path4109_fdef;
path4109_fdef.shape = &path4109_s;

path4109_fdef.density = 1.0f;
path4109_fdef.friction = 0.1f;
path4109_fdef.restitution = 0.8f;
path4109_fdef.filter.groupIndex = -1;

path4109_body->CreateFixture(&path4109_fdef);
bodylist.push_back(path4109_body);
uData* path4109_ud = new uData();
path4109_ud->name = "TITLE";
path4109_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4109_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4109_ud->strokewidth=24.23800087f;
path4109_body->SetUserData(path4109_ud);

//
//path4107
//
b2BodyDef path4107_bdef;
path4107_bdef.type = b2_staticBody;
path4107_bdef.bullet = false;
path4107_bdef.position.Set(-4.82412613921f,16.4990192603f);
path4107_bdef.linearDamping = 0.0f;
path4107_bdef.angularDamping =0.0f;
b2Body* path4107_body = m_world->CreateBody(&path4107_bdef);

b2CircleShape path4107_s;
path4107_s.m_radius=0.318572368514f;

b2FixtureDef path4107_fdef;
path4107_fdef.shape = &path4107_s;

path4107_fdef.density = 1.0f;
path4107_fdef.friction = 0.1f;
path4107_fdef.restitution = 0.8f;
path4107_fdef.filter.groupIndex = -1;

path4107_body->CreateFixture(&path4107_fdef);
bodylist.push_back(path4107_body);
uData* path4107_ud = new uData();
path4107_ud->name = "TITLE";
path4107_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4107_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4107_ud->strokewidth=24.23800087f;
path4107_body->SetUserData(path4107_ud);

//
//path4105
//
b2BodyDef path4105_bdef;
path4105_bdef.type = b2_staticBody;
path4105_bdef.bullet = false;
path4105_bdef.position.Set(-4.82412613921f,17.3273073413f);
path4105_bdef.linearDamping = 0.0f;
path4105_bdef.angularDamping =0.0f;
b2Body* path4105_body = m_world->CreateBody(&path4105_bdef);

b2CircleShape path4105_s;
path4105_s.m_radius=0.318572368514f;

b2FixtureDef path4105_fdef;
path4105_fdef.shape = &path4105_s;

path4105_fdef.density = 1.0f;
path4105_fdef.friction = 0.1f;
path4105_fdef.restitution = 0.8f;
path4105_fdef.filter.groupIndex = -1;

path4105_body->CreateFixture(&path4105_fdef);
bodylist.push_back(path4105_body);
uData* path4105_ud = new uData();
path4105_ud->name = "TITLE";
path4105_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4105_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4105_ud->strokewidth=24.23800087f;
path4105_body->SetUserData(path4105_ud);

//
//path4103
//
b2BodyDef path4103_bdef;
path4103_bdef.type = b2_staticBody;
path4103_bdef.bullet = false;
path4103_bdef.position.Set(-4.84536426905f,18.0812621081f);
path4103_bdef.linearDamping = 0.0f;
path4103_bdef.angularDamping =0.0f;
b2Body* path4103_body = m_world->CreateBody(&path4103_bdef);

b2CircleShape path4103_s;
path4103_s.m_radius=0.318572368514f;

b2FixtureDef path4103_fdef;
path4103_fdef.shape = &path4103_s;

path4103_fdef.density = 1.0f;
path4103_fdef.friction = 0.1f;
path4103_fdef.restitution = 0.8f;
path4103_fdef.filter.groupIndex = -1;

path4103_body->CreateFixture(&path4103_fdef);
bodylist.push_back(path4103_body);
uData* path4103_ud = new uData();
path4103_ud->name = "TITLE";
path4103_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4103_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4103_ud->strokewidth=24.23800087f;
path4103_body->SetUserData(path4103_ud);

//
//path4101
//
b2BodyDef path4101_bdef;
path4101_bdef.type = b2_staticBody;
path4101_bdef.bullet = false;
path4101_bdef.position.Set(-4.80288800936f,18.9414074773f);
path4101_bdef.linearDamping = 0.0f;
path4101_bdef.angularDamping =0.0f;
b2Body* path4101_body = m_world->CreateBody(&path4101_bdef);

b2CircleShape path4101_s;
path4101_s.m_radius=0.318572368514f;

b2FixtureDef path4101_fdef;
path4101_fdef.shape = &path4101_s;

path4101_fdef.density = 1.0f;
path4101_fdef.friction = 0.1f;
path4101_fdef.restitution = 0.8f;
path4101_fdef.filter.groupIndex = -1;

path4101_body->CreateFixture(&path4101_fdef);
bodylist.push_back(path4101_body);
uData* path4101_ud = new uData();
path4101_ud->name = "TITLE";
path4101_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4101_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4101_ud->strokewidth=24.23800087f;
path4101_body->SetUserData(path4101_ud);

//
//path4099
//
b2BodyDef path4099_bdef;
path4099_bdef.type = b2_staticBody;
path4099_bdef.bullet = false;
path4099_bdef.position.Set(-8.39213674605f,15.4264922419f);
path4099_bdef.linearDamping = 0.0f;
path4099_bdef.angularDamping =0.0f;
b2Body* path4099_body = m_world->CreateBody(&path4099_bdef);

b2CircleShape path4099_s;
path4099_s.m_radius=0.318572368514f;

b2FixtureDef path4099_fdef;
path4099_fdef.shape = &path4099_s;

path4099_fdef.density = 1.0f;
path4099_fdef.friction = 0.1f;
path4099_fdef.restitution = 0.8f;
path4099_fdef.filter.groupIndex = -1;

path4099_body->CreateFixture(&path4099_fdef);
bodylist.push_back(path4099_body);
uData* path4099_ud = new uData();
path4099_ud->name = "TITLE";
path4099_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4099_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4099_ud->strokewidth=24.23800087f;
path4099_body->SetUserData(path4099_ud);

//
//path4097
//
b2BodyDef path4097_bdef;
path4097_bdef.type = b2_staticBody;
path4097_bdef.bullet = false;
path4097_bdef.position.Set(-7.69127752597f,15.4264922419f);
path4097_bdef.linearDamping = 0.0f;
path4097_bdef.angularDamping =0.0f;
b2Body* path4097_body = m_world->CreateBody(&path4097_bdef);

b2CircleShape path4097_s;
path4097_s.m_radius=0.318572368514f;

b2FixtureDef path4097_fdef;
path4097_fdef.shape = &path4097_s;

path4097_fdef.density = 1.0f;
path4097_fdef.friction = 0.1f;
path4097_fdef.restitution = 0.8f;
path4097_fdef.filter.groupIndex = -1;

path4097_body->CreateFixture(&path4097_fdef);
bodylist.push_back(path4097_body);
uData* path4097_ud = new uData();
path4097_ud->name = "TITLE";
path4097_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4097_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4097_ud->strokewidth=24.23800087f;
path4097_body->SetUserData(path4097_ud);

//
//path4095
//
b2BodyDef path4095_bdef;
path4095_bdef.type = b2_staticBody;
path4095_bdef.bullet = false;
path4095_bdef.position.Set(-6.93732292283f,15.4477304886f);
path4095_bdef.linearDamping = 0.0f;
path4095_bdef.angularDamping =0.0f;
b2Body* path4095_body = m_world->CreateBody(&path4095_bdef);

b2CircleShape path4095_s;
path4095_s.m_radius=0.318572368514f;

b2FixtureDef path4095_fdef;
path4095_fdef.shape = &path4095_s;

path4095_fdef.density = 1.0f;
path4095_fdef.friction = 0.1f;
path4095_fdef.restitution = 0.8f;
path4095_fdef.filter.groupIndex = -1;

path4095_body->CreateFixture(&path4095_fdef);
bodylist.push_back(path4095_body);
uData* path4095_ud = new uData();
path4095_ud->name = "TITLE";
path4095_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4095_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4095_ud->strokewidth=24.23800087f;
path4095_body->SetUserData(path4095_ud);

//
//path4093
//
b2BodyDef path4093_bdef;
path4093_bdef.type = b2_staticBody;
path4093_bdef.bullet = false;
path4093_bdef.position.Set(-6.05593930681f,14.7574902867f);
path4093_bdef.linearDamping = 0.0f;
path4093_bdef.angularDamping =0.0f;
b2Body* path4093_body = m_world->CreateBody(&path4093_bdef);

b2CircleShape path4093_s;
path4093_s.m_radius=0.318572368514f;

b2FixtureDef path4093_fdef;
path4093_fdef.shape = &path4093_s;

path4093_fdef.density = 1.0f;
path4093_fdef.friction = 0.1f;
path4093_fdef.restitution = 0.8f;
path4093_fdef.filter.groupIndex = -1;

path4093_body->CreateFixture(&path4093_fdef);
bodylist.push_back(path4093_body);
uData* path4093_ud = new uData();
path4093_ud->name = "TITLE";
path4093_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4093_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4093_ud->strokewidth=24.23800087f;
path4093_body->SetUserData(path4093_ud);

//
//path4091
//
b2BodyDef path4091_bdef;
path4091_bdef.type = b2_staticBody;
path4091_bdef.bullet = false;
path4091_bdef.position.Set(-6.30017809228f,15.4689686184f);
path4091_bdef.linearDamping = 0.0f;
path4091_bdef.angularDamping =0.0f;
b2Body* path4091_body = m_world->CreateBody(&path4091_bdef);

b2CircleShape path4091_s;
path4091_s.m_radius=0.318572368514f;

b2FixtureDef path4091_fdef;
path4091_fdef.shape = &path4091_s;

path4091_fdef.density = 1.0f;
path4091_fdef.friction = 0.1f;
path4091_fdef.restitution = 0.8f;
path4091_fdef.filter.groupIndex = -1;

path4091_body->CreateFixture(&path4091_fdef);
bodylist.push_back(path4091_body);
uData* path4091_ud = new uData();
path4091_ud->name = "TITLE";
path4091_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4091_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4091_ud->strokewidth=24.23800087f;
path4091_body->SetUserData(path4091_ud);

//
//path4089
//
b2BodyDef path4089_bdef;
path4089_bdef.type = b2_staticBody;
path4089_bdef.bullet = false;
path4089_bdef.position.Set(-6.5231787479f,16.2123040986f);
path4089_bdef.linearDamping = 0.0f;
path4089_bdef.angularDamping =0.0f;
b2Body* path4089_body = m_world->CreateBody(&path4089_bdef);

b2CircleShape path4089_s;
path4089_s.m_radius=0.318572368514f;

b2FixtureDef path4089_fdef;
path4089_fdef.shape = &path4089_s;

path4089_fdef.density = 1.0f;
path4089_fdef.friction = 0.1f;
path4089_fdef.restitution = 0.8f;
path4089_fdef.filter.groupIndex = -1;

path4089_body->CreateFixture(&path4089_fdef);
bodylist.push_back(path4089_body);
uData* path4089_ud = new uData();
path4089_ud->name = "TITLE";
path4089_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4089_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4089_ud->strokewidth=24.23800087f;
path4089_body->SetUserData(path4089_ud);

//
//path4087
//
b2BodyDef path4087_bdef;
path4087_bdef.type = b2_staticBody;
path4087_bdef.bullet = false;
path4087_bdef.position.Set(-6.73556039705f,16.8600679352f);
path4087_bdef.linearDamping = 0.0f;
path4087_bdef.angularDamping =0.0f;
b2Body* path4087_body = m_world->CreateBody(&path4087_bdef);

b2CircleShape path4087_s;
path4087_s.m_radius=0.318572368514f;

b2FixtureDef path4087_fdef;
path4087_fdef.shape = &path4087_s;

path4087_fdef.density = 1.0f;
path4087_fdef.friction = 0.1f;
path4087_fdef.restitution = 0.8f;
path4087_fdef.filter.groupIndex = -1;

path4087_body->CreateFixture(&path4087_fdef);
bodylist.push_back(path4087_body);
uData* path4087_ud = new uData();
path4087_ud->name = "TITLE";
path4087_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4087_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4087_ud->strokewidth=24.23800087f;
path4087_body->SetUserData(path4087_ud);

//
//path4085
//
b2BodyDef path4085_bdef;
path4085_bdef.type = b2_staticBody;
path4085_bdef.bullet = false;
path4085_bdef.position.Set(-6.95856105267f,17.560927167f);
path4085_bdef.linearDamping = 0.0f;
path4085_bdef.angularDamping =0.0f;
b2Body* path4085_body = m_world->CreateBody(&path4085_bdef);

b2CircleShape path4085_s;
path4085_s.m_radius=0.318572368514f;

b2FixtureDef path4085_fdef;
path4085_fdef.shape = &path4085_s;

path4085_fdef.density = 1.0f;
path4085_fdef.friction = 0.1f;
path4085_fdef.restitution = 0.8f;
path4085_fdef.filter.groupIndex = -1;

path4085_body->CreateFixture(&path4085_fdef);
bodylist.push_back(path4085_body);
uData* path4085_ud = new uData();
path4085_ud->name = "TITLE";
path4085_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4085_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4085_ud->strokewidth=24.23800087f;
path4085_body->SetUserData(path4085_ud);

//
//path4083
//
b2BodyDef path4083_bdef;
path4083_bdef.type = b2_staticBody;
path4083_bdef.bullet = false;
path4083_bdef.position.Set(-7.18156170829f,18.3148817702f);
path4083_bdef.linearDamping = 0.0f;
path4083_bdef.angularDamping =0.0f;
b2Body* path4083_body = m_world->CreateBody(&path4083_bdef);

b2CircleShape path4083_s;
path4083_s.m_radius=0.318572368514f;

b2FixtureDef path4083_fdef;
path4083_fdef.shape = &path4083_s;

path4083_fdef.density = 1.0f;
path4083_fdef.friction = 0.1f;
path4083_fdef.restitution = 0.8f;
path4083_fdef.filter.groupIndex = -1;

path4083_body->CreateFixture(&path4083_fdef);
bodylist.push_back(path4083_body);
uData* path4083_ud = new uData();
path4083_ud->name = "TITLE";
path4083_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4083_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4083_ud->strokewidth=24.23800087f;
path4083_body->SetUserData(path4083_ud);

//
//path4081
//
b2BodyDef path4081_bdef;
path4081_bdef.type = b2_staticBody;
path4081_bdef.bullet = false;
path4081_bdef.position.Set(-7.34084785748f,19.0157409902f);
path4081_bdef.linearDamping = 0.0f;
path4081_bdef.angularDamping =0.0f;
b2Body* path4081_body = m_world->CreateBody(&path4081_bdef);

b2CircleShape path4081_s;
path4081_s.m_radius=0.318572368514f;

b2FixtureDef path4081_fdef;
path4081_fdef.shape = &path4081_s;

path4081_fdef.density = 1.0f;
path4081_fdef.friction = 0.1f;
path4081_fdef.restitution = 0.8f;
path4081_fdef.filter.groupIndex = -1;

path4081_body->CreateFixture(&path4081_fdef);
bodylist.push_back(path4081_body);
uData* path4081_ud = new uData();
path4081_ud->name = "TITLE";
path4081_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4081_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4081_ud->strokewidth=24.23800087f;
path4081_body->SetUserData(path4081_ud);

//
//path4079
//
b2BodyDef path4079_bdef;
path4079_bdef.type = b2_staticBody;
path4079_bdef.bullet = false;
path4079_bdef.position.Set(-8.07356433078f,19.0475982435f);
path4079_bdef.linearDamping = 0.0f;
path4079_bdef.angularDamping =0.0f;
b2Body* path4079_body = m_world->CreateBody(&path4079_bdef);

b2CircleShape path4079_s;
path4079_s.m_radius=0.318572368514f;

b2FixtureDef path4079_fdef;
path4079_fdef.shape = &path4079_s;

path4079_fdef.density = 1.0f;
path4079_fdef.friction = 0.1f;
path4079_fdef.restitution = 0.8f;
path4079_fdef.filter.groupIndex = -1;

path4079_body->CreateFixture(&path4079_fdef);
bodylist.push_back(path4079_body);
uData* path4079_ud = new uData();
path4079_ud->name = "TITLE";
path4079_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4079_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4079_ud->strokewidth=24.23800087f;
path4079_body->SetUserData(path4079_ud);

//
//path4077
//
b2BodyDef path4077_bdef;
path4077_bdef.type = b2_staticBody;
path4077_bdef.bullet = false;
path4077_bdef.position.Set(-8.24346960334f,18.3573581467f);
path4077_bdef.linearDamping = 0.0f;
path4077_bdef.angularDamping =0.0f;
b2Body* path4077_body = m_world->CreateBody(&path4077_bdef);

b2CircleShape path4077_s;
path4077_s.m_radius=0.318572368514f;

b2FixtureDef path4077_fdef;
path4077_fdef.shape = &path4077_s;

path4077_fdef.density = 1.0f;
path4077_fdef.friction = 0.1f;
path4077_fdef.restitution = 0.8f;
path4077_fdef.filter.groupIndex = -1;

path4077_body->CreateFixture(&path4077_fdef);
bodylist.push_back(path4077_body);
uData* path4077_ud = new uData();
path4077_ud->name = "TITLE";
path4077_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4077_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4077_ud->strokewidth=24.23800087f;
path4077_body->SetUserData(path4077_ud);

//
//path4075
//
b2BodyDef path4075_bdef;
path4075_bdef.type = b2_staticBody;
path4075_bdef.bullet = false;
path4075_bdef.position.Set(-8.49832751218f,17.6671179331f);
path4075_bdef.linearDamping = 0.0f;
path4075_bdef.angularDamping =0.0f;
b2Body* path4075_body = m_world->CreateBody(&path4075_bdef);

b2CircleShape path4075_s;
path4075_s.m_radius=0.318572368514f;

b2FixtureDef path4075_fdef;
path4075_fdef.shape = &path4075_s;

path4075_fdef.density = 1.0f;
path4075_fdef.friction = 0.1f;
path4075_fdef.restitution = 0.8f;
path4075_fdef.filter.groupIndex = -1;

path4075_body->CreateFixture(&path4075_fdef);
bodylist.push_back(path4075_body);
uData* path4075_ud = new uData();
path4075_ud->name = "TITLE";
path4075_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4075_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4075_ud->strokewidth=24.23800087f;
path4075_body->SetUserData(path4075_ud);

//
//path4073
//
b2BodyDef path4073_bdef;
path4073_bdef.type = b2_staticBody;
path4073_bdef.bullet = false;
path4073_bdef.position.Set(-8.66823278474f,16.9981159546f);
path4073_bdef.linearDamping = 0.0f;
path4073_bdef.angularDamping =0.0f;
b2Body* path4073_body = m_world->CreateBody(&path4073_bdef);

b2CircleShape path4073_s;
path4073_s.m_radius=0.318572368514f;

b2FixtureDef path4073_fdef;
path4073_fdef.shape = &path4073_s;

path4073_fdef.density = 1.0f;
path4073_fdef.friction = 0.1f;
path4073_fdef.restitution = 0.8f;
path4073_fdef.filter.groupIndex = -1;

path4073_body->CreateFixture(&path4073_fdef);
bodylist.push_back(path4073_body);
uData* path4073_ud = new uData();
path4073_ud->name = "TITLE";
path4073_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4073_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4073_ud->strokewidth=24.23800087f;
path4073_body->SetUserData(path4073_ud);

//
//path4071
//
b2BodyDef path4071_bdef;
path4071_bdef.type = b2_staticBody;
path4071_bdef.bullet = false;
path4071_bdef.position.Set(-8.92309057668f,16.2123040986f);
path4071_bdef.linearDamping = 0.0f;
path4071_bdef.angularDamping =0.0f;
b2Body* path4071_body = m_world->CreateBody(&path4071_bdef);

b2CircleShape path4071_s;
path4071_s.m_radius=0.318572368514f;

b2FixtureDef path4071_fdef;
path4071_fdef.shape = &path4071_s;

path4071_fdef.density = 1.0f;
path4071_fdef.friction = 0.1f;
path4071_fdef.restitution = 0.8f;
path4071_fdef.filter.groupIndex = -1;

path4071_body->CreateFixture(&path4071_fdef);
bodylist.push_back(path4071_body);
uData* path4071_ud = new uData();
path4071_ud->name = "TITLE";
path4071_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4071_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4071_ud->strokewidth=24.23800087f;
path4071_body->SetUserData(path4071_ud);

//
//path4069
//
b2BodyDef path4069_bdef;
path4069_bdef.type = b2_staticBody;
path4069_bdef.bullet = false;
path4069_bdef.position.Set(-9.17794848552f,15.5326831249f);
path4069_bdef.linearDamping = 0.0f;
path4069_bdef.angularDamping =0.0f;
b2Body* path4069_body = m_world->CreateBody(&path4069_bdef);

b2CircleShape path4069_s;
path4069_s.m_radius=0.318572368514f;

b2FixtureDef path4069_fdef;
path4069_fdef.shape = &path4069_s;

path4069_fdef.density = 1.0f;
path4069_fdef.friction = 0.1f;
path4069_fdef.restitution = 0.8f;
path4069_fdef.filter.groupIndex = -1;

path4069_body->CreateFixture(&path4069_fdef);
bodylist.push_back(path4069_body);
uData* path4069_ud = new uData();
path4069_ud->name = "TITLE";
path4069_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4069_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4069_ud->strokewidth=24.23800087f;
path4069_body->SetUserData(path4069_ud);

//
//path4067
//
b2BodyDef path4067_bdef;
path4067_bdef.type = b2_staticBody;
path4067_bdef.bullet = false;
path4067_bdef.position.Set(-9.31599650486f,14.7681094101f);
path4067_bdef.linearDamping = 0.0f;
path4067_bdef.angularDamping =0.0f;
b2Body* path4067_body = m_world->CreateBody(&path4067_bdef);

b2CircleShape path4067_s;
path4067_s.m_radius=0.318572368514f;

b2FixtureDef path4067_fdef;
path4067_fdef.shape = &path4067_s;

path4067_fdef.density = 1.0f;
path4067_fdef.friction = 0.1f;
path4067_fdef.restitution = 0.8f;
path4067_fdef.filter.groupIndex = -1;

path4067_body->CreateFixture(&path4067_fdef);
bodylist.push_back(path4067_body);
uData* path4067_ud = new uData();
path4067_ud->name = "TITLE";
path4067_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4067_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4067_ud->strokewidth=24.23800087f;
path4067_body->SetUserData(path4067_ud);

//
//path4065
//
b2BodyDef path4065_bdef;
path4065_bdef.type = b2_staticBody;
path4065_bdef.bullet = false;
path4065_bdef.position.Set(-11.1743353796f,16.647686403f);
path4065_bdef.linearDamping = 0.0f;
path4065_bdef.angularDamping =0.0f;
b2Body* path4065_body = m_world->CreateBody(&path4065_bdef);

b2CircleShape path4065_s;
path4065_s.m_radius=0.318572368514f;

b2FixtureDef path4065_fdef;
path4065_fdef.shape = &path4065_s;

path4065_fdef.density = 1.0f;
path4065_fdef.friction = 0.1f;
path4065_fdef.restitution = 0.8f;
path4065_fdef.filter.groupIndex = -1;

path4065_body->CreateFixture(&path4065_fdef);
bodylist.push_back(path4065_body);
uData* path4065_ud = new uData();
path4065_ud->name = "TITLE";
path4065_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4065_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4065_ud->strokewidth=24.23800087f;
path4065_body->SetUserData(path4065_ud);

//
//path4063
//
b2BodyDef path4063_bdef;
path4063_bdef.type = b2_staticBody;
path4063_bdef.bullet = false;
path4063_bdef.position.Set(-11.0044301071f,15.9149699297f);
path4063_bdef.linearDamping = 0.0f;
path4063_bdef.angularDamping =0.0f;
b2Body* path4063_body = m_world->CreateBody(&path4063_bdef);

b2CircleShape path4063_s;
path4063_s.m_radius=0.318572368514f;

b2FixtureDef path4063_fdef;
path4063_fdef.shape = &path4063_s;

path4063_fdef.density = 1.0f;
path4063_fdef.friction = 0.1f;
path4063_fdef.restitution = 0.8f;
path4063_fdef.filter.groupIndex = -1;

path4063_body->CreateFixture(&path4063_fdef);
bodylist.push_back(path4063_body);
uData* path4063_ud = new uData();
path4063_ud->name = "TITLE";
path4063_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4063_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4063_ud->strokewidth=24.23800087f;
path4063_body->SetUserData(path4063_ud);

//
//path4061
//
b2BodyDef path4061_bdef;
path4061_bdef.type = b2_staticBody;
path4061_bdef.bullet = false;
path4061_bdef.position.Set(-11.1530972498f,15.1928725798f);
path4061_bdef.linearDamping = 0.0f;
path4061_bdef.angularDamping =0.0f;
b2Body* path4061_body = m_world->CreateBody(&path4061_bdef);

b2CircleShape path4061_s;
path4061_s.m_radius=0.318572368514f;

b2FixtureDef path4061_fdef;
path4061_fdef.shape = &path4061_s;

path4061_fdef.density = 1.0f;
path4061_fdef.friction = 0.1f;
path4061_fdef.restitution = 0.8f;
path4061_fdef.filter.groupIndex = -1;

path4061_body->CreateFixture(&path4061_fdef);
bodylist.push_back(path4061_body);
uData* path4061_ud = new uData();
path4061_ud->name = "TITLE";
path4061_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4061_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4061_ud->strokewidth=24.23800087f;
path4061_body->SetUserData(path4061_ud);

//
//path4059
//
b2BodyDef path4059_bdef;
path4059_bdef.type = b2_staticBody;
path4059_bdef.bullet = false;
path4059_bdef.position.Set(-11.7371465804f,14.9698719358f);
path4059_bdef.linearDamping = 0.0f;
path4059_bdef.angularDamping =0.0f;
b2Body* path4059_body = m_world->CreateBody(&path4059_bdef);

b2CircleShape path4059_s;
path4059_s.m_radius=0.318572368514f;

b2FixtureDef path4059_fdef;
path4059_fdef.shape = &path4059_s;

path4059_fdef.density = 1.0f;
path4059_fdef.friction = 0.1f;
path4059_fdef.restitution = 0.8f;
path4059_fdef.filter.groupIndex = -1;

path4059_body->CreateFixture(&path4059_fdef);
bodylist.push_back(path4059_body);
uData* path4059_ud = new uData();
path4059_ud->name = "TITLE";
path4059_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4059_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4059_ud->strokewidth=24.23800087f;
path4059_body->SetUserData(path4059_ud);

//
//path4057
//
b2BodyDef path4057_bdef;
path4057_bdef.type = b2_staticBody;
path4057_bdef.bullet = false;
path4057_bdef.position.Set(-12.4167676239f,14.8743001762f);
path4057_bdef.linearDamping = 0.0f;
path4057_bdef.angularDamping =0.0f;
b2Body* path4057_body = m_world->CreateBody(&path4057_bdef);

b2CircleShape path4057_s;
path4057_s.m_radius=0.318572368514f;

b2FixtureDef path4057_fdef;
path4057_fdef.shape = &path4057_s;

path4057_fdef.density = 1.0f;
path4057_fdef.friction = 0.1f;
path4057_fdef.restitution = 0.8f;
path4057_fdef.filter.groupIndex = -1;

path4057_body->CreateFixture(&path4057_fdef);
bodylist.push_back(path4057_body);
uData* path4057_ud = new uData();
path4057_ud->name = "TITLE";
path4057_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4057_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4057_ud->strokewidth=24.23800087f;
path4057_body->SetUserData(path4057_ud);

//
//path4055
//
b2BodyDef path4055_bdef;
path4055_bdef.type = b2_staticBody;
path4055_bdef.bullet = false;
path4055_bdef.position.Set(-13.1176268673f,14.842442923f);
path4055_bdef.linearDamping = 0.0f;
path4055_bdef.angularDamping =0.0f;
b2Body* path4055_body = m_world->CreateBody(&path4055_bdef);

b2CircleShape path4055_s;
path4055_s.m_radius=0.318572368514f;

b2FixtureDef path4055_fdef;
path4055_fdef.shape = &path4055_s;

path4055_fdef.density = 1.0f;
path4055_fdef.friction = 0.1f;
path4055_fdef.restitution = 0.8f;
path4055_fdef.filter.groupIndex = -1;

path4055_body->CreateFixture(&path4055_fdef);
bodylist.push_back(path4055_body);
uData* path4055_ud = new uData();
path4055_ud->name = "TITLE";
path4055_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4055_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4055_ud->strokewidth=24.23800087f;
path4055_body->SetUserData(path4055_ud);

//
//path4053
//
b2BodyDef path4053_bdef;
path4053_bdef.type = b2_staticBody;
path4053_bdef.bullet = false;
path4053_bdef.position.Set(-13.1388650205f,15.6707311442f);
path4053_bdef.linearDamping = 0.0f;
path4053_bdef.angularDamping =0.0f;
b2Body* path4053_body = m_world->CreateBody(&path4053_bdef);

b2CircleShape path4053_s;
path4053_s.m_radius=0.318572368514f;

b2FixtureDef path4053_fdef;
path4053_fdef.shape = &path4053_s;

path4053_fdef.density = 1.0f;
path4053_fdef.friction = 0.1f;
path4053_fdef.restitution = 0.8f;
path4053_fdef.filter.groupIndex = -1;

path4053_body->CreateFixture(&path4053_fdef);
bodylist.push_back(path4053_body);
uData* path4053_ud = new uData();
path4053_ud->name = "TITLE";
path4053_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4053_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4053_ud->strokewidth=24.23800087f;
path4053_body->SetUserData(path4053_ud);

//
//path4051
//
b2BodyDef path4051_bdef;
path4051_bdef.type = b2_staticBody;
path4051_bdef.bullet = false;
path4051_bdef.position.Set(-13.1282459439f,16.4884002538f);
path4051_bdef.linearDamping = 0.0f;
path4051_bdef.angularDamping =0.0f;
b2Body* path4051_body = m_world->CreateBody(&path4051_bdef);

b2CircleShape path4051_s;
path4051_s.m_radius=0.318572368514f;

b2FixtureDef path4051_fdef;
path4051_fdef.shape = &path4051_s;

path4051_fdef.density = 1.0f;
path4051_fdef.friction = 0.1f;
path4051_fdef.restitution = 0.8f;
path4051_fdef.filter.groupIndex = -1;

path4051_body->CreateFixture(&path4051_fdef);
bodylist.push_back(path4051_body);
uData* path4051_ud = new uData();
path4051_ud->name = "TITLE";
path4051_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4051_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4051_ud->strokewidth=24.23800087f;
path4051_body->SetUserData(path4051_ud);

//
//path4049
//
b2BodyDef path4049_bdef;
path4049_bdef.type = b2_staticBody;
path4049_bdef.bullet = false;
path4049_bdef.position.Set(-13.1388650205f,17.3166883348f);
path4049_bdef.linearDamping = 0.0f;
path4049_bdef.angularDamping =0.0f;
b2Body* path4049_body = m_world->CreateBody(&path4049_bdef);

b2CircleShape path4049_s;
path4049_s.m_radius=0.318572368514f;

b2FixtureDef path4049_fdef;
path4049_fdef.shape = &path4049_s;

path4049_fdef.density = 1.0f;
path4049_fdef.friction = 0.1f;
path4049_fdef.restitution = 0.8f;
path4049_fdef.filter.groupIndex = -1;

path4049_body->CreateFixture(&path4049_fdef);
bodylist.push_back(path4049_body);
uData* path4049_ud = new uData();
path4049_ud->name = "TITLE";
path4049_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4049_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4049_ud->strokewidth=24.23800087f;
path4049_body->SetUserData(path4049_ud);

//
//path4047
//
b2BodyDef path4047_bdef;
path4047_bdef.type = b2_staticBody;
path4047_bdef.bullet = false;
path4047_bdef.position.Set(-13.1282459439f,18.1449766145f);
path4047_bdef.linearDamping = 0.0f;
path4047_bdef.angularDamping =0.0f;
b2Body* path4047_body = m_world->CreateBody(&path4047_bdef);

b2CircleShape path4047_s;
path4047_s.m_radius=0.318572368514f;

b2FixtureDef path4047_fdef;
path4047_fdef.shape = &path4047_s;

path4047_fdef.density = 1.0f;
path4047_fdef.friction = 0.1f;
path4047_fdef.restitution = 0.8f;
path4047_fdef.filter.groupIndex = -1;

path4047_body->CreateFixture(&path4047_fdef);
bodylist.push_back(path4047_body);
uData* path4047_ud = new uData();
path4047_ud->name = "TITLE";
path4047_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4047_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4047_ud->strokewidth=24.23800087f;
path4047_body->SetUserData(path4047_ud);

//
//path4045
//
b2BodyDef path4045_bdef;
path4045_bdef.type = b2_staticBody;
path4045_bdef.bullet = false;
path4045_bdef.position.Set(-12.2681006448f,16.9768778247f);
path4045_bdef.linearDamping = 0.0f;
path4045_bdef.angularDamping =0.0f;
b2Body* path4045_body = m_world->CreateBody(&path4045_bdef);

b2CircleShape path4045_s;
path4045_s.m_radius=0.318572368514f;

b2FixtureDef path4045_fdef;
path4045_fdef.shape = &path4045_s;

path4045_fdef.density = 1.0f;
path4045_fdef.friction = 0.1f;
path4045_fdef.restitution = 0.8f;
path4045_fdef.filter.groupIndex = -1;

path4045_body->CreateFixture(&path4045_fdef);
bodylist.push_back(path4045_body);
uData* path4045_ud = new uData();
path4045_ud->name = "TITLE";
path4045_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4045_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4045_ud->strokewidth=24.23800087f;
path4045_body->SetUserData(path4045_ud);

//
//path4043
//
b2BodyDef path4043_bdef;
path4043_bdef.type = b2_staticBody;
path4043_bdef.bullet = false;
path4043_bdef.position.Set(-11.6415748207f,17.2211165751f);
path4043_bdef.linearDamping = 0.0f;
path4043_bdef.angularDamping =0.0f;
b2Body* path4043_body = m_world->CreateBody(&path4043_bdef);

b2CircleShape path4043_s;
path4043_s.m_radius=0.318572368514f;

b2FixtureDef path4043_fdef;
path4043_fdef.shape = &path4043_s;

path4043_fdef.density = 1.0f;
path4043_fdef.friction = 0.1f;
path4043_fdef.restitution = 0.8f;
path4043_fdef.filter.groupIndex = -1;

path4043_body->CreateFixture(&path4043_fdef);
bodylist.push_back(path4043_body);
uData* path4043_ud = new uData();
path4043_ud->name = "TITLE";
path4043_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4043_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4043_ud->strokewidth=24.23800087f;
path4043_body->SetUserData(path4043_ud);

//
//path4041
//
b2BodyDef path4041_bdef;
path4041_bdef.type = b2_staticBody;
path4041_bdef.bullet = false;
path4041_bdef.position.Set(-11.1106208732f,17.8051660694f);
path4041_bdef.linearDamping = 0.0f;
path4041_bdef.angularDamping =0.0f;
b2Body* path4041_body = m_world->CreateBody(&path4041_bdef);

b2CircleShape path4041_s;
path4041_s.m_radius=0.318572368514f;

b2FixtureDef path4041_fdef;
path4041_fdef.shape = &path4041_s;

path4041_fdef.density = 1.0f;
path4041_fdef.friction = 0.1f;
path4041_fdef.restitution = 0.8f;
path4041_fdef.filter.groupIndex = -1;

path4041_body->CreateFixture(&path4041_fdef);
bodylist.push_back(path4041_body);
uData* path4041_ud = new uData();
path4041_ud->name = "TITLE";
path4041_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4041_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4041_ud->strokewidth=24.23800087f;
path4041_body->SetUserData(path4041_ud);

//
//path4039
//
b2BodyDef path4039_bdef;
path4039_bdef.type = b2_staticBody;
path4039_bdef.bullet = false;
path4039_bdef.position.Set(-11.2592880159f,18.5060252895f);
path4039_bdef.linearDamping = 0.0f;
path4039_bdef.angularDamping =0.0f;
b2Body* path4039_body = m_world->CreateBody(&path4039_bdef);

b2CircleShape path4039_s;
path4039_s.m_radius=0.318572368514f;

b2FixtureDef path4039_fdef;
path4039_fdef.shape = &path4039_s;

path4039_fdef.density = 1.0f;
path4039_fdef.friction = 0.1f;
path4039_fdef.restitution = 0.8f;
path4039_fdef.filter.groupIndex = -1;

path4039_body->CreateFixture(&path4039_fdef);
bodylist.push_back(path4039_body);
uData* path4039_ud = new uData();
path4039_ud->name = "TITLE";
path4039_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4039_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4039_ud->strokewidth=24.23800087f;
path4039_body->SetUserData(path4039_ud);

//
//path3962
//
b2BodyDef path3962_bdef;
path3962_bdef.type = b2_staticBody;
path3962_bdef.bullet = false;
path3962_bdef.position.Set(-11.2592880159f,18.5060252895f);
path3962_bdef.linearDamping = 0.0f;
path3962_bdef.angularDamping =0.0f;
b2Body* path3962_body = m_world->CreateBody(&path3962_bdef);

b2CircleShape path3962_s;
path3962_s.m_radius=0.318572368514f;

b2FixtureDef path3962_fdef;
path3962_fdef.shape = &path3962_s;

path3962_fdef.density = 1.0f;
path3962_fdef.friction = 0.1f;
path3962_fdef.restitution = 0.8f;
path3962_fdef.filter.groupIndex = -1;

path3962_body->CreateFixture(&path3962_fdef);
bodylist.push_back(path3962_body);
uData* path3962_ud = new uData();
path3962_ud->name = "TITLE";
path3962_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path3962_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path3962_ud->strokewidth=24.23800087f;
path3962_body->SetUserData(path3962_ud);

//
//path4037
//
b2BodyDef path4037_bdef;
path4037_bdef.type = b2_staticBody;
path4037_bdef.bullet = false;
path4037_bdef.position.Set(-11.7583847102f,18.9520266007f);
path4037_bdef.linearDamping = 0.0f;
path4037_bdef.angularDamping =0.0f;
b2Body* path4037_body = m_world->CreateBody(&path4037_bdef);

b2CircleShape path4037_s;
path4037_s.m_radius=0.318572368514f;

b2FixtureDef path4037_fdef;
path4037_fdef.shape = &path4037_s;

path4037_fdef.density = 1.0f;
path4037_fdef.friction = 0.1f;
path4037_fdef.restitution = 0.8f;
path4037_fdef.filter.groupIndex = -1;

path4037_body->CreateFixture(&path4037_fdef);
bodylist.push_back(path4037_body);
uData* path4037_ud = new uData();
path4037_ud->name = "TITLE";
path4037_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4037_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4037_ud->strokewidth=24.23800087f;
path4037_body->SetUserData(path4037_ud);

//
//path4035
//
b2BodyDef path4035_bdef;
path4035_bdef.type = b2_staticBody;
path4035_bdef.bullet = false;
path4035_bdef.position.Set(-12.4380057537f,18.9626456072f);
path4035_bdef.linearDamping = 0.0f;
path4035_bdef.angularDamping =0.0f;
b2Body* path4035_body = m_world->CreateBody(&path4035_bdef);

b2CircleShape path4035_s;
path4035_s.m_radius=0.318572368514f;

b2FixtureDef path4035_fdef;
path4035_fdef.shape = &path4035_s;

path4035_fdef.density = 1.0f;
path4035_fdef.friction = 0.1f;
path4035_fdef.restitution = 0.8f;
path4035_fdef.filter.groupIndex = -1;

path4035_body->CreateFixture(&path4035_fdef);
bodylist.push_back(path4035_body);
uData* path4035_ud = new uData();
path4035_ud->name = "TITLE";
path4035_ud->fill.Set(float(211)/255,float(213)/255,float(208)/255);
path4035_ud->stroke.Set(float(203)/255,float(207)/255,float(204)/255);
path4035_ud->strokewidth=24.23800087f;
path4035_body->SetUserData(path4035_ud);

//
//path4279
//
b2BodyDef path4279_bdef;
path4279_bdef.type = b2_staticBody;
path4279_bdef.bullet = false;
path4279_bdef.position.Set(3.8534012279f,18.6394602326f);
path4279_bdef.linearDamping = 0.0f;
path4279_bdef.angularDamping =0.0f;
b2Body* path4279_body = m_world->CreateBody(&path4279_bdef);

b2CircleShape path4279_s;
path4279_s.m_radius=0.318572368514f;

b2FixtureDef path4279_fdef;
path4279_fdef.shape = &path4279_s;

path4279_fdef.density = 1.0f;
path4279_fdef.friction = 0.1f;
path4279_fdef.restitution = 0.8f;
path4279_fdef.filter.groupIndex = -1;

path4279_body->CreateFixture(&path4279_fdef);
bodylist.push_back(path4279_body);
uData* path4279_ud = new uData();
path4279_ud->name = "TITLE";
path4279_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4279_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4279_ud->strokewidth=24.23800087f;
path4279_body->SetUserData(path4279_ud);

//
//path4277
//
b2BodyDef path4277_bdef;
path4277_bdef.type = b2_staticBody;
path4277_bdef.bullet = false;
path4277_bdef.position.Set(3.54544898809f,18.0447917786f);
path4277_bdef.linearDamping = 0.0f;
path4277_bdef.angularDamping =0.0f;
b2Body* path4277_body = m_world->CreateBody(&path4277_bdef);

b2CircleShape path4277_s;
path4277_s.m_radius=0.318572368514f;

b2FixtureDef path4277_fdef;
path4277_fdef.shape = &path4277_s;

path4277_fdef.density = 1.0f;
path4277_fdef.friction = 0.1f;
path4277_fdef.restitution = 0.8f;
path4277_fdef.filter.groupIndex = -1;

path4277_body->CreateFixture(&path4277_fdef);
bodylist.push_back(path4277_body);
uData* path4277_ud = new uData();
path4277_ud->name = "TITLE";
path4277_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4277_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4277_ud->strokewidth=24.23800087f;
path4277_body->SetUserData(path4277_ud);

//
//path4275
//
b2BodyDef path4275_bdef;
path4275_bdef.type = b2_staticBody;
path4275_bdef.bullet = false;
path4275_bdef.position.Set(4.50116541562f,19.1597950567f);
path4275_bdef.linearDamping = 0.0f;
path4275_bdef.angularDamping =0.0f;
b2Body* path4275_body = m_world->CreateBody(&path4275_bdef);

b2CircleShape path4275_s;
path4275_s.m_radius=0.318572368514f;

b2FixtureDef path4275_fdef;
path4275_fdef.shape = &path4275_s;

path4275_fdef.density = 1.0f;
path4275_fdef.friction = 0.1f;
path4275_fdef.restitution = 0.8f;
path4275_fdef.filter.groupIndex = -1;

path4275_body->CreateFixture(&path4275_fdef);
bodylist.push_back(path4275_body);
uData* path4275_ud = new uData();
path4275_ud->name = "TITLE";
path4275_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4275_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4275_ud->strokewidth=24.23800087f;
path4275_body->SetUserData(path4275_ud);

//
//path4273
//
b2BodyDef path4273_bdef;
path4273_bdef.type = b2_staticBody;
path4273_bdef.bullet = false;
path4273_bdef.position.Set(14.5555008216f,15.2343381757f);
path4273_bdef.linearDamping = 0.0f;
path4273_bdef.angularDamping =0.0f;
b2Body* path4273_body = m_world->CreateBody(&path4273_bdef);

b2CircleShape path4273_s;
path4273_s.m_radius=0.318572368514f;

b2FixtureDef path4273_fdef;
path4273_fdef.shape = &path4273_s;

path4273_fdef.density = 1.0f;
path4273_fdef.friction = 0.1f;
path4273_fdef.restitution = 0.8f;
path4273_fdef.filter.groupIndex = -1;

path4273_body->CreateFixture(&path4273_fdef);
bodylist.push_back(path4273_body);
uData* path4273_ud = new uData();
path4273_ud->name = "TITLE";
path4273_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4273_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4273_ud->strokewidth=24.23800087f;
path4273_body->SetUserData(path4273_ud);

//
//path4271
//
b2BodyDef path4271_bdef;
path4271_bdef.type = b2_staticBody;
path4271_bdef.bullet = false;
path4271_bdef.position.Set(14.5555008216f,15.2343381757f);
path4271_bdef.linearDamping = 0.0f;
path4271_bdef.angularDamping =0.0f;
b2Body* path4271_body = m_world->CreateBody(&path4271_bdef);

b2CircleShape path4271_s;
path4271_s.m_radius=0.318572368514f;

b2FixtureDef path4271_fdef;
path4271_fdef.shape = &path4271_s;

path4271_fdef.density = 1.0f;
path4271_fdef.friction = 0.1f;
path4271_fdef.restitution = 0.8f;
path4271_fdef.filter.groupIndex = -1;

path4271_body->CreateFixture(&path4271_fdef);
bodylist.push_back(path4271_body);
uData* path4271_ud = new uData();
path4271_ud->name = "TITLE";
path4271_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4271_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4271_ud->strokewidth=24.23800087f;
path4271_body->SetUserData(path4271_ud);

//
//path4269
//
b2BodyDef path4269_bdef;
path4269_bdef.type = b2_staticBody;
path4269_bdef.bullet = false;
path4269_bdef.position.Set(13.8083442576f,15.2061434914f);
path4269_bdef.linearDamping = 0.0f;
path4269_bdef.angularDamping =0.0f;
b2Body* path4269_body = m_world->CreateBody(&path4269_bdef);

b2CircleShape path4269_s;
path4269_s.m_radius=0.318572368514f;

b2FixtureDef path4269_fdef;
path4269_fdef.shape = &path4269_s;

path4269_fdef.density = 1.0f;
path4269_fdef.friction = 0.1f;
path4269_fdef.restitution = 0.8f;
path4269_fdef.filter.groupIndex = -1;

path4269_body->CreateFixture(&path4269_fdef);
bodylist.push_back(path4269_body);
uData* path4269_ud = new uData();
path4269_ud->name = "TITLE";
path4269_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4269_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4269_ud->strokewidth=24.23800087f;
path4269_body->SetUserData(path4269_ud);

//
//path4267
//
b2BodyDef path4267_bdef;
path4267_bdef.type = b2_staticBody;
path4267_bdef.bullet = false;
path4267_bdef.position.Set(13.0188935629f,15.2625328601f);
path4267_bdef.linearDamping = 0.0f;
path4267_bdef.angularDamping =0.0f;
b2Body* path4267_body = m_world->CreateBody(&path4267_bdef);

b2CircleShape path4267_s;
path4267_s.m_radius=0.318572368514f;

b2FixtureDef path4267_fdef;
path4267_fdef.shape = &path4267_s;

path4267_fdef.density = 1.0f;
path4267_fdef.friction = 0.1f;
path4267_fdef.restitution = 0.8f;
path4267_fdef.filter.groupIndex = -1;

path4267_body->CreateFixture(&path4267_fdef);
bodylist.push_back(path4267_body);
uData* path4267_ud = new uData();
path4267_ud->name = "TITLE";
path4267_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4267_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4267_ud->strokewidth=24.23800087f;
path4267_body->SetUserData(path4267_ud);

//
//path4265
//
b2BodyDef path4265_bdef;
path4265_bdef.type = b2_staticBody;
path4265_bdef.bullet = false;
path4265_bdef.position.Set(13.0188935629f,16.0942744123f);
path4265_bdef.linearDamping = 0.0f;
path4265_bdef.angularDamping =0.0f;
b2Body* path4265_body = m_world->CreateBody(&path4265_bdef);

b2CircleShape path4265_s;
path4265_s.m_radius=0.318572368514f;

b2FixtureDef path4265_fdef;
path4265_fdef.shape = &path4265_s;

path4265_fdef.density = 1.0f;
path4265_fdef.friction = 0.1f;
path4265_fdef.restitution = 0.8f;
path4265_fdef.filter.groupIndex = -1;

path4265_body->CreateFixture(&path4265_fdef);
bodylist.push_back(path4265_body);
uData* path4265_ud = new uData();
path4265_ud->name = "TITLE";
path4265_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4265_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4265_ud->strokewidth=24.23800087f;
path4265_body->SetUserData(path4265_ud);

//
//path4263
//
b2BodyDef path4263_bdef;
path4263_bdef.type = b2_staticBody;
path4263_bdef.bullet = false;
path4263_bdef.position.Set(13.0611865247f,16.7286535247f);
path4263_bdef.linearDamping = 0.0f;
path4263_bdef.angularDamping =0.0f;
b2Body* path4263_body = m_world->CreateBody(&path4263_bdef);

b2CircleShape path4263_s;
path4263_s.m_radius=0.318572368514f;

b2FixtureDef path4263_fdef;
path4263_fdef.shape = &path4263_s;

path4263_fdef.density = 1.0f;
path4263_fdef.friction = 0.1f;
path4263_fdef.restitution = 0.8f;
path4263_fdef.filter.groupIndex = -1;

path4263_body->CreateFixture(&path4263_fdef);
bodylist.push_back(path4263_body);
uData* path4263_ud = new uData();
path4263_ud->name = "TITLE";
path4263_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4263_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4263_ud->strokewidth=24.23800087f;
path4263_body->SetUserData(path4263_ud);

//
//path4261
//
b2BodyDef path4261_bdef;
path4261_bdef.type = b2_staticBody;
path4261_bdef.bullet = false;
path4261_bdef.position.Set(13.0893802739f,17.6872708877f);
path4261_bdef.linearDamping = 0.0f;
path4261_bdef.angularDamping =0.0f;
b2Body* path4261_body = m_world->CreateBody(&path4261_bdef);

b2CircleShape path4261_s;
path4261_s.m_radius=0.318572368514f;

b2FixtureDef path4261_fdef;
path4261_fdef.shape = &path4261_s;

path4261_fdef.density = 1.0f;
path4261_fdef.friction = 0.1f;
path4261_fdef.restitution = 0.8f;
path4261_fdef.filter.groupIndex = -1;

path4261_body->CreateFixture(&path4261_fdef);
bodylist.push_back(path4261_body);
uData* path4261_ud = new uData();
path4261_ud->name = "TITLE";
path4261_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4261_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4261_ud->strokewidth=24.23800087f;
path4261_body->SetUserData(path4261_ud);

//
//path3942
//
b2BodyDef path3942_bdef;
path3942_bdef.type = b2_staticBody;
path3942_bdef.bullet = false;
path3942_bdef.position.Set(-15.047809793f,14.25920008f);
path3942_bdef.linearDamping = 0.0f;
path3942_bdef.angularDamping =0.0f;
b2Body* path3942_body = m_world->CreateBody(&path3942_bdef);

b2CircleShape path3942_s;
path3942_s.m_radius=0.318572368514f;

b2FixtureDef path3942_fdef;
path3942_fdef.shape = &path3942_s;

path3942_fdef.density = 1.0f;
path3942_fdef.friction = 0.1f;
path3942_fdef.restitution = 0.8f;
path3942_fdef.filter.groupIndex = -1;

path3942_body->CreateFixture(&path3942_fdef);
bodylist.push_back(path3942_body);
uData* path3942_ud = new uData();
path3942_ud->name = "TITLE";
path3942_ud->fill.Set(float(104)/255,float(239)/255,float(215)/255);
path3942_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3942_ud->strokewidth=24.23800087f;
path3942_body->SetUserData(path3942_ud);

//
//path3940
//
b2BodyDef path3940_bdef;
path3940_bdef.type = b2_staticBody;
path3940_bdef.bullet = false;
path3940_bdef.position.Set(-15.4416780354f,15.8704793097f);
path3940_bdef.linearDamping = 0.0f;
path3940_bdef.angularDamping =0.0f;
b2Body* path3940_body = m_world->CreateBody(&path3940_bdef);

b2CircleShape path3940_s;
path3940_s.m_radius=0.318572368514f;

b2FixtureDef path3940_fdef;
path3940_fdef.shape = &path3940_s;

path3940_fdef.density = 1.0f;
path3940_fdef.friction = 0.1f;
path3940_fdef.restitution = 0.8f;
path3940_fdef.filter.groupIndex = -1;

path3940_body->CreateFixture(&path3940_fdef);
bodylist.push_back(path3940_body);
uData* path3940_ud = new uData();
path3940_ud->name = "TITLE";
path3940_ud->fill.Set(float(104)/255,float(239)/255,float(215)/255);
path3940_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3940_ud->strokewidth=24.23800087f;
path3940_body->SetUserData(path3940_ud);

//
//path3938
//
b2BodyDef path3938_bdef;
path3938_bdef.type = b2_staticBody;
path3938_bdef.bullet = false;
path3938_bdef.position.Set(-15.3700657021f,17.4459522443f);
path3938_bdef.linearDamping = 0.0f;
path3938_bdef.angularDamping =0.0f;
b2Body* path3938_body = m_world->CreateBody(&path3938_bdef);

b2CircleShape path3938_s;
path3938_s.m_radius=0.318572368514f;

b2FixtureDef path3938_fdef;
path3938_fdef.shape = &path3938_s;

path3938_fdef.density = 1.0f;
path3938_fdef.friction = 0.1f;
path3938_fdef.restitution = 0.8f;
path3938_fdef.filter.groupIndex = -1;

path3938_body->CreateFixture(&path3938_fdef);
bodylist.push_back(path3938_body);
uData* path3938_ud = new uData();
path3938_ud->name = "TITLE";
path3938_ud->fill.Set(float(104)/255,float(239)/255,float(215)/255);
path3938_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3938_ud->strokewidth=24.23800087f;
path3938_body->SetUserData(path3938_ud);

//
//path3936
//
b2BodyDef path3936_bdef;
path3936_bdef.type = b2_staticBody;
path3936_bdef.bullet = false;
path3936_bdef.position.Set(-14.6897477757f,18.6275569715f);
path3936_bdef.linearDamping = 0.0f;
path3936_bdef.angularDamping =0.0f;
b2Body* path3936_body = m_world->CreateBody(&path3936_bdef);

b2CircleShape path3936_s;
path3936_s.m_radius=0.318572368514f;

b2FixtureDef path3936_fdef;
path3936_fdef.shape = &path3936_s;

path3936_fdef.density = 1.0f;
path3936_fdef.friction = 0.1f;
path3936_fdef.restitution = 0.8f;
path3936_fdef.filter.groupIndex = -1;

path3936_body->CreateFixture(&path3936_fdef);
bodylist.push_back(path3936_body);
uData* path3936_ud = new uData();
path3936_ud->name = "TITLE";
path3936_ud->fill.Set(float(104)/255,float(239)/255,float(215)/255);
path3936_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3936_ud->strokewidth=24.23800087f;
path3936_body->SetUserData(path3936_ud);

//
//path4259
//
b2BodyDef path4259_bdef;
path4259_bdef.type = b2_staticBody;
path4259_bdef.bullet = false;
path4259_bdef.position.Set(-13.5797554871f,19.3436811229f);
path4259_bdef.linearDamping = 0.0f;
path4259_bdef.angularDamping =0.0f;
b2Body* path4259_body = m_world->CreateBody(&path4259_bdef);

b2CircleShape path4259_s;
path4259_s.m_radius=0.318572368514f;

b2FixtureDef path4259_fdef;
path4259_fdef.shape = &path4259_s;

path4259_fdef.density = 1.0f;
path4259_fdef.friction = 0.1f;
path4259_fdef.restitution = 0.8f;
path4259_fdef.filter.groupIndex = -1;

path4259_body->CreateFixture(&path4259_fdef);
bodylist.push_back(path4259_body);
uData* path4259_ud = new uData();
path4259_ud->name = "TITLE";
path4259_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4259_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4259_ud->strokewidth=24.23800087f;
path4259_body->SetUserData(path4259_ud);

//
//path3932
//
b2BodyDef path3932_bdef;
path3932_bdef.type = b2_staticBody;
path3932_bdef.bullet = false;
path3932_bdef.position.Set(14.9577883782f,22.0649525946f);
path3932_bdef.linearDamping = 0.0f;
path3932_bdef.angularDamping =0.0f;
b2Body* path3932_body = m_world->CreateBody(&path3932_bdef);

b2CircleShape path3932_s;
path3932_s.m_radius=0.318572368514f;

b2FixtureDef path3932_fdef;
path3932_fdef.shape = &path3932_s;

path3932_fdef.density = 1.0f;
path3932_fdef.friction = 0.1f;
path3932_fdef.restitution = 0.8f;
path3932_fdef.filter.groupIndex = -1;

path3932_body->CreateFixture(&path3932_fdef);
bodylist.push_back(path3932_body);
uData* path3932_ud = new uData();
path3932_ud->name = "TITLE";
path3932_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path3932_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3932_ud->strokewidth=24.23800087f;
path3932_body->SetUserData(path3932_ud);

//
//path3930
//
b2BodyDef path3930_bdef;
path3930_bdef.type = b2_staticBody;
path3930_bdef.bullet = false;
path3930_bdef.position.Set(15.6381066553f,20.9191539756f);
path3930_bdef.linearDamping = 0.0f;
path3930_bdef.angularDamping =0.0f;
b2Body* path3930_body = m_world->CreateBody(&path3930_bdef);

b2CircleShape path3930_s;
path3930_s.m_radius=0.318572368514f;

b2FixtureDef path3930_fdef;
path3930_fdef.shape = &path3930_s;

path3930_fdef.density = 1.0f;
path3930_fdef.friction = 0.1f;
path3930_fdef.restitution = 0.8f;
path3930_fdef.filter.groupIndex = -1;

path3930_body->CreateFixture(&path3930_fdef);
bodylist.push_back(path3930_body);
uData* path3930_ud = new uData();
path3930_ud->name = "TITLE";
path3930_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path3930_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3930_ud->strokewidth=24.23800087f;
path3930_body->SetUserData(path3930_ud);

//
//path3928
//
b2BodyDef path3928_bdef;
path3928_bdef.type = b2_staticBody;
path3928_bdef.bullet = false;
path3928_bdef.position.Set(15.5306879799f,19.4869059065f);
path3928_bdef.linearDamping = 0.0f;
path3928_bdef.angularDamping =0.0f;
b2Body* path3928_body = m_world->CreateBody(&path3928_bdef);

b2CircleShape path3928_s;
path3928_s.m_radius=0.318572368514f;

b2FixtureDef path3928_fdef;
path3928_fdef.shape = &path3928_s;

path3928_fdef.density = 1.0f;
path3928_fdef.friction = 0.1f;
path3928_fdef.restitution = 0.8f;
path3928_fdef.filter.groupIndex = -1;

path3928_body->CreateFixture(&path3928_fdef);
bodylist.push_back(path3928_body);
uData* path3928_ud = new uData();
path3928_ud->name = "TITLE";
path3928_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path3928_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3928_ud->strokewidth=24.23800087f;
path3928_body->SetUserData(path3928_ud);

//
//path3918
//
b2BodyDef path3918_bdef;
path3918_bdef.type = b2_staticBody;
path3918_bdef.bullet = false;
path3918_bdef.position.Set(14.4565012267f,18.6633631966f);
path3918_bdef.linearDamping = 0.0f;
path3918_bdef.angularDamping =0.0f;
b2Body* path3918_body = m_world->CreateBody(&path3918_bdef);

b2CircleShape path3918_s;
path3918_s.m_radius=0.318572368514f;

b2FixtureDef path3918_fdef;
path3918_fdef.shape = &path3918_s;

path3918_fdef.density = 1.0f;
path3918_fdef.friction = 0.1f;
path3918_fdef.restitution = 0.8f;
path3918_fdef.filter.groupIndex = -1;

path3918_body->CreateFixture(&path3918_fdef);
bodylist.push_back(path3918_body);
uData* path3918_ud = new uData();
path3918_ud->name = "TITLE";
path3918_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path3918_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path3918_ud->strokewidth=24.23800087f;
path3918_body->SetUserData(path3918_ud);

//
//path4257
//
b2BodyDef path4257_bdef;
path4257_bdef.type = b2_staticBody;
path4257_bdef.bullet = false;
path4257_bdef.position.Set(13.1316720666f,18.4485259628f);
path4257_bdef.linearDamping = 0.0f;
path4257_bdef.angularDamping =0.0f;
b2Body* path4257_body = m_world->CreateBody(&path4257_bdef);

b2CircleShape path4257_s;
path4257_s.m_radius=0.318572368514f;

b2FixtureDef path4257_fdef;
path4257_fdef.shape = &path4257_s;

path4257_fdef.density = 1.0f;
path4257_fdef.friction = 0.1f;
path4257_fdef.restitution = 0.8f;
path4257_fdef.filter.groupIndex = -1;

path4257_body->CreateFixture(&path4257_fdef);
bodylist.push_back(path4257_body);
uData* path4257_ud = new uData();
path4257_ud->name = "TITLE";
path4257_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4257_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4257_ud->strokewidth=24.23800087f;
path4257_body->SetUserData(path4257_ud);

//
//path4255
//
b2BodyDef path4255_bdef;
path4255_bdef.type = b2_staticBody;
path4255_bdef.bullet = false;
path4255_bdef.position.Set(13.1034783174f,19.3507541091f);
path4255_bdef.linearDamping = 0.0f;
path4255_bdef.angularDamping =0.0f;
b2Body* path4255_body = m_world->CreateBody(&path4255_bdef);

b2CircleShape path4255_s;
path4255_s.m_radius=0.318572368514f;

b2FixtureDef path4255_fdef;
path4255_fdef.shape = &path4255_s;

path4255_fdef.density = 1.0f;
path4255_fdef.friction = 0.1f;
path4255_fdef.restitution = 0.8f;
path4255_fdef.filter.groupIndex = -1;

path4255_body->CreateFixture(&path4255_fdef);
bodylist.push_back(path4255_body);
uData* path4255_ud = new uData();
path4255_ud->name = "TITLE";
path4255_ud->fill.Set(float(104)/255,float(254)/255,float(215)/255);
path4255_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4255_ud->strokewidth=24.23800087f;
path4255_body->SetUserData(path4255_ud);

//
//path4252
//
b2BodyDef path4252_bdef;
path4252_bdef.type = b2_staticBody;
path4252_bdef.bullet = false;
path4252_bdef.position.Set(10.6787399041f,19.1392944439f);
path4252_bdef.linearDamping = 0.0f;
path4252_bdef.angularDamping =0.0f;
b2Body* path4252_body = m_world->CreateBody(&path4252_bdef);

b2CircleShape path4252_s;
path4252_s.m_radius=0.318572368514f;

b2FixtureDef path4252_fdef;
path4252_fdef.shape = &path4252_s;

path4252_fdef.density = 1.0f;
path4252_fdef.friction = 0.1f;
path4252_fdef.restitution = 0.8f;
path4252_fdef.filter.groupIndex = -1;

path4252_body->CreateFixture(&path4252_fdef);
bodylist.push_back(path4252_body);
uData* path4252_ud = new uData();
path4252_ud->name = "TITLE";
path4252_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4252_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4252_ud->strokewidth=24.23800087f;
path4252_body->SetUserData(path4252_ud);

//
//path4250
//
b2BodyDef path4250_bdef;
path4250_bdef.type = b2_staticBody;
path4250_bdef.bullet = false;
path4250_bdef.position.Set(11.2990210897f,18.6317910605f);
path4250_bdef.linearDamping = 0.0f;
path4250_bdef.angularDamping =0.0f;
b2Body* path4250_body = m_world->CreateBody(&path4250_bdef);

b2CircleShape path4250_s;
path4250_s.m_radius=0.318572368514f;

b2FixtureDef path4250_fdef;
path4250_fdef.shape = &path4250_s;

path4250_fdef.density = 1.0f;
path4250_fdef.friction = 0.1f;
path4250_fdef.restitution = 0.8f;
path4250_fdef.filter.groupIndex = -1;

path4250_body->CreateFixture(&path4250_fdef);
bodylist.push_back(path4250_body);
uData* path4250_ud = new uData();
path4250_ud->name = "TITLE";
path4250_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4250_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4250_ud->strokewidth=24.23800087f;
path4250_body->SetUserData(path4250_ud);

//
//path4248
//
b2BodyDef path4248_bdef;
path4248_bdef.type = b2_staticBody;
path4248_bdef.bullet = false;
path4248_bdef.position.Set(11.9244914309f,17.2801396478f);
path4248_bdef.linearDamping = 0.0f;
path4248_bdef.angularDamping =0.0f;
b2Body* path4248_body = m_world->CreateBody(&path4248_bdef);

b2CircleShape path4248_s;
path4248_s.m_radius=0.318572368514f;

b2FixtureDef path4248_fdef;
path4248_fdef.shape = &path4248_s;

path4248_fdef.density = 1.0f;
path4248_fdef.friction = 0.1f;
path4248_fdef.restitution = 0.8f;
path4248_fdef.filter.groupIndex = -1;

path4248_body->CreateFixture(&path4248_fdef);
bodylist.push_back(path4248_body);
uData* path4248_ud = new uData();
path4248_ud->name = "TITLE";
path4248_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4248_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4248_ud->strokewidth=24.23800087f;
path4248_body->SetUserData(path4248_ud);

//
//path4246
//
b2BodyDef path4246_bdef;
path4246_bdef.type = b2_staticBody;
path4246_bdef.bullet = false;
path4246_bdef.position.Set(11.7219413553f,17.9890662804f);
path4246_bdef.linearDamping = 0.0f;
path4246_bdef.angularDamping =0.0f;
b2Body* path4246_body = m_world->CreateBody(&path4246_bdef);

b2CircleShape path4246_s;
path4246_s.m_radius=0.318572368514f;

b2FixtureDef path4246_fdef;
path4246_fdef.shape = &path4246_s;

path4246_fdef.density = 1.0f;
path4246_fdef.friction = 0.1f;
path4246_fdef.restitution = 0.8f;
path4246_fdef.filter.groupIndex = -1;

path4246_body->CreateFixture(&path4246_fdef);
bodylist.push_back(path4246_body);
uData* path4246_ud = new uData();
path4246_ud->name = "TITLE";
path4246_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4246_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4246_ud->strokewidth=24.23800087f;
path4246_body->SetUserData(path4246_ud);

//
//path4244
//
b2BodyDef path4244_bdef;
path4244_bdef.type = b2_staticBody;
path4244_bdef.bullet = false;
path4244_bdef.position.Set(11.7078433117f,16.5030965173f);
path4244_bdef.linearDamping = 0.0f;
path4244_bdef.angularDamping =0.0f;
b2Body* path4244_body = m_world->CreateBody(&path4244_bdef);

b2CircleShape path4244_s;
path4244_s.m_radius=0.318572368514f;

b2FixtureDef path4244_fdef;
path4244_fdef.shape = &path4244_s;

path4244_fdef.density = 1.0f;
path4244_fdef.friction = 0.1f;
path4244_fdef.restitution = 0.8f;
path4244_fdef.filter.groupIndex = -1;

path4244_body->CreateFixture(&path4244_fdef);
bodylist.push_back(path4244_body);
uData* path4244_ud = new uData();
path4244_ud->name = "TITLE";
path4244_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4244_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4244_ud->strokewidth=24.23800087f;
path4244_body->SetUserData(path4244_ud);

//
//path4242
//
b2BodyDef path4242_bdef;
path4242_bdef.type = b2_staticBody;
path4242_bdef.bullet = false;
path4242_bdef.position.Set(11.3695078007f,15.8546200628f);
path4242_bdef.linearDamping = 0.0f;
path4242_bdef.angularDamping =0.0f;
b2Body* path4242_body = m_world->CreateBody(&path4242_bdef);

b2CircleShape path4242_s;
path4242_s.m_radius=0.318572368514f;

b2FixtureDef path4242_fdef;
path4242_fdef.shape = &path4242_s;

path4242_fdef.density = 1.0f;
path4242_fdef.friction = 0.1f;
path4242_fdef.restitution = 0.8f;
path4242_fdef.filter.groupIndex = -1;

path4242_body->CreateFixture(&path4242_fdef);
bodylist.push_back(path4242_body);
uData* path4242_ud = new uData();
path4242_ud->name = "TITLE";
path4242_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4242_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4242_ud->strokewidth=24.23800087f;
path4242_body->SetUserData(path4242_ud);

//
//path4240
//
b2BodyDef path4240_bdef;
path4240_bdef.type = b2_staticBody;
path4240_bdef.bullet = false;
path4240_bdef.position.Set(10.8902000368f,15.4457979578f);
path4240_bdef.linearDamping = 0.0f;
path4240_bdef.angularDamping =0.0f;
b2Body* path4240_body = m_world->CreateBody(&path4240_bdef);

b2CircleShape path4240_s;
path4240_s.m_radius=0.318572368514f;

b2FixtureDef path4240_fdef;
path4240_fdef.shape = &path4240_s;

path4240_fdef.density = 1.0f;
path4240_fdef.friction = 0.1f;
path4240_fdef.restitution = 0.8f;
path4240_fdef.filter.groupIndex = -1;

path4240_body->CreateFixture(&path4240_fdef);
bodylist.push_back(path4240_body);
uData* path4240_ud = new uData();
path4240_ud->name = "TITLE";
path4240_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4240_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4240_ud->strokewidth=24.23800087f;
path4240_body->SetUserData(path4240_ud);

//
//path4238
//
b2BodyDef path4238_bdef;
path4238_bdef.type = b2_staticBody;
path4238_bdef.bullet = false;
path4238_bdef.position.Set(10.1148473857f,15.1638515817f);
path4238_bdef.linearDamping = 0.0f;
path4238_bdef.angularDamping =0.0f;
b2Body* path4238_body = m_world->CreateBody(&path4238_bdef);

b2CircleShape path4238_s;
path4238_s.m_radius=0.318572368514f;

b2FixtureDef path4238_fdef;
path4238_fdef.shape = &path4238_s;

path4238_fdef.density = 1.0f;
path4238_fdef.friction = 0.1f;
path4238_fdef.restitution = 0.8f;
path4238_fdef.filter.groupIndex = -1;

path4238_body->CreateFixture(&path4238_fdef);
bodylist.push_back(path4238_body);
uData* path4238_ud = new uData();
path4238_ud->name = "TITLE";
path4238_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4238_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4238_ud->strokewidth=24.23800087f;
path4238_body->SetUserData(path4238_ud);

//
//path4236
//
b2BodyDef path4236_bdef;
path4236_bdef.type = b2_staticBody;
path4236_bdef.bullet = false;
path4236_bdef.position.Set(9.38178652737f,15.2343381757f);
path4236_bdef.linearDamping = 0.0f;
path4236_bdef.angularDamping =0.0f;
b2Body* path4236_body = m_world->CreateBody(&path4236_bdef);

b2CircleShape path4236_s;
path4236_s.m_radius=0.318572368514f;

b2FixtureDef path4236_fdef;
path4236_fdef.shape = &path4236_s;

path4236_fdef.density = 1.0f;
path4236_fdef.friction = 0.1f;
path4236_fdef.restitution = 0.8f;
path4236_fdef.filter.groupIndex = -1;

path4236_body->CreateFixture(&path4236_fdef);
bodylist.push_back(path4236_body);
uData* path4236_ud = new uData();
path4236_ud->name = "TITLE";
path4236_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4236_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4236_ud->strokewidth=24.23800087f;
path4236_body->SetUserData(path4236_ud);

//
//path4234
//
b2BodyDef path4234_bdef;
path4234_bdef.type = b2_staticBody;
path4234_bdef.bullet = false;
path4234_bdef.position.Set(8.76150534169f,15.8546200628f);
path4234_bdef.linearDamping = 0.0f;
path4234_bdef.angularDamping =0.0f;
b2Body* path4234_body = m_world->CreateBody(&path4234_bdef);

b2CircleShape path4234_s;
path4234_s.m_radius=0.318572368514f;

b2FixtureDef path4234_fdef;
path4234_fdef.shape = &path4234_s;

path4234_fdef.density = 1.0f;
path4234_fdef.friction = 0.1f;
path4234_fdef.restitution = 0.8f;
path4234_fdef.filter.groupIndex = -1;

path4234_body->CreateFixture(&path4234_fdef);
bodylist.push_back(path4234_body);
uData* path4234_ud = new uData();
path4234_ud->name = "TITLE";
path4234_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4234_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4234_ud->strokewidth=24.23800087f;
path4234_body->SetUserData(path4234_ud);

//
//path4232
//
b2BodyDef path4232_bdef;
path4232_bdef.type = b2_staticBody;
path4232_bdef.bullet = false;
path4232_bdef.position.Set(8.38087803793f,16.4889991752f);
path4232_bdef.linearDamping = 0.0f;
path4232_bdef.angularDamping =0.0f;
b2Body* path4232_body = m_world->CreateBody(&path4232_bdef);

b2CircleShape path4232_s;
path4232_s.m_radius=0.318572368514f;

b2FixtureDef path4232_fdef;
path4232_fdef.shape = &path4232_s;

path4232_fdef.density = 1.0f;
path4232_fdef.friction = 0.1f;
path4232_fdef.restitution = 0.8f;
path4232_fdef.filter.groupIndex = -1;

path4232_body->CreateFixture(&path4232_fdef);
bodylist.push_back(path4232_body);
uData* path4232_ud = new uData();
path4232_ud->name = "TITLE";
path4232_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4232_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4232_ud->strokewidth=24.23800087f;
path4232_body->SetUserData(path4232_ud);

//
//path4230
//
b2BodyDef path4230_bdef;
path4230_bdef.type = b2_staticBody;
path4230_bdef.bullet = false;
path4230_bdef.position.Set(8.28219640885f,17.1656703141f);
path4230_bdef.linearDamping = 0.0f;
path4230_bdef.angularDamping =0.0f;
b2Body* path4230_body = m_world->CreateBody(&path4230_bdef);

b2CircleShape path4230_s;
path4230_s.m_radius=0.318572368514f;

b2FixtureDef path4230_fdef;
path4230_fdef.shape = &path4230_s;

path4230_fdef.density = 1.0f;
path4230_fdef.friction = 0.1f;
path4230_fdef.restitution = 0.8f;
path4230_fdef.filter.groupIndex = -1;

path4230_body->CreateFixture(&path4230_fdef);
bodylist.push_back(path4230_body);
uData* path4230_ud = new uData();
path4230_ud->name = "TITLE";
path4230_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4230_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4230_ud->strokewidth=24.23800087f;
path4230_body->SetUserData(path4230_ud);

//
//path4228
//
b2BodyDef path4228_bdef;
path4228_bdef.type = b2_staticBody;
path4228_bdef.bullet = false;
path4228_bdef.position.Set(8.38087803793f,17.9833144891f);
path4228_bdef.linearDamping = 0.0f;
path4228_bdef.angularDamping =0.0f;
b2Body* path4228_body = m_world->CreateBody(&path4228_bdef);

b2CircleShape path4228_s;
path4228_s.m_radius=0.318572368514f;

b2FixtureDef path4228_fdef;
path4228_fdef.shape = &path4228_s;

path4228_fdef.density = 1.0f;
path4228_fdef.friction = 0.1f;
path4228_fdef.restitution = 0.8f;
path4228_fdef.filter.groupIndex = -1;

path4228_body->CreateFixture(&path4228_fdef);
bodylist.push_back(path4228_body);
uData* path4228_ud = new uData();
path4228_ud->name = "TITLE";
path4228_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4228_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4228_ud->strokewidth=24.23800087f;
path4228_body->SetUserData(path4228_ud);

//
//path4226
//
b2BodyDef path4226_bdef;
path4226_bdef.type = b2_staticBody;
path4226_bdef.bullet = false;
path4226_bdef.position.Set(8.74740729811f,18.603596493f);
path4226_bdef.linearDamping = 0.0f;
path4226_bdef.angularDamping =0.0f;
b2Body* path4226_body = m_world->CreateBody(&path4226_bdef);

b2CircleShape path4226_s;
path4226_s.m_radius=0.318572368514f;

b2FixtureDef path4226_fdef;
path4226_fdef.shape = &path4226_s;

path4226_fdef.density = 1.0f;
path4226_fdef.friction = 0.1f;
path4226_fdef.restitution = 0.8f;
path4226_fdef.filter.groupIndex = -1;

path4226_body->CreateFixture(&path4226_fdef);
bodylist.push_back(path4226_body);
uData* path4226_ud = new uData();
path4226_ud->name = "TITLE";
path4226_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4226_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4226_ud->strokewidth=24.23800087f;
path4226_body->SetUserData(path4226_ud);

//
//path4224
//
b2BodyDef path4224_bdef;
path4224_bdef.type = b2_staticBody;
path4224_bdef.bullet = false;
path4224_bdef.position.Set(9.35359160921f,19.0688078499f);
path4224_bdef.linearDamping = 0.0f;
path4224_bdef.angularDamping =0.0f;
b2Body* path4224_body = m_world->CreateBody(&path4224_bdef);

b2CircleShape path4224_s;
path4224_s.m_radius=0.318572368514f;

b2FixtureDef path4224_fdef;
path4224_fdef.shape = &path4224_s;

path4224_fdef.density = 1.0f;
path4224_fdef.friction = 0.1f;
path4224_fdef.restitution = 0.8f;
path4224_fdef.filter.groupIndex = -1;

path4224_body->CreateFixture(&path4224_fdef);
bodylist.push_back(path4224_body);
uData* path4224_ud = new uData();
path4224_ud->name = "TITLE";
path4224_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4224_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4224_ud->strokewidth=24.23800087f;
path4224_body->SetUserData(path4224_ud);

//
//path4222
//
b2BodyDef path4222_bdef;
path4222_bdef.type = b2_staticBody;
path4222_bdef.bullet = false;
path4222_bdef.position.Set(10.0161657566f,19.3225595416f);
path4222_bdef.linearDamping = 0.0f;
path4222_bdef.angularDamping =0.0f;
b2Body* path4222_body = m_world->CreateBody(&path4222_bdef);

b2CircleShape path4222_s;
path4222_s.m_radius=0.318572368514f;

b2FixtureDef path4222_fdef;
path4222_fdef.shape = &path4222_s;

path4222_fdef.density = 1.0f;
path4222_fdef.friction = 0.1f;
path4222_fdef.restitution = 0.8f;
path4222_fdef.filter.groupIndex = -1;

path4222_body->CreateFixture(&path4222_fdef);
bodylist.push_back(path4222_body);
uData* path4222_ud = new uData();
path4222_ud->name = "TITLE";
path4222_ud->fill.Set(float(244)/255,float(170)/255,float(61)/255);
path4222_ud->stroke.Set(float(208)/255,float(47)/255,float(47)/255);
path4222_ud->strokewidth=24.23800087f;
path4222_body->SetUserData(path4222_ud);

//
//path4220
//
b2BodyDef path4220_bdef;
path4220_bdef.type = b2_staticBody;
path4220_bdef.bullet = false;
path4220_bdef.position.Set(5.18078697345f,19.3084621994f);
path4220_bdef.linearDamping = 0.0f;
path4220_bdef.angularDamping =0.0f;
b2Body* path4220_body = m_world->CreateBody(&path4220_bdef);

b2CircleShape path4220_s;
path4220_s.m_radius=0.318572368514f;

b2FixtureDef path4220_fdef;
path4220_fdef.shape = &path4220_s;

path4220_fdef.density = 1.0f;
path4220_fdef.friction = 0.1f;
path4220_fdef.restitution = 0.8f;
path4220_fdef.filter.groupIndex = -1;

path4220_body->CreateFixture(&path4220_fdef);
bodylist.push_back(path4220_body);
uData* path4220_ud = new uData();
path4220_ud->name = "TITLE";
path4220_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4220_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4220_ud->strokewidth=24.23800087f;
path4220_body->SetUserData(path4220_ud);

//
//path4218
//
b2BodyDef path4218_bdef;
path4218_bdef.type = b2_staticBody;
path4218_bdef.bullet = false;
path4218_bdef.position.Set(5.95597947265f,19.1066996736f);
path4218_bdef.linearDamping = 0.0f;
path4218_bdef.angularDamping =0.0f;
b2Body* path4218_body = m_world->CreateBody(&path4218_bdef);

b2CircleShape path4218_s;
path4218_s.m_radius=0.318572368514f;

b2FixtureDef path4218_fdef;
path4218_fdef.shape = &path4218_s;

path4218_fdef.density = 1.0f;
path4218_fdef.friction = 0.1f;
path4218_fdef.restitution = 0.8f;
path4218_fdef.filter.groupIndex = -1;

path4218_body->CreateFixture(&path4218_fdef);
bodylist.push_back(path4218_body);
uData* path4218_ud = new uData();
path4218_ud->name = "TITLE";
path4218_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4218_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4218_ud->strokewidth=24.23800087f;
path4218_body->SetUserData(path4218_ud);

//
//path4216
//
b2BodyDef path4216_bdef;
path4216_bdef.type = b2_staticBody;
path4216_bdef.bullet = false;
path4216_bdef.position.Set(6.48693330327f,18.5757457261f);
path4216_bdef.linearDamping = 0.0f;
path4216_bdef.angularDamping =0.0f;
b2Body* path4216_body = m_world->CreateBody(&path4216_bdef);

b2CircleShape path4216_s;
path4216_s.m_radius=0.318572368514f;

b2FixtureDef path4216_fdef;
path4216_fdef.shape = &path4216_s;

path4216_fdef.density = 1.0f;
path4216_fdef.friction = 0.1f;
path4216_fdef.restitution = 0.8f;
path4216_fdef.filter.groupIndex = -1;

path4216_body->CreateFixture(&path4216_fdef);
bodylist.push_back(path4216_body);
uData* path4216_ud = new uData();
path4216_ud->name = "TITLE";
path4216_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4216_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4216_ud->strokewidth=24.23800087f;
path4216_body->SetUserData(path4216_ud);

//
//path4214
//
b2BodyDef path4214_bdef;
path4214_bdef.type = b2_staticBody;
path4214_bdef.bullet = false;
path4214_bdef.position.Set(6.83736320556f,18.0447917786f);
path4214_bdef.linearDamping = 0.0f;
path4214_bdef.angularDamping =0.0f;
b2Body* path4214_body = m_world->CreateBody(&path4214_bdef);

b2CircleShape path4214_s;
path4214_s.m_radius=0.318572368514f;

b2FixtureDef path4214_fdef;
path4214_fdef.shape = &path4214_s;

path4214_fdef.density = 1.0f;
path4214_fdef.friction = 0.1f;
path4214_fdef.restitution = 0.8f;
path4214_fdef.filter.groupIndex = -1;

path4214_body->CreateFixture(&path4214_fdef);
bodylist.push_back(path4214_body);
uData* path4214_ud = new uData();
path4214_ud->name = "TITLE";
path4214_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4214_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4214_ud->strokewidth=24.23800087f;
path4214_body->SetUserData(path4214_ud);

//
//path4212
//
b2BodyDef path4212_bdef;
path4212_bdef.type = b2_staticBody;
path4212_bdef.bullet = false;
path4212_bdef.position.Set(6.97541064041f,17.2058845742f);
path4212_bdef.linearDamping = 0.0f;
path4212_bdef.angularDamping =0.0f;
b2Body* path4212_body = m_world->CreateBody(&path4212_bdef);

b2CircleShape path4212_s;
path4212_s.m_radius=0.318572368514f;

b2FixtureDef path4212_fdef;
path4212_fdef.shape = &path4212_s;

path4212_fdef.density = 1.0f;
path4212_fdef.friction = 0.1f;
path4212_fdef.restitution = 0.8f;
path4212_fdef.filter.groupIndex = -1;

path4212_body->CreateFixture(&path4212_fdef);
bodylist.push_back(path4212_body);
uData* path4212_ud = new uData();
path4212_ud->name = "TITLE";
path4212_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4212_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4212_ud->strokewidth=24.23800087f;
path4212_body->SetUserData(path4212_ud);

//
//path4210
//
b2BodyDef path4210_bdef;
path4210_bdef.type = b2_staticBody;
path4210_bdef.bullet = false;
path4210_bdef.position.Set(6.86922057568f,16.5368826074f);
path4210_bdef.linearDamping = 0.0f;
path4210_bdef.angularDamping =0.0f;
b2Body* path4210_body = m_world->CreateBody(&path4210_bdef);

b2CircleShape path4210_s;
path4210_s.m_radius=0.318572368514f;

b2FixtureDef path4210_fdef;
path4210_fdef.shape = &path4210_s;

path4210_fdef.density = 1.0f;
path4210_fdef.friction = 0.1f;
path4210_fdef.restitution = 0.8f;
path4210_fdef.filter.groupIndex = -1;

path4210_body->CreateFixture(&path4210_fdef);
bodylist.push_back(path4210_body);
uData* path4210_ud = new uData();
path4210_ud->name = "TITLE";
path4210_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4210_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4210_ud->strokewidth=24.23800087f;
path4210_body->SetUserData(path4210_ud);

//
//path4208
//
b2BodyDef path4208_bdef;
path4208_bdef.type = b2_staticBody;
path4208_bdef.bullet = false;
path4208_bdef.position.Set(6.4763141799f,15.8678806405f);
path4208_bdef.linearDamping = 0.0f;
path4208_bdef.angularDamping =0.0f;
b2Body* path4208_body = m_world->CreateBody(&path4208_bdef);

b2CircleShape path4208_s;
path4208_s.m_radius=0.318572368514f;

b2FixtureDef path4208_fdef;
path4208_fdef.shape = &path4208_s;

path4208_fdef.density = 1.0f;
path4208_fdef.friction = 0.1f;
path4208_fdef.restitution = 0.8f;
path4208_fdef.filter.groupIndex = -1;

path4208_body->CreateFixture(&path4208_fdef);
bodylist.push_back(path4208_body);
uData* path4208_ud = new uData();
path4208_ud->name = "TITLE";
path4208_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4208_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4208_ud->strokewidth=24.23800087f;
path4208_body->SetUserData(path4208_ud);

//
//path4206
//
b2BodyDef path4206_bdef;
path4206_bdef.type = b2_staticBody;
path4206_bdef.bullet = false;
path4206_bdef.position.Set(5.95597947265f,15.390022076f);
path4206_bdef.linearDamping = 0.0f;
path4206_bdef.angularDamping =0.0f;
b2Body* path4206_body = m_world->CreateBody(&path4206_bdef);

b2CircleShape path4206_s;
path4206_s.m_radius=0.318572368514f;

b2FixtureDef path4206_fdef;
path4206_fdef.shape = &path4206_s;

path4206_fdef.density = 1.0f;
path4206_fdef.friction = 0.1f;
path4206_fdef.restitution = 0.8f;
path4206_fdef.filter.groupIndex = -1;

path4206_body->CreateFixture(&path4206_fdef);
bodylist.push_back(path4206_body);
uData* path4206_ud = new uData();
path4206_ud->name = "TITLE";
path4206_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4206_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4206_ud->strokewidth=24.23800087f;
path4206_body->SetUserData(path4206_ud);

//
//path4204
//
b2BodyDef path4204_bdef;
path4204_bdef.type = b2_staticBody;
path4204_bdef.bullet = false;
path4204_bdef.position.Set(5.22326346694f,15.1670214204f);
path4204_bdef.linearDamping = 0.0f;
path4204_bdef.angularDamping =0.0f;
b2Body* path4204_body = m_world->CreateBody(&path4204_bdef);

b2CircleShape path4204_s;
path4204_s.m_radius=0.318572368514f;

b2FixtureDef path4204_fdef;
path4204_fdef.shape = &path4204_s;

path4204_fdef.density = 1.0f;
path4204_fdef.friction = 0.1f;
path4204_fdef.restitution = 0.8f;
path4204_fdef.filter.groupIndex = -1;

path4204_body->CreateFixture(&path4204_fdef);
bodylist.push_back(path4204_body);
uData* path4204_ud = new uData();
path4204_ud->name = "TITLE";
path4204_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4204_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4204_ud->strokewidth=24.23800087f;
path4204_body->SetUserData(path4204_ud);

//
//path4202
//
b2BodyDef path4202_bdef;
path4202_bdef.type = b2_staticBody;
path4202_bdef.bullet = false;
path4202_bdef.position.Set(4.43745067539f,15.4218793293f);
path4202_bdef.linearDamping = 0.0f;
path4202_bdef.angularDamping =0.0f;
b2Body* path4202_body = m_world->CreateBody(&path4202_bdef);

b2CircleShape path4202_s;
path4202_s.m_radius=0.318572368514f;

b2FixtureDef path4202_fdef;
path4202_fdef.shape = &path4202_s;

path4202_fdef.density = 1.0f;
path4202_fdef.friction = 0.1f;
path4202_fdef.restitution = 0.8f;
path4202_fdef.filter.groupIndex = -1;

path4202_body->CreateFixture(&path4202_fdef);
bodylist.push_back(path4202_body);
uData* path4202_ud = new uData();
path4202_ud->name = "TITLE";
path4202_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4202_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4202_ud->strokewidth=24.23800087f;
path4202_body->SetUserData(path4202_ud);

//
//path4200
//
b2BodyDef path4200_bdef;
path4200_bdef.type = b2_staticBody;
path4200_bdef.bullet = false;
path4200_bdef.position.Set(3.94897333825f,15.8147852574f);
path4200_bdef.linearDamping = 0.0f;
path4200_bdef.angularDamping =0.0f;
b2Body* path4200_body = m_world->CreateBody(&path4200_bdef);

b2CircleShape path4200_s;
path4200_s.m_radius=0.318572368514f;

b2FixtureDef path4200_fdef;
path4200_fdef.shape = &path4200_s;

path4200_fdef.density = 1.0f;
path4200_fdef.friction = 0.1f;
path4200_fdef.restitution = 0.8f;
path4200_fdef.filter.groupIndex = -1;

path4200_body->CreateFixture(&path4200_fdef);
bodylist.push_back(path4200_body);
uData* path4200_ud = new uData();
path4200_ud->name = "TITLE";
path4200_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4200_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4200_ud->strokewidth=24.23800087f;
path4200_body->SetUserData(path4200_ud);

//
//path4198
//
b2BodyDef path4198_bdef;
path4198_bdef.type = b2_staticBody;
path4198_bdef.bullet = false;
path4198_bdef.position.Set(3.51359161798f,16.4519299711f);
path4198_bdef.linearDamping = 0.0f;
path4198_bdef.angularDamping =0.0f;
b2Body* path4198_body = m_world->CreateBody(&path4198_bdef);

b2CircleShape path4198_s;
path4198_s.m_radius=0.318572368514f;

b2FixtureDef path4198_fdef;
path4198_fdef.shape = &path4198_s;

path4198_fdef.density = 1.0f;
path4198_fdef.friction = 0.1f;
path4198_fdef.restitution = 0.8f;
path4198_fdef.filter.groupIndex = -1;

path4198_body->CreateFixture(&path4198_fdef);
bodylist.push_back(path4198_body);
uData* path4198_ud = new uData();
path4198_ud->name = "TITLE";
path4198_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4198_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4198_ud->strokewidth=24.23800087f;
path4198_body->SetUserData(path4198_ud);

//
//path4196
//
b2BodyDef path4196_bdef;
path4196_bdef.type = b2_staticBody;
path4196_bdef.bullet = false;
path4196_bdef.position.Set(3.428638631f,17.2377418275f);
path4196_bdef.linearDamping = 0.0f;
path4196_bdef.angularDamping =0.0f;
b2Body* path4196_body = m_world->CreateBody(&path4196_bdef);

b2CircleShape path4196_s;
path4196_s.m_radius=0.318572368514f;

b2FixtureDef path4196_fdef;
path4196_fdef.shape = &path4196_s;

path4196_fdef.density = 1.0f;
path4196_fdef.friction = 0.1f;
path4196_fdef.restitution = 0.8f;
path4196_fdef.filter.groupIndex = -1;

path4196_body->CreateFixture(&path4196_fdef);
bodylist.push_back(path4196_body);
uData* path4196_ud = new uData();
path4196_ud->name = "TITLE";
path4196_ud->fill.Set(float(114)/255,float(220)/255,float(236)/255);
path4196_ud->stroke.Set(float(0)/255,float(148)/255,float(224)/255);
path4196_ud->strokewidth=24.23800087f;
path4196_body->SetUserData(path4196_ud);

//
//path4194
//
b2BodyDef path4194_bdef;
path4194_bdef.type = b2_staticBody;
path4194_bdef.bullet = false;
path4194_bdef.position.Set(0.922536036096f,16.9085504057f);
path4194_bdef.linearDamping = 0.0f;
path4194_bdef.angularDamping =0.0f;
b2Body* path4194_body = m_world->CreateBody(&path4194_bdef);

b2CircleShape path4194_s;
path4194_s.m_radius=0.318572368514f;

b2FixtureDef path4194_fdef;
path4194_fdef.shape = &path4194_s;

path4194_fdef.density = 1.0f;
path4194_fdef.friction = 0.1f;
path4194_fdef.restitution = 0.8f;
path4194_fdef.filter.groupIndex = -1;

path4194_body->CreateFixture(&path4194_fdef);
bodylist.push_back(path4194_body);
uData* path4194_ud = new uData();
path4194_ud->name = "TITLE";
path4194_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4194_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4194_ud->strokewidth=24.23800087f;
path4194_body->SetUserData(path4194_ud);

//
//path4192
//
b2BodyDef path4192_bdef;
path4192_bdef.type = b2_staticBody;
path4192_bdef.bullet = false;
path4192_bdef.position.Set(1.54906080808f,16.9297885356f);
path4192_bdef.linearDamping = 0.0f;
path4192_bdef.angularDamping =0.0f;
b2Body* path4192_body = m_world->CreateBody(&path4192_bdef);

b2CircleShape path4192_s;
path4192_s.m_radius=0.318572368514f;

b2FixtureDef path4192_fdef;
path4192_fdef.shape = &path4192_s;

path4192_fdef.density = 1.0f;
path4192_fdef.friction = 0.1f;
path4192_fdef.restitution = 0.8f;
path4192_fdef.filter.groupIndex = -1;

path4192_body->CreateFixture(&path4192_fdef);
bodylist.push_back(path4192_body);
uData* path4192_ud = new uData();
path4192_ud->name = "TITLE";
path4192_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4192_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4192_ud->strokewidth=24.23800087f;
path4192_body->SetUserData(path4192_ud);

//
//path4190
//
b2BodyDef path4190_bdef;
path4190_bdef.type = b2_staticBody;
path4190_bdef.bullet = false;
path4190_bdef.position.Set(2.12249113219f,17.3226944637f);
path4190_bdef.linearDamping = 0.0f;
path4190_bdef.angularDamping =0.0f;
b2Body* path4190_body = m_world->CreateBody(&path4190_bdef);

b2CircleShape path4190_s;
path4190_s.m_radius=0.318572368514f;

b2FixtureDef path4190_fdef;
path4190_fdef.shape = &path4190_s;

path4190_fdef.density = 1.0f;
path4190_fdef.friction = 0.1f;
path4190_fdef.restitution = 0.8f;
path4190_fdef.filter.groupIndex = -1;

path4190_body->CreateFixture(&path4190_fdef);
bodylist.push_back(path4190_body);
uData* path4190_ud = new uData();
path4190_ud->name = "TITLE";
path4190_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4190_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4190_ud->strokewidth=24.23800087f;
path4190_body->SetUserData(path4190_ud);

//
//path4188
//
b2BodyDef path4188_bdef;
path4188_bdef.type = b2_staticBody;
path4188_bdef.bullet = false;
path4188_bdef.position.Set(2.1968249958f,18.0447917786f);
path4188_bdef.linearDamping = 0.0f;
path4188_bdef.angularDamping =0.0f;
b2Body* path4188_body = m_world->CreateBody(&path4188_bdef);

b2CircleShape path4188_s;
path4188_s.m_radius=0.318572368514f;

b2FixtureDef path4188_fdef;
path4188_fdef.shape = &path4188_s;

path4188_fdef.density = 1.0f;
path4188_fdef.friction = 0.1f;
path4188_fdef.restitution = 0.8f;
path4188_fdef.filter.groupIndex = -1;

path4188_body->CreateFixture(&path4188_fdef);
bodylist.push_back(path4188_body);
uData* path4188_ud = new uData();
path4188_ud->name = "TITLE";
path4188_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4188_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4188_ud->strokewidth=24.23800087f;
path4188_body->SetUserData(path4188_ud);

//
//path4186
//
b2BodyDef path4186_bdef;
path4186_bdef.type = b2_staticBody;
path4186_bdef.bullet = false;
path4186_bdef.position.Set(2.05877756095f,18.7668891285f);
path4186_bdef.linearDamping = 0.0f;
path4186_bdef.angularDamping =0.0f;
b2Body* path4186_body = m_world->CreateBody(&path4186_bdef);

b2CircleShape path4186_s;
path4186_s.m_radius=0.318572368514f;

b2FixtureDef path4186_fdef;
path4186_fdef.shape = &path4186_s;

path4186_fdef.density = 1.0f;
path4186_fdef.friction = 0.1f;
path4186_fdef.restitution = 0.8f;
path4186_fdef.filter.groupIndex = -1;

path4186_body->CreateFixture(&path4186_fdef);
bodylist.push_back(path4186_body);
uData* path4186_ud = new uData();
path4186_ud->name = "TITLE";
path4186_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4186_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4186_ud->strokewidth=24.23800087f;
path4186_body->SetUserData(path4186_ud);

//
//path4184
//
b2BodyDef path4184_bdef;
path4184_bdef.type = b2_staticBody;
path4184_bdef.bullet = false;
path4184_bdef.position.Set(1.53844168471f,19.2128904398f);
path4184_bdef.linearDamping = 0.0f;
path4184_bdef.angularDamping =0.0f;
b2Body* path4184_body = m_world->CreateBody(&path4184_bdef);

b2CircleShape path4184_s;
path4184_s.m_radius=0.318572368514f;

b2FixtureDef path4184_fdef;
path4184_fdef.shape = &path4184_s;

path4184_fdef.density = 1.0f;
path4184_fdef.friction = 0.1f;
path4184_fdef.restitution = 0.8f;
path4184_fdef.filter.groupIndex = -1;

path4184_body->CreateFixture(&path4184_fdef);
bodylist.push_back(path4184_body);
uData* path4184_ud = new uData();
path4184_ud->name = "TITLE";
path4184_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4184_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4184_ud->strokewidth=24.23800087f;
path4184_body->SetUserData(path4184_ud);

//
//path4182
//
b2BodyDef path4182_bdef;
path4182_bdef.type = b2_staticBody;
path4182_bdef.bullet = false;
path4182_bdef.position.Set(0.869440419236f,19.2872240696f);
path4182_bdef.linearDamping = 0.0f;
path4182_bdef.angularDamping =0.0f;
b2Body* path4182_body = m_world->CreateBody(&path4182_bdef);

b2CircleShape path4182_s;
path4182_s.m_radius=0.318572368514f;

b2FixtureDef path4182_fdef;
path4182_fdef.shape = &path4182_s;

path4182_fdef.density = 1.0f;
path4182_fdef.friction = 0.1f;
path4182_fdef.restitution = 0.8f;
path4182_fdef.filter.groupIndex = -1;

path4182_body->CreateFixture(&path4182_fdef);
bodylist.push_back(path4182_body);
uData* path4182_ud = new uData();
path4182_ud->name = "TITLE";
path4182_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4182_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4182_ud->strokewidth=24.23800087f;
path4182_body->SetUserData(path4182_ud);

//
//path4180
//
b2BodyDef path4180_bdef;
path4180_bdef.type = b2_staticBody;
path4180_bdef.bullet = false;
path4180_bdef.position.Set(0.179199738029f,19.2553668164f);
path4180_bdef.linearDamping = 0.0f;
path4180_bdef.angularDamping =0.0f;
b2Body* path4180_body = m_world->CreateBody(&path4180_bdef);

b2CircleShape path4180_s;
path4180_s.m_radius=0.318572368514f;

b2FixtureDef path4180_fdef;
path4180_fdef.shape = &path4180_s;

path4180_fdef.density = 1.0f;
path4180_fdef.friction = 0.1f;
path4180_fdef.restitution = 0.8f;
path4180_fdef.filter.groupIndex = -1;

path4180_body->CreateFixture(&path4180_fdef);
bodylist.push_back(path4180_body);
uData* path4180_ud = new uData();
path4180_ud->name = "TITLE";
path4180_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4180_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4180_ud->strokewidth=24.23800087f;
path4180_body->SetUserData(path4180_ud);

//
//path4178
//
b2BodyDef path4178_bdef;
path4178_bdef.type = b2_staticBody;
path4178_bdef.bullet = false;
path4178_bdef.position.Set(0.179199738029f,18.46955496f);
path4178_bdef.linearDamping = 0.0f;
path4178_bdef.angularDamping =0.0f;
b2Body* path4178_body = m_world->CreateBody(&path4178_bdef);

b2CircleShape path4178_s;
path4178_s.m_radius=0.318572368514f;

b2FixtureDef path4178_fdef;
path4178_fdef.shape = &path4178_s;

path4178_fdef.density = 1.0f;
path4178_fdef.friction = 0.1f;
path4178_fdef.restitution = 0.8f;
path4178_fdef.filter.groupIndex = -1;

path4178_body->CreateFixture(&path4178_fdef);
bodylist.push_back(path4178_body);
uData* path4178_ud = new uData();
path4178_ud->name = "TITLE";
path4178_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4178_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4178_ud->strokewidth=24.23800087f;
path4178_body->SetUserData(path4178_ud);

//
//path4176
//
b2BodyDef path4176_bdef;
path4176_bdef.type = b2_staticBody;
path4176_bdef.bullet = false;
path4176_bdef.position.Set(0.211057108145f,17.6625049738f);
path4176_bdef.linearDamping = 0.0f;
path4176_bdef.angularDamping =0.0f;
b2Body* path4176_body = m_world->CreateBody(&path4176_bdef);

b2CircleShape path4176_s;
path4176_s.m_radius=0.318572368514f;

b2FixtureDef path4176_fdef;
path4176_fdef.shape = &path4176_s;

path4176_fdef.density = 1.0f;
path4176_fdef.friction = 0.1f;
path4176_fdef.restitution = 0.8f;
path4176_fdef.filter.groupIndex = -1;

path4176_body->CreateFixture(&path4176_fdef);
bodylist.push_back(path4176_body);
uData* path4176_ud = new uData();
path4176_ud->name = "TITLE";
path4176_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4176_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4176_ud->strokewidth=24.23800087f;
path4176_body->SetUserData(path4176_ud);

//
//path4174
//
b2BodyDef path4174_bdef;
path4174_bdef.type = b2_staticBody;
path4174_bdef.bullet = false;
path4174_bdef.position.Set(0.23229535489f,16.8766931525f);
path4174_bdef.linearDamping = 0.0f;
path4174_bdef.angularDamping =0.0f;
b2Body* path4174_body = m_world->CreateBody(&path4174_bdef);

b2CircleShape path4174_s;
path4174_s.m_radius=0.318572368514f;

b2FixtureDef path4174_fdef;
path4174_fdef.shape = &path4174_s;

path4174_fdef.density = 1.0f;
path4174_fdef.friction = 0.1f;
path4174_fdef.restitution = 0.8f;
path4174_fdef.filter.groupIndex = -1;

path4174_body->CreateFixture(&path4174_fdef);
bodylist.push_back(path4174_body);
uData* path4174_ud = new uData();
path4174_ud->name = "TITLE";
path4174_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4174_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4174_ud->strokewidth=24.23800087f;
path4174_body->SetUserData(path4174_ud);

//
//path4172
//
b2BodyDef path4172_bdef;
path4172_bdef.type = b2_staticBody;
path4172_bdef.bullet = false;
path4172_bdef.position.Set(0.189818861401f,16.1015003084f);
path4172_bdef.linearDamping = 0.0f;
path4172_bdef.angularDamping =0.0f;
b2Body* path4172_body = m_world->CreateBody(&path4172_bdef);

b2CircleShape path4172_s;
path4172_s.m_radius=0.318572368514f;

b2FixtureDef path4172_fdef;
path4172_fdef.shape = &path4172_s;

path4172_fdef.density = 1.0f;
path4172_fdef.friction = 0.1f;
path4172_fdef.restitution = 0.8f;
path4172_fdef.filter.groupIndex = -1;

path4172_body->CreateFixture(&path4172_fdef);
bodylist.push_back(path4172_body);
uData* path4172_ud = new uData();
path4172_ud->name = "TITLE";
path4172_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4172_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4172_ud->strokewidth=24.23800087f;
path4172_body->SetUserData(path4172_ud);

//
//path4170
//
b2BodyDef path4170_bdef;
path4170_bdef.type = b2_staticBody;
path4170_bdef.bullet = false;
path4170_bdef.position.Set(0.23229535489f,15.2625930632f);
path4170_bdef.linearDamping = 0.0f;
path4170_bdef.angularDamping =0.0f;
b2Body* path4170_body = m_world->CreateBody(&path4170_bdef);

b2CircleShape path4170_s;
path4170_s.m_radius=0.318572368514f;

b2FixtureDef path4170_fdef;
path4170_fdef.shape = &path4170_s;

path4170_fdef.density = 1.0f;
path4170_fdef.friction = 0.1f;
path4170_fdef.restitution = 0.8f;
path4170_fdef.filter.groupIndex = -1;

path4170_body->CreateFixture(&path4170_fdef);
bodylist.push_back(path4170_body);
uData* path4170_ud = new uData();
path4170_ud->name = "TITLE";
path4170_ud->fill.Set(float(247)/255,float(220)/255,float(236)/255);
path4170_ud->stroke.Set(float(249)/255,float(75)/255,float(233)/255);
path4170_ud->strokewidth=24.23800087f;
path4170_body->SetUserData(path4170_ud);

//
//path4168
//
b2BodyDef path4168_bdef;
path4168_bdef.type = b2_staticBody;
path4168_bdef.bullet = false;
path4168_bdef.position.Set(-1.04199360481f,15.2838313099f);
path4168_bdef.linearDamping = 0.0f;
path4168_bdef.angularDamping =0.0f;
b2Body* path4168_body = m_world->CreateBody(&path4168_bdef);

b2CircleShape path4168_s;
path4168_s.m_radius=0.318572368514f;

b2FixtureDef path4168_fdef;
path4168_fdef.shape = &path4168_s;

path4168_fdef.density = 1.0f;
path4168_fdef.friction = 0.1f;
path4168_fdef.restitution = 0.8f;
path4168_fdef.filter.groupIndex = -1;

path4168_body->CreateFixture(&path4168_fdef);
bodylist.push_back(path4168_body);
uData* path4168_ud = new uData();
path4168_ud->name = "TITLE";
path4168_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4168_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4168_ud->strokewidth=24.23800087f;
path4168_body->SetUserData(path4168_ud);

//
//path4166
//
b2BodyDef path4166_bdef;
path4166_bdef.type = b2_staticBody;
path4166_bdef.bullet = false;
path4166_bdef.position.Set(-1.80656709753f,15.2838313099f);
path4166_bdef.linearDamping = 0.0f;
path4166_bdef.angularDamping =0.0f;
b2Body* path4166_body = m_world->CreateBody(&path4166_bdef);

b2CircleShape path4166_s;
path4166_s.m_radius=0.318572368514f;

b2FixtureDef path4166_fdef;
path4166_fdef.shape = &path4166_s;

path4166_fdef.density = 1.0f;
path4166_fdef.friction = 0.1f;
path4166_fdef.restitution = 0.8f;
path4166_fdef.filter.groupIndex = -1;

path4166_body->CreateFixture(&path4166_fdef);
bodylist.push_back(path4166_body);
uData* path4166_ud = new uData();
path4166_ud->name = "TITLE";
path4166_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4166_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4166_ud->strokewidth=24.23800087f;
path4166_body->SetUserData(path4166_ud);

//
//path4164
//
b2BodyDef path4164_bdef;
path4164_bdef.type = b2_staticBody;
path4164_bdef.bullet = false;
path4164_bdef.position.Set(-2.50742631761f,15.2094976801f);
path4164_bdef.linearDamping = 0.0f;
path4164_bdef.angularDamping =0.0f;
b2Body* path4164_body = m_world->CreateBody(&path4164_bdef);

b2CircleShape path4164_s;
path4164_s.m_radius=0.318572368514f;

b2FixtureDef path4164_fdef;
path4164_fdef.shape = &path4164_s;

path4164_fdef.density = 1.0f;
path4164_fdef.friction = 0.1f;
path4164_fdef.restitution = 0.8f;
path4164_fdef.filter.groupIndex = -1;

path4164_body->CreateFixture(&path4164_fdef);
bodylist.push_back(path4164_body);
uData* path4164_ud = new uData();
path4164_ud->name = "TITLE";
path4164_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4164_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4164_ud->strokewidth=24.23800087f;
path4164_body->SetUserData(path4164_ud);

//
//path4162
//
b2BodyDef path4162_bdef;
path4162_bdef.type = b2_staticBody;
path4162_bdef.bullet = false;
path4162_bdef.position.Set(-2.52866444746f,16.0165476663f);
path4162_bdef.linearDamping = 0.0f;
path4162_bdef.angularDamping =0.0f;
b2Body* path4162_body = m_world->CreateBody(&path4162_bdef);

b2CircleShape path4162_s;
path4162_s.m_radius=0.318572368514f;

b2FixtureDef path4162_fdef;
path4162_fdef.shape = &path4162_s;

path4162_fdef.density = 1.0f;
path4162_fdef.friction = 0.1f;
path4162_fdef.restitution = 0.8f;
path4162_fdef.filter.groupIndex = -1;

path4162_body->CreateFixture(&path4162_fdef);
bodylist.push_back(path4162_body);
uData* path4162_ud = new uData();
path4162_ud->name = "TITLE";
path4162_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4162_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4162_ud->strokewidth=24.23800087f;
path4162_body->SetUserData(path4162_ud);

//
//path4160
//
b2BodyDef path4160_bdef;
path4160_bdef.type = b2_staticBody;
path4160_bdef.bullet = false;
path4160_bdef.position.Set(-2.49680719424f,16.8342167759f);
path4160_bdef.linearDamping = 0.0f;
path4160_bdef.angularDamping =0.0f;
b2Body* path4160_body = m_world->CreateBody(&path4160_bdef);

b2CircleShape path4160_s;
path4160_s.m_radius=0.318572368514f;

b2FixtureDef path4160_fdef;
path4160_fdef.shape = &path4160_s;

path4160_fdef.density = 1.0f;
path4160_fdef.friction = 0.1f;
path4160_fdef.restitution = 0.8f;
path4160_fdef.filter.groupIndex = -1;

path4160_body->CreateFixture(&path4160_fdef);
bodylist.push_back(path4160_body);
uData* path4160_ud = new uData();
path4160_ud->name = "TITLE";
path4160_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4160_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4160_ud->strokewidth=24.23800087f;
path4160_body->SetUserData(path4160_ud);

//
//path4158
//
b2BodyDef path4158_bdef;
path4158_bdef.type = b2_staticBody;
path4158_bdef.bullet = false;
path4158_bdef.position.Set(-2.49680719424f,17.6094094738f);
path4158_bdef.linearDamping = 0.0f;
path4158_bdef.angularDamping =0.0f;
b2Body* path4158_body = m_world->CreateBody(&path4158_bdef);

b2CircleShape path4158_s;
path4158_s.m_radius=0.318572368514f;

b2FixtureDef path4158_fdef;
path4158_fdef.shape = &path4158_s;

path4158_fdef.density = 1.0f;
path4158_fdef.friction = 0.1f;
path4158_fdef.restitution = 0.8f;
path4158_fdef.filter.groupIndex = -1;

path4158_body->CreateFixture(&path4158_fdef);
bodylist.push_back(path4158_body);
uData* path4158_ud = new uData();
path4158_ud->name = "TITLE";
path4158_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4158_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4158_ud->strokewidth=24.23800087f;
path4158_body->SetUserData(path4158_ud);

//
//path4156
//
b2BodyDef path4156_bdef;
path4156_bdef.type = b2_staticBody;
path4156_bdef.bullet = false;
path4156_bdef.position.Set(-2.50742631761f,18.4058404536f);
path4156_bdef.linearDamping = 0.0f;
path4156_bdef.angularDamping =0.0f;
b2Body* path4156_body = m_world->CreateBody(&path4156_bdef);

b2CircleShape path4156_s;
path4156_s.m_radius=0.318572368514f;

b2FixtureDef path4156_fdef;
path4156_fdef.shape = &path4156_s;

path4156_fdef.density = 1.0f;
path4156_fdef.friction = 0.1f;
path4156_fdef.restitution = 0.8f;
path4156_fdef.filter.groupIndex = -1;

path4156_body->CreateFixture(&path4156_fdef);
bodylist.push_back(path4156_body);
uData* path4156_ud = new uData();
path4156_ud->name = "TITLE";
path4156_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4156_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4156_ud->strokewidth=24.23800087f;
path4156_body->SetUserData(path4156_ud);

//
//path4154
//
b2BodyDef path4154_bdef;
path4154_bdef.type = b2_staticBody;
path4154_bdef.bullet = false;
path4154_bdef.position.Set(-2.49680719424f,19.2872240696f);
path4154_bdef.linearDamping = 0.0f;
path4154_bdef.angularDamping =0.0f;
b2Body* path4154_body = m_world->CreateBody(&path4154_bdef);

b2CircleShape path4154_s;
path4154_s.m_radius=0.318572368514f;

b2FixtureDef path4154_fdef;
path4154_fdef.shape = &path4154_s;

path4154_fdef.density = 1.0f;
path4154_fdef.friction = 0.1f;
path4154_fdef.restitution = 0.8f;
path4154_fdef.filter.groupIndex = -1;

path4154_body->CreateFixture(&path4154_fdef);
bodylist.push_back(path4154_body);
uData* path4154_ud = new uData();
path4154_ud->name = "TITLE";
path4154_ud->fill.Set(float(225)/255,float(199)/255,float(51)/255);
path4154_ud->stroke.Set(float(200)/255,float(62)/255,float(30)/255);
path4154_ud->strokewidth=24.23800087f;
path4154_body->SetUserData(path4154_ud);

//
//path4152
//
b2BodyDef path4152_bdef;
path4152_bdef.type = b2_staticBody;
path4152_bdef.bullet = false;
path4152_bdef.position.Set(-3.67552497878f,15.2732121866f);
path4152_bdef.linearDamping = 0.0f;
path4152_bdef.angularDamping =0.0f;
b2Body* path4152_body = m_world->CreateBody(&path4152_bdef);

b2CircleShape path4152_s;
path4152_s.m_radius=0.318572368514f;

b2FixtureDef path4152_fdef;
path4152_fdef.shape = &path4152_s;

path4152_fdef.density = 1.0f;
path4152_fdef.friction = 0.1f;
path4152_fdef.restitution = 0.8f;
path4152_fdef.filter.groupIndex = -1;

path4152_body->CreateFixture(&path4152_fdef);
bodylist.push_back(path4152_body);
uData* path4152_ud = new uData();
path4152_ud->name = "TITLE";
path4152_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path4152_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4152_ud->strokewidth=24.23800087f;
path4152_body->SetUserData(path4152_ud);

//
//path4150
//
b2BodyDef path4150_bdef;
path4150_bdef.type = b2_staticBody;
path4150_bdef.bullet = false;
path4150_bdef.position.Set(-4.42947958193f,15.23073581f);
path4150_bdef.linearDamping = 0.0f;
path4150_bdef.angularDamping =0.0f;
b2Body* path4150_body = m_world->CreateBody(&path4150_bdef);

b2CircleShape path4150_s;
path4150_s.m_radius=0.318572368514f;

b2FixtureDef path4150_fdef;
path4150_fdef.shape = &path4150_s;

path4150_fdef.density = 1.0f;
path4150_fdef.friction = 0.1f;
path4150_fdef.restitution = 0.8f;
path4150_fdef.filter.groupIndex = -1;

path4150_body->CreateFixture(&path4150_fdef);
bodylist.push_back(path4150_body);
uData* path4150_ud = new uData();
path4150_ud->name = "TITLE";
path4150_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path4150_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4150_ud->strokewidth=24.23800087f;
path4150_body->SetUserData(path4150_ud);

//
//path4148
//
b2BodyDef path4148_bdef;
path4148_bdef.type = b2_staticBody;
path4148_bdef.bullet = false;
path4148_bdef.position.Set(-5.16219593833f,15.2201166866f);
path4148_bdef.linearDamping = 0.0f;
path4148_bdef.angularDamping =0.0f;
b2Body* path4148_body = m_world->CreateBody(&path4148_bdef);

b2CircleShape path4148_s;
path4148_s.m_radius=0.318572368514f;

b2FixtureDef path4148_fdef;
path4148_fdef.shape = &path4148_s;

path4148_fdef.density = 1.0f;
path4148_fdef.friction = 0.1f;
path4148_fdef.restitution = 0.8f;
path4148_fdef.filter.groupIndex = -1;

path4148_body->CreateFixture(&path4148_fdef);
bodylist.push_back(path4148_body);
uData* path4148_ud = new uData();
path4148_ud->name = "TITLE";
path4148_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path4148_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4148_ud->strokewidth=24.23800087f;
path4148_body->SetUserData(path4148_ud);

//
//path4146
//
b2BodyDef path4146_bdef;
path4146_bdef.type = b2_staticBody;
path4146_bdef.bullet = false;
path4146_bdef.position.Set(-5.15157693186f,16.0271667897f);
path4146_bdef.linearDamping = 0.0f;
path4146_bdef.angularDamping =0.0f;
b2Body* path4146_body = m_world->CreateBody(&path4146_bdef);

b2CircleShape path4146_s;
path4146_s.m_radius=0.318572368514f;

b2FixtureDef path4146_fdef;
path4146_fdef.shape = &path4146_s;

path4146_fdef.density = 1.0f;
path4146_fdef.friction = 0.1f;
path4146_fdef.restitution = 0.8f;
path4146_fdef.filter.groupIndex = -1;

path4146_body->CreateFixture(&path4146_fdef);
bodylist.push_back(path4146_body);
uData* path4146_ud = new uData();
path4146_ud->name = "TITLE";
path4146_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path4146_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4146_ud->strokewidth=24.23800087f;
path4146_body->SetUserData(path4146_ud);

//
//path4144
//
b2BodyDef path4144_bdef;
path4144_bdef.type = b2_staticBody;
path4144_bdef.bullet = false;
path4144_bdef.position.Set(-5.14095780848f,16.8554549057f);
path4144_bdef.linearDamping = 0.0f;
path4144_bdef.angularDamping =0.0f;
b2Body* path4144_body = m_world->CreateBody(&path4144_bdef);

b2CircleShape path4144_s;
path4144_s.m_radius=0.318572368514f;

b2FixtureDef path4144_fdef;
path4144_fdef.shape = &path4144_s;

path4144_fdef.density = 1.0f;
path4144_fdef.friction = 0.1f;
path4144_fdef.restitution = 0.8f;
path4144_fdef.filter.groupIndex = -1;

path4144_body->CreateFixture(&path4144_fdef);
bodylist.push_back(path4144_body);
uData* path4144_ud = new uData();
path4144_ud->name = "TITLE";
path4144_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path4144_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4144_ud->strokewidth=24.23800087f;
path4144_body->SetUserData(path4144_ud);

//
//path4142
//
b2BodyDef path4142_bdef;
path4142_bdef.type = b2_staticBody;
path4142_bdef.bullet = false;
path4142_bdef.position.Set(-5.14095780848f,17.6837429867f);
path4142_bdef.linearDamping = 0.0f;
path4142_bdef.angularDamping =0.0f;
b2Body* path4142_body = m_world->CreateBody(&path4142_bdef);

b2CircleShape path4142_s;
path4142_s.m_radius=0.318572368514f;

b2FixtureDef path4142_fdef;
path4142_fdef.shape = &path4142_s;

path4142_fdef.density = 1.0f;
path4142_fdef.friction = 0.1f;
path4142_fdef.restitution = 0.8f;
path4142_fdef.filter.groupIndex = -1;

path4142_body->CreateFixture(&path4142_fdef);
bodylist.push_back(path4142_body);
uData* path4142_ud = new uData();
path4142_ud->name = "TITLE";
path4142_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path4142_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4142_ud->strokewidth=24.23800087f;
path4142_body->SetUserData(path4142_ud);

//
//path4140
//
b2BodyDef path4140_bdef;
path4140_bdef.type = b2_staticBody;
path4140_bdef.bullet = false;
path4140_bdef.position.Set(-5.16219593833f,18.4376977068f);
path4140_bdef.linearDamping = 0.0f;
path4140_bdef.angularDamping =0.0f;
b2Body* path4140_body = m_world->CreateBody(&path4140_bdef);

b2CircleShape path4140_s;
path4140_s.m_radius=0.318572368514f;

b2FixtureDef path4140_fdef;
path4140_fdef.shape = &path4140_s;

path4140_fdef.density = 1.0f;
path4140_fdef.friction = 0.1f;
path4140_fdef.restitution = 0.8f;
path4140_fdef.filter.groupIndex = -1;

path4140_body->CreateFixture(&path4140_fdef);
bodylist.push_back(path4140_body);
uData* path4140_ud = new uData();
path4140_ud->name = "TITLE";
path4140_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path4140_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4140_ud->strokewidth=24.23800087f;
path4140_body->SetUserData(path4140_ud);

//
//path4138
//
b2BodyDef path4138_bdef;
path4138_bdef.type = b2_staticBody;
path4138_bdef.bullet = false;
path4138_bdef.position.Set(-5.11971967864f,19.297843076f);
path4138_bdef.linearDamping = 0.0f;
path4138_bdef.angularDamping =0.0f;
b2Body* path4138_body = m_world->CreateBody(&path4138_bdef);

b2CircleShape path4138_s;
path4138_s.m_radius=0.318572368514f;

b2FixtureDef path4138_fdef;
path4138_fdef.shape = &path4138_s;

path4138_fdef.density = 1.0f;
path4138_fdef.friction = 0.1f;
path4138_fdef.restitution = 0.8f;
path4138_fdef.filter.groupIndex = -1;

path4138_body->CreateFixture(&path4138_fdef);
bodylist.push_back(path4138_body);
uData* path4138_ud = new uData();
path4138_ud->name = "TITLE";
path4138_ud->fill.Set(float(64)/255,float(247)/255,float(248)/255);
path4138_ud->stroke.Set(float(94)/255,float(33)/255,float(208)/255);
path4138_ud->strokewidth=24.23800087f;
path4138_body->SetUserData(path4138_ud);

//
//path4136
//
b2BodyDef path4136_bdef;
path4136_bdef.type = b2_staticBody;
path4136_bdef.bullet = false;
path4136_bdef.position.Set(-8.70896841533f,15.7829278873f);
path4136_bdef.linearDamping = 0.0f;
path4136_bdef.angularDamping =0.0f;
b2Body* path4136_body = m_world->CreateBody(&path4136_bdef);

b2CircleShape path4136_s;
path4136_s.m_radius=0.318572368514f;

b2FixtureDef path4136_fdef;
path4136_fdef.shape = &path4136_s;

path4136_fdef.density = 1.0f;
path4136_fdef.friction = 0.1f;
path4136_fdef.restitution = 0.8f;
path4136_fdef.filter.groupIndex = -1;

path4136_body->CreateFixture(&path4136_fdef);
bodylist.push_back(path4136_body);
uData* path4136_ud = new uData();
path4136_ud->name = "TITLE";
path4136_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4136_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4136_ud->strokewidth=24.23800087f;
path4136_body->SetUserData(path4136_ud);

//
//path4134
//
b2BodyDef path4134_bdef;
path4134_bdef.type = b2_staticBody;
path4134_bdef.bullet = false;
path4134_bdef.position.Set(-8.00810919525f,15.7829278873f);
path4134_bdef.linearDamping = 0.0f;
path4134_bdef.angularDamping =0.0f;
b2Body* path4134_body = m_world->CreateBody(&path4134_bdef);

b2CircleShape path4134_s;
path4134_s.m_radius=0.318572368514f;

b2FixtureDef path4134_fdef;
path4134_fdef.shape = &path4134_s;

path4134_fdef.density = 1.0f;
path4134_fdef.friction = 0.1f;
path4134_fdef.restitution = 0.8f;
path4134_fdef.filter.groupIndex = -1;

path4134_body->CreateFixture(&path4134_fdef);
bodylist.push_back(path4134_body);
uData* path4134_ud = new uData();
path4134_ud->name = "TITLE";
path4134_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4134_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4134_ud->strokewidth=24.23800087f;
path4134_body->SetUserData(path4134_ud);

//
//path4132
//
b2BodyDef path4132_bdef;
path4132_bdef.type = b2_staticBody;
path4132_bdef.bullet = false;
path4132_bdef.position.Set(-7.25415459211f,15.8041661341f);
path4132_bdef.linearDamping = 0.0f;
path4132_bdef.angularDamping =0.0f;
b2Body* path4132_body = m_world->CreateBody(&path4132_bdef);

b2CircleShape path4132_s;
path4132_s.m_radius=0.318572368514f;

b2FixtureDef path4132_fdef;
path4132_fdef.shape = &path4132_s;

path4132_fdef.density = 1.0f;
path4132_fdef.friction = 0.1f;
path4132_fdef.restitution = 0.8f;
path4132_fdef.filter.groupIndex = -1;

path4132_body->CreateFixture(&path4132_fdef);
bodylist.push_back(path4132_body);
uData* path4132_ud = new uData();
path4132_ud->name = "TITLE";
path4132_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4132_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4132_ud->strokewidth=24.23800087f;
path4132_body->SetUserData(path4132_ud);

//
//path4130
//
b2BodyDef path4130_bdef;
path4130_bdef.type = b2_staticBody;
path4130_bdef.bullet = false;
path4130_bdef.position.Set(-6.37277097609f,15.1139259205f);
path4130_bdef.linearDamping = 0.0f;
path4130_bdef.angularDamping =0.0f;
b2Body* path4130_body = m_world->CreateBody(&path4130_bdef);

b2CircleShape path4130_s;
path4130_s.m_radius=0.318572368514f;

b2FixtureDef path4130_fdef;
path4130_fdef.shape = &path4130_s;

path4130_fdef.density = 1.0f;
path4130_fdef.friction = 0.1f;
path4130_fdef.restitution = 0.8f;
path4130_fdef.filter.groupIndex = -1;

path4130_body->CreateFixture(&path4130_fdef);
bodylist.push_back(path4130_body);
uData* path4130_ud = new uData();
path4130_ud->name = "TITLE";
path4130_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4130_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4130_ud->strokewidth=24.23800087f;
path4130_body->SetUserData(path4130_ud);

//
//path4128
//
b2BodyDef path4128_bdef;
path4128_bdef.type = b2_staticBody;
path4128_bdef.bullet = false;
path4128_bdef.position.Set(-6.61700976156f,15.8254042639f);
path4128_bdef.linearDamping = 0.0f;
path4128_bdef.angularDamping =0.0f;
b2Body* path4128_body = m_world->CreateBody(&path4128_bdef);

b2CircleShape path4128_s;
path4128_s.m_radius=0.318572368514f;

b2FixtureDef path4128_fdef;
path4128_fdef.shape = &path4128_s;

path4128_fdef.density = 1.0f;
path4128_fdef.friction = 0.1f;
path4128_fdef.restitution = 0.8f;
path4128_fdef.filter.groupIndex = -1;

path4128_body->CreateFixture(&path4128_fdef);
bodylist.push_back(path4128_body);
uData* path4128_ud = new uData();
path4128_ud->name = "TITLE";
path4128_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4128_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4128_ud->strokewidth=24.23800087f;
path4128_body->SetUserData(path4128_ud);

//
//path4126
//
b2BodyDef path4126_bdef;
path4126_bdef.type = b2_staticBody;
path4126_bdef.bullet = false;
path4126_bdef.position.Set(-6.84001041718f,16.5687397437f);
path4126_bdef.linearDamping = 0.0f;
path4126_bdef.angularDamping =0.0f;
b2Body* path4126_body = m_world->CreateBody(&path4126_bdef);

b2CircleShape path4126_s;
path4126_s.m_radius=0.318572368514f;

b2FixtureDef path4126_fdef;
path4126_fdef.shape = &path4126_s;

path4126_fdef.density = 1.0f;
path4126_fdef.friction = 0.1f;
path4126_fdef.restitution = 0.8f;
path4126_fdef.filter.groupIndex = -1;

path4126_body->CreateFixture(&path4126_fdef);
bodylist.push_back(path4126_body);
uData* path4126_ud = new uData();
path4126_ud->name = "TITLE";
path4126_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4126_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4126_ud->strokewidth=24.23800087f;
path4126_body->SetUserData(path4126_ud);

//
//path4124
//
b2BodyDef path4124_bdef;
path4124_bdef.type = b2_staticBody;
path4124_bdef.bullet = false;
path4124_bdef.position.Set(-7.05239206633f,17.2165035807f);
path4124_bdef.linearDamping = 0.0f;
path4124_bdef.angularDamping =0.0f;
b2Body* path4124_body = m_world->CreateBody(&path4124_bdef);

b2CircleShape path4124_s;
path4124_s.m_radius=0.318572368514f;

b2FixtureDef path4124_fdef;
path4124_fdef.shape = &path4124_s;

path4124_fdef.density = 1.0f;
path4124_fdef.friction = 0.1f;
path4124_fdef.restitution = 0.8f;
path4124_fdef.filter.groupIndex = -1;

path4124_body->CreateFixture(&path4124_fdef);
bodylist.push_back(path4124_body);
uData* path4124_ud = new uData();
path4124_ud->name = "TITLE";
path4124_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4124_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4124_ud->strokewidth=24.23800087f;
path4124_body->SetUserData(path4124_ud);

//
//path4122
//
b2BodyDef path4122_bdef;
path4122_bdef.type = b2_staticBody;
path4122_bdef.bullet = false;
path4122_bdef.position.Set(-7.27539272195f,17.9173627657f);
path4122_bdef.linearDamping = 0.0f;
path4122_bdef.angularDamping =0.0f;
b2Body* path4122_body = m_world->CreateBody(&path4122_bdef);

b2CircleShape path4122_s;
path4122_s.m_radius=0.318572368514f;

b2FixtureDef path4122_fdef;
path4122_fdef.shape = &path4122_s;

path4122_fdef.density = 1.0f;
path4122_fdef.friction = 0.1f;
path4122_fdef.restitution = 0.8f;
path4122_fdef.filter.groupIndex = -1;

path4122_body->CreateFixture(&path4122_fdef);
bodylist.push_back(path4122_body);
uData* path4122_ud = new uData();
path4122_ud->name = "TITLE";
path4122_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4122_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4122_ud->strokewidth=24.23800087f;
path4122_body->SetUserData(path4122_ud);

//
//path4120
//
b2BodyDef path4120_bdef;
path4120_bdef.type = b2_staticBody;
path4120_bdef.bullet = false;
path4120_bdef.position.Set(-7.49839337757f,18.6713173689f);
path4120_bdef.linearDamping = 0.0f;
path4120_bdef.angularDamping =0.0f;
b2Body* path4120_body = m_world->CreateBody(&path4120_bdef);

b2CircleShape path4120_s;
path4120_s.m_radius=0.318572368514f;

b2FixtureDef path4120_fdef;
path4120_fdef.shape = &path4120_s;

path4120_fdef.density = 1.0f;
path4120_fdef.friction = 0.1f;
path4120_fdef.restitution = 0.8f;
path4120_fdef.filter.groupIndex = -1;

path4120_body->CreateFixture(&path4120_fdef);
bodylist.push_back(path4120_body);
uData* path4120_ud = new uData();
path4120_ud->name = "TITLE";
path4120_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4120_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4120_ud->strokewidth=24.23800087f;
path4120_body->SetUserData(path4120_ud);

//
//path4118
//
b2BodyDef path4118_bdef;
path4118_bdef.type = b2_staticBody;
path4118_bdef.bullet = false;
path4118_bdef.position.Set(-7.65767952676f,19.372176589f);
path4118_bdef.linearDamping = 0.0f;
path4118_bdef.angularDamping =0.0f;
b2Body* path4118_body = m_world->CreateBody(&path4118_bdef);

b2CircleShape path4118_s;
path4118_s.m_radius=0.318572368514f;

b2FixtureDef path4118_fdef;
path4118_fdef.shape = &path4118_s;

path4118_fdef.density = 1.0f;
path4118_fdef.friction = 0.1f;
path4118_fdef.restitution = 0.8f;
path4118_fdef.filter.groupIndex = -1;

path4118_body->CreateFixture(&path4118_fdef);
bodylist.push_back(path4118_body);
uData* path4118_ud = new uData();
path4118_ud->name = "TITLE";
path4118_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4118_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4118_ud->strokewidth=24.23800087f;
path4118_body->SetUserData(path4118_ud);

//
//path4116
//
b2BodyDef path4116_bdef;
path4116_bdef.type = b2_staticBody;
path4116_bdef.bullet = false;
path4116_bdef.position.Set(-8.39039600006f,19.4040338422f);
path4116_bdef.linearDamping = 0.0f;
path4116_bdef.angularDamping =0.0f;
b2Body* path4116_body = m_world->CreateBody(&path4116_bdef);

b2CircleShape path4116_s;
path4116_s.m_radius=0.318572368514f;

b2FixtureDef path4116_fdef;
path4116_fdef.shape = &path4116_s;

path4116_fdef.density = 1.0f;
path4116_fdef.friction = 0.1f;
path4116_fdef.restitution = 0.8f;
path4116_fdef.filter.groupIndex = -1;

path4116_body->CreateFixture(&path4116_fdef);
bodylist.push_back(path4116_body);
uData* path4116_ud = new uData();
path4116_ud->name = "TITLE";
path4116_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4116_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4116_ud->strokewidth=24.23800087f;
path4116_body->SetUserData(path4116_ud);

//
//path4114
//
b2BodyDef path4114_bdef;
path4114_bdef.type = b2_staticBody;
path4114_bdef.bullet = false;
path4114_bdef.position.Set(-8.56030127262f,18.7137937455f);
path4114_bdef.linearDamping = 0.0f;
path4114_bdef.angularDamping =0.0f;
b2Body* path4114_body = m_world->CreateBody(&path4114_bdef);

b2CircleShape path4114_s;
path4114_s.m_radius=0.318572368514f;

b2FixtureDef path4114_fdef;
path4114_fdef.shape = &path4114_s;

path4114_fdef.density = 1.0f;
path4114_fdef.friction = 0.1f;
path4114_fdef.restitution = 0.8f;
path4114_fdef.filter.groupIndex = -1;

path4114_body->CreateFixture(&path4114_fdef);
bodylist.push_back(path4114_body);
uData* path4114_ud = new uData();
path4114_ud->name = "TITLE";
path4114_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4114_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4114_ud->strokewidth=24.23800087f;
path4114_body->SetUserData(path4114_ud);

//
//path4112
//
b2BodyDef path4112_bdef;
path4112_bdef.type = b2_staticBody;
path4112_bdef.bullet = false;
path4112_bdef.position.Set(-8.81515918146f,18.0235535319f);
path4112_bdef.linearDamping = 0.0f;
path4112_bdef.angularDamping =0.0f;
b2Body* path4112_body = m_world->CreateBody(&path4112_bdef);

b2CircleShape path4112_s;
path4112_s.m_radius=0.318572368514f;

b2FixtureDef path4112_fdef;
path4112_fdef.shape = &path4112_s;

path4112_fdef.density = 1.0f;
path4112_fdef.friction = 0.1f;
path4112_fdef.restitution = 0.8f;
path4112_fdef.filter.groupIndex = -1;

path4112_body->CreateFixture(&path4112_fdef);
bodylist.push_back(path4112_body);
uData* path4112_ud = new uData();
path4112_ud->name = "TITLE";
path4112_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4112_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4112_ud->strokewidth=24.23800087f;
path4112_body->SetUserData(path4112_ud);

//
//path4110
//
b2BodyDef path4110_bdef;
path4110_bdef.type = b2_staticBody;
path4110_bdef.bullet = false;
path4110_bdef.position.Set(-8.98506445402f,17.3545516001f);
path4110_bdef.linearDamping = 0.0f;
path4110_bdef.angularDamping =0.0f;
b2Body* path4110_body = m_world->CreateBody(&path4110_bdef);

b2CircleShape path4110_s;
path4110_s.m_radius=0.318572368514f;

b2FixtureDef path4110_fdef;
path4110_fdef.shape = &path4110_s;

path4110_fdef.density = 1.0f;
path4110_fdef.friction = 0.1f;
path4110_fdef.restitution = 0.8f;
path4110_fdef.filter.groupIndex = -1;

path4110_body->CreateFixture(&path4110_fdef);
bodylist.push_back(path4110_body);
uData* path4110_ud = new uData();
path4110_ud->name = "TITLE";
path4110_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4110_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4110_ud->strokewidth=24.23800087f;
path4110_body->SetUserData(path4110_ud);

//
//path4108
//
b2BodyDef path4108_bdef;
path4108_bdef.type = b2_staticBody;
path4108_bdef.bullet = false;
path4108_bdef.position.Set(-9.23992224596f,16.5687397437f);
path4108_bdef.linearDamping = 0.0f;
path4108_bdef.angularDamping =0.0f;
b2Body* path4108_body = m_world->CreateBody(&path4108_bdef);

b2CircleShape path4108_s;
path4108_s.m_radius=0.318572368514f;

b2FixtureDef path4108_fdef;
path4108_fdef.shape = &path4108_s;

path4108_fdef.density = 1.0f;
path4108_fdef.friction = 0.1f;
path4108_fdef.restitution = 0.8f;
path4108_fdef.filter.groupIndex = -1;

path4108_body->CreateFixture(&path4108_fdef);
bodylist.push_back(path4108_body);
uData* path4108_ud = new uData();
path4108_ud->name = "TITLE";
path4108_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4108_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4108_ud->strokewidth=24.23800087f;
path4108_body->SetUserData(path4108_ud);

//
//path4106
//
b2BodyDef path4106_bdef;
path4106_bdef.type = b2_staticBody;
path4106_bdef.bullet = false;
path4106_bdef.position.Set(-9.4947801548f,15.8891187704f);
path4106_bdef.linearDamping = 0.0f;
path4106_bdef.angularDamping =0.0f;
b2Body* path4106_body = m_world->CreateBody(&path4106_bdef);

b2CircleShape path4106_s;
path4106_s.m_radius=0.318572368514f;

b2FixtureDef path4106_fdef;
path4106_fdef.shape = &path4106_s;

path4106_fdef.density = 1.0f;
path4106_fdef.friction = 0.1f;
path4106_fdef.restitution = 0.8f;
path4106_fdef.filter.groupIndex = -1;

path4106_body->CreateFixture(&path4106_fdef);
bodylist.push_back(path4106_body);
uData* path4106_ud = new uData();
path4106_ud->name = "TITLE";
path4106_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4106_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4106_ud->strokewidth=24.23800087f;
path4106_body->SetUserData(path4106_ud);

//
//path4104
//
b2BodyDef path4104_bdef;
path4104_bdef.type = b2_staticBody;
path4104_bdef.bullet = false;
path4104_bdef.position.Set(-9.63282817414f,15.1245450438f);
path4104_bdef.linearDamping = 0.0f;
path4104_bdef.angularDamping =0.0f;
b2Body* path4104_body = m_world->CreateBody(&path4104_bdef);

b2CircleShape path4104_s;
path4104_s.m_radius=0.318572368514f;

b2FixtureDef path4104_fdef;
path4104_fdef.shape = &path4104_s;

path4104_fdef.density = 1.0f;
path4104_fdef.friction = 0.1f;
path4104_fdef.restitution = 0.8f;
path4104_fdef.filter.groupIndex = -1;

path4104_body->CreateFixture(&path4104_fdef);
bodylist.push_back(path4104_body);
uData* path4104_ud = new uData();
path4104_ud->name = "TITLE";
path4104_ud->fill.Set(float(231)/255,float(247)/255,float(168)/255);
path4104_ud->stroke.Set(float(52)/255,float(211)/255,float(24)/255);
path4104_ud->strokewidth=24.23800087f;
path4104_body->SetUserData(path4104_ud);

//
//path4102
//
b2BodyDef path4102_bdef;
path4102_bdef.type = b2_staticBody;
path4102_bdef.bullet = false;
path4102_bdef.position.Set(-11.4911670489f,17.0041220485f);
path4102_bdef.linearDamping = 0.0f;
path4102_bdef.angularDamping =0.0f;
b2Body* path4102_body = m_world->CreateBody(&path4102_bdef);

b2CircleShape path4102_s;
path4102_s.m_radius=0.318572368514f;

b2FixtureDef path4102_fdef;
path4102_fdef.shape = &path4102_s;

path4102_fdef.density = 1.0f;
path4102_fdef.friction = 0.1f;
path4102_fdef.restitution = 0.8f;
path4102_fdef.filter.groupIndex = -1;

path4102_body->CreateFixture(&path4102_fdef);
bodylist.push_back(path4102_body);
uData* path4102_ud = new uData();
path4102_ud->name = "TITLE";
path4102_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4102_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4102_ud->strokewidth=24.23800087f;
path4102_body->SetUserData(path4102_ud);

//
//path4100
//
b2BodyDef path4100_bdef;
path4100_bdef.type = b2_staticBody;
path4100_bdef.bullet = false;
path4100_bdef.position.Set(-11.3212617764f,16.271405581f);
path4100_bdef.linearDamping = 0.0f;
path4100_bdef.angularDamping =0.0f;
b2Body* path4100_body = m_world->CreateBody(&path4100_bdef);

b2CircleShape path4100_s;
path4100_s.m_radius=0.318572368514f;

b2FixtureDef path4100_fdef;
path4100_fdef.shape = &path4100_s;

path4100_fdef.density = 1.0f;
path4100_fdef.friction = 0.1f;
path4100_fdef.restitution = 0.8f;
path4100_fdef.filter.groupIndex = -1;

path4100_body->CreateFixture(&path4100_fdef);
bodylist.push_back(path4100_body);
uData* path4100_ud = new uData();
path4100_ud->name = "TITLE";
path4100_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4100_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4100_ud->strokewidth=24.23800087f;
path4100_body->SetUserData(path4100_ud);

//
//path4098
//
b2BodyDef path4098_bdef;
path4098_bdef.type = b2_staticBody;
path4098_bdef.bullet = false;
path4098_bdef.position.Set(-11.4699289191f,15.5493082252f);
path4098_bdef.linearDamping = 0.0f;
path4098_bdef.angularDamping =0.0f;
b2Body* path4098_body = m_world->CreateBody(&path4098_bdef);

b2CircleShape path4098_s;
path4098_s.m_radius=0.318572368514f;

b2FixtureDef path4098_fdef;
path4098_fdef.shape = &path4098_s;

path4098_fdef.density = 1.0f;
path4098_fdef.friction = 0.1f;
path4098_fdef.restitution = 0.8f;
path4098_fdef.filter.groupIndex = -1;

path4098_body->CreateFixture(&path4098_fdef);
bodylist.push_back(path4098_body);
uData* path4098_ud = new uData();
path4098_ud->name = "TITLE";
path4098_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4098_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4098_ud->strokewidth=24.23800087f;
path4098_body->SetUserData(path4098_ud);

//
//path4096
//
b2BodyDef path4096_bdef;
path4096_bdef.type = b2_staticBody;
path4096_bdef.bullet = false;
path4096_bdef.position.Set(-12.0539782497f,15.3263075696f);
path4096_bdef.linearDamping = 0.0f;
path4096_bdef.angularDamping =0.0f;
b2Body* path4096_body = m_world->CreateBody(&path4096_bdef);

b2CircleShape path4096_s;
path4096_s.m_radius=0.318572368514f;

b2FixtureDef path4096_fdef;
path4096_fdef.shape = &path4096_s;

path4096_fdef.density = 1.0f;
path4096_fdef.friction = 0.1f;
path4096_fdef.restitution = 0.8f;
path4096_fdef.filter.groupIndex = -1;

path4096_body->CreateFixture(&path4096_fdef);
bodylist.push_back(path4096_body);
uData* path4096_ud = new uData();
path4096_ud->name = "TITLE";
path4096_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4096_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4096_ud->strokewidth=24.23800087f;
path4096_body->SetUserData(path4096_ud);

//
//path4094
//
b2BodyDef path4094_bdef;
path4094_bdef.type = b2_staticBody;
path4094_bdef.bullet = false;
path4094_bdef.position.Set(-12.7335993399f,15.23073581f);
path4094_bdef.linearDamping = 0.0f;
path4094_bdef.angularDamping =0.0f;
b2Body* path4094_body = m_world->CreateBody(&path4094_bdef);

b2CircleShape path4094_s;
path4094_s.m_radius=0.318572368514f;

b2FixtureDef path4094_fdef;
path4094_fdef.shape = &path4094_s;

path4094_fdef.density = 1.0f;
path4094_fdef.friction = 0.1f;
path4094_fdef.restitution = 0.8f;
path4094_fdef.filter.groupIndex = -1;

path4094_body->CreateFixture(&path4094_fdef);
bodylist.push_back(path4094_body);
uData* path4094_ud = new uData();
path4094_ud->name = "TITLE";
path4094_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4094_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4094_ud->strokewidth=24.23800087f;
path4094_body->SetUserData(path4094_ud);

//
//path4092
//
b2BodyDef path4092_bdef;
path4092_bdef.type = b2_staticBody;
path4092_bdef.bullet = false;
path4092_bdef.position.Set(-13.4344585506f,15.1988785567f);
path4092_bdef.linearDamping = 0.0f;
path4092_bdef.angularDamping =0.0f;
b2Body* path4092_body = m_world->CreateBody(&path4092_bdef);

b2CircleShape path4092_s;
path4092_s.m_radius=0.318572368514f;

b2FixtureDef path4092_fdef;
path4092_fdef.shape = &path4092_s;

path4092_fdef.density = 1.0f;
path4092_fdef.friction = 0.1f;
path4092_fdef.restitution = 0.8f;
path4092_fdef.filter.groupIndex = -1;

path4092_body->CreateFixture(&path4092_fdef);
bodylist.push_back(path4092_body);
uData* path4092_ud = new uData();
path4092_ud->name = "TITLE";
path4092_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4092_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4092_ud->strokewidth=24.23800087f;
path4092_body->SetUserData(path4092_ud);

//
//path4090
//
b2BodyDef path4090_bdef;
path4090_bdef.type = b2_staticBody;
path4090_bdef.bullet = false;
path4090_bdef.position.Set(-13.4556967091f,16.0271667897f);
path4090_bdef.linearDamping = 0.0f;
path4090_bdef.angularDamping =0.0f;
b2Body* path4090_body = m_world->CreateBody(&path4090_bdef);

b2CircleShape path4090_s;
path4090_s.m_radius=0.318572368514f;

b2FixtureDef path4090_fdef;
path4090_fdef.shape = &path4090_s;

path4090_fdef.density = 1.0f;
path4090_fdef.friction = 0.1f;
path4090_fdef.restitution = 0.8f;
path4090_fdef.filter.groupIndex = -1;

path4090_body->CreateFixture(&path4090_fdef);
bodylist.push_back(path4090_body);
uData* path4090_ud = new uData();
path4090_ud->name = "TITLE";
path4090_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4090_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4090_ud->strokewidth=24.23800087f;
path4090_body->SetUserData(path4090_ud);

//
//path4088
//
b2BodyDef path4088_bdef;
path4088_bdef.type = b2_staticBody;
path4088_bdef.bullet = false;
path4088_bdef.position.Set(-13.4450776272f,16.8448358993f);
path4088_bdef.linearDamping = 0.0f;
path4088_bdef.angularDamping =0.0f;
b2Body* path4088_body = m_world->CreateBody(&path4088_bdef);

b2CircleShape path4088_s;
path4088_s.m_radius=0.318572368514f;

b2FixtureDef path4088_fdef;
path4088_fdef.shape = &path4088_s;

path4088_fdef.density = 1.0f;
path4088_fdef.friction = 0.1f;
path4088_fdef.restitution = 0.8f;
path4088_fdef.filter.groupIndex = -1;

path4088_body->CreateFixture(&path4088_fdef);
bodylist.push_back(path4088_body);
uData* path4088_ud = new uData();
path4088_ud->name = "TITLE";
path4088_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4088_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4088_ud->strokewidth=24.23800087f;
path4088_body->SetUserData(path4088_ud);

//
//path4086
//
b2BodyDef path4086_bdef;
path4086_bdef.type = b2_staticBody;
path4086_bdef.bullet = false;
path4086_bdef.position.Set(-13.4556967068f,17.6731239803f);
path4086_bdef.linearDamping = 0.0f;
path4086_bdef.angularDamping =0.0f;
b2Body* path4086_body = m_world->CreateBody(&path4086_bdef);

b2CircleShape path4086_s;
path4086_s.m_radius=0.318572368514f;

b2FixtureDef path4086_fdef;
path4086_fdef.shape = &path4086_s;

path4086_fdef.density = 1.0f;
path4086_fdef.friction = 0.1f;
path4086_fdef.restitution = 0.8f;
path4086_fdef.filter.groupIndex = -1;

path4086_body->CreateFixture(&path4086_fdef);
bodylist.push_back(path4086_body);
uData* path4086_ud = new uData();
path4086_ud->name = "TITLE";
path4086_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4086_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4086_ud->strokewidth=24.23800087f;
path4086_body->SetUserData(path4086_ud);

//
//path4084
//
b2BodyDef path4084_bdef;
path4084_bdef.type = b2_staticBody;
path4084_bdef.bullet = false;
path4084_bdef.position.Set(-13.4450776272f,18.5014122132f);
path4084_bdef.linearDamping = 0.0f;
path4084_bdef.angularDamping =0.0f;
b2Body* path4084_body = m_world->CreateBody(&path4084_bdef);

b2CircleShape path4084_s;
path4084_s.m_radius=0.318572368514f;

b2FixtureDef path4084_fdef;
path4084_fdef.shape = &path4084_s;

path4084_fdef.density = 1.0f;
path4084_fdef.friction = 0.1f;
path4084_fdef.restitution = 0.8f;
path4084_fdef.filter.groupIndex = -1;

path4084_body->CreateFixture(&path4084_fdef);
bodylist.push_back(path4084_body);
uData* path4084_ud = new uData();
path4084_ud->name = "TITLE";
path4084_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4084_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4084_ud->strokewidth=24.23800087f;
path4084_body->SetUserData(path4084_ud);

//
//path4082
//
b2BodyDef path4082_bdef;
path4082_bdef.type = b2_staticBody;
path4082_bdef.bullet = false;
path4082_bdef.position.Set(-12.5849322323f,17.3333134702f);
path4082_bdef.linearDamping = 0.0f;
path4082_bdef.angularDamping =0.0f;
b2Body* path4082_body = m_world->CreateBody(&path4082_bdef);

b2CircleShape path4082_s;
path4082_s.m_radius=0.318572368514f;

b2FixtureDef path4082_fdef;
path4082_fdef.shape = &path4082_s;

path4082_fdef.density = 1.0f;
path4082_fdef.friction = 0.1f;
path4082_fdef.restitution = 0.8f;
path4082_fdef.filter.groupIndex = -1;

path4082_body->CreateFixture(&path4082_fdef);
bodylist.push_back(path4082_body);
uData* path4082_ud = new uData();
path4082_ud->name = "TITLE";
path4082_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4082_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4082_ud->strokewidth=24.23800087f;
path4082_body->SetUserData(path4082_ud);

//
//path4080
//
b2BodyDef path4080_bdef;
path4080_bdef.type = b2_staticBody;
path4080_bdef.bullet = false;
path4080_bdef.position.Set(-11.95840649f,17.5775522206f);
path4080_bdef.linearDamping = 0.0f;
path4080_bdef.angularDamping =0.0f;
b2Body* path4080_body = m_world->CreateBody(&path4080_bdef);

b2CircleShape path4080_s;
path4080_s.m_radius=0.318572368514f;

b2FixtureDef path4080_fdef;
path4080_fdef.shape = &path4080_s;

path4080_fdef.density = 1.0f;
path4080_fdef.friction = 0.1f;
path4080_fdef.restitution = 0.8f;
path4080_fdef.filter.groupIndex = -1;

path4080_body->CreateFixture(&path4080_fdef);
bodylist.push_back(path4080_body);
uData* path4080_ud = new uData();
path4080_ud->name = "TITLE";
path4080_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4080_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4080_ud->strokewidth=24.23800087f;
path4080_body->SetUserData(path4080_ud);

//
//path4078
//
b2BodyDef path4078_bdef;
path4078_bdef.type = b2_staticBody;
path4078_bdef.bullet = false;
path4078_bdef.position.Set(-11.4274525425f,18.1616016681f);
path4078_bdef.linearDamping = 0.0f;
path4078_bdef.angularDamping =0.0f;
b2Body* path4078_body = m_world->CreateBody(&path4078_bdef);

b2CircleShape path4078_s;
path4078_s.m_radius=0.318572368514f;

b2FixtureDef path4078_fdef;
path4078_fdef.shape = &path4078_s;

path4078_fdef.density = 1.0f;
path4078_fdef.friction = 0.1f;
path4078_fdef.restitution = 0.8f;
path4078_fdef.filter.groupIndex = -1;

path4078_body->CreateFixture(&path4078_fdef);
bodylist.push_back(path4078_body);
uData* path4078_ud = new uData();
path4078_ud->name = "TITLE";
path4078_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4078_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4078_ud->strokewidth=24.23800087f;
path4078_body->SetUserData(path4078_ud);

//
//path4076
//
b2BodyDef path4076_bdef;
path4076_bdef.type = b2_staticBody;
path4076_bdef.bullet = false;
path4076_bdef.position.Set(-11.5761196852f,18.8624608882f);
path4076_bdef.linearDamping = 0.0f;
path4076_bdef.angularDamping =0.0f;
b2Body* path4076_body = m_world->CreateBody(&path4076_bdef);

b2CircleShape path4076_s;
path4076_s.m_radius=0.318572368514f;

b2FixtureDef path4076_fdef;
path4076_fdef.shape = &path4076_s;

path4076_fdef.density = 1.0f;
path4076_fdef.friction = 0.1f;
path4076_fdef.restitution = 0.8f;
path4076_fdef.filter.groupIndex = -1;

path4076_body->CreateFixture(&path4076_fdef);
bodylist.push_back(path4076_body);
uData* path4076_ud = new uData();
path4076_ud->name = "TITLE";
path4076_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4076_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4076_ud->strokewidth=24.23800087f;
path4076_body->SetUserData(path4076_ud);

//
//path3968
//
b2BodyDef path3968_bdef;
path3968_bdef.type = b2_staticBody;
path3968_bdef.bullet = false;
path3968_bdef.position.Set(-6.83125688815f,11.3018935749f);
path3968_bdef.linearDamping = 0.0f;
path3968_bdef.angularDamping =0.0f;
b2Body* path3968_body = m_world->CreateBody(&path3968_bdef);

b2CircleShape path3968_s;
path3968_s.m_radius=0.318572368514f;

b2FixtureDef path3968_fdef;
path3968_fdef.shape = &path3968_s;

path3968_fdef.density = 1.0f;
path3968_fdef.friction = 0.1f;
path3968_fdef.restitution = 0.8f;
path3968_fdef.filter.groupIndex = -1;

path3968_body->CreateFixture(&path3968_fdef);
bodylist.push_back(path3968_body);
uData* path3968_ud = new uData();
path3968_ud->name = "TITLE";
path3968_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3968_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3968_ud->strokewidth=24.23800087f;
path3968_body->SetUserData(path3968_ud);

//
//path3966
//
b2BodyDef path3966_bdef;
path3966_bdef.type = b2_staticBody;
path3966_bdef.bullet = false;
path3966_bdef.position.Set(-6.86706311326f,12.1970486181f);
path3966_bdef.linearDamping = 0.0f;
path3966_bdef.angularDamping =0.0f;
b2Body* path3966_body = m_world->CreateBody(&path3966_bdef);

b2CircleShape path3966_s;
path3966_s.m_radius=0.318572368514f;

b2FixtureDef path3966_fdef;
path3966_fdef.shape = &path3966_s;

path3966_fdef.density = 1.0f;
path3966_fdef.friction = 0.1f;
path3966_fdef.restitution = 0.8f;
path3966_fdef.filter.groupIndex = -1;

path3966_body->CreateFixture(&path3966_fdef);
bodylist.push_back(path3966_body);
uData* path3966_ud = new uData();
path3966_ud->name = "TITLE";
path3966_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3966_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3966_ud->strokewidth=24.23800087f;
path3966_body->SetUserData(path3966_ud);

//
//path3964
//
b2BodyDef path3964_bdef;
path3964_bdef.type = b2_staticBody;
path3964_bdef.bullet = false;
path3964_bdef.position.Set(-6.86706311326f,13.0563975531f);
path3964_bdef.linearDamping = 0.0f;
path3964_bdef.angularDamping =0.0f;
b2Body* path3964_body = m_world->CreateBody(&path3964_bdef);

b2CircleShape path3964_s;
path3964_s.m_radius=0.318572368514f;

b2FixtureDef path3964_fdef;
path3964_fdef.shape = &path3964_s;

path3964_fdef.density = 1.0f;
path3964_fdef.friction = 0.1f;
path3964_fdef.restitution = 0.8f;
path3964_fdef.filter.groupIndex = -1;

path3964_body->CreateFixture(&path3964_fdef);
bodylist.push_back(path3964_body);
uData* path3964_ud = new uData();
path3964_ud->name = "TITLE";
path3964_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3964_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3964_ud->strokewidth=24.23800087f;
path3964_body->SetUserData(path3964_ud);

//
//path4050
//
b2BodyDef path4050_bdef;
path4050_bdef.type = b2_staticBody;
path4050_bdef.bullet = false;
path4050_bdef.position.Set(4.68043735352f,11.1228624494f);
path4050_bdef.linearDamping = 0.0f;
path4050_bdef.angularDamping =0.0f;
b2Body* path4050_body = m_world->CreateBody(&path4050_bdef);

b2CircleShape path4050_s;
path4050_s.m_radius=0.318572368514f;

b2FixtureDef path4050_fdef;
path4050_fdef.shape = &path4050_s;

path4050_fdef.density = 1.0f;
path4050_fdef.friction = 0.1f;
path4050_fdef.restitution = 0.8f;
path4050_fdef.filter.groupIndex = -1;

path4050_body->CreateFixture(&path4050_fdef);
bodylist.push_back(path4050_body);
uData* path4050_ud = new uData();
path4050_ud->name = "TITLE";
path4050_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4050_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4050_ud->strokewidth=24.23800087f;
path4050_body->SetUserData(path4050_ud);

//
//path4048
//
b2BodyDef path4048_bdef;
path4048_bdef.type = b2_staticBody;
path4048_bdef.bullet = false;
path4048_bdef.position.Set(4.68043735352f,11.8210834883f);
path4048_bdef.linearDamping = 0.0f;
path4048_bdef.angularDamping =0.0f;
b2Body* path4048_body = m_world->CreateBody(&path4048_bdef);

b2CircleShape path4048_s;
path4048_s.m_radius=0.318572368514f;

b2FixtureDef path4048_fdef;
path4048_fdef.shape = &path4048_s;

path4048_fdef.density = 1.0f;
path4048_fdef.friction = 0.1f;
path4048_fdef.restitution = 0.8f;
path4048_fdef.filter.groupIndex = -1;

path4048_body->CreateFixture(&path4048_fdef);
bodylist.push_back(path4048_body);
uData* path4048_ud = new uData();
path4048_ud->name = "TITLE";
path4048_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4048_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4048_ud->strokewidth=24.23800087f;
path4048_body->SetUserData(path4048_ud);

//
//path4046
//
b2BodyDef path4046_bdef;
path4046_bdef.type = b2_staticBody;
path4046_bdef.bullet = false;
path4046_bdef.position.Set(4.66253424096f,12.4655950726f);
path4046_bdef.linearDamping = 0.0f;
path4046_bdef.angularDamping =0.0f;
b2Body* path4046_body = m_world->CreateBody(&path4046_bdef);

b2CircleShape path4046_s;
path4046_s.m_radius=0.318572368514f;

b2FixtureDef path4046_fdef;
path4046_fdef.shape = &path4046_s;

path4046_fdef.density = 1.0f;
path4046_fdef.friction = 0.1f;
path4046_fdef.restitution = 0.8f;
path4046_fdef.filter.groupIndex = -1;

path4046_body->CreateFixture(&path4046_fdef);
bodylist.push_back(path4046_body);
uData* path4046_ud = new uData();
path4046_ud->name = "TITLE";
path4046_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4046_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4046_ud->strokewidth=24.23800087f;
path4046_body->SetUserData(path4046_ud);

//
//path4044
//
b2BodyDef path4044_bdef;
path4044_bdef.type = b2_staticBody;
path4044_bdef.bullet = false;
path4044_bdef.position.Set(5.64720426247f,13.2712347869f);
path4044_bdef.linearDamping = 0.0f;
path4044_bdef.angularDamping =0.0f;
b2Body* path4044_body = m_world->CreateBody(&path4044_bdef);

b2CircleShape path4044_s;
path4044_s.m_radius=0.318572368514f;

b2FixtureDef path4044_fdef;
path4044_fdef.shape = &path4044_s;

path4044_fdef.density = 1.0f;
path4044_fdef.friction = 0.1f;
path4044_fdef.restitution = 0.8f;
path4044_fdef.filter.groupIndex = -1;

path4044_body->CreateFixture(&path4044_fdef);
bodylist.push_back(path4044_body);
uData* path4044_ud = new uData();
path4044_ud->name = "TITLE";
path4044_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4044_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4044_ud->strokewidth=24.23800087f;
path4044_body->SetUserData(path4044_ud);

//
//path4042
//
b2BodyDef path4042_bdef;
path4042_bdef.type = b2_staticBody;
path4042_bdef.bullet = false;
path4042_bdef.position.Set(4.69834046607f,13.307041012f);
path4042_bdef.linearDamping = 0.0f;
path4042_bdef.angularDamping =0.0f;
b2Body* path4042_body = m_world->CreateBody(&path4042_bdef);

b2CircleShape path4042_s;
path4042_s.m_radius=0.318572368514f;

b2FixtureDef path4042_fdef;
path4042_fdef.shape = &path4042_s;

path4042_fdef.density = 1.0f;
path4042_fdef.friction = 0.1f;
path4042_fdef.restitution = 0.8f;
path4042_fdef.filter.groupIndex = -1;

path4042_body->CreateFixture(&path4042_fdef);
bodylist.push_back(path4042_body);
uData* path4042_ud = new uData();
path4042_ud->name = "TITLE";
path4042_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4042_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4042_ud->strokewidth=24.23800087f;
path4042_body->SetUserData(path4042_ud);

//
//path4040
//
b2BodyDef path4040_bdef;
path4040_bdef.type = b2_staticBody;
path4040_bdef.bullet = false;
path4040_bdef.position.Set(3.73157238813f,13.2712347869f);
path4040_bdef.linearDamping = 0.0f;
path4040_bdef.angularDamping =0.0f;
b2Body* path4040_body = m_world->CreateBody(&path4040_bdef);

b2CircleShape path4040_s;
path4040_s.m_radius=0.318572368514f;

b2FixtureDef path4040_fdef;
path4040_fdef.shape = &path4040_s;

path4040_fdef.density = 1.0f;
path4040_fdef.friction = 0.1f;
path4040_fdef.restitution = 0.8f;
path4040_fdef.filter.groupIndex = -1;

path4040_body->CreateFixture(&path4040_fdef);
bodylist.push_back(path4040_body);
uData* path4040_ud = new uData();
path4040_ud->name = "TITLE";
path4040_ud->fill.Set(float(0)/255,float(255)/255,float(4)/255);
path4040_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4040_ud->strokewidth=24.23800087f;
path4040_body->SetUserData(path4040_ud);

//
//path4038
//
b2BodyDef path4038_bdef;
path4038_bdef.type = b2_staticBody;
path4038_bdef.bullet = false;
path4038_bdef.position.Set(1.08191406806f,11.1944747827f);
path4038_bdef.linearDamping = 0.0f;
path4038_bdef.angularDamping =0.0f;
b2Body* path4038_body = m_world->CreateBody(&path4038_bdef);

b2CircleShape path4038_s;
path4038_s.m_radius=0.318572368514f;

b2FixtureDef path4038_fdef;
path4038_fdef.shape = &path4038_s;

path4038_fdef.density = 1.0f;
path4038_fdef.friction = 0.1f;
path4038_fdef.restitution = 0.8f;
path4038_fdef.filter.groupIndex = -1;

path4038_body->CreateFixture(&path4038_fdef);
bodylist.push_back(path4038_body);
uData* path4038_ud = new uData();
path4038_ud->name = "TITLE";
path4038_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4038_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4038_ud->strokewidth=24.23800087f;
path4038_body->SetUserData(path4038_ud);

//
//path4036
//
b2BodyDef path4036_bdef;
path4036_bdef.type = b2_staticBody;
path4036_bdef.bullet = false;
path4036_bdef.position.Set(1.8159405138f,11.1944747827f);
path4036_bdef.linearDamping = 0.0f;
path4036_bdef.angularDamping =0.0f;
b2Body* path4036_body = m_world->CreateBody(&path4036_bdef);

b2CircleShape path4036_s;
path4036_s.m_radius=0.318572368514f;

b2FixtureDef path4036_fdef;
path4036_fdef.shape = &path4036_s;

path4036_fdef.density = 1.0f;
path4036_fdef.friction = 0.1f;
path4036_fdef.restitution = 0.8f;
path4036_fdef.filter.groupIndex = -1;

path4036_body->CreateFixture(&path4036_fdef);
bodylist.push_back(path4036_body);
uData* path4036_ud = new uData();
path4036_ud->name = "TITLE";
path4036_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4036_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4036_ud->strokewidth=24.23800087f;
path4036_body->SetUserData(path4036_ud);

//
//path4034
//
b2BodyDef path4034_bdef;
path4034_bdef.type = b2_staticBody;
path4034_bdef.bullet = false;
path4034_bdef.position.Set(2.33513077788f,11.6599555922f);
path4034_bdef.linearDamping = 0.0f;
path4034_bdef.angularDamping =0.0f;
b2Body* path4034_body = m_world->CreateBody(&path4034_bdef);

b2CircleShape path4034_s;
path4034_s.m_radius=0.318572368514f;

b2FixtureDef path4034_fdef;
path4034_fdef.shape = &path4034_s;

path4034_fdef.density = 1.0f;
path4034_fdef.friction = 0.1f;
path4034_fdef.restitution = 0.8f;
path4034_fdef.filter.groupIndex = -1;

path4034_body->CreateFixture(&path4034_fdef);
bodylist.push_back(path4034_body);
uData* path4034_ud = new uData();
path4034_ud->name = "TITLE";
path4034_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4034_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4034_ud->strokewidth=24.23800087f;
path4034_body->SetUserData(path4034_ud);

//
//path4032
//
b2BodyDef path4032_bdef;
path4032_bdef.type = b2_staticBody;
path4032_bdef.bullet = false;
path4032_bdef.position.Set(1.69061872591f,12.1791455056f);
path4032_bdef.linearDamping = 0.0f;
path4032_bdef.angularDamping =0.0f;
b2Body* path4032_body = m_world->CreateBody(&path4032_bdef);

b2CircleShape path4032_s;
path4032_s.m_radius=0.318572368514f;

b2FixtureDef path4032_fdef;
path4032_fdef.shape = &path4032_s;

path4032_fdef.density = 1.0f;
path4032_fdef.friction = 0.1f;
path4032_fdef.restitution = 0.8f;
path4032_fdef.filter.groupIndex = -1;

path4032_body->CreateFixture(&path4032_fdef);
bodylist.push_back(path4032_body);
uData* path4032_ud = new uData();
path4032_ud->name = "TITLE";
path4032_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4032_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4032_ud->strokewidth=24.23800087f;
path4032_body->SetUserData(path4032_ud);

//
//path4030
//
b2BodyDef path4030_bdef;
path4030_bdef.type = b2_staticBody;
path4030_bdef.bullet = false;
path4030_bdef.position.Set(1.01030161784f,12.6446261982f);
path4030_bdef.linearDamping = 0.0f;
path4030_bdef.angularDamping =0.0f;
b2Body* path4030_body = m_world->CreateBody(&path4030_bdef);

b2CircleShape path4030_s;
path4030_s.m_radius=0.318572368514f;

b2FixtureDef path4030_fdef;
path4030_fdef.shape = &path4030_s;

path4030_fdef.density = 1.0f;
path4030_fdef.friction = 0.1f;
path4030_fdef.restitution = 0.8f;
path4030_fdef.filter.groupIndex = -1;

path4030_body->CreateFixture(&path4030_fdef);
bodylist.push_back(path4030_body);
uData* path4030_ud = new uData();
path4030_ud->name = "TITLE";
path4030_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4030_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4030_ud->strokewidth=24.23800087f;
path4030_body->SetUserData(path4030_ud);

//
//path4028
//
b2BodyDef path4028_bdef;
path4028_bdef.type = b2_staticBody;
path4028_bdef.bullet = false;
path4028_bdef.position.Set(1.51158760037f,13.2354285618f);
path4028_bdef.linearDamping = 0.0f;
path4028_bdef.angularDamping =0.0f;
b2Body* path4028_body = m_world->CreateBody(&path4028_bdef);

b2CircleShape path4028_s;
path4028_s.m_radius=0.318572368514f;

b2FixtureDef path4028_fdef;
path4028_fdef.shape = &path4028_s;

path4028_fdef.density = 1.0f;
path4028_fdef.friction = 0.1f;
path4028_fdef.restitution = 0.8f;
path4028_fdef.filter.groupIndex = -1;

path4028_body->CreateFixture(&path4028_fdef);
bodylist.push_back(path4028_body);
uData* path4028_ud = new uData();
path4028_ud->name = "TITLE";
path4028_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4028_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4028_ud->strokewidth=24.23800087f;
path4028_body->SetUserData(path4028_ud);

//
//path4026
//
b2BodyDef path4026_bdef;
path4026_bdef.type = b2_staticBody;
path4026_bdef.bullet = false;
path4026_bdef.position.Set(2.29932455277f,13.1638161116f);
path4026_bdef.linearDamping = 0.0f;
path4026_bdef.angularDamping =0.0f;
b2Body* path4026_body = m_world->CreateBody(&path4026_bdef);

b2CircleShape path4026_s;
path4026_s.m_radius=0.318572368514f;

b2FixtureDef path4026_fdef;
path4026_fdef.shape = &path4026_s;

path4026_fdef.density = 1.0f;
path4026_fdef.friction = 0.1f;
path4026_fdef.restitution = 0.8f;
path4026_fdef.filter.groupIndex = -1;

path4026_body->CreateFixture(&path4026_fdef);
bodylist.push_back(path4026_body);
uData* path4026_ud = new uData();
path4026_ud->name = "TITLE";
path4026_ud->fill.Set(float(29)/255,float(54)/255,float(244)/255);
path4026_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4026_ud->strokewidth=24.23800087f;
path4026_body->SetUserData(path4026_ud);

//
//path4012
//
b2BodyDef path4012_bdef;
path4012_bdef.type = b2_staticBody;
path4012_bdef.bullet = false;
path4012_bdef.position.Set(-0.600977343072f,11.2481842372f);
path4012_bdef.linearDamping = 0.0f;
path4012_bdef.angularDamping =0.0f;
b2Body* path4012_body = m_world->CreateBody(&path4012_bdef);

b2CircleShape path4012_s;
path4012_s.m_radius=0.318572368514f;

b2FixtureDef path4012_fdef;
path4012_fdef.shape = &path4012_s;

path4012_fdef.density = 1.0f;
path4012_fdef.friction = 0.1f;
path4012_fdef.restitution = 0.8f;
path4012_fdef.filter.groupIndex = -1;

path4012_body->CreateFixture(&path4012_fdef);
bodylist.push_back(path4012_body);
uData* path4012_ud = new uData();
path4012_ud->name = "TITLE";
path4012_ud->fill.Set(float(245)/255,float(0)/255,float(4)/255);
path4012_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4012_ud->strokewidth=24.23800087f;
path4012_body->SetUserData(path4012_ud);

//
//path4010
//
b2BodyDef path4010_bdef;
path4010_bdef.type = b2_staticBody;
path4010_bdef.bullet = false;
path4010_bdef.position.Set(-0.583074230517f,12.1970486181f);
path4010_bdef.linearDamping = 0.0f;
path4010_bdef.angularDamping =0.0f;
b2Body* path4010_body = m_world->CreateBody(&path4010_bdef);

b2CircleShape path4010_s;
path4010_s.m_radius=0.318572368514f;

b2FixtureDef path4010_fdef;
path4010_fdef.shape = &path4010_s;

path4010_fdef.density = 1.0f;
path4010_fdef.friction = 0.1f;
path4010_fdef.restitution = 0.8f;
path4010_fdef.filter.groupIndex = -1;

path4010_body->CreateFixture(&path4010_fdef);
bodylist.push_back(path4010_body);
uData* path4010_ud = new uData();
path4010_ud->name = "TITLE";
path4010_ud->fill.Set(float(245)/255,float(0)/255,float(4)/255);
path4010_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4010_ud->strokewidth=24.23800087f;
path4010_body->SetUserData(path4010_ud);

//
//path4008
//
b2BodyDef path4008_bdef;
path4008_bdef.type = b2_staticBody;
path4008_bdef.bullet = false;
path4008_bdef.position.Set(-0.600977343072f,13.1638162285f);
path4008_bdef.linearDamping = 0.0f;
path4008_bdef.angularDamping =0.0f;
b2Body* path4008_body = m_world->CreateBody(&path4008_bdef);

b2CircleShape path4008_s;
path4008_s.m_radius=0.318572368514f;

b2FixtureDef path4008_fdef;
path4008_fdef.shape = &path4008_s;

path4008_fdef.density = 1.0f;
path4008_fdef.friction = 0.1f;
path4008_fdef.restitution = 0.8f;
path4008_fdef.filter.groupIndex = -1;

path4008_body->CreateFixture(&path4008_fdef);
bodylist.push_back(path4008_body);
uData* path4008_ud = new uData();
path4008_ud->name = "TITLE";
path4008_ud->fill.Set(float(245)/255,float(0)/255,float(4)/255);
path4008_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4008_ud->strokewidth=24.23800087f;
path4008_body->SetUserData(path4008_ud);

//
//path4006
//
b2BodyDef path4006_bdef;
path4006_bdef.type = b2_staticBody;
path4006_bdef.bullet = false;
path4006_bdef.position.Set(-1.99741977162f,11.2660873498f);
path4006_bdef.linearDamping = 0.0f;
path4006_bdef.angularDamping =0.0f;
b2Body* path4006_body = m_world->CreateBody(&path4006_bdef);

b2CircleShape path4006_s;
path4006_s.m_radius=0.318572368514f;

b2FixtureDef path4006_fdef;
path4006_fdef.shape = &path4006_s;

path4006_fdef.density = 1.0f;
path4006_fdef.friction = 0.1f;
path4006_fdef.restitution = 0.8f;
path4006_fdef.filter.groupIndex = -1;

path4006_body->CreateFixture(&path4006_fdef);
bodylist.push_back(path4006_body);
uData* path4006_ud = new uData();
path4006_ud->name = "TITLE";
path4006_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path4006_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4006_ud->strokewidth=24.23800087f;
path4006_body->SetUserData(path4006_ud);

//
//path4004
//
b2BodyDef path4004_bdef;
path4004_bdef.type = b2_staticBody;
path4004_bdef.bullet = false;
path4004_bdef.position.Set(-2.33757832566f,12.161242393f);
path4004_bdef.linearDamping = 0.0f;
path4004_bdef.angularDamping =0.0f;
b2Body* path4004_body = m_world->CreateBody(&path4004_bdef);

b2CircleShape path4004_s;
path4004_s.m_radius=0.318572368514f;

b2FixtureDef path4004_fdef;
path4004_fdef.shape = &path4004_s;

path4004_fdef.density = 1.0f;
path4004_fdef.friction = 0.1f;
path4004_fdef.restitution = 0.8f;
path4004_fdef.filter.groupIndex = -1;

path4004_body->CreateFixture(&path4004_fdef);
bodylist.push_back(path4004_body);
uData* path4004_ud = new uData();
path4004_ud->name = "TITLE";
path4004_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path4004_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4004_ud->strokewidth=24.23800087f;
path4004_body->SetUserData(path4004_ud);

//
//path4002
//
b2BodyDef path4002_bdef;
path4002_bdef.type = b2_staticBody;
path4002_bdef.bullet = false;
path4002_bdef.position.Set(-2.89257446415f,13.0922037782f);
path4002_bdef.linearDamping = 0.0f;
path4002_bdef.angularDamping =0.0f;
b2Body* path4002_body = m_world->CreateBody(&path4002_bdef);

b2CircleShape path4002_s;
path4002_s.m_radius=0.318572368514f;

b2FixtureDef path4002_fdef;
path4002_fdef.shape = &path4002_s;

path4002_fdef.density = 1.0f;
path4002_fdef.friction = 0.1f;
path4002_fdef.restitution = 0.8f;
path4002_fdef.filter.groupIndex = -1;

path4002_body->CreateFixture(&path4002_fdef);
bodylist.push_back(path4002_body);
uData* path4002_ud = new uData();
path4002_ud->name = "TITLE";
path4002_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path4002_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4002_ud->strokewidth=24.23800087f;
path4002_body->SetUserData(path4002_ud);

//
//path4000
//
b2BodyDef path4000_bdef;
path4000_bdef.type = b2_staticBody;
path4000_bdef.bullet = false;
path4000_bdef.position.Set(-3.32224893166f,12.2149517307f);
path4000_bdef.linearDamping = 0.0f;
path4000_bdef.angularDamping =0.0f;
b2Body* path4000_body = m_world->CreateBody(&path4000_bdef);

b2CircleShape path4000_s;
path4000_s.m_radius=0.318572368514f;

b2FixtureDef path4000_fdef;
path4000_fdef.shape = &path4000_s;

path4000_fdef.density = 1.0f;
path4000_fdef.friction = 0.1f;
path4000_fdef.restitution = 0.8f;
path4000_fdef.filter.groupIndex = -1;

path4000_body->CreateFixture(&path4000_fdef);
bodylist.push_back(path4000_body);
uData* path4000_ud = new uData();
path4000_ud->name = "TITLE";
path4000_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path4000_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path4000_ud->strokewidth=24.23800087f;
path4000_body->SetUserData(path4000_ud);

//
//path3998
//
b2BodyDef path3998_bdef;
path3998_bdef.type = b2_staticBody;
path3998_bdef.bullet = false;
path3998_bdef.position.Set(-3.68031094895f,11.3376998f);
path3998_bdef.linearDamping = 0.0f;
path3998_bdef.angularDamping =0.0f;
b2Body* path3998_body = m_world->CreateBody(&path3998_bdef);

b2CircleShape path3998_s;
path3998_s.m_radius=0.318572368514f;

b2FixtureDef path3998_fdef;
path3998_fdef.shape = &path3998_s;

path3998_fdef.density = 1.0f;
path3998_fdef.friction = 0.1f;
path3998_fdef.restitution = 0.8f;
path3998_fdef.filter.groupIndex = -1;

path3998_body->CreateFixture(&path3998_fdef);
bodylist.push_back(path3998_body);
uData* path3998_ud = new uData();
path3998_ud->name = "TITLE";
path3998_ud->fill.Set(float(2)/255,float(202)/255,float(4)/255);
path3998_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3998_ud->strokewidth=24.23800087f;
path3998_body->SetUserData(path3998_ud);

//
//path3996
//
b2BodyDef path3996_bdef;
path3996_bdef.type = b2_staticBody;
path3996_bdef.bullet = false;
path3996_bdef.position.Set(-5.11255913501f,13.0922037782f);
path3996_bdef.linearDamping = 0.0f;
path3996_bdef.angularDamping =0.0f;
b2Body* path3996_body = m_world->CreateBody(&path3996_bdef);

b2CircleShape path3996_s;
path3996_s.m_radius=0.318572368514f;

b2FixtureDef path3996_fdef;
path3996_fdef.shape = &path3996_s;

path3996_fdef.density = 1.0f;
path3996_fdef.friction = 0.1f;
path3996_fdef.restitution = 0.8f;
path3996_fdef.filter.groupIndex = -1;

path3996_body->CreateFixture(&path3996_fdef);
bodylist.push_back(path3996_body);
uData* path3996_ud = new uData();
path3996_ud->name = "TITLE";
path3996_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3996_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3996_ud->strokewidth=24.23800087f;
path3996_body->SetUserData(path3996_ud);

//
//path3994
//
b2BodyDef path3994_bdef;
path3994_bdef.type = b2_staticBody;
path3994_bdef.bullet = false;
path3994_bdef.position.Set(-5.11255913501f,12.161242393f);
path3994_bdef.linearDamping = 0.0f;
path3994_bdef.angularDamping =0.0f;
b2Body* path3994_body = m_world->CreateBody(&path3994_bdef);

b2CircleShape path3994_s;
path3994_s.m_radius=0.318572368514f;

b2FixtureDef path3994_fdef;
path3994_fdef.shape = &path3994_s;

path3994_fdef.density = 1.0f;
path3994_fdef.friction = 0.1f;
path3994_fdef.restitution = 0.8f;
path3994_fdef.filter.groupIndex = -1;

path3994_body->CreateFixture(&path3994_fdef);
bodylist.push_back(path3994_body);
uData* path3994_ud = new uData();
path3994_ud->name = "TITLE";
path3994_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3994_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3994_ud->strokewidth=24.23800087f;
path3994_body->SetUserData(path3994_ud);

//
//path3992
//
b2BodyDef path3992_bdef;
path3992_bdef.type = b2_staticBody;
path3992_bdef.bullet = false;
path3992_bdef.position.Set(-5.11255901811f,11.3018935749f);
path3992_bdef.linearDamping = 0.0f;
path3992_bdef.angularDamping =0.0f;
b2Body* path3992_body = m_world->CreateBody(&path3992_bdef);

b2CircleShape path3992_s;
path3992_s.m_radius=0.318572368514f;

b2FixtureDef path3992_fdef;
path3992_fdef.shape = &path3992_s;

path3992_fdef.density = 1.0f;
path3992_fdef.friction = 0.1f;
path3992_fdef.restitution = 0.8f;
path3992_fdef.filter.groupIndex = -1;

path3992_body->CreateFixture(&path3992_fdef);
bodylist.push_back(path3992_body);
uData* path3992_ud = new uData();
path3992_ud->name = "TITLE";
path3992_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3992_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3992_ud->strokewidth=24.23800087f;
path3992_body->SetUserData(path3992_ud);

//
//path3990
//
b2BodyDef path3990_bdef;
path3990_bdef.type = b2_staticBody;
path3990_bdef.bullet = false;
path3990_bdef.position.Set(-5.82868316959f,11.8747928259f);
path3990_bdef.linearDamping = 0.0f;
path3990_bdef.angularDamping =0.0f;
b2Body* path3990_body = m_world->CreateBody(&path3990_bdef);

b2CircleShape path3990_s;
path3990_s.m_radius=0.318572368514f;

b2FixtureDef path3990_fdef;
path3990_fdef.shape = &path3990_s;

path3990_fdef.density = 1.0f;
path3990_fdef.friction = 0.1f;
path3990_fdef.restitution = 0.8f;
path3990_fdef.filter.groupIndex = -1;

path3990_body->CreateFixture(&path3990_fdef);
bodylist.push_back(path3990_body);
uData* path3990_ud = new uData();
path3990_ud->name = "TITLE";
path3990_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3990_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3990_ud->strokewidth=24.23800087f;
path3990_body->SetUserData(path3990_ud);

//
//path3988
//
b2BodyDef path3988_bdef;
path3988_bdef.type = b2_staticBody;
path3988_bdef.bullet = false;
path3988_bdef.position.Set(-6.15093907868f,12.5551107523f);
path3988_bdef.linearDamping = 0.0f;
path3988_bdef.angularDamping =0.0f;
b2Body* path3988_body = m_world->CreateBody(&path3988_bdef);

b2CircleShape path3988_s;
path3988_s.m_radius=0.318572368514f;

b2FixtureDef path3988_fdef;
path3988_fdef.shape = &path3988_s;

path3988_fdef.density = 1.0f;
path3988_fdef.friction = 0.1f;
path3988_fdef.restitution = 0.8f;
path3988_fdef.filter.groupIndex = -1;

path3988_body->CreateFixture(&path3988_fdef);
bodylist.push_back(path3988_body);
uData* path3988_ud = new uData();
path3988_ud->name = "TITLE";
path3988_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3988_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3988_ud->strokewidth=24.23800087f;
path3988_body->SetUserData(path3988_ud);

//
//path3986
//
b2BodyDef path3986_bdef;
path3986_bdef.type = b2_staticBody;
path3986_bdef.bullet = false;
path3986_bdef.position.Set(-6.86706311326f,13.0563975531f);
path3986_bdef.linearDamping = 0.0f;
path3986_bdef.angularDamping =0.0f;
b2Body* path3986_body = m_world->CreateBody(&path3986_bdef);

b2CircleShape path3986_s;
path3986_s.m_radius=0.318572368514f;

b2FixtureDef path3986_fdef;
path3986_fdef.shape = &path3986_s;

path3986_fdef.density = 1.0f;
path3986_fdef.friction = 0.1f;
path3986_fdef.restitution = 0.8f;
path3986_fdef.filter.groupIndex = -1;

path3986_body->CreateFixture(&path3986_fdef);
bodylist.push_back(path3986_body);
uData* path3986_ud = new uData();
path3986_ud->name = "TITLE";
path3986_ud->fill.Set(float(245)/255,float(2)/255,float(4)/255);
path3986_ud->stroke.Set(float(46)/255,float(48)/255,float(46)/255);
path3986_ud->strokewidth=24.23800087f;
path3986_body->SetUserData(path3986_ud);

//
//path4074
//
b2BodyDef path4074_bdef;
path4074_bdef.type = b2_staticBody;
path4074_bdef.bullet = false;
path4074_bdef.position.Set(-11.5761196852f,18.8624608882f);
path4074_bdef.linearDamping = 0.0f;
path4074_bdef.angularDamping =0.0f;
b2Body* path4074_body = m_world->CreateBody(&path4074_bdef);

b2CircleShape path4074_s;
path4074_s.m_radius=0.318572368514f;

b2FixtureDef path4074_fdef;
path4074_fdef.shape = &path4074_s;

path4074_fdef.density = 1.0f;
path4074_fdef.friction = 0.1f;
path4074_fdef.restitution = 0.8f;
path4074_fdef.filter.groupIndex = -1;

path4074_body->CreateFixture(&path4074_fdef);
bodylist.push_back(path4074_body);
uData* path4074_ud = new uData();
path4074_ud->name = "TITLE";
path4074_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4074_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4074_ud->strokewidth=24.23800087f;
path4074_body->SetUserData(path4074_ud);

//
//path4072
//
b2BodyDef path4072_bdef;
path4072_bdef.type = b2_staticBody;
path4072_bdef.bullet = false;
path4072_bdef.position.Set(-12.0752163795f,19.3084621994f);
path4072_bdef.linearDamping = 0.0f;
path4072_bdef.angularDamping =0.0f;
b2Body* path4072_body = m_world->CreateBody(&path4072_bdef);

b2CircleShape path4072_s;
path4072_s.m_radius=0.318572368514f;

b2FixtureDef path4072_fdef;
path4072_fdef.shape = &path4072_s;

path4072_fdef.density = 1.0f;
path4072_fdef.friction = 0.1f;
path4072_fdef.restitution = 0.8f;
path4072_fdef.filter.groupIndex = -1;

path4072_body->CreateFixture(&path4072_fdef);
bodylist.push_back(path4072_body);
uData* path4072_ud = new uData();
path4072_ud->name = "TITLE";
path4072_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4072_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4072_ud->strokewidth=24.23800087f;
path4072_body->SetUserData(path4072_ud);

//
//path4070
//
b2BodyDef path4070_bdef;
path4070_bdef.type = b2_staticBody;
path4070_bdef.bullet = false;
path4070_bdef.position.Set(-12.7548374931f,19.3190812059f);
path4070_bdef.linearDamping = 0.0f;
path4070_bdef.angularDamping =0.0f;
b2Body* path4070_body = m_world->CreateBody(&path4070_bdef);

b2CircleShape path4070_s;
path4070_s.m_radius=0.318572368514f;

b2FixtureDef path4070_fdef;
path4070_fdef.shape = &path4070_s;

path4070_fdef.density = 1.0f;
path4070_fdef.friction = 0.1f;
path4070_fdef.restitution = 0.8f;
path4070_fdef.filter.groupIndex = -1;

path4070_body->CreateFixture(&path4070_fdef);
bodylist.push_back(path4070_body);
uData* path4070_ud = new uData();
path4070_ud->name = "TITLE";
path4070_ud->fill.Set(float(255)/255,float(71)/255,float(4)/255);
path4070_ud->stroke.Set(float(211)/255,float(33)/255,float(208)/255);
path4070_ud->strokewidth=24.23800087f;
path4070_body->SetUserData(path4070_ud);
