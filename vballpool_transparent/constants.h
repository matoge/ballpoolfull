//#define DEBUG
#define AUTO_CALIBRATION
#define USE_FTGL
#define USE_OPENNI
#define WW 15.0f
#define WH 12.5f
#define WSCALE 2.5f // Controls X range of robots
#define YOFFSET -8.0f // Position offsets of robots
#define GRAVITY_X 0.0f
#define GRAVITY_Y -1.5f
#define VELOCITY_ITERATIONS 8
#define POSITION_ITERATIONS 3
#define TIMESTEP 10.0f/60.0f
	
#define MAX_USERS 32
#define MAX_PROBLEM 6

#define	RAND_LIMIT	32767

#define RAGDOLL_ROBOT 0
#define RAGDOLL_HUMAN 1

// Dolls
#define MAX_DOOLS 2
#define NOT_CAPTURED 10000

//openNI
#define NI_W 640.0f
#define NI_H 480.0f

#define MAX_BALLS 30
#define MAX_FIRES 300

// Games

#define INITIAL_SCORE 99
#define SCORE_OUT_OF_GAME -1
#define CLEAR_NFRAMES 777
#define ETIME 7777

#define XN_CALIBRATION_FILE_NAME "UserCalibration.bin"

