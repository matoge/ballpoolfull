/*
Class ragdool

"ragdoll_bodies" is the list of pointers of all bodies.
All items are added by svgbox script.
*/

#pragma once

#include <Box2D/Box2D.h>
#include <vector>
#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>
#include "userdata.h"
#include <vector>
#include <math.h>

using namespace std;

// Globals
extern xn::DepthGenerator g_DepthGenerator;
extern xn::UserGenerator g_UserGenerator;

struct Polar{
	Polar(){
		angle = 0.0f;
		radius = 0.0f; 
	}
	Polar(float32 angle, float32 radius) : angle(angle), radius(radius) {}
	float angle;
	float radius;
	b2Vec2 getReal(){
		return b2Vec2( radius* cos(angle), radius * sin(angle) );
	}
};

class ragdoll
{
public:
	ragdoll(b2World* m_world, b2Body* gB, int dolltype, int dollid);

	void update();
	void MoveLimb(XnUserID player, XnSkeletonJoint eJoint1, XnSkeletonJoint eJoint2);

	vector<b2Body*> ragdoll_bodylist;
	bool isfree();
	// pointers to all bodies
	//vector<b2Body*> ragdoll_bodies; 

	// robot

private:
	XnUserID uid; // Corresponding User ID
	int doolid;
	int dooltype;

	b2Body* groundBody;
	b2MouseJoint* mj_head;
	b2MouseJoint* mj_neck;

	b2MouseJoint* mj_torso;

	b2MouseJoint* mj_rightshoulder;
	b2MouseJoint* mj_leftshoulder;

	b2MouseJoint* mj_righthand;
	b2MouseJoint* mj_rightelbow;
	b2MouseJoint* mj_lefthand;
	b2MouseJoint* mj_leftelbow;

	b2MouseJoint* mj_leftweist;
	b2MouseJoint* mj_rightweist;

	b2MouseJoint* mj_rightknee;
	b2MouseJoint* mj_rightfoot;
	b2MouseJoint* mj_leftknee;
	b2MouseJoint* mj_leftfoot;

	float torso_width;
	float torso_height;
	b2Vec2 torso_center;
	b2Vec2 torso_init_extent;
	
	// stores relative polar points of all the parts of the ragdoll
	vector<Polar> m_pPoints;

	// ground offset. determined when first caliburated
	float goffset;
	bool isCaliburated;

	// default position
	b2Vec2 defPos[2];

	b2Vec2 kinect_to_box2d(XnPoint3D input);

	void mj_intialize();
	void mj_looseall();
	void mj_destroy();
};
