
// Set world's gravity
// This changes balls' and stars' behavior
#define GRAVITY_X 0.0f
#define GRAVITY_Y -2.5f

// Number of balls
#define MAX_BALLS 60
#define MAX_FIRES 300


#define AUTO_CALIBRATION
#define USE_FTGL
#define WW 15.0f
#define WH 12.5f
#define WSCALE 2.5f // Controls X range of robots
#define YOFFSET -8.0f // Position offsets of robots


#define VELOCITY_ITERATIONS 8
#define POSITION_ITERATIONS 3
#define TIMESTEP 10.0f/60.0f
	
#define MAX_USERS 32
#define MAX_PROBLEM 6

#define	RAND_LIMIT	32767

#define RAGDOLL_ROBOT 0
#define RAGDOLL_HUMAN 1

// Dolls
#define MAX_DOOLS 2
#define NOT_CAPTURED 10000

//openNI
#define NI_W 640.0f
#define NI_H 480.0f


// Games

#define INITIAL_SCORE 99
#define SCORE_OUT_OF_GAME -1
#define CLEAR_NFRAMES 777
#define ETIME 7777

#define XN_CALIBRATION_FILE_NAME "UserCalibration.bin"

