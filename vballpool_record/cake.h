vector<b2Body*> bodylist;

//
//rect3909
//
b2BodyDef rect3909_bdef;
rect3909_bdef.type = b2_dynamicBody;
rect3909_bdef.bullet = false;
rect3909_bdef.position.Set(-3.51564764696f,0.57650849048f);
rect3909_bdef.angle=-0.0f;
rect3909_bdef.linearDamping = 0.0f;
rect3909_bdef.angularDamping =0.0f;
b2Body* rect3909_body = m_world->CreateBody(&rect3909_bdef);

b2PolygonShape rect3909_ps;
rect3909_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3909_fdef;
rect3909_fdef.shape = &rect3909_ps;

rect3909_fdef.density = 1.0f;
rect3909_fdef.friction = 100.0f;
rect3909_fdef.restitution = 0.0f;
rect3909_fdef.filter.groupIndex = 0;

rect3909_body->CreateFixture(&rect3909_fdef);
bodylist.push_back(rect3909_body);
uData* rect3909_ud = new uData();
rect3909_ud->name = "TITLE";
rect3909_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3909_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3909_body->SetUserData(rect3909_ud);

//
//rect3911
//
b2BodyDef rect3911_bdef;
rect3911_bdef.type = b2_dynamicBody;
rect3911_bdef.bullet = false;
rect3911_bdef.position.Set(-3.51564764696f,1.49908867064f);
rect3911_bdef.angle=-0.0f;
rect3911_bdef.linearDamping = 0.0f;
rect3911_bdef.angularDamping =0.0f;
b2Body* rect3911_body = m_world->CreateBody(&rect3911_bdef);

b2PolygonShape rect3911_ps;
rect3911_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3911_fdef;
rect3911_fdef.shape = &rect3911_ps;

rect3911_fdef.density = 1.0f;
rect3911_fdef.friction = 100.0f;
rect3911_fdef.restitution = 0.0f;
rect3911_fdef.filter.groupIndex = 0;

rect3911_body->CreateFixture(&rect3911_fdef);
bodylist.push_back(rect3911_body);
uData* rect3911_ud = new uData();
rect3911_ud->name = "TITLE";
rect3911_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3911_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3911_body->SetUserData(rect3911_ud);

//
//rect3891
//
b2BodyDef rect3891_bdef;
rect3891_bdef.type = b2_dynamicBody;
rect3891_bdef.bullet = false;
rect3891_bdef.position.Set(-3.51564764696f,2.42166885079f);
rect3891_bdef.angle=-0.0f;
rect3891_bdef.linearDamping = 0.0f;
rect3891_bdef.angularDamping =0.0f;
b2Body* rect3891_body = m_world->CreateBody(&rect3891_bdef);

b2PolygonShape rect3891_ps;
rect3891_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3891_fdef;
rect3891_fdef.shape = &rect3891_ps;

rect3891_fdef.density = 1.0f;
rect3891_fdef.friction = 100.0f;
rect3891_fdef.restitution = 0.0f;
rect3891_fdef.filter.groupIndex = 0;

rect3891_body->CreateFixture(&rect3891_fdef);
bodylist.push_back(rect3891_body);
uData* rect3891_ud = new uData();
rect3891_ud->name = "TITLE";
rect3891_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3891_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3891_body->SetUserData(rect3891_ud);

//
//rect3913
//
b2BodyDef rect3913_bdef;
rect3913_bdef.type = b2_dynamicBody;
rect3913_bdef.bullet = false;
rect3913_bdef.position.Set(-2.59306641471f,0.57650849048f);
rect3913_bdef.angle=-0.0f;
rect3913_bdef.linearDamping = 0.0f;
rect3913_bdef.angularDamping =0.0f;
b2Body* rect3913_body = m_world->CreateBody(&rect3913_bdef);

b2PolygonShape rect3913_ps;
rect3913_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3913_fdef;
rect3913_fdef.shape = &rect3913_ps;

rect3913_fdef.density = 1.0f;
rect3913_fdef.friction = 100.0f;
rect3913_fdef.restitution = 0.0f;
rect3913_fdef.filter.groupIndex = 0;

rect3913_body->CreateFixture(&rect3913_fdef);
bodylist.push_back(rect3913_body);
uData* rect3913_ud = new uData();
rect3913_ud->name = "TITLE";
rect3913_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3913_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3913_body->SetUserData(rect3913_ud);

//
//rect3071
//
b2BodyDef rect3071_bdef;
rect3071_bdef.type = b2_dynamicBody;
rect3071_bdef.bullet = false;
rect3071_bdef.position.Set(-2.59306641471f,5.18941149545f);
rect3071_bdef.angle=-0.0f;
rect3071_bdef.linearDamping = 0.0f;
rect3071_bdef.angularDamping =0.0f;
b2Body* rect3071_body = m_world->CreateBody(&rect3071_bdef);

b2PolygonShape rect3071_ps;
rect3071_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3071_fdef;
rect3071_fdef.shape = &rect3071_ps;

rect3071_fdef.density = 1.0f;
rect3071_fdef.friction = 100.0f;
rect3071_fdef.restitution = 0.0f;
rect3071_fdef.filter.groupIndex = 0;

rect3071_body->CreateFixture(&rect3071_fdef);
bodylist.push_back(rect3071_body);
uData* rect3071_ud = new uData();
rect3071_ud->name = "TITLE";
rect3071_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3071_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3071_body->SetUserData(rect3071_ud);

//
//rect3915
//
b2BodyDef rect3915_bdef;
rect3915_bdef.type = b2_dynamicBody;
rect3915_bdef.bullet = false;
rect3915_bdef.position.Set(-2.59306641471f,1.49908867064f);
rect3915_bdef.angle=-0.0f;
rect3915_bdef.linearDamping = 0.0f;
rect3915_bdef.angularDamping =0.0f;
b2Body* rect3915_body = m_world->CreateBody(&rect3915_bdef);

b2PolygonShape rect3915_ps;
rect3915_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3915_fdef;
rect3915_fdef.shape = &rect3915_ps;

rect3915_fdef.density = 1.0f;
rect3915_fdef.friction = 100.0f;
rect3915_fdef.restitution = 0.0f;
rect3915_fdef.filter.groupIndex = 0;

rect3915_body->CreateFixture(&rect3915_fdef);
bodylist.push_back(rect3915_body);
uData* rect3915_ud = new uData();
rect3915_ud->name = "TITLE";
rect3915_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3915_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3915_body->SetUserData(rect3915_ud);

//
//rect3089
//
b2BodyDef rect3089_bdef;
rect3089_bdef.type = b2_dynamicBody;
rect3089_bdef.bullet = false;
rect3089_bdef.position.Set(-2.59306641471f,3.34424903095f);
rect3089_bdef.angle=-0.0f;
rect3089_bdef.linearDamping = 0.0f;
rect3089_bdef.angularDamping =0.0f;
b2Body* rect3089_body = m_world->CreateBody(&rect3089_bdef);

b2PolygonShape rect3089_ps;
rect3089_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3089_fdef;
rect3089_fdef.shape = &rect3089_ps;

rect3089_fdef.density = 1.0f;
rect3089_fdef.friction = 100.0f;
rect3089_fdef.restitution = 0.0f;
rect3089_fdef.filter.groupIndex = 0;

rect3089_body->CreateFixture(&rect3089_fdef);
bodylist.push_back(rect3089_body);
uData* rect3089_ud = new uData();
rect3089_ud->name = "TITLE";
rect3089_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3089_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3089_body->SetUserData(rect3089_ud);

//
//rect3893
//
b2BodyDef rect3893_bdef;
rect3893_bdef.type = b2_dynamicBody;
rect3893_bdef.bullet = false;
rect3893_bdef.position.Set(-2.59306641471f,2.42166885079f);
rect3893_bdef.angle=-0.0f;
rect3893_bdef.linearDamping = 0.0f;
rect3893_bdef.angularDamping =0.0f;
b2Body* rect3893_body = m_world->CreateBody(&rect3893_bdef);

b2PolygonShape rect3893_ps;
rect3893_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3893_fdef;
rect3893_fdef.shape = &rect3893_ps;

rect3893_fdef.density = 1.0f;
rect3893_fdef.friction = 100.0f;
rect3893_fdef.restitution = 0.0f;
rect3893_fdef.filter.groupIndex = 0;

rect3893_body->CreateFixture(&rect3893_fdef);
bodylist.push_back(rect3893_body);
uData* rect3893_ud = new uData();
rect3893_ud->name = "TITLE";
rect3893_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3893_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3893_body->SetUserData(rect3893_ud);

//
//rect3055
//
b2BodyDef rect3055_bdef;
rect3055_bdef.type = b2_dynamicBody;
rect3055_bdef.bullet = false;
rect3055_bdef.position.Set(-2.59306641471f,4.26682921111f);
rect3055_bdef.angle=-0.0f;
rect3055_bdef.linearDamping = 0.0f;
rect3055_bdef.angularDamping =0.0f;
b2Body* rect3055_body = m_world->CreateBody(&rect3055_bdef);

b2PolygonShape rect3055_ps;
rect3055_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3055_fdef;
rect3055_fdef.shape = &rect3055_ps;

rect3055_fdef.density = 1.0f;
rect3055_fdef.friction = 100.0f;
rect3055_fdef.restitution = 0.0f;
rect3055_fdef.filter.groupIndex = 0;

rect3055_body->CreateFixture(&rect3055_fdef);
bodylist.push_back(rect3055_body);
uData* rect3055_ud = new uData();
rect3055_ud->name = "TITLE";
rect3055_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3055_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3055_body->SetUserData(rect3055_ud);

//
//rect3917
//
b2BodyDef rect3917_bdef;
rect3917_bdef.type = b2_dynamicBody;
rect3917_bdef.bullet = false;
rect3917_bdef.position.Set(-1.67048541626f,0.57650849048f);
rect3917_bdef.angle=-0.0f;
rect3917_bdef.linearDamping = 0.0f;
rect3917_bdef.angularDamping =0.0f;
b2Body* rect3917_body = m_world->CreateBody(&rect3917_bdef);

b2PolygonShape rect3917_ps;
rect3917_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3917_fdef;
rect3917_fdef.shape = &rect3917_ps;

rect3917_fdef.density = 1.0f;
rect3917_fdef.friction = 100.0f;
rect3917_fdef.restitution = 0.0f;
rect3917_fdef.filter.groupIndex = 0;

rect3917_body->CreateFixture(&rect3917_fdef);
bodylist.push_back(rect3917_body);
uData* rect3917_ud = new uData();
rect3917_ud->name = "TITLE";
rect3917_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3917_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3917_body->SetUserData(rect3917_ud);

//
//rect3073
//
b2BodyDef rect3073_bdef;
rect3073_bdef.type = b2_dynamicBody;
rect3073_bdef.bullet = false;
rect3073_bdef.position.Set(-1.67048541626f,5.18941149545f);
rect3073_bdef.angle=-0.0f;
rect3073_bdef.linearDamping = 0.0f;
rect3073_bdef.angularDamping =0.0f;
b2Body* rect3073_body = m_world->CreateBody(&rect3073_bdef);

b2PolygonShape rect3073_ps;
rect3073_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3073_fdef;
rect3073_fdef.shape = &rect3073_ps;

rect3073_fdef.density = 1.0f;
rect3073_fdef.friction = 100.0f;
rect3073_fdef.restitution = 0.0f;
rect3073_fdef.filter.groupIndex = 0;

rect3073_body->CreateFixture(&rect3073_fdef);
bodylist.push_back(rect3073_body);
uData* rect3073_ud = new uData();
rect3073_ud->name = "TITLE";
rect3073_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3073_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3073_body->SetUserData(rect3073_ud);

//
//rect3919
//
b2BodyDef rect3919_bdef;
rect3919_bdef.type = b2_dynamicBody;
rect3919_bdef.bullet = false;
rect3919_bdef.position.Set(-1.67048541626f,1.49908867064f);
rect3919_bdef.angle=-0.0f;
rect3919_bdef.linearDamping = 0.0f;
rect3919_bdef.angularDamping =0.0f;
b2Body* rect3919_body = m_world->CreateBody(&rect3919_bdef);

b2PolygonShape rect3919_ps;
rect3919_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3919_fdef;
rect3919_fdef.shape = &rect3919_ps;

rect3919_fdef.density = 1.0f;
rect3919_fdef.friction = 100.0f;
rect3919_fdef.restitution = 0.0f;
rect3919_fdef.filter.groupIndex = 0;

rect3919_body->CreateFixture(&rect3919_fdef);
bodylist.push_back(rect3919_body);
uData* rect3919_ud = new uData();
rect3919_ud->name = "TITLE";
rect3919_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3919_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3919_body->SetUserData(rect3919_ud);

//
//rect3091
//
b2BodyDef rect3091_bdef;
rect3091_bdef.type = b2_dynamicBody;
rect3091_bdef.bullet = false;
rect3091_bdef.position.Set(-1.67048541626f,3.34424903095f);
rect3091_bdef.angle=-0.0f;
rect3091_bdef.linearDamping = 0.0f;
rect3091_bdef.angularDamping =0.0f;
b2Body* rect3091_body = m_world->CreateBody(&rect3091_bdef);

b2PolygonShape rect3091_ps;
rect3091_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3091_fdef;
rect3091_fdef.shape = &rect3091_ps;

rect3091_fdef.density = 1.0f;
rect3091_fdef.friction = 100.0f;
rect3091_fdef.restitution = 0.0f;
rect3091_fdef.filter.groupIndex = 0;

rect3091_body->CreateFixture(&rect3091_fdef);
bodylist.push_back(rect3091_body);
uData* rect3091_ud = new uData();
rect3091_ud->name = "TITLE";
rect3091_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3091_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3091_body->SetUserData(rect3091_ud);

//
//rect3895
//
b2BodyDef rect3895_bdef;
rect3895_bdef.type = b2_dynamicBody;
rect3895_bdef.bullet = false;
rect3895_bdef.position.Set(-1.67048541626f,2.42166885079f);
rect3895_bdef.angle=-0.0f;
rect3895_bdef.linearDamping = 0.0f;
rect3895_bdef.angularDamping =0.0f;
b2Body* rect3895_body = m_world->CreateBody(&rect3895_bdef);

b2PolygonShape rect3895_ps;
rect3895_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3895_fdef;
rect3895_fdef.shape = &rect3895_ps;

rect3895_fdef.density = 1.0f;
rect3895_fdef.friction = 100.0f;
rect3895_fdef.restitution = 0.0f;
rect3895_fdef.filter.groupIndex = 0;

rect3895_body->CreateFixture(&rect3895_fdef);
bodylist.push_back(rect3895_body);
uData* rect3895_ud = new uData();
rect3895_ud->name = "TITLE";
rect3895_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3895_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3895_body->SetUserData(rect3895_ud);

//
//rect3057
//
b2BodyDef rect3057_bdef;
rect3057_bdef.type = b2_dynamicBody;
rect3057_bdef.bullet = false;
rect3057_bdef.position.Set(-1.67048541626f,4.26682921111f);
rect3057_bdef.angle=-0.0f;
rect3057_bdef.linearDamping = 0.0f;
rect3057_bdef.angularDamping =0.0f;
b2Body* rect3057_body = m_world->CreateBody(&rect3057_bdef);

b2PolygonShape rect3057_ps;
rect3057_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3057_fdef;
rect3057_fdef.shape = &rect3057_ps;

rect3057_fdef.density = 1.0f;
rect3057_fdef.friction = 100.0f;
rect3057_fdef.restitution = 0.0f;
rect3057_fdef.filter.groupIndex = 0;

rect3057_body->CreateFixture(&rect3057_fdef);
bodylist.push_back(rect3057_body);
uData* rect3057_ud = new uData();
rect3057_ud->name = "TITLE";
rect3057_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3057_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3057_body->SetUserData(rect3057_ud);

//
//rect3921
//
b2BodyDef rect3921_bdef;
rect3921_bdef.type = b2_dynamicBody;
rect3921_bdef.bullet = false;
rect3921_bdef.position.Set(-0.747905236101f,0.57650849048f);
rect3921_bdef.angle=-0.0f;
rect3921_bdef.linearDamping = 0.0f;
rect3921_bdef.angularDamping =0.0f;
b2Body* rect3921_body = m_world->CreateBody(&rect3921_bdef);

b2PolygonShape rect3921_ps;
rect3921_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3921_fdef;
rect3921_fdef.shape = &rect3921_ps;

rect3921_fdef.density = 1.0f;
rect3921_fdef.friction = 100.0f;
rect3921_fdef.restitution = 0.0f;
rect3921_fdef.filter.groupIndex = 0;

rect3921_body->CreateFixture(&rect3921_fdef);
bodylist.push_back(rect3921_body);
uData* rect3921_ud = new uData();
rect3921_ud->name = "TITLE";
rect3921_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3921_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3921_body->SetUserData(rect3921_ud);

//
//rect3075
//
b2BodyDef rect3075_bdef;
rect3075_bdef.type = b2_dynamicBody;
rect3075_bdef.bullet = false;
rect3075_bdef.position.Set(-0.747905236101f,5.18941149545f);
rect3075_bdef.angle=-0.0f;
rect3075_bdef.linearDamping = 0.0f;
rect3075_bdef.angularDamping =0.0f;
b2Body* rect3075_body = m_world->CreateBody(&rect3075_bdef);

b2PolygonShape rect3075_ps;
rect3075_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3075_fdef;
rect3075_fdef.shape = &rect3075_ps;

rect3075_fdef.density = 1.0f;
rect3075_fdef.friction = 100.0f;
rect3075_fdef.restitution = 0.0f;
rect3075_fdef.filter.groupIndex = 0;

rect3075_body->CreateFixture(&rect3075_fdef);
bodylist.push_back(rect3075_body);
uData* rect3075_ud = new uData();
rect3075_ud->name = "TITLE";
rect3075_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3075_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3075_body->SetUserData(rect3075_ud);

//
//rect3923
//
b2BodyDef rect3923_bdef;
rect3923_bdef.type = b2_dynamicBody;
rect3923_bdef.bullet = false;
rect3923_bdef.position.Set(-0.747905236101f,1.49908867064f);
rect3923_bdef.angle=-0.0f;
rect3923_bdef.linearDamping = 0.0f;
rect3923_bdef.angularDamping =0.0f;
b2Body* rect3923_body = m_world->CreateBody(&rect3923_bdef);

b2PolygonShape rect3923_ps;
rect3923_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3923_fdef;
rect3923_fdef.shape = &rect3923_ps;

rect3923_fdef.density = 1.0f;
rect3923_fdef.friction = 100.0f;
rect3923_fdef.restitution = 0.0f;
rect3923_fdef.filter.groupIndex = 0;

rect3923_body->CreateFixture(&rect3923_fdef);
bodylist.push_back(rect3923_body);
uData* rect3923_ud = new uData();
rect3923_ud->name = "TITLE";
rect3923_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3923_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3923_body->SetUserData(rect3923_ud);

//
//rect3945
//
b2BodyDef rect3945_bdef;
rect3945_bdef.type = b2_dynamicBody;
rect3945_bdef.bullet = false;
rect3945_bdef.position.Set(-0.747905236101f,6.11199190941f);
rect3945_bdef.angle=-0.0f;
rect3945_bdef.linearDamping = 0.0f;
rect3945_bdef.angularDamping =0.0f;
b2Body* rect3945_body = m_world->CreateBody(&rect3945_bdef);

b2PolygonShape rect3945_ps;
rect3945_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3945_fdef;
rect3945_fdef.shape = &rect3945_ps;

rect3945_fdef.density = 1.0f;
rect3945_fdef.friction = 100.0f;
rect3945_fdef.restitution = 0.0f;
rect3945_fdef.filter.groupIndex = 0;

rect3945_body->CreateFixture(&rect3945_fdef);
bodylist.push_back(rect3945_body);
uData* rect3945_ud = new uData();
rect3945_ud->name = "TITLE";
rect3945_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3945_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3945_body->SetUserData(rect3945_ud);

//
//rect3879
//
b2BodyDef rect3879_bdef;
rect3879_bdef.type = b2_dynamicBody;
rect3879_bdef.bullet = false;
rect3879_bdef.position.Set(-0.747905236101f,7.03457314166f);
rect3879_bdef.angle=-0.0f;
rect3879_bdef.linearDamping = 0.0f;
rect3879_bdef.angularDamping =0.0f;
b2Body* rect3879_body = m_world->CreateBody(&rect3879_bdef);

b2PolygonShape rect3879_ps;
rect3879_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3879_fdef;
rect3879_fdef.shape = &rect3879_ps;

rect3879_fdef.density = 1.0f;
rect3879_fdef.friction = 100.0f;
rect3879_fdef.restitution = 0.0f;
rect3879_fdef.filter.groupIndex = 0;

rect3879_body->CreateFixture(&rect3879_fdef);
bodylist.push_back(rect3879_body);
uData* rect3879_ud = new uData();
rect3879_ud->name = "TITLE";
rect3879_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3879_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3879_body->SetUserData(rect3879_ud);

//
//rect3093
//
b2BodyDef rect3093_bdef;
rect3093_bdef.type = b2_dynamicBody;
rect3093_bdef.bullet = false;
rect3093_bdef.position.Set(-0.747905236101f,3.34424903095f);
rect3093_bdef.angle=-0.0f;
rect3093_bdef.linearDamping = 0.0f;
rect3093_bdef.angularDamping =0.0f;
b2Body* rect3093_body = m_world->CreateBody(&rect3093_bdef);

b2PolygonShape rect3093_ps;
rect3093_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3093_fdef;
rect3093_fdef.shape = &rect3093_ps;

rect3093_fdef.density = 1.0f;
rect3093_fdef.friction = 100.0f;
rect3093_fdef.restitution = 0.0f;
rect3093_fdef.filter.groupIndex = 0;

rect3093_body->CreateFixture(&rect3093_fdef);
bodylist.push_back(rect3093_body);
uData* rect3093_ud = new uData();
rect3093_ud->name = "TITLE";
rect3093_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3093_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3093_body->SetUserData(rect3093_ud);

//
//rect3897
//
b2BodyDef rect3897_bdef;
rect3897_bdef.type = b2_dynamicBody;
rect3897_bdef.bullet = false;
rect3897_bdef.position.Set(-0.747906405094f,2.42166885079f);
rect3897_bdef.angle=-0.0f;
rect3897_bdef.linearDamping = 0.0f;
rect3897_bdef.angularDamping =0.0f;
b2Body* rect3897_body = m_world->CreateBody(&rect3897_bdef);

b2PolygonShape rect3897_ps;
rect3897_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3897_fdef;
rect3897_fdef.shape = &rect3897_ps;

rect3897_fdef.density = 1.0f;
rect3897_fdef.friction = 100.0f;
rect3897_fdef.restitution = 0.0f;
rect3897_fdef.filter.groupIndex = 0;

rect3897_body->CreateFixture(&rect3897_fdef);
bodylist.push_back(rect3897_body);
uData* rect3897_ud = new uData();
rect3897_ud->name = "TITLE";
rect3897_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3897_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3897_body->SetUserData(rect3897_ud);

//
//rect3059
//
b2BodyDef rect3059_bdef;
rect3059_bdef.type = b2_dynamicBody;
rect3059_bdef.bullet = false;
rect3059_bdef.position.Set(-0.747906405094f,4.26682921111f);
rect3059_bdef.angle=-0.0f;
rect3059_bdef.linearDamping = 0.0f;
rect3059_bdef.angularDamping =0.0f;
b2Body* rect3059_body = m_world->CreateBody(&rect3059_bdef);

b2PolygonShape rect3059_ps;
rect3059_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3059_fdef;
rect3059_fdef.shape = &rect3059_ps;

rect3059_fdef.density = 1.0f;
rect3059_fdef.friction = 100.0f;
rect3059_fdef.restitution = 0.0f;
rect3059_fdef.filter.groupIndex = 0;

rect3059_body->CreateFixture(&rect3059_fdef);
bodylist.push_back(rect3059_body);
uData* rect3059_ud = new uData();
rect3059_ud->name = "TITLE";
rect3059_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3059_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3059_body->SetUserData(rect3059_ud);

//
//rect3925
//
b2BodyDef rect3925_bdef;
rect3925_bdef.type = b2_dynamicBody;
rect3925_bdef.bullet = false;
rect3925_bdef.position.Set(0.174674944056f,0.57650849048f);
rect3925_bdef.angle=-0.0f;
rect3925_bdef.linearDamping = 0.0f;
rect3925_bdef.angularDamping =0.0f;
b2Body* rect3925_body = m_world->CreateBody(&rect3925_bdef);

b2PolygonShape rect3925_ps;
rect3925_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3925_fdef;
rect3925_fdef.shape = &rect3925_ps;

rect3925_fdef.density = 1.0f;
rect3925_fdef.friction = 100.0f;
rect3925_fdef.restitution = 0.0f;
rect3925_fdef.filter.groupIndex = 0;

rect3925_body->CreateFixture(&rect3925_fdef);
bodylist.push_back(rect3925_body);
uData* rect3925_ud = new uData();
rect3925_ud->name = "TITLE";
rect3925_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3925_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3925_body->SetUserData(rect3925_ud);

//
//rect3077
//
b2BodyDef rect3077_bdef;
rect3077_bdef.type = b2_dynamicBody;
rect3077_bdef.bullet = false;
rect3077_bdef.position.Set(0.174674944056f,5.18941149545f);
rect3077_bdef.angle=-0.0f;
rect3077_bdef.linearDamping = 0.0f;
rect3077_bdef.angularDamping =0.0f;
b2Body* rect3077_body = m_world->CreateBody(&rect3077_bdef);

b2PolygonShape rect3077_ps;
rect3077_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3077_fdef;
rect3077_fdef.shape = &rect3077_ps;

rect3077_fdef.density = 1.0f;
rect3077_fdef.friction = 100.0f;
rect3077_fdef.restitution = 0.0f;
rect3077_fdef.filter.groupIndex = 0;

rect3077_body->CreateFixture(&rect3077_fdef);
bodylist.push_back(rect3077_body);
uData* rect3077_ud = new uData();
rect3077_ud->name = "TITLE";
rect3077_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3077_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3077_body->SetUserData(rect3077_ud);

//
//rect3927
//
b2BodyDef rect3927_bdef;
rect3927_bdef.type = b2_dynamicBody;
rect3927_bdef.bullet = false;
rect3927_bdef.position.Set(0.174674944056f,1.49908867064f);
rect3927_bdef.angle=-0.0f;
rect3927_bdef.linearDamping = 0.0f;
rect3927_bdef.angularDamping =0.0f;
b2Body* rect3927_body = m_world->CreateBody(&rect3927_bdef);

b2PolygonShape rect3927_ps;
rect3927_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3927_fdef;
rect3927_fdef.shape = &rect3927_ps;

rect3927_fdef.density = 1.0f;
rect3927_fdef.friction = 100.0f;
rect3927_fdef.restitution = 0.0f;
rect3927_fdef.filter.groupIndex = 0;

rect3927_body->CreateFixture(&rect3927_fdef);
bodylist.push_back(rect3927_body);
uData* rect3927_ud = new uData();
rect3927_ud->name = "TITLE";
rect3927_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3927_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3927_body->SetUserData(rect3927_ud);

//
//rect3947
//
b2BodyDef rect3947_bdef;
rect3947_bdef.type = b2_dynamicBody;
rect3947_bdef.bullet = false;
rect3947_bdef.position.Set(0.174674944056f,6.11199190941f);
rect3947_bdef.angle=-0.0f;
rect3947_bdef.linearDamping = 0.0f;
rect3947_bdef.angularDamping =0.0f;
b2Body* rect3947_body = m_world->CreateBody(&rect3947_bdef);

b2PolygonShape rect3947_ps;
rect3947_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3947_fdef;
rect3947_fdef.shape = &rect3947_ps;

rect3947_fdef.density = 1.0f;
rect3947_fdef.friction = 100.0f;
rect3947_fdef.restitution = 0.0f;
rect3947_fdef.filter.groupIndex = 0;

rect3947_body->CreateFixture(&rect3947_fdef);
bodylist.push_back(rect3947_body);
uData* rect3947_ud = new uData();
rect3947_ud->name = "TITLE";
rect3947_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3947_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3947_body->SetUserData(rect3947_ud);

//
//rect3881
//
b2BodyDef rect3881_bdef;
rect3881_bdef.type = b2_dynamicBody;
rect3881_bdef.bullet = false;
rect3881_bdef.position.Set(0.174674944056f,7.03457314166f);
rect3881_bdef.angle=-0.0f;
rect3881_bdef.linearDamping = 0.0f;
rect3881_bdef.angularDamping =0.0f;
b2Body* rect3881_body = m_world->CreateBody(&rect3881_bdef);

b2PolygonShape rect3881_ps;
rect3881_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3881_fdef;
rect3881_fdef.shape = &rect3881_ps;

rect3881_fdef.density = 1.0f;
rect3881_fdef.friction = 100.0f;
rect3881_fdef.restitution = 0.0f;
rect3881_fdef.filter.groupIndex = 0;

rect3881_body->CreateFixture(&rect3881_fdef);
bodylist.push_back(rect3881_body);
uData* rect3881_ud = new uData();
rect3881_ud->name = "TITLE";
rect3881_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3881_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3881_body->SetUserData(rect3881_ud);

//
//rect3095
//
b2BodyDef rect3095_bdef;
rect3095_bdef.type = b2_dynamicBody;
rect3095_bdef.bullet = false;
rect3095_bdef.position.Set(0.174674944056f,3.34424903095f);
rect3095_bdef.angle=-0.0f;
rect3095_bdef.linearDamping = 0.0f;
rect3095_bdef.angularDamping =0.0f;
b2Body* rect3095_body = m_world->CreateBody(&rect3095_bdef);

b2PolygonShape rect3095_ps;
rect3095_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3095_fdef;
rect3095_fdef.shape = &rect3095_ps;

rect3095_fdef.density = 1.0f;
rect3095_fdef.friction = 100.0f;
rect3095_fdef.restitution = 0.0f;
rect3095_fdef.filter.groupIndex = 0;

rect3095_body->CreateFixture(&rect3095_fdef);
bodylist.push_back(rect3095_body);
uData* rect3095_ud = new uData();
rect3095_ud->name = "TITLE";
rect3095_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3095_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3095_body->SetUserData(rect3095_ud);

//
//rect3899
//
b2BodyDef rect3899_bdef;
rect3899_bdef.type = b2_dynamicBody;
rect3899_bdef.bullet = false;
rect3899_bdef.position.Set(0.174673775064f,2.42166885079f);
rect3899_bdef.angle=-0.0f;
rect3899_bdef.linearDamping = 0.0f;
rect3899_bdef.angularDamping =0.0f;
b2Body* rect3899_body = m_world->CreateBody(&rect3899_bdef);

b2PolygonShape rect3899_ps;
rect3899_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3899_fdef;
rect3899_fdef.shape = &rect3899_ps;

rect3899_fdef.density = 1.0f;
rect3899_fdef.friction = 100.0f;
rect3899_fdef.restitution = 0.0f;
rect3899_fdef.filter.groupIndex = 0;

rect3899_body->CreateFixture(&rect3899_fdef);
bodylist.push_back(rect3899_body);
uData* rect3899_ud = new uData();
rect3899_ud->name = "TITLE";
rect3899_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3899_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3899_body->SetUserData(rect3899_ud);

//
//rect3061
//
b2BodyDef rect3061_bdef;
rect3061_bdef.type = b2_dynamicBody;
rect3061_bdef.bullet = false;
rect3061_bdef.position.Set(0.174673775064f,4.26682921111f);
rect3061_bdef.angle=-0.0f;
rect3061_bdef.linearDamping = 0.0f;
rect3061_bdef.angularDamping =0.0f;
b2Body* rect3061_body = m_world->CreateBody(&rect3061_bdef);

b2PolygonShape rect3061_ps;
rect3061_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3061_fdef;
rect3061_fdef.shape = &rect3061_ps;

rect3061_fdef.density = 1.0f;
rect3061_fdef.friction = 100.0f;
rect3061_fdef.restitution = 0.0f;
rect3061_fdef.filter.groupIndex = 0;

rect3061_body->CreateFixture(&rect3061_fdef);
bodylist.push_back(rect3061_body);
uData* rect3061_ud = new uData();
rect3061_ud->name = "TITLE";
rect3061_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3061_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3061_body->SetUserData(rect3061_ud);

//
//rect3929
//
b2BodyDef rect3929_bdef;
rect3929_bdef.type = b2_dynamicBody;
rect3929_bdef.bullet = false;
rect3929_bdef.position.Set(1.0972562932f,0.57650849048f);
rect3929_bdef.angle=-0.0f;
rect3929_bdef.linearDamping = 0.0f;
rect3929_bdef.angularDamping =0.0f;
b2Body* rect3929_body = m_world->CreateBody(&rect3929_bdef);

b2PolygonShape rect3929_ps;
rect3929_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3929_fdef;
rect3929_fdef.shape = &rect3929_ps;

rect3929_fdef.density = 1.0f;
rect3929_fdef.friction = 100.0f;
rect3929_fdef.restitution = 0.0f;
rect3929_fdef.filter.groupIndex = 0;

rect3929_body->CreateFixture(&rect3929_fdef);
bodylist.push_back(rect3929_body);
uData* rect3929_ud = new uData();
rect3929_ud->name = "TITLE";
rect3929_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3929_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3929_body->SetUserData(rect3929_ud);

//
//rect3079
//
b2BodyDef rect3079_bdef;
rect3079_bdef.type = b2_dynamicBody;
rect3079_bdef.bullet = false;
rect3079_bdef.position.Set(1.0972562932f,5.18941149545f);
rect3079_bdef.angle=-0.0f;
rect3079_bdef.linearDamping = 0.0f;
rect3079_bdef.angularDamping =0.0f;
b2Body* rect3079_body = m_world->CreateBody(&rect3079_bdef);

b2PolygonShape rect3079_ps;
rect3079_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3079_fdef;
rect3079_fdef.shape = &rect3079_ps;

rect3079_fdef.density = 1.0f;
rect3079_fdef.friction = 100.0f;
rect3079_fdef.restitution = 0.0f;
rect3079_fdef.filter.groupIndex = 0;

rect3079_body->CreateFixture(&rect3079_fdef);
bodylist.push_back(rect3079_body);
uData* rect3079_ud = new uData();
rect3079_ud->name = "TITLE";
rect3079_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3079_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3079_body->SetUserData(rect3079_ud);

//
//rect3931
//
b2BodyDef rect3931_bdef;
rect3931_bdef.type = b2_dynamicBody;
rect3931_bdef.bullet = false;
rect3931_bdef.position.Set(1.0972562932f,1.49908867064f);
rect3931_bdef.angle=-0.0f;
rect3931_bdef.linearDamping = 0.0f;
rect3931_bdef.angularDamping =0.0f;
b2Body* rect3931_body = m_world->CreateBody(&rect3931_bdef);

b2PolygonShape rect3931_ps;
rect3931_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3931_fdef;
rect3931_fdef.shape = &rect3931_ps;

rect3931_fdef.density = 1.0f;
rect3931_fdef.friction = 100.0f;
rect3931_fdef.restitution = 0.0f;
rect3931_fdef.filter.groupIndex = 0;

rect3931_body->CreateFixture(&rect3931_fdef);
bodylist.push_back(rect3931_body);
uData* rect3931_ud = new uData();
rect3931_ud->name = "TITLE";
rect3931_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3931_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3931_body->SetUserData(rect3931_ud);

//
//rect3949
//
b2BodyDef rect3949_bdef;
rect3949_bdef.type = b2_dynamicBody;
rect3949_bdef.bullet = false;
rect3949_bdef.position.Set(1.0972562932f,6.11199190941f);
rect3949_bdef.angle=-0.0f;
rect3949_bdef.linearDamping = 0.0f;
rect3949_bdef.angularDamping =0.0f;
b2Body* rect3949_body = m_world->CreateBody(&rect3949_bdef);

b2PolygonShape rect3949_ps;
rect3949_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3949_fdef;
rect3949_fdef.shape = &rect3949_ps;

rect3949_fdef.density = 1.0f;
rect3949_fdef.friction = 100.0f;
rect3949_fdef.restitution = 0.0f;
rect3949_fdef.filter.groupIndex = 0;

rect3949_body->CreateFixture(&rect3949_fdef);
bodylist.push_back(rect3949_body);
uData* rect3949_ud = new uData();
rect3949_ud->name = "TITLE";
rect3949_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3949_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3949_body->SetUserData(rect3949_ud);

//
//rect3883
//
b2BodyDef rect3883_bdef;
rect3883_bdef.type = b2_dynamicBody;
rect3883_bdef.bullet = false;
rect3883_bdef.position.Set(1.0972562932f,7.03457314166f);
rect3883_bdef.angle=-0.0f;
rect3883_bdef.linearDamping = 0.0f;
rect3883_bdef.angularDamping =0.0f;
b2Body* rect3883_body = m_world->CreateBody(&rect3883_bdef);

b2PolygonShape rect3883_ps;
rect3883_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3883_fdef;
rect3883_fdef.shape = &rect3883_ps;

rect3883_fdef.density = 1.0f;
rect3883_fdef.friction = 100.0f;
rect3883_fdef.restitution = 0.0f;
rect3883_fdef.filter.groupIndex = 0;

rect3883_body->CreateFixture(&rect3883_fdef);
bodylist.push_back(rect3883_body);
uData* rect3883_ud = new uData();
rect3883_ud->name = "TITLE";
rect3883_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3883_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3883_body->SetUserData(rect3883_ud);

//
//rect3097
//
b2BodyDef rect3097_bdef;
rect3097_bdef.type = b2_dynamicBody;
rect3097_bdef.bullet = false;
rect3097_bdef.position.Set(1.0972562932f,3.34424903095f);
rect3097_bdef.angle=-0.0f;
rect3097_bdef.linearDamping = 0.0f;
rect3097_bdef.angularDamping =0.0f;
b2Body* rect3097_body = m_world->CreateBody(&rect3097_bdef);

b2PolygonShape rect3097_ps;
rect3097_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3097_fdef;
rect3097_fdef.shape = &rect3097_ps;

rect3097_fdef.density = 1.0f;
rect3097_fdef.friction = 100.0f;
rect3097_fdef.restitution = 0.0f;
rect3097_fdef.filter.groupIndex = 0;

rect3097_body->CreateFixture(&rect3097_fdef);
bodylist.push_back(rect3097_body);
uData* rect3097_ud = new uData();
rect3097_ud->name = "TITLE";
rect3097_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3097_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3097_body->SetUserData(rect3097_ud);

//
//rect3901
//
b2BodyDef rect3901_bdef;
rect3901_bdef.type = b2_dynamicBody;
rect3901_bdef.bullet = false;
rect3901_bdef.position.Set(1.09725395522f,2.42166885079f);
rect3901_bdef.angle=-0.0f;
rect3901_bdef.linearDamping = 0.0f;
rect3901_bdef.angularDamping =0.0f;
b2Body* rect3901_body = m_world->CreateBody(&rect3901_bdef);

b2PolygonShape rect3901_ps;
rect3901_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3901_fdef;
rect3901_fdef.shape = &rect3901_ps;

rect3901_fdef.density = 1.0f;
rect3901_fdef.friction = 100.0f;
rect3901_fdef.restitution = 0.0f;
rect3901_fdef.filter.groupIndex = 0;

rect3901_body->CreateFixture(&rect3901_fdef);
bodylist.push_back(rect3901_body);
uData* rect3901_ud = new uData();
rect3901_ud->name = "TITLE";
rect3901_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3901_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3901_body->SetUserData(rect3901_ud);

//
//rect3063
//
b2BodyDef rect3063_bdef;
rect3063_bdef.type = b2_dynamicBody;
rect3063_bdef.bullet = false;
rect3063_bdef.position.Set(1.09725395522f,4.26682921111f);
rect3063_bdef.angle=-0.0f;
rect3063_bdef.linearDamping = 0.0f;
rect3063_bdef.angularDamping =0.0f;
b2Body* rect3063_body = m_world->CreateBody(&rect3063_bdef);

b2PolygonShape rect3063_ps;
rect3063_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3063_fdef;
rect3063_fdef.shape = &rect3063_ps;

rect3063_fdef.density = 1.0f;
rect3063_fdef.friction = 100.0f;
rect3063_fdef.restitution = 0.0f;
rect3063_fdef.filter.groupIndex = 0;

rect3063_body->CreateFixture(&rect3063_fdef);
bodylist.push_back(rect3063_body);
uData* rect3063_ud = new uData();
rect3063_ud->name = "TITLE";
rect3063_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3063_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3063_body->SetUserData(rect3063_ud);

//
//rect3933
//
b2BodyDef rect3933_bdef;
rect3933_bdef.type = b2_dynamicBody;
rect3933_bdef.bullet = false;
rect3933_bdef.position.Set(2.01983647336f,0.57650849048f);
rect3933_bdef.angle=-0.0f;
rect3933_bdef.linearDamping = 0.0f;
rect3933_bdef.angularDamping =0.0f;
b2Body* rect3933_body = m_world->CreateBody(&rect3933_bdef);

b2PolygonShape rect3933_ps;
rect3933_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3933_fdef;
rect3933_fdef.shape = &rect3933_ps;

rect3933_fdef.density = 1.0f;
rect3933_fdef.friction = 100.0f;
rect3933_fdef.restitution = 0.0f;
rect3933_fdef.filter.groupIndex = 0;

rect3933_body->CreateFixture(&rect3933_fdef);
bodylist.push_back(rect3933_body);
uData* rect3933_ud = new uData();
rect3933_ud->name = "TITLE";
rect3933_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3933_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3933_body->SetUserData(rect3933_ud);

//
//rect3081
//
b2BodyDef rect3081_bdef;
rect3081_bdef.type = b2_dynamicBody;
rect3081_bdef.bullet = false;
rect3081_bdef.position.Set(2.01983647336f,5.18941149545f);
rect3081_bdef.angle=-0.0f;
rect3081_bdef.linearDamping = 0.0f;
rect3081_bdef.angularDamping =0.0f;
b2Body* rect3081_body = m_world->CreateBody(&rect3081_bdef);

b2PolygonShape rect3081_ps;
rect3081_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3081_fdef;
rect3081_fdef.shape = &rect3081_ps;

rect3081_fdef.density = 1.0f;
rect3081_fdef.friction = 100.0f;
rect3081_fdef.restitution = 0.0f;
rect3081_fdef.filter.groupIndex = 0;

rect3081_body->CreateFixture(&rect3081_fdef);
bodylist.push_back(rect3081_body);
uData* rect3081_ud = new uData();
rect3081_ud->name = "TITLE";
rect3081_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3081_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3081_body->SetUserData(rect3081_ud);

//
//rect3935
//
b2BodyDef rect3935_bdef;
rect3935_bdef.type = b2_dynamicBody;
rect3935_bdef.bullet = false;
rect3935_bdef.position.Set(2.01983647336f,1.49908867064f);
rect3935_bdef.angle=-0.0f;
rect3935_bdef.linearDamping = 0.0f;
rect3935_bdef.angularDamping =0.0f;
b2Body* rect3935_body = m_world->CreateBody(&rect3935_bdef);

b2PolygonShape rect3935_ps;
rect3935_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3935_fdef;
rect3935_fdef.shape = &rect3935_ps;

rect3935_fdef.density = 1.0f;
rect3935_fdef.friction = 100.0f;
rect3935_fdef.restitution = 0.0f;
rect3935_fdef.filter.groupIndex = 0;

rect3935_body->CreateFixture(&rect3935_fdef);
bodylist.push_back(rect3935_body);
uData* rect3935_ud = new uData();
rect3935_ud->name = "TITLE";
rect3935_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3935_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3935_body->SetUserData(rect3935_ud);

//
//rect3099
//
b2BodyDef rect3099_bdef;
rect3099_bdef.type = b2_dynamicBody;
rect3099_bdef.bullet = false;
rect3099_bdef.position.Set(2.01983647336f,3.34424903095f);
rect3099_bdef.angle=-0.0f;
rect3099_bdef.linearDamping = 0.0f;
rect3099_bdef.angularDamping =0.0f;
b2Body* rect3099_body = m_world->CreateBody(&rect3099_bdef);

b2PolygonShape rect3099_ps;
rect3099_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3099_fdef;
rect3099_fdef.shape = &rect3099_ps;

rect3099_fdef.density = 1.0f;
rect3099_fdef.friction = 100.0f;
rect3099_fdef.restitution = 0.0f;
rect3099_fdef.filter.groupIndex = 0;

rect3099_body->CreateFixture(&rect3099_fdef);
bodylist.push_back(rect3099_body);
uData* rect3099_ud = new uData();
rect3099_ud->name = "TITLE";
rect3099_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3099_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3099_body->SetUserData(rect3099_ud);

//
//rect3903
//
b2BodyDef rect3903_bdef;
rect3903_bdef.type = b2_dynamicBody;
rect3903_bdef.bullet = false;
rect3903_bdef.position.Set(2.01983530437f,2.42166885079f);
rect3903_bdef.angle=-0.0f;
rect3903_bdef.linearDamping = 0.0f;
rect3903_bdef.angularDamping =0.0f;
b2Body* rect3903_body = m_world->CreateBody(&rect3903_bdef);

b2PolygonShape rect3903_ps;
rect3903_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3903_fdef;
rect3903_fdef.shape = &rect3903_ps;

rect3903_fdef.density = 1.0f;
rect3903_fdef.friction = 100.0f;
rect3903_fdef.restitution = 0.0f;
rect3903_fdef.filter.groupIndex = 0;

rect3903_body->CreateFixture(&rect3903_fdef);
bodylist.push_back(rect3903_body);
uData* rect3903_ud = new uData();
rect3903_ud->name = "TITLE";
rect3903_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3903_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3903_body->SetUserData(rect3903_ud);

//
//rect3065
//
b2BodyDef rect3065_bdef;
rect3065_bdef.type = b2_dynamicBody;
rect3065_bdef.bullet = false;
rect3065_bdef.position.Set(2.01983530437f,4.26682921111f);
rect3065_bdef.angle=-0.0f;
rect3065_bdef.linearDamping = 0.0f;
rect3065_bdef.angularDamping =0.0f;
b2Body* rect3065_body = m_world->CreateBody(&rect3065_bdef);

b2PolygonShape rect3065_ps;
rect3065_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3065_fdef;
rect3065_fdef.shape = &rect3065_ps;

rect3065_fdef.density = 1.0f;
rect3065_fdef.friction = 100.0f;
rect3065_fdef.restitution = 0.0f;
rect3065_fdef.filter.groupIndex = 0;

rect3065_body->CreateFixture(&rect3065_fdef);
bodylist.push_back(rect3065_body);
uData* rect3065_ud = new uData();
rect3065_ud->name = "TITLE";
rect3065_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3065_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3065_body->SetUserData(rect3065_ud);

//
//rect3937
//
b2BodyDef rect3937_bdef;
rect3937_bdef.type = b2_dynamicBody;
rect3937_bdef.bullet = false;
rect3937_bdef.position.Set(2.94241665352f,0.57650849048f);
rect3937_bdef.angle=-0.0f;
rect3937_bdef.linearDamping = 0.0f;
rect3937_bdef.angularDamping =0.0f;
b2Body* rect3937_body = m_world->CreateBody(&rect3937_bdef);

b2PolygonShape rect3937_ps;
rect3937_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3937_fdef;
rect3937_fdef.shape = &rect3937_ps;

rect3937_fdef.density = 1.0f;
rect3937_fdef.friction = 100.0f;
rect3937_fdef.restitution = 0.0f;
rect3937_fdef.filter.groupIndex = 0;

rect3937_body->CreateFixture(&rect3937_fdef);
bodylist.push_back(rect3937_body);
uData* rect3937_ud = new uData();
rect3937_ud->name = "TITLE";
rect3937_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3937_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3937_body->SetUserData(rect3937_ud);

//
//rect3083
//
b2BodyDef rect3083_bdef;
rect3083_bdef.type = b2_dynamicBody;
rect3083_bdef.bullet = false;
rect3083_bdef.position.Set(2.94241665352f,5.18941149545f);
rect3083_bdef.angle=-0.0f;
rect3083_bdef.linearDamping = 0.0f;
rect3083_bdef.angularDamping =0.0f;
b2Body* rect3083_body = m_world->CreateBody(&rect3083_bdef);

b2PolygonShape rect3083_ps;
rect3083_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3083_fdef;
rect3083_fdef.shape = &rect3083_ps;

rect3083_fdef.density = 1.0f;
rect3083_fdef.friction = 100.0f;
rect3083_fdef.restitution = 0.0f;
rect3083_fdef.filter.groupIndex = 0;

rect3083_body->CreateFixture(&rect3083_fdef);
bodylist.push_back(rect3083_body);
uData* rect3083_ud = new uData();
rect3083_ud->name = "TITLE";
rect3083_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3083_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3083_body->SetUserData(rect3083_ud);

//
//rect3939
//
b2BodyDef rect3939_bdef;
rect3939_bdef.type = b2_dynamicBody;
rect3939_bdef.bullet = false;
rect3939_bdef.position.Set(2.94241665352f,1.49908867064f);
rect3939_bdef.angle=-0.0f;
rect3939_bdef.linearDamping = 0.0f;
rect3939_bdef.angularDamping =0.0f;
b2Body* rect3939_body = m_world->CreateBody(&rect3939_bdef);

b2PolygonShape rect3939_ps;
rect3939_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3939_fdef;
rect3939_fdef.shape = &rect3939_ps;

rect3939_fdef.density = 1.0f;
rect3939_fdef.friction = 100.0f;
rect3939_fdef.restitution = 0.0f;
rect3939_fdef.filter.groupIndex = 0;

rect3939_body->CreateFixture(&rect3939_fdef);
bodylist.push_back(rect3939_body);
uData* rect3939_ud = new uData();
rect3939_ud->name = "TITLE";
rect3939_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3939_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3939_body->SetUserData(rect3939_ud);

//
//rect3101
//
b2BodyDef rect3101_bdef;
rect3101_bdef.type = b2_dynamicBody;
rect3101_bdef.bullet = false;
rect3101_bdef.position.Set(2.94241665352f,3.34424903095f);
rect3101_bdef.angle=-0.0f;
rect3101_bdef.linearDamping = 0.0f;
rect3101_bdef.angularDamping =0.0f;
b2Body* rect3101_body = m_world->CreateBody(&rect3101_bdef);

b2PolygonShape rect3101_ps;
rect3101_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3101_fdef;
rect3101_fdef.shape = &rect3101_ps;

rect3101_fdef.density = 1.0f;
rect3101_fdef.friction = 100.0f;
rect3101_fdef.restitution = 0.0f;
rect3101_fdef.filter.groupIndex = 0;

rect3101_body->CreateFixture(&rect3101_fdef);
bodylist.push_back(rect3101_body);
uData* rect3101_ud = new uData();
rect3101_ud->name = "TITLE";
rect3101_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3101_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3101_body->SetUserData(rect3101_ud);

//
//rect3905
//
b2BodyDef rect3905_bdef;
rect3905_bdef.type = b2_dynamicBody;
rect3905_bdef.bullet = false;
rect3905_bdef.position.Set(2.94241548453f,2.42166885079f);
rect3905_bdef.angle=-0.0f;
rect3905_bdef.linearDamping = 0.0f;
rect3905_bdef.angularDamping =0.0f;
b2Body* rect3905_body = m_world->CreateBody(&rect3905_bdef);

b2PolygonShape rect3905_ps;
rect3905_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3905_fdef;
rect3905_fdef.shape = &rect3905_ps;

rect3905_fdef.density = 1.0f;
rect3905_fdef.friction = 100.0f;
rect3905_fdef.restitution = 0.0f;
rect3905_fdef.filter.groupIndex = 0;

rect3905_body->CreateFixture(&rect3905_fdef);
bodylist.push_back(rect3905_body);
uData* rect3905_ud = new uData();
rect3905_ud->name = "TITLE";
rect3905_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3905_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3905_body->SetUserData(rect3905_ud);

//
//rect3067
//
b2BodyDef rect3067_bdef;
rect3067_bdef.type = b2_dynamicBody;
rect3067_bdef.bullet = false;
rect3067_bdef.position.Set(2.94241548453f,4.26682921111f);
rect3067_bdef.angle=-0.0f;
rect3067_bdef.linearDamping = 0.0f;
rect3067_bdef.angularDamping =0.0f;
b2Body* rect3067_body = m_world->CreateBody(&rect3067_bdef);

b2PolygonShape rect3067_ps;
rect3067_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3067_fdef;
rect3067_fdef.shape = &rect3067_ps;

rect3067_fdef.density = 1.0f;
rect3067_fdef.friction = 100.0f;
rect3067_fdef.restitution = 0.0f;
rect3067_fdef.filter.groupIndex = 0;

rect3067_body->CreateFixture(&rect3067_fdef);
bodylist.push_back(rect3067_body);
uData* rect3067_ud = new uData();
rect3067_ud->name = "TITLE";
rect3067_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3067_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3067_body->SetUserData(rect3067_ud);

//
//rect3941
//
b2BodyDef rect3941_bdef;
rect3941_bdef.type = b2_dynamicBody;
rect3941_bdef.bullet = false;
rect3941_bdef.position.Set(3.86499683368f,0.57650849048f);
rect3941_bdef.angle=-0.0f;
rect3941_bdef.linearDamping = 0.0f;
rect3941_bdef.angularDamping =0.0f;
b2Body* rect3941_body = m_world->CreateBody(&rect3941_bdef);

b2PolygonShape rect3941_ps;
rect3941_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3941_fdef;
rect3941_fdef.shape = &rect3941_ps;

rect3941_fdef.density = 1.0f;
rect3941_fdef.friction = 100.0f;
rect3941_fdef.restitution = 0.0f;
rect3941_fdef.filter.groupIndex = 0;

rect3941_body->CreateFixture(&rect3941_fdef);
bodylist.push_back(rect3941_body);
uData* rect3941_ud = new uData();
rect3941_ud->name = "TITLE";
rect3941_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3941_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3941_body->SetUserData(rect3941_ud);

//
//rect3943
//
b2BodyDef rect3943_bdef;
rect3943_bdef.type = b2_dynamicBody;
rect3943_bdef.bullet = false;
rect3943_bdef.position.Set(3.86499683368f,1.49908867064f);
rect3943_bdef.angle=-0.0f;
rect3943_bdef.linearDamping = 0.0f;
rect3943_bdef.angularDamping =0.0f;
b2Body* rect3943_body = m_world->CreateBody(&rect3943_bdef);

b2PolygonShape rect3943_ps;
rect3943_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3943_fdef;
rect3943_fdef.shape = &rect3943_ps;

rect3943_fdef.density = 1.0f;
rect3943_fdef.friction = 100.0f;
rect3943_fdef.restitution = 0.0f;
rect3943_fdef.filter.groupIndex = 0;

rect3943_body->CreateFixture(&rect3943_fdef);
bodylist.push_back(rect3943_body);
uData* rect3943_ud = new uData();
rect3943_ud->name = "TITLE";
rect3943_ud->fill.Set(float(255)/255,float(255)/255,float(255)/255);
rect3943_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3943_body->SetUserData(rect3943_ud);

//
//rect3907
//
b2BodyDef rect3907_bdef;
rect3907_bdef.type = b2_dynamicBody;
rect3907_bdef.bullet = false;
rect3907_bdef.position.Set(3.86499566468f,2.42166885079f);
rect3907_bdef.angle=-0.0f;
rect3907_bdef.linearDamping = 0.0f;
rect3907_bdef.angularDamping =0.0f;
b2Body* rect3907_body = m_world->CreateBody(&rect3907_bdef);

b2PolygonShape rect3907_ps;
rect3907_ps.SetAsBox(0.455445562581f, 0.455445562581f);

b2FixtureDef rect3907_fdef;
rect3907_fdef.shape = &rect3907_ps;

rect3907_fdef.density = 1.0f;
rect3907_fdef.friction = 100.0f;
rect3907_fdef.restitution = 0.0f;
rect3907_fdef.filter.groupIndex = 0;

rect3907_body->CreateFixture(&rect3907_fdef);
bodylist.push_back(rect3907_body);
uData* rect3907_ud = new uData();
rect3907_ud->name = "TITLE";
rect3907_ud->fill.Set(float(255)/255,float(153)/255,float(85)/255);
rect3907_ud->stroke.Set(float(255)/255,float(0)/255,float(0)/255);
rect3907_body->SetUserData(rect3907_ud);
