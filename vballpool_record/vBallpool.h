#pragma once
#include <string>
#include "Render.h"
#include <Box2D/Box2D.h>
#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>
#include "ragdoll.h"
#include "constants.h"
//#include "userdata.h"

extern xn::DepthGenerator g_DepthGenerator;
extern xn::UserGenerator g_UserGenerator;

class vBallpool
{
public:
	vBallpool(void);

	void Step(float32 timeStep, int32 velocityIterations, int32 positionIterations);
	void UpdateSkeletons();

	b2World* m_world;
	DebugDraw* m_debugDraw;
	b2Body* groundBody;
	b2Body* wallBody;

	void Draw();
	void DrawColor();
	// Draw Functions. Copied from Box2D DebugDraw
	void DrawShape(b2Fixture* fixture, const b2Transform& xf, const b2Color& color);
	void DrawShape2(b2Fixture* fixture, const b2Transform& xf, const b2Color& color, const b2Color& color2, const float linewidth);
	void DrawJoint(b2Joint* joint);

	vector<b2Body*> test_bodylist;
	vector<b2Body*> title_bodylist;
	vector<b2Body*> blackets_bodylist;
	vector<b2Body*> fireworks_bodylist;
	vector<b2Joint*> blackets_jointlist;
	vector<b2Body*> rails_bodylist;
	vector<b2Body*> cake_bodylist;
	vector<uData> title_udatalist;
	void dropTitle(); // drops "virtual ball pool" title
	void dropCircle();
	void dropStar();
	void fireworks();
	void dropParticle(b2Vec2 direction, b2Vec2 position, b2Color color);
	void destroyOne();
	void createHanged(float y); 
	void createCake(int nProblem);
	void destroyCake();
	void createRail();
	void dropAll();
	void changeGravity(float x, float y);
	void resetGravity();
	void changeColors(vector<b2Body*> bodies);
	void createTitle();
	
	// Robot
	ragdoll* m_ragdools[MAX_DOOLS];
private:
	b2Body* testball; // for test
	b2Body* rim1_s;
	b2Body* rim1; // for test
	b2Body* rim2; // for testtest
	b2Body* rim3; // for test
	b2Body* rim4; // for test
	b2Joint* joint_test;

	// robot
	b2MouseJoint* mj_head;
	b2MouseJoint* mj_neck;

	b2MouseJoint* mj_torso;

	b2MouseJoint* mj_righthand;
	b2MouseJoint* mj_rightelbow;
	b2MouseJoint* mj_lefthand;
	b2MouseJoint* mj_leftelbow;

	b2MouseJoint* mj_rightknee;
	b2MouseJoint* mj_rightfoot;
	b2MouseJoint* mj_leftknee;
	b2MouseJoint* mj_leftfoot;

	void mj_intialize();
	void mj_destroy();
	
	bool bHanged;

};

