vector<b2Body*> bodylist;
//
//hat
//
b2Vec2 hat_vertices[3];
hat_vertices[0].Set(-11.9911901257f, 14.1977104063f);
hat_vertices[1].Set(-12.8377836346f, 12.731367503f);
hat_vertices[2].Set(-11.1445966798f, 12.731367503f);
b2PolygonShape hat_polygon;
hat_polygon.Set(hat_vertices, 3);

b2FixtureDef hat_triangleShapeDef;
hat_triangleShapeDef.shape = &hat_polygon;
hat_triangleShapeDef.density = 1009.0f;
hat_triangleShapeDef.restitution = 0.0f;

hat_triangleShapeDef.filter.groupIndex = -1;

b2BodyDef hat_triangleBodyDef;
hat_triangleBodyDef.type = b2_dynamicBody;
hat_triangleBodyDef.bullet = true;
hat_triangleBodyDef.linearDamping = 1.0f;
hat_triangleBodyDef.angularDamping = 1.0f;
hat_triangleBodyDef.position.Set(-0.0f, 0.0f);

b2Body* hat_body = m_world->CreateBody(&hat_triangleBodyDef);
hat_body->CreateFixture(&hat_triangleShapeDef);
bodylist.push_back(hat_body);
uData* hat_ud = new uData();
hat_ud->name = "HUMAN";
hat_ud->fill.Set(float(60)/255,float(179)/255,float(113)/255);
hat_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
hat_ud->strokewidth=0.30000001f;
hat_body->SetUserData(hat_ud);
//
//torso
//
b2Vec2 torso_vertices[4];
torso_vertices[0].Set(-10.5565413201f, 11.2255663959f);
torso_vertices[1].Set(-13.3308023012f, 11.1613648172f);
torso_vertices[2].Set(-13.9881374135f, 5.60553033237f);
torso_vertices[3].Set(-10.1843153963f, 5.62158125313f);
b2PolygonShape torso_polygon;
torso_polygon.Set(torso_vertices, 4);

b2FixtureDef torso_triangleShapeDef;
torso_triangleShapeDef.shape = &torso_polygon;
torso_triangleShapeDef.density = 1009.0f;
torso_triangleShapeDef.restitution = 0.0f;

torso_triangleShapeDef.filter.groupIndex = -1;

b2BodyDef torso_triangleBodyDef;
torso_triangleBodyDef.type = b2_dynamicBody;
torso_triangleBodyDef.bullet = true;
torso_triangleBodyDef.linearDamping = 1.0f;
torso_triangleBodyDef.angularDamping = 1.0f;
torso_triangleBodyDef.position.Set(-0.0f, 0.0f);

b2Body* torso_body = m_world->CreateBody(&torso_triangleBodyDef);
torso_body->CreateFixture(&torso_triangleShapeDef);
bodylist.push_back(torso_body);
uData* torso_ud = new uData();
torso_ud->name = "HUMAN";
torso_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
torso_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
torso_ud->strokewidth=0.30199152f;
torso_body->SetUserData(torso_ud);
//
//lforearm
//
b2Vec2 lforearm_vertices[4];
lforearm_vertices[0].Set(-13.3931072718f, 7.96176074876f);
lforearm_vertices[1].Set(-13.8713192148f, 8.04095758967f);
lforearm_vertices[2].Set(-13.8645587912f, 5.74478818543f);
lforearm_vertices[3].Set(-13.4062426108f, 5.7534803591f);
b2PolygonShape lforearm_polygon;
lforearm_polygon.Set(lforearm_vertices, 4);

b2FixtureDef lforearm_triangleShapeDef;
lforearm_triangleShapeDef.shape = &lforearm_polygon;
lforearm_triangleShapeDef.density = 1009.0f;
lforearm_triangleShapeDef.restitution = 0.0f;

lforearm_triangleShapeDef.filter.groupIndex = -1;

b2BodyDef lforearm_triangleBodyDef;
lforearm_triangleBodyDef.type = b2_dynamicBody;
lforearm_triangleBodyDef.bullet = true;
lforearm_triangleBodyDef.linearDamping = 1.0f;
lforearm_triangleBodyDef.angularDamping = 1.0f;
lforearm_triangleBodyDef.position.Set(-0.0f, 0.0f);

b2Body* lforearm_body = m_world->CreateBody(&lforearm_triangleBodyDef);
lforearm_body->CreateFixture(&lforearm_triangleShapeDef);
bodylist.push_back(lforearm_body);
uData* lforearm_ud = new uData();
lforearm_ud->name = "HUMAN";
lforearm_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
lforearm_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
lforearm_ud->strokewidth=0.30000001f;
lforearm_body->SetUserData(lforearm_ud);
//
//ltheigh
//
b2Vec2 ltheigh_vertices[4];
ltheigh_vertices[0].Set(-12.2824775473f, 5.77338215409f);
ltheigh_vertices[1].Set(-13.1346829584f, 5.77477099398f);
ltheigh_vertices[2].Set(-12.9607262033f, 2.76356130252f);
ltheigh_vertices[3].Set(-12.2967254312f, 2.75795684257f);
b2PolygonShape ltheigh_polygon;
ltheigh_polygon.Set(ltheigh_vertices, 4);

b2FixtureDef ltheigh_triangleShapeDef;
ltheigh_triangleShapeDef.shape = &ltheigh_polygon;
ltheigh_triangleShapeDef.density = 1009.0f;
ltheigh_triangleShapeDef.restitution = 0.0f;

ltheigh_triangleShapeDef.filter.groupIndex = -1;

b2BodyDef ltheigh_triangleBodyDef;
ltheigh_triangleBodyDef.type = b2_dynamicBody;
ltheigh_triangleBodyDef.bullet = true;
ltheigh_triangleBodyDef.linearDamping = 1.0f;
ltheigh_triangleBodyDef.angularDamping = 1.0f;
ltheigh_triangleBodyDef.position.Set(-0.0f, 0.0f);

b2Body* ltheigh_body = m_world->CreateBody(&ltheigh_triangleBodyDef);
ltheigh_body->CreateFixture(&ltheigh_triangleShapeDef);
bodylist.push_back(ltheigh_body);
uData* ltheigh_ud = new uData();
ltheigh_ud->name = "HUMAN";
ltheigh_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
ltheigh_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
ltheigh_ud->strokewidth=0.30000001f;
ltheigh_body->SetUserData(ltheigh_ud);
//
//rtheigh
//
b2Vec2 rtheigh_vertices[4];
rtheigh_vertices[0].Set(-10.8013015534f, 5.76141849092f);
rtheigh_vertices[1].Set(-11.5797265992f, 5.74680340614f);
rtheigh_vertices[2].Set(-11.5782886589f, 2.86789614536f);
rtheigh_vertices[3].Set(-10.9298774742f, 2.89632176979f);
b2PolygonShape rtheigh_polygon;
rtheigh_polygon.Set(rtheigh_vertices, 4);

b2FixtureDef rtheigh_triangleShapeDef;
rtheigh_triangleShapeDef.shape = &rtheigh_polygon;
rtheigh_triangleShapeDef.density = 1009.0f;
rtheigh_triangleShapeDef.restitution = 0.0f;

rtheigh_triangleShapeDef.filter.groupIndex = -1;

b2BodyDef rtheigh_triangleBodyDef;
rtheigh_triangleBodyDef.type = b2_dynamicBody;
rtheigh_triangleBodyDef.bullet = true;
rtheigh_triangleBodyDef.linearDamping = 1.0f;
rtheigh_triangleBodyDef.angularDamping = 1.0f;
rtheigh_triangleBodyDef.position.Set(-0.0f, 0.0f);

b2Body* rtheigh_body = m_world->CreateBody(&rtheigh_triangleBodyDef);
rtheigh_body->CreateFixture(&rtheigh_triangleShapeDef);
bodylist.push_back(rtheigh_body);
uData* rtheigh_ud = new uData();
rtheigh_ud->name = "HUMAN";
rtheigh_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
rtheigh_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rtheigh_ud->strokewidth=0.30000001f;
rtheigh_body->SetUserData(rtheigh_ud);
//
//lleg
//
b2Vec2 lleg_vertices[4];
lleg_vertices[0].Set(-12.3243123507f, 2.9018743236f);
lleg_vertices[1].Set(-12.9576713167f, 2.90173403674f);
lleg_vertices[2].Set(-12.8808895141f, 0.211889970493f);
lleg_vertices[3].Set(-12.3226423058f, 0.207604207006f);
b2PolygonShape lleg_polygon;
lleg_polygon.Set(lleg_vertices, 4);

b2FixtureDef lleg_triangleShapeDef;
lleg_triangleShapeDef.shape = &lleg_polygon;
lleg_triangleShapeDef.density = 1009.0f;
lleg_triangleShapeDef.restitution = 0.0f;

lleg_triangleShapeDef.filter.groupIndex = -1;

b2BodyDef lleg_triangleBodyDef;
lleg_triangleBodyDef.type = b2_dynamicBody;
lleg_triangleBodyDef.bullet = true;
lleg_triangleBodyDef.linearDamping = 1.0f;
lleg_triangleBodyDef.angularDamping = 1.0f;
lleg_triangleBodyDef.position.Set(-0.0f, 0.0f);

b2Body* lleg_body = m_world->CreateBody(&lleg_triangleBodyDef);
lleg_body->CreateFixture(&lleg_triangleShapeDef);
bodylist.push_back(lleg_body);
uData* lleg_ud = new uData();
lleg_ud->name = "HUMAN";
lleg_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
lleg_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
lleg_ud->strokewidth=0.30000001f;
lleg_body->SetUserData(lleg_ud);
//
//rleg
//
b2Vec2 rleg_vertices[4];
rleg_vertices[0].Set(-10.9198849115f, 2.95445103193f);
rleg_vertices[1].Set(-11.597455982f, 2.94925340387f);
rleg_vertices[2].Set(-11.5966998359f, 0.22598108386f);
rleg_vertices[3].Set(-10.9783931585f, 0.221940822374f);
b2PolygonShape rleg_polygon;
rleg_polygon.Set(rleg_vertices, 4);

b2FixtureDef rleg_triangleShapeDef;
rleg_triangleShapeDef.shape = &rleg_polygon;
rleg_triangleShapeDef.density = 1009.0f;
rleg_triangleShapeDef.restitution = 0.0f;

rleg_triangleShapeDef.filter.groupIndex = -1;

b2BodyDef rleg_triangleBodyDef;
rleg_triangleBodyDef.type = b2_dynamicBody;
rleg_triangleBodyDef.bullet = true;
rleg_triangleBodyDef.linearDamping = 1.0f;
rleg_triangleBodyDef.angularDamping = 1.0f;
rleg_triangleBodyDef.position.Set(-0.0f, 0.0f);

b2Body* rleg_body = m_world->CreateBody(&rleg_triangleBodyDef);
rleg_body->CreateFixture(&rleg_triangleShapeDef);
bodylist.push_back(rleg_body);
uData* rleg_ud = new uData();
rleg_ud->name = "HUMAN";
rleg_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
rleg_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rleg_ud->strokewidth=0.30000001f;
rleg_body->SetUserData(rleg_ud);

//
//head
//
b2BodyDef head_bdef;
head_bdef.type = b2_dynamicBody;
head_bdef.bullet = true;
head_bdef.position.Set(-11.9911906167f,11.9727954674f);
head_bdef.angle=-0.0f;
head_bdef.linearDamping = 1.0f;
head_bdef.angularDamping =1.0f;
b2Body* head_body = m_world->CreateBody(&head_bdef);

b2PolygonShape head_ps;
head_ps.SetAsBox(0.871308693303f, 0.669356328126f);

b2FixtureDef head_fdef;
head_fdef.shape = &head_ps;

head_fdef.density = 1009.0f;
head_fdef.friction = 0.0f;
head_fdef.restitution = 0.0f;
head_fdef.filter.groupIndex = -1;

head_body->CreateFixture(&head_fdef);
bodylist.push_back(head_body);
uData* head_ud = new uData();
head_ud->name = "HUMAN";
head_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
head_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
head_ud->strokewidth=0.30000001f;
head_body->SetUserData(head_ud);

//
//rshoulder
//
b2BodyDef rshoulder_bdef;
rshoulder_bdef.type = b2_dynamicBody;
rshoulder_bdef.bullet = true;
rshoulder_bdef.position.Set(-10.3534743113f,10.5020121757f);
rshoulder_bdef.angle=0.140810299225f;
rshoulder_bdef.linearDamping = 1.0f;
rshoulder_bdef.angularDamping =1.0f;
b2Body* rshoulder_body = m_world->CreateBody(&rshoulder_bdef);

b2PolygonShape rshoulder_ps;
rshoulder_ps.SetAsBox(0.289314851615f, 0.289314851615f);

b2FixtureDef rshoulder_fdef;
rshoulder_fdef.shape = &rshoulder_ps;

rshoulder_fdef.density = 1009.0f;
rshoulder_fdef.friction = 0.0f;
rshoulder_fdef.restitution = 0.0f;
rshoulder_fdef.filter.groupIndex = -1;

rshoulder_body->CreateFixture(&rshoulder_fdef);
bodylist.push_back(rshoulder_body);
uData* rshoulder_ud = new uData();
rshoulder_ud->name = "HUMAN";
rshoulder_ud->fill.Set(float(100)/255,float(149)/255,float(237)/255);
rshoulder_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rshoulder_ud->strokewidth=0.40590033f;
rshoulder_body->SetUserData(rshoulder_ud);

//
//lshoulder
//
b2BodyDef lshoulder_bdef;
lshoulder_bdef.type = b2_dynamicBody;
lshoulder_bdef.bullet = true;
lshoulder_bdef.position.Set(-13.5514953033f,10.4156676302f);
lshoulder_bdef.angle=-0.146251325262f;
lshoulder_bdef.linearDamping = 1.0f;
lshoulder_bdef.angularDamping =1.0f;
b2Body* lshoulder_body = m_world->CreateBody(&lshoulder_bdef);

b2PolygonShape lshoulder_ps;
lshoulder_ps.SetAsBox(0.24551074692f, 0.24551074692f);

b2FixtureDef lshoulder_fdef;
lshoulder_fdef.shape = &lshoulder_ps;

lshoulder_fdef.density = 1009.0f;
lshoulder_fdef.friction = 0.0f;
lshoulder_fdef.restitution = 0.0f;
lshoulder_fdef.filter.groupIndex = -1;

lshoulder_body->CreateFixture(&lshoulder_fdef);
bodylist.push_back(lshoulder_body);
uData* lshoulder_ud = new uData();
lshoulder_ud->name = "HUMAN";
lshoulder_ud->fill.Set(float(100)/255,float(149)/255,float(237)/255);
lshoulder_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
lshoulder_ud->strokewidth=0.30000001f;
lshoulder_body->SetUserData(lshoulder_ud);

//
//larm
//
b2BodyDef larm_bdef;
larm_bdef.type = b2_dynamicBody;
larm_bdef.bullet = true;
larm_bdef.position.Set(-13.6067396942f,9.08726379066f);
larm_bdef.angle=-0.023600770863f;
larm_bdef.linearDamping = 1.0f;
larm_bdef.angularDamping =1.0f;
b2Body* larm_body = m_world->CreateBody(&larm_bdef);

b2PolygonShape larm_ps;
larm_ps.SetAsBox(0.229671336652f, 1.16065440452f);

b2FixtureDef larm_fdef;
larm_fdef.shape = &larm_ps;

larm_fdef.density = 1009.0f;
larm_fdef.friction = 0.0f;
larm_fdef.restitution = 0.0f;
larm_fdef.filter.groupIndex = -1;

larm_body->CreateFixture(&larm_fdef);
bodylist.push_back(larm_body);
uData* larm_ud = new uData();
larm_ud->name = "HUMAN";
larm_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
larm_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
larm_ud->strokewidth=0.30694059f;
larm_body->SetUserData(larm_ud);

//
//rarm
//
b2BodyDef rarm_bdef;
rarm_bdef.type = b2_dynamicBody;
rarm_bdef.bullet = true;
rarm_bdef.position.Set(-10.3151446606f,9.08978211143f);
rarm_bdef.angle=-0.0155138223002f;
rarm_bdef.linearDamping = 1.0f;
rarm_bdef.angularDamping =1.0f;
b2Body* rarm_body = m_world->CreateBody(&rarm_bdef);

b2PolygonShape rarm_ps;
rarm_ps.SetAsBox(0.24550576323f, 1.15282808133f);

b2FixtureDef rarm_fdef;
rarm_fdef.shape = &rarm_ps;

rarm_fdef.density = 1009.0f;
rarm_fdef.friction = 0.0f;
rarm_fdef.restitution = 0.0f;
rarm_fdef.filter.groupIndex = -1;

rarm_body->CreateFixture(&rarm_fdef);
bodylist.push_back(rarm_body);
uData* rarm_ud = new uData();
rarm_ud->name = "HUMAN";
rarm_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
rarm_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rarm_ud->strokewidth=0.28704247f;
rarm_body->SetUserData(rarm_ud);

//
//rforearm
//
b2BodyDef rforearm_bdef;
rforearm_bdef.type = b2_dynamicBody;
rforearm_bdef.bullet = true;
rforearm_bdef.position.Set(-10.2979849483f,6.8767590521f);
rforearm_bdef.angle=0.0195077772682f;
rforearm_bdef.linearDamping = 1.0f;
rforearm_bdef.angularDamping =1.0f;
b2Body* rforearm_body = m_world->CreateBody(&rforearm_bdef);

b2PolygonShape rforearm_ps;
rforearm_ps.SetAsBox(0.24550576323f, 1.15282808133f);

b2FixtureDef rforearm_fdef;
rforearm_fdef.shape = &rforearm_ps;

rforearm_fdef.density = 1009.0f;
rforearm_fdef.friction = 0.0f;
rforearm_fdef.restitution = 0.0f;
rforearm_fdef.filter.groupIndex = -1;

rforearm_body->CreateFixture(&rforearm_fdef);
bodylist.push_back(rforearm_body);
uData* rforearm_ud = new uData();
rforearm_ud->name = "HUMAN";
rforearm_ud->fill.Set(float(192)/255,float(192)/255,float(192)/255);
rforearm_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rforearm_ud->strokewidth=0.28704247f;
rforearm_body->SetUserData(rforearm_ud);

//
//lfist
//
b2BodyDef lfist_bdef;
lfist_bdef.type = b2_dynamicBody;
lfist_bdef.bullet = true;
lfist_bdef.position.Set(-13.6477955283f,5.56229402003f);
lfist_bdef.angle=-0.0437182850447f;
lfist_bdef.linearDamping = 1.0f;
lfist_bdef.angularDamping =1.0f;
b2Body* lfist_body = m_world->CreateBody(&lfist_bdef);

b2PolygonShape lfist_ps;
lfist_ps.SetAsBox(0.229671336652f, 0.213831943921f);

b2FixtureDef lfist_fdef;
lfist_fdef.shape = &lfist_ps;

lfist_fdef.density = 1009.0f;
lfist_fdef.friction = 0.0f;
lfist_fdef.restitution = 0.0f;
lfist_fdef.filter.groupIndex = -1;

lfist_body->CreateFixture(&lfist_fdef);
bodylist.push_back(lfist_body);
uData* lfist_ud = new uData();
lfist_ud->name = "HUMAN";
lfist_ud->fill.Set(float(255)/255,float(127)/255,float(80)/255);
lfist_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
lfist_ud->strokewidth=0.30000001f;
lfist_body->SetUserData(lfist_ud);

//
//rfist
//
b2BodyDef rfist_bdef;
rfist_bdef.type = b2_dynamicBody;
rfist_bdef.bullet = true;
rfist_bdef.position.Set(-10.2736524248f,5.5877981181f);
rfist_bdef.angle=0.0763261069268f;
rfist_bdef.linearDamping = 1.0f;
rfist_bdef.angularDamping =1.0f;
b2Body* rfist_body = m_world->CreateBody(&rfist_bdef);

b2PolygonShape rfist_ps;
rfist_ps.SetAsBox(0.24551074692f, 0.22175164204f);

b2FixtureDef rfist_fdef;
rfist_fdef.shape = &rfist_ps;

rfist_fdef.density = 1009.0f;
rfist_fdef.friction = 0.0f;
rfist_fdef.restitution = 0.0f;
rfist_fdef.filter.groupIndex = -1;

rfist_body->CreateFixture(&rfist_fdef);
bodylist.push_back(rfist_body);
uData* rfist_ud = new uData();
rfist_ud->name = "HUMAN";
rfist_ud->fill.Set(float(255)/255,float(127)/255,float(80)/255);
rfist_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rfist_ud->strokewidth=0.30000001f;
rfist_body->SetUserData(rfist_ud);

//
//lfoot
//
b2BodyDef lfoot_bdef;
lfoot_bdef.type = b2_dynamicBody;
lfoot_bdef.bullet = true;
lfoot_bdef.position.Set(-12.7448954418f,0.175705839357f);
lfoot_bdef.angle=-0.00452626545498f;
lfoot_bdef.linearDamping = 1.0f;
lfoot_bdef.angularDamping =1.0f;
b2Body* lfoot_body = m_world->CreateBody(&lfoot_bdef);

b2PolygonShape lfoot_ps;
lfoot_ps.SetAsBox(0.411824449516f, 0.1504743274f);

b2FixtureDef lfoot_fdef;
lfoot_fdef.shape = &lfoot_ps;

lfoot_fdef.density = 1009.0f;
lfoot_fdef.friction = 0.0f;
lfoot_fdef.restitution = 0.0f;
lfoot_fdef.filter.groupIndex = -1;

lfoot_body->CreateFixture(&lfoot_fdef);
bodylist.push_back(lfoot_body);
uData* lfoot_ud = new uData();
lfoot_ud->name = "HUMAN";
lfoot_ud->fill.Set(float(255)/255,float(127)/255,float(80)/255);
lfoot_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
lfoot_ud->strokewidth=0.30000001f;
lfoot_body->SetUserData(lfoot_ud);

//
//rfoot
//
b2BodyDef rfoot_bdef;
rfoot_bdef.type = b2_dynamicBody;
rfoot_bdef.bullet = true;
rfoot_bdef.position.Set(-11.1627441041f,0.174260873119f);
rfoot_bdef.angle=0.0284198755839f;
rfoot_bdef.linearDamping = 1.0f;
rfoot_bdef.angularDamping =1.0f;
b2Body* rfoot_body = m_world->CreateBody(&rfoot_bdef);

b2PolygonShape rfoot_ps;
rfoot_ps.SetAsBox(0.435583571932f, 0.1504743274f);

b2FixtureDef rfoot_fdef;
rfoot_fdef.shape = &rfoot_ps;

rfoot_fdef.density = 1009.0f;
rfoot_fdef.friction = 0.0f;
rfoot_fdef.restitution = 0.0f;
rfoot_fdef.filter.groupIndex = -1;

rfoot_body->CreateFixture(&rfoot_fdef);
bodylist.push_back(rfoot_body);
uData* rfoot_ud = new uData();
rfoot_ud->name = "HUMAN";
rfoot_ud->fill.Set(float(255)/255,float(127)/255,float(80)/255);
rfoot_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rfoot_ud->strokewidth=0.30000001f;
rfoot_body->SetUserData(rfoot_ud);

//
//leye
//
b2BodyDef leye_bdef;
leye_bdef.type = b2_dynamicBody;
leye_bdef.bullet = true;
leye_bdef.position.Set(-12.3930598969f,12.2200720682f);
leye_bdef.angle=-0.0f;
leye_bdef.linearDamping = 1.0f;
leye_bdef.angularDamping =1.0f;
b2Body* leye_body = m_world->CreateBody(&leye_bdef);

b2PolygonShape leye_ps;
leye_ps.SetAsBox(0.0690687891694f, 0.0460458571082f);

b2FixtureDef leye_fdef;
leye_fdef.shape = &leye_ps;

leye_fdef.density = 1009.0f;
leye_fdef.friction = 0.0f;
leye_fdef.restitution = 0.0f;
leye_fdef.filter.groupIndex = -1;

leye_body->CreateFixture(&leye_fdef);
bodylist.push_back(leye_body);
uData* leye_ud = new uData();
leye_ud->name = "HUMAN";
leye_ud->fill.Set(1.0f,1.0f,1.0f);
leye_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
leye_ud->strokewidth=0.2f;
leye_body->SetUserData(leye_ud);

//
//reye
//
b2BodyDef reye_bdef;
reye_bdef.type = b2_dynamicBody;
reye_bdef.bullet = true;
reye_bdef.position.Set(-11.6010706974f,12.1959516755f);
reye_bdef.angle=-0.0f;
reye_bdef.linearDamping = 1.0f;
reye_bdef.angularDamping =1.0f;
b2Body* reye_body = m_world->CreateBody(&reye_bdef);

b2PolygonShape reye_ps;
reye_ps.SetAsBox(0.0598596163449f, 0.0403438592199f);

b2FixtureDef reye_fdef;
reye_fdef.shape = &reye_ps;

reye_fdef.density = 1009.0f;
reye_fdef.friction = 0.0f;
reye_fdef.restitution = 0.0f;
reye_fdef.filter.groupIndex = -1;

reye_body->CreateFixture(&reye_fdef);
bodylist.push_back(reye_body);
uData* reye_ud = new uData();
reye_ud->name = "HUMAN";
reye_ud->fill.Set(1.0f,1.0f,1.0f);
reye_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
reye_ud->strokewidth=0.22375585f;
reye_body->SetUserData(reye_ud);
//lknee
b2RevoluteJointDef lknee_rjd;

lknee_rjd.Initialize(ltheigh_body, lleg_body, b2Vec2(-12.7280455803f, 2.80262067072f));
lknee_rjd.motorSpeed = 100.0f * b2_pi;
lknee_rjd.maxMotorTorque = 10.0f;
lknee_rjd.enableMotor = false;
lknee_rjd.lowerAngle = -0.5f * b2_pi;
lknee_rjd.upperAngle = 0.5f * b2_pi;
lknee_rjd.enableLimit = false;
lknee_rjd.collideConnected = false;

b2RevoluteJoint* lknee_joint = (b2RevoluteJoint*)m_world->CreateJoint(&lknee_rjd);

//lweist
b2RevoluteJointDef lweist_rjd;

lweist_rjd.Initialize(torso_body, ltheigh_body, b2Vec2(-12.7323594713f, 5.65581405202f));
lweist_rjd.motorSpeed = 100.0f * b2_pi;
lweist_rjd.maxMotorTorque = 10.0f;
lweist_rjd.enableMotor = false;
lweist_rjd.lowerAngle = -0.5f * b2_pi;
lweist_rjd.upperAngle = 0.5f * b2_pi;
lweist_rjd.enableLimit = false;
lweist_rjd.collideConnected = false;

b2RevoluteJoint* lweist_joint = (b2RevoluteJoint*)m_world->CreateJoint(&lweist_rjd);

//path3041
b2RevoluteJointDef path3041_rjd;

path3041_rjd.Initialize(lleg_body, lfoot_body, b2Vec2(-12.8166866727f, 0.275249828096f));
path3041_rjd.motorSpeed = 100.0f * b2_pi;
path3041_rjd.maxMotorTorque = 10.0f;
path3041_rjd.enableMotor = false;
path3041_rjd.lowerAngle = -0.5f * b2_pi;
path3041_rjd.upperAngle = 0.5f * b2_pi;
path3041_rjd.enableLimit = false;
path3041_rjd.collideConnected = false;

b2RevoluteJoint* path3041_joint = (b2RevoluteJoint*)m_world->CreateJoint(&path3041_rjd);

//lankle
b2RevoluteJointDef lankle_rjd;

lankle_rjd.Initialize(lleg_body, lfoot_body, b2Vec2(-12.5054849706f, 0.275249828096f));
lankle_rjd.motorSpeed = 100.0f * b2_pi;
lankle_rjd.maxMotorTorque = 10.0f;
lankle_rjd.enableMotor = false;
lankle_rjd.lowerAngle = -0.5f * b2_pi;
lankle_rjd.upperAngle = 0.5f * b2_pi;
lankle_rjd.enableLimit = false;
lankle_rjd.collideConnected = false;

b2RevoluteJoint* lankle_joint = (b2RevoluteJoint*)m_world->CreateJoint(&lankle_rjd);

//rweist
b2RevoluteJointDef rweist_rjd;

rweist_rjd.Initialize(torso_body, rtheigh_body, b2Vec2(-11.1553737266f, 5.67695598282f));
rweist_rjd.motorSpeed = 100.0f * b2_pi;
rweist_rjd.maxMotorTorque = 10.0f;
rweist_rjd.enableMotor = false;
rweist_rjd.lowerAngle = -0.5f * b2_pi;
rweist_rjd.upperAngle = 0.5f * b2_pi;
rweist_rjd.enableLimit = false;
rweist_rjd.collideConnected = false;

b2RevoluteJoint* rweist_joint = (b2RevoluteJoint*)m_world->CreateJoint(&rweist_rjd);

//rknee
b2RevoluteJointDef rknee_rjd;

rknee_rjd.Initialize(rtheigh_body, rleg_body, b2Vec2(-11.2887990835f, 2.89149590191f));
rknee_rjd.motorSpeed = 100.0f * b2_pi;
rknee_rjd.maxMotorTorque = 10.0f;
rknee_rjd.enableMotor = false;
rknee_rjd.lowerAngle = -0.5f * b2_pi;
rknee_rjd.upperAngle = 0.5f * b2_pi;
rknee_rjd.enableLimit = false;
rknee_rjd.collideConnected = false;

b2RevoluteJoint* rknee_joint = (b2RevoluteJoint*)m_world->CreateJoint(&rknee_rjd);

//path3043
b2RevoluteJointDef path3043_rjd;

path3043_rjd.Initialize(rleg_body, rfoot_body, b2Vec2(-11.4921476887f, 0.275506553045f));
path3043_rjd.motorSpeed = 100.0f * b2_pi;
path3043_rjd.maxMotorTorque = 10.0f;
path3043_rjd.enableMotor = false;
path3043_rjd.lowerAngle = -0.5f * b2_pi;
path3043_rjd.upperAngle = 0.5f * b2_pi;
path3043_rjd.enableLimit = false;
path3043_rjd.collideConnected = false;

b2RevoluteJoint* path3043_joint = (b2RevoluteJoint*)m_world->CreateJoint(&path3043_rjd);

//rankle
b2RevoluteJointDef rankle_rjd;

rankle_rjd.Initialize(rleg_body, rfoot_body, b2Vec2(-11.1670199908f, 0.283182348434f));
rankle_rjd.motorSpeed = 100.0f * b2_pi;
rankle_rjd.maxMotorTorque = 10.0f;
rankle_rjd.enableMotor = false;
rankle_rjd.lowerAngle = -0.5f * b2_pi;
rankle_rjd.upperAngle = 0.5f * b2_pi;
rankle_rjd.enableLimit = false;
rankle_rjd.collideConnected = false;

b2RevoluteJoint* rankle_joint = (b2RevoluteJoint*)m_world->CreateJoint(&rankle_rjd);

//lshoulder_arm
b2RevoluteJointDef lshoulder_arm_rjd;

lshoulder_arm_rjd.Initialize(lshoulder_body, larm_body, b2Vec2(-13.6472627965f, 10.2001866096f));
lshoulder_arm_rjd.motorSpeed = 100.0f * b2_pi;
lshoulder_arm_rjd.maxMotorTorque = 10.0f;
lshoulder_arm_rjd.enableMotor = false;
lshoulder_arm_rjd.lowerAngle = -0.5f * b2_pi;
lshoulder_arm_rjd.upperAngle = 0.5f * b2_pi;
lshoulder_arm_rjd.enableLimit = false;
lshoulder_arm_rjd.collideConnected = false;

b2RevoluteJoint* lshoulder_arm_joint = (b2RevoluteJoint*)m_world->CreateJoint(&lshoulder_arm_rjd);

//lbody_shoulder
b2RevoluteJointDef lbody_shoulder_rjd;

lbody_shoulder_rjd.Initialize(torso_body, lshoulder_body, b2Vec2(-13.3612712037f, 10.4887476964f));
lbody_shoulder_rjd.motorSpeed = 100.0f * b2_pi;
lbody_shoulder_rjd.maxMotorTorque = 10.0f;
lbody_shoulder_rjd.enableMotor = false;
lbody_shoulder_rjd.lowerAngle = -0.5f * b2_pi;
lbody_shoulder_rjd.upperAngle = 0.5f * b2_pi;
lbody_shoulder_rjd.enableLimit = false;
lbody_shoulder_rjd.collideConnected = false;

b2RevoluteJoint* lbody_shoulder_joint = (b2RevoluteJoint*)m_world->CreateJoint(&lbody_shoulder_rjd);

//larm_forearm
b2RevoluteJointDef larm_forearm_rjd;

larm_forearm_rjd.Initialize(larm_body, lforearm_body, b2Vec2(-13.6845836598f, 7.96373023595f));
larm_forearm_rjd.motorSpeed = 100.0f * b2_pi;
larm_forearm_rjd.maxMotorTorque = 10.0f;
larm_forearm_rjd.enableMotor = false;
larm_forearm_rjd.lowerAngle = -0.5f * b2_pi;
larm_forearm_rjd.upperAngle = 0.5f * b2_pi;
larm_forearm_rjd.enableLimit = false;
larm_forearm_rjd.collideConnected = false;

b2RevoluteJoint* larm_forearm_joint = (b2RevoluteJoint*)m_world->CreateJoint(&larm_forearm_rjd);

//path3037
b2RevoluteJointDef path3037_rjd;

path3037_rjd.Initialize(lforearm_body, lfist_body, b2Vec2(-13.824166761f, 5.74913427227f));
path3037_rjd.motorSpeed = 100.0f * b2_pi;
path3037_rjd.maxMotorTorque = 10.0f;
path3037_rjd.enableMotor = false;
path3037_rjd.lowerAngle = -0.5f * b2_pi;
path3037_rjd.upperAngle = 0.5f * b2_pi;
path3037_rjd.enableLimit = false;
path3037_rjd.collideConnected = false;

b2RevoluteJoint* path3037_joint = (b2RevoluteJoint*)m_world->CreateJoint(&path3037_rjd);

//lforearm_fist
b2RevoluteJointDef lforearm_fist_rjd;

lforearm_fist_rjd.Initialize(lforearm_body, lfist_body, b2Vec2(-13.5544585723f, 5.73530339102f));
lforearm_fist_rjd.motorSpeed = 100.0f * b2_pi;
lforearm_fist_rjd.maxMotorTorque = 10.0f;
lforearm_fist_rjd.enableMotor = false;
lforearm_fist_rjd.lowerAngle = -0.5f * b2_pi;
lforearm_fist_rjd.upperAngle = 0.5f * b2_pi;
lforearm_fist_rjd.enableLimit = false;
lforearm_fist_rjd.collideConnected = false;

b2RevoluteJoint* lforearm_fist_joint = (b2RevoluteJoint*)m_world->CreateJoint(&lforearm_fist_rjd);

//rbody_shoulder
b2RevoluteJointDef rbody_shoulder_rjd;

rbody_shoulder_rjd.Initialize(torso_body, rshoulder_body, b2Vec2(-10.6082405342f, 10.6506823588f));
rbody_shoulder_rjd.motorSpeed = 100.0f * b2_pi;
rbody_shoulder_rjd.maxMotorTorque = 10.0f;
rbody_shoulder_rjd.enableMotor = false;
rbody_shoulder_rjd.lowerAngle = -0.5f * b2_pi;
rbody_shoulder_rjd.upperAngle = 0.5f * b2_pi;
rbody_shoulder_rjd.enableLimit = false;
rbody_shoulder_rjd.collideConnected = false;

b2RevoluteJoint* rbody_shoulder_joint = (b2RevoluteJoint*)m_world->CreateJoint(&rbody_shoulder_rjd);

//rshoulder_arm
b2RevoluteJointDef rshoulder_arm_rjd;

rshoulder_arm_rjd.Initialize(rshoulder_body, rarm_body, b2Vec2(-10.3633244422f, 10.1845571105f));
rshoulder_arm_rjd.motorSpeed = 100.0f * b2_pi;
rshoulder_arm_rjd.maxMotorTorque = 10.0f;
rshoulder_arm_rjd.enableMotor = false;
rshoulder_arm_rjd.lowerAngle = -0.5f * b2_pi;
rshoulder_arm_rjd.upperAngle = 0.5f * b2_pi;
rshoulder_arm_rjd.enableLimit = false;
rshoulder_arm_rjd.collideConnected = false;

b2RevoluteJoint* rshoulder_arm_joint = (b2RevoluteJoint*)m_world->CreateJoint(&rshoulder_arm_rjd);

//rarm_forearm
b2RevoluteJointDef rarm_forearm_rjd;

rarm_forearm_rjd.Initialize(rarm_body, rforearm_body, b2Vec2(-10.3997313366f, 7.99604861024f));
rarm_forearm_rjd.motorSpeed = 100.0f * b2_pi;
rarm_forearm_rjd.maxMotorTorque = 10.0f;
rarm_forearm_rjd.enableMotor = false;
rarm_forearm_rjd.lowerAngle = -0.5f * b2_pi;
rarm_forearm_rjd.upperAngle = 0.5f * b2_pi;
rarm_forearm_rjd.enableLimit = false;
rarm_forearm_rjd.collideConnected = false;

b2RevoluteJoint* rarm_forearm_joint = (b2RevoluteJoint*)m_world->CreateJoint(&rarm_forearm_rjd);

//path3039
b2RevoluteJointDef path3039_rjd;

path3039_rjd.Initialize(rforearm_body, rfist_body, b2Vec2(-10.4679315818f, 5.74723057962f));
path3039_rjd.motorSpeed = 100.0f * b2_pi;
path3039_rjd.maxMotorTorque = 10.0f;
path3039_rjd.enableMotor = false;
path3039_rjd.lowerAngle = -0.5f * b2_pi;
path3039_rjd.upperAngle = 0.5f * b2_pi;
path3039_rjd.enableLimit = false;
path3039_rjd.collideConnected = false;

b2RevoluteJoint* path3039_joint = (b2RevoluteJoint*)m_world->CreateJoint(&path3039_rjd);

//rforearm_fist
b2RevoluteJointDef rforearm_fist_rjd;

rforearm_fist_rjd.Initialize(rforearm_body, rfist_body, b2Vec2(-10.2328013399f, 5.7610621623f));
rforearm_fist_rjd.motorSpeed = 100.0f * b2_pi;
rforearm_fist_rjd.maxMotorTorque = 10.0f;
rforearm_fist_rjd.enableMotor = false;
rforearm_fist_rjd.lowerAngle = -0.5f * b2_pi;
rforearm_fist_rjd.upperAngle = 0.5f * b2_pi;
rforearm_fist_rjd.enableLimit = false;
rforearm_fist_rjd.collideConnected = false;

b2RevoluteJoint* rforearm_fist_joint = (b2RevoluteJoint*)m_world->CreateJoint(&rforearm_fist_rjd);

//lbody_shoulder_1
b2RevoluteJointDef lbody_shoulder_1_rjd;

lbody_shoulder_1_rjd.Initialize(torso_body, lshoulder_body, b2Vec2(-13.3879306166f, 10.2176431345f));
lbody_shoulder_1_rjd.motorSpeed = 100.0f * b2_pi;
lbody_shoulder_1_rjd.maxMotorTorque = 10.0f;
lbody_shoulder_1_rjd.enableMotor = false;
lbody_shoulder_1_rjd.lowerAngle = -0.5f * b2_pi;
lbody_shoulder_1_rjd.upperAngle = 0.5f * b2_pi;
lbody_shoulder_1_rjd.enableLimit = false;
lbody_shoulder_1_rjd.collideConnected = false;

b2RevoluteJoint* lbody_shoulder_1_joint = (b2RevoluteJoint*)m_world->CreateJoint(&lbody_shoulder_1_rjd);

//rbody_shoulder_1
b2RevoluteJointDef rbody_shoulder_1_rjd;

rbody_shoulder_1_rjd.Initialize(torso_body, rshoulder_body, b2Vec2(-10.5650356893f, 10.2957855093f));
rbody_shoulder_1_rjd.motorSpeed = 100.0f * b2_pi;
rbody_shoulder_1_rjd.maxMotorTorque = 10.0f;
rbody_shoulder_1_rjd.enableMotor = false;
rbody_shoulder_1_rjd.lowerAngle = -0.5f * b2_pi;
rbody_shoulder_1_rjd.upperAngle = 0.5f * b2_pi;
rbody_shoulder_1_rjd.enableLimit = false;
rbody_shoulder_1_rjd.collideConnected = false;

b2RevoluteJoint* rbody_shoulder_1_joint = (b2RevoluteJoint*)m_world->CreateJoint(&rbody_shoulder_1_rjd);

//head_body
b2RevoluteJointDef head_body_rjd;

head_body_rjd.Initialize(head_body, torso_body, b2Vec2(-11.2346643493f, 11.2589735365f));
head_body_rjd.motorSpeed = 100.0f * b2_pi;
head_body_rjd.maxMotorTorque = 10.0f;
head_body_rjd.enableMotor = false;
head_body_rjd.lowerAngle = -0.5f * b2_pi;
head_body_rjd.upperAngle = 0.5f * b2_pi;
head_body_rjd.enableLimit = false;
head_body_rjd.collideConnected = false;

b2RevoluteJoint* head_body_joint = (b2RevoluteJoint*)m_world->CreateJoint(&head_body_rjd);

//hat_head_0
b2RevoluteJointDef hat_head_0_rjd;

hat_head_0_rjd.Initialize(hat_body, head_body, b2Vec2(-12.8480085895f, 12.688739658f));
hat_head_0_rjd.motorSpeed = 100.0f * b2_pi;
hat_head_0_rjd.maxMotorTorque = 10.0f;
hat_head_0_rjd.enableMotor = false;
hat_head_0_rjd.lowerAngle = -0.5f * b2_pi;
hat_head_0_rjd.upperAngle = 0.5f * b2_pi;
hat_head_0_rjd.enableLimit = false;
hat_head_0_rjd.collideConnected = false;

b2RevoluteJoint* hat_head_0_joint = (b2RevoluteJoint*)m_world->CreateJoint(&hat_head_0_rjd);

//hat_head_1
b2RevoluteJointDef hat_head_1_rjd;

hat_head_1_rjd.Initialize(hat_body, head_body, b2Vec2(-11.2036375961f, 12.6905870956f));
hat_head_1_rjd.motorSpeed = 100.0f * b2_pi;
hat_head_1_rjd.maxMotorTorque = 10.0f;
hat_head_1_rjd.enableMotor = false;
hat_head_1_rjd.lowerAngle = -0.5f * b2_pi;
hat_head_1_rjd.upperAngle = 0.5f * b2_pi;
hat_head_1_rjd.enableLimit = false;
hat_head_1_rjd.collideConnected = false;

b2RevoluteJoint* hat_head_1_joint = (b2RevoluteJoint*)m_world->CreateJoint(&hat_head_1_rjd);

//head_leye
b2RevoluteJointDef head_leye_rjd;

head_leye_rjd.Initialize(head_body, leye_body, b2Vec2(-12.5508080768f, 12.220136032f));
head_leye_rjd.motorSpeed = 100.0f * b2_pi;
head_leye_rjd.maxMotorTorque = 10.0f;
head_leye_rjd.enableMotor = false;
head_leye_rjd.lowerAngle = -0.5f * b2_pi;
head_leye_rjd.upperAngle = 0.5f * b2_pi;
head_leye_rjd.enableLimit = false;
head_leye_rjd.collideConnected = false;

b2RevoluteJoint* head_leye_joint = (b2RevoluteJoint*)m_world->CreateJoint(&head_leye_rjd);

//head_reye
b2RevoluteJointDef head_reye_rjd;

head_reye_rjd.Initialize(head_body, reye_body, b2Vec2(-11.4927162012f, 12.1983976716f));
head_reye_rjd.motorSpeed = 100.0f * b2_pi;
head_reye_rjd.maxMotorTorque = 10.0f;
head_reye_rjd.enableMotor = false;
head_reye_rjd.lowerAngle = -0.5f * b2_pi;
head_reye_rjd.upperAngle = 0.5f * b2_pi;
head_reye_rjd.enableLimit = false;
head_reye_rjd.collideConnected = false;

b2RevoluteJoint* head_reye_joint = (b2RevoluteJoint*)m_world->CreateJoint(&head_reye_rjd);

//head_leye_1
b2RevoluteJointDef head_leye_1_rjd;

head_leye_1_rjd.Initialize(head_body, leye_body, b2Vec2(-12.258247552f, 12.2260252742f));
head_leye_1_rjd.motorSpeed = 100.0f * b2_pi;
head_leye_1_rjd.maxMotorTorque = 10.0f;
head_leye_1_rjd.enableMotor = false;
head_leye_1_rjd.lowerAngle = -0.5f * b2_pi;
head_leye_1_rjd.upperAngle = 0.5f * b2_pi;
head_leye_1_rjd.enableLimit = false;
head_leye_1_rjd.collideConnected = false;

b2RevoluteJoint* head_leye_1_joint = (b2RevoluteJoint*)m_world->CreateJoint(&head_leye_1_rjd);

//head_reye_1
b2RevoluteJointDef head_reye_1_rjd;

head_reye_1_rjd.Initialize(head_body, reye_body, b2Vec2(-11.7493597834f, 12.1983977417f));
head_reye_1_rjd.motorSpeed = 100.0f * b2_pi;
head_reye_1_rjd.maxMotorTorque = 10.0f;
head_reye_1_rjd.enableMotor = false;
head_reye_1_rjd.lowerAngle = -0.5f * b2_pi;
head_reye_1_rjd.upperAngle = 0.5f * b2_pi;
head_reye_1_rjd.enableLimit = false;
head_reye_1_rjd.collideConnected = false;

b2RevoluteJoint* head_reye_1_joint = (b2RevoluteJoint*)m_world->CreateJoint(&head_reye_1_rjd);

//head_body_0
b2RevoluteJointDef head_body_0_rjd;

head_body_0_rjd.Initialize(head_body, torso_body, b2Vec2(-12.8330657245f, 11.2355444389f));
head_body_0_rjd.motorSpeed = 100.0f * b2_pi;
head_body_0_rjd.maxMotorTorque = 10.0f;
head_body_0_rjd.enableMotor = false;
head_body_0_rjd.lowerAngle = -0.5f * b2_pi;
head_body_0_rjd.upperAngle = 0.5f * b2_pi;
head_body_0_rjd.enableLimit = false;
head_body_0_rjd.collideConnected = false;

b2RevoluteJoint* head_body_0_joint = (b2RevoluteJoint*)m_world->CreateJoint(&head_body_0_rjd);

