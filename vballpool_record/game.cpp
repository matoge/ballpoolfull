#include "game.h"
#include "Render.h"
#include "constants.h"
#define _USE_MATH_DEFINES
#include <math.h>

game::game(int nProblem)
{	
	b2Vec2 gravity;
	gravity.Set(0, 0);
	m_world = new b2World(gravity);
	m_debugDraw = new DebugDraw();
	eTime = ETIME;

	nProblem %= MAX_PROBLEM;
	switch(nProblem){
		case 0:
			{
#include "problem1_ans.h"
		ans_list = bodylist;
		max_score = 30;
		break;
			}
		case 1:
			{
#include "problem2_ans.h"
		ans_list = bodylist;
		max_score = 20;
		break;
			}
		case 2:
			{
#include "problem3_ans.h"
		ans_list = bodylist;
		max_score = 45;
		break;
			}
		case 3:
			{
#include "problem5_ans.h"
		ans_list = bodylist;
		max_score = 20;
		break;
			}
		case 4:
			{
#include "problem6_ans.h"
		ans_list = bodylist;
		max_score = 10;
		break;
			}
		case 5:
			{
#include "problem4_ans.h"
		ans_list = bodylist;
		max_score = 10;
		break;
			}
	}
}

game::~game(void){
	//DestroyProblem();
}

void game::DestroyProblem(){
	b2Body* pB = m_world->GetBodyList();
	while(pB){
		m_world->DestroyBody(pB);
		pB->GetNext();
		}
}


void game::DrawColor(){
	// Draw Cake
	b2Body* it = m_world->GetBodyList();
	while( it != NULL ){
		uData* ud = static_cast<uData*>( it->GetUserData() );
		const b2Transform& xf = it->GetTransform();
		b2Fixture* f = it->GetFixtureList();
		DrawShape2(f, xf, ud->fill, ud->stroke, ud->strokewidth);
		it = it->GetNext();
	}
}


void game::DrawShape2(b2Fixture* fixture, const b2Transform& xf, const b2Color& color, const b2Color& color2, const float linewidth)
{
	switch (fixture->GetType())
	{
	case b2Shape::e_circle:
		{
			b2CircleShape* circle = (b2CircleShape*)fixture->GetShape();

			b2Vec2 center = b2Mul(xf, circle->m_p);
			float32 radius = circle->m_radius;
			b2Vec2 axis = b2Vec2(xf.q.c,-xf.q.s);

			m_debugDraw->DrawSolidCircle2(center, radius, axis, color, color2, linewidth);
		}
		break;

	case b2Shape::e_polygon:
		{
			b2PolygonShape* poly = (b2PolygonShape*)fixture->GetShape();
			int32 vertexCount = poly->m_vertexCount;
			b2Assert(vertexCount <= b2_maxPolygonVertices);
			b2Vec2 vertices[b2_maxPolygonVertices];

			for (int32 i = 0; i < vertexCount; ++i)
			{
				vertices[i] = b2Mul(xf, poly->m_vertices[i]);
			}

			m_debugDraw->DrawSolidPolygon2(vertices, vertexCount, color, color2, linewidth);
		}
		break;
	}
}


bool game::isMatched(vector<b2Body*> prb_list){
	vector<b2Body*>::iterator it_prb;
	vector<b2Body*>::iterator it_ans;
	int nCombinations=0;

	if( prb_list.size() != ans_list.size() ){
		printf("Error: Size is different. \n");
		return false;
	}

	double score=0.0f;

	/* Compare all pairs */
	// Relative position
	for( it_prb = prb_list.begin(), it_ans = ans_list.begin() ; it_prb != prb_list.end()-1; it_prb++, it_ans++){
		vector<b2Body*>::iterator it_prb2 = it_prb+1;
		vector<b2Body*>::iterator it_ans2 = it_ans+1;
		for( it_prb2 ; it_prb2 != prb_list.end(); it_prb2++, it_ans2++){
			score += abs((*it_prb)->GetPosition().x - (*it_prb2)->GetPosition().x - ((*it_ans)->GetPosition().x - (*it_ans2)->GetPosition().x));
			score += abs((*it_prb)->GetPosition().y - (*it_prb2)->GetPosition().y - ((*it_ans)->GetPosition().y - (*it_ans2)->GetPosition().y));
			nCombinations++; // Combination of pairs. Determines the threshold of score/
		}
	}
	score *= 10;

	// Each angles
	int id=0;
	for( it_prb = prb_list.begin(), it_ans = ans_list.begin() ; it_prb != prb_list.end(); it_prb++, it_ans++){
		double difangle = abs((*it_prb)->GetAngle() - (*it_ans)->GetAngle())/(M_PI/2);
		score += abs(difangle - (int)(difangle+0.5))*90;
		//cout << "id:" << id << " difangle:" << abs(difangle - (int)(difangle+0.5))*90 << "; ";
		id++;
	}

	//cout << "Score is " << score << endl;
	if( score < nCombinations*3)
		return true;
	else
		return false;
}

