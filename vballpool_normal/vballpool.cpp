
#include "vBallpool.h"
#include "constants.h"
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>
#include <time.h>
#include "vBsound.h"
extern int nframes;
extern int score1;
extern int score2;
extern vBsound *vS;
extern int gScore;

extern double speed[MAX_USERS];
extern double prevPos[MAX_USERS];
extern int userframes[MAX_USERS];

/// Random floating point number in range [lo, hi]
inline float32 RandomFloat(float32 lo, float32 hi)
{
	float32 r = (float32)(rand() & (RAND_LIMIT));
	r /= RAND_LIMIT;
	r = (hi - lo) * r + lo;
	return r;
}

inline b2Color GetBrilliant(){
	// Choose Brilliant Color : set one element to zero.
	int zeroColor = (int)(RandomFloat(0.0f,2.99f));
	float colorR = RandomFloat(0.3f,1.0f);
	float colorB = 1.3f - colorR;
	float colorG = 0.1f;
	// Shuffle Color;
	if( RandomFloat(0.0f,1.0f) < 0.5f){
		float tmp = colorR;
		colorR = colorB;
		colorB = tmp;
	}
	if( RandomFloat(0.0f,1.0f) < 0.5f){
		float tmp = colorR;
		colorR = colorG;
		colorG = tmp;
	}
	if( RandomFloat(0.0f,1.0f) < 0.5f){
		float tmp = colorB;
		colorB = colorG;
		colorG = tmp;
	}
	return b2Color(colorR, colorG, colorB);
}
/* 
	Here you can make the world.
*/
vBallpool::vBallpool(void)
{

	 srand((unsigned int)time(NULL));

	b2Vec2 gravity;
	gravity.Set(GRAVITY_X, GRAVITY_Y);
	bool doSleep = true;
	m_world = new b2World(gravity);
	m_debugDraw = new DebugDraw();

	bHanged=false;
	// DebugDraw functions are overrided from Box2D virtual function

	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0.0f, 0.0f);

	// The extents are the half-widths of the box.

	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBody = m_world->CreateBody(&groundBodyDef);


	b2EdgeShape shape;
	b2FixtureDef sd;
	sd.shape = &shape;
	sd.restitution = 0.1f;
	sd.friction = 0.5f;
	sd.filter.groupIndex=-2;
	// Bottom horizontal
	shape.Set(b2Vec2(-100*WW, -0.0f), b2Vec2(100*WW, -0.0f));
	groundBody->CreateFixture(&sd);

	
	// Wall
	
	b2BodyDef wallBodyDef;
	wallBodyDef.position.Set(0.0f, 0.0f);

	// The extents are the half-widths of the box.

	wallBody = m_world->CreateBody(&wallBodyDef);
	sd.shape = &shape;
	sd.restitution = 0.0f;
	sd.friction = 0.0f;
	sd.filter.groupIndex = -1;

	// Left vertical
	shape.Set(b2Vec2(-WW, -0.0f), b2Vec2(-WW, 3*WH));
	wallBody->CreateFixture(&sd);

	// Right vertical
	shape.Set(b2Vec2(WW, -0.0f), b2Vec2(WW, 3*WH));
	wallBody->CreateFixture(&sd);

	// Top horizontal
	shape.Set(b2Vec2(-WW, 3*WH), b2Vec2(WW, 3*WH));
	wallBody->CreateFixture(&sd);


	/*
	Creating Ragdolls
	*/
	m_ragdools[0] = new ragdoll(m_world, groundBody, RAGDOLL_ROBOT, 0);
	m_ragdools[1] = new ragdoll(m_world, groundBody, RAGDOLL_HUMAN, 1);

	/* This is the title */
//#include "title.h"
//	title_bodylist = bodylist;


}

void vBallpool::Step(float32 timeStep, int32 velocityIterations, int32 positionIterations){

	// Update
	m_world->Step(timeStep, velocityIterations, positionIterations);


	if(m_ragdools[0]->isfree() && m_ragdools[1]->isfree()){
		float th = RandomFloat(0.0f, 100.0f);
		if(th < 3.0f)
			dropCircle();
		if(th < 0.5f)
			dropStar();
		
	}

	//if(nframes%30 == 0){
	//	dropCircle();
	//	dropStar();
	//}


	vector<b2Body*>::iterator it = title_bodylist.begin();
	for( it; it != title_bodylist.end(); it++){
		b2Vec2 f = (*it) ->GetWorldPoint(b2Vec2(0.0f,0.0f));
		if(( f.x > WW-7.5f) && (f.y < WW) ||
			( f.x < -WW+7.5f) && (f.y < WW) )
			(*it)->ApplyForce(b2Vec2(0 ,16.0f),f);
	}



	// printf("nframes %d\n", nframes);
	nframes++;
	
	// Draw the shapes of m_world.
#ifdef DEBUG
	Draw();
#else
	DrawColor();
#endif
}

void vBallpool::UpdateSkeletons(){

	// Calculate Users Speeds
	
	XnUInt16 nUsers = g_UserGenerator.GetNumberOfUsers();
	XnUserID aUsers[MAX_USERS];
	for (int i=0;i<nUsers;i++) aUsers[i]=0;
	g_UserGenerator.GetUsers(aUsers, nUsers);

	// User selection part
	XnUInt16 userDepth[MAX_USERS];
	for (int i=0;i<nUsers;i++){
		if(g_UserGenerator.GetSkeletonCap().IsTracking(aUsers[i])){
			// Calculate users speeds
			userframes[i]++;
			XnSkeletonJointPosition torso;
			if(userframes[i] < 10){
				prevPos[i] = 0;
				speed[i] = 0;
				userDepth[aUsers[i]] = 10000;
			} else{
				g_UserGenerator.GetSkeletonCap().GetSkeletonJointPosition(i, XN_SKEL_TORSO, torso);
				// Filter user speed with forgetting factor 
				const float forgetting = 0.99;
				speed[i] = forgetting*speed[i] + abs(torso.position.X - prevPos[i])/sqrt((double)userframes[i]);
				//printf("nID: %d, speed: %f, userframes: %d\n", i,speed[i],userframes[i]);
				prevPos[i] = torso.position.X;
				userDepth[aUsers[i]] = torso.position.Z;
			}
		}else{
			userDepth[aUsers[i]] = 10000;
		}
		//cout << userframes[i] << " uid:" << aUsers[i] << "," << userDepth[aUsers[i]] << endl;
	}
	// list userDepth

	// Update ragdools
	for (int i = 0; i < MAX_DOOLS; ++i){
		/* Ragdoll process is two folds:
		1. Assign if new user is detected
		2. Update joints
		*/

		if( m_ragdools[i]->uid == 0 ){// ragdoll is free
			for (int j=0;j<nUsers;j++){
				if(g_UserGenerator.GetSkeletonCap().IsTracking(aUsers[j])){
					// This function could not be doll-private
					// since it consults m_ragdools, whole ragdools of vBallpool class.
					for ( int k=0; k<sizeof(m_ragdools)/sizeof(ragdoll*); k++){
						if( k != m_ragdools[i]->doolid ){
							if (m_ragdools[k]->uid != aUsers[j]){
								m_ragdools[i]->uid = aUsers[j];
								std::cout << "dool initialized: uid = " <<m_ragdools[i]->uid << endl;
								continue;
							}
						}else{						
							// already assigned
						}
					}
				}
			}
			return; // available user not found.
		} else {
			// check if uid is valid. if not, loose joint and set uid to zero
			if ( !g_UserGenerator.GetSkeletonCap().IsTracking(m_ragdools[i]->uid) ){
				printf("loose all");
				m_ragdools[i]->mj_looseall();
				m_ragdools[i]->uid = 0;
				return;
			}

		}

		m_ragdools[i]->update(&g_UserGenerator, &g_DepthGenerator);

		//cout << "dolls:" << m_ragdools[i]->uid << endl;
	}
}


void vBallpool::Draw(){
	for (b2Body* b = m_world->GetBodyList(); b; b = b->GetNext())
	{
		const b2Transform& xf = b->GetTransform();
		for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext())
		{
			if (b->IsActive() == false)
			{
				DrawShape(f, xf, b2Color(0.5f, 0.5f, 0.3f));
			}
			else if (b->GetType() == b2_staticBody)
			{
				DrawShape(f, xf, b2Color(0.5f, 0.9f, 0.5f));
			}
			else if (b->GetType() == b2_kinematicBody)
			{
				DrawShape(f, xf, b2Color(0.5f, 0.5f, 0.9f));
			}
			else if (b->IsAwake() == false)
			{
				DrawShape(f, xf, b2Color(0.6f, 0.6f, 0.6f));
			}	
			else
			{
				DrawShape(f, xf, b2Color(0.9f, 0.7f, 0.7f));
			}
		}
	}

	for (b2Joint* j = m_world->GetJointList(); j; j = j->GetNext())
	{
		DrawJoint(j);
	}

}

void vBallpool::DrawColor(){

	// Draw Balls
	vector<b2Body*>::iterator it;

	
	// Draw Fireworks
	it = fireworks_bodylist.begin();

		for( it; it != fireworks_bodylist.end(); it++){
			uData* ud = static_cast<uData*>( (*it)->GetUserData() );
			const b2Transform& xf = (*it)->GetTransform();
			b2Fixture* f = (*it)->GetFixtureList();
			DrawShape2(f, xf, ud->fill, ud->stroke, ud->strokewidth);
		}
	
	it = title_bodylist.begin();
	for( it; it != title_bodylist.end(); it++){
		uData* ud = static_cast<uData*>( (*it)->GetUserData() );
		const b2Transform& xf = (*it)->GetTransform();
		b2Fixture* f = (*it)->GetFixtureList();
		DrawShape2(f, xf, ud->fill, ud->stroke, ud->strokewidth);
	}

	// Draw Targets
	it = blackets_bodylist.begin();

	for( it; it != blackets_bodylist.end(); it++){
		uData* ud = static_cast<uData*>( (*it)->GetUserData() );
		const b2Transform& xf = (*it)->GetTransform();
		b2Fixture* f = (*it)->GetFixtureList();
		DrawShape2(f, xf, ud->fill, ud->stroke, ud->strokewidth);
	}

	// Draw Rails
	it = rails_bodylist.begin();
	for( it; it != rails_bodylist.end(); it++){
		uData* ud = static_cast<uData*>( (*it)->GetUserData() );
		const b2Transform& xf = (*it)->GetTransform();
		b2Fixture* f = (*it)->GetFixtureList();
		DrawShape2(f, xf, ud->fill, ud->stroke, ud->strokewidth);
	}
	// Draw Cake
	it = cake_bodylist.begin();
	for( it; it != cake_bodylist.end(); it++){
		uData* ud = static_cast<uData*>( (*it)->GetUserData() );
		const b2Transform& xf = (*it)->GetTransform();
		b2Fixture* f = (*it)->GetFixtureList();
		DrawShape2(f, xf, ud->fill, ud->stroke, ud->strokewidth);
	}

	// Draw Ragdolls
	for ( int i=0; i< MAX_DOOLS; i++){
		it = m_ragdools[i]->ragdoll_bodylist.begin();
		for( it; it != m_ragdools[i]->ragdoll_bodylist.end(); it++){
			uData* ud = static_cast<uData*>( (*it)->GetUserData() );
			const b2Transform& xf = (*it)->GetTransform();
			b2Fixture* f = (*it)->GetFixtureList();
			DrawShape2(f, xf, ud->fill, ud->stroke, ud->strokewidth);
		}
	}
	
	// Draw joints
	vector<b2Joint*>::iterator it2 = blackets_jointlist.begin();
	for( it2; it2 != blackets_jointlist.end(); it2++){
		DrawJoint(*it2);
	}

}


void vBallpool::DrawShape(b2Fixture* fixture, const b2Transform& xf, const b2Color& color)
{
	switch (fixture->GetType())
	{
	case b2Shape::e_circle:
		{
			b2CircleShape* circle = (b2CircleShape*)fixture->GetShape();

			b2Vec2 center = b2Mul(xf, circle->m_p);
			float32 radius = circle->m_radius;
			b2Vec2 axis =  b2Mul(xf.q, b2Vec2(1.0f, 0.0f));

			m_debugDraw->DrawSolidCircle(center, radius, axis, color);
		}
		break;

	case b2Shape::e_polygon:
		{
			b2PolygonShape* poly = (b2PolygonShape*)fixture->GetShape();
			int32 vertexCount = poly->m_vertexCount;
			b2Assert(vertexCount <= b2_maxPolygonVertices);
			b2Vec2 vertices[b2_maxPolygonVertices];

			for (int32 i = 0; i < vertexCount; ++i)
			{
				vertices[i] = b2Mul(xf, poly->m_vertices[i]);
			}

			m_debugDraw->DrawSolidPolygon(vertices, vertexCount, color);
		}
		break;
	}
}

void vBallpool::DrawShape2(b2Fixture* fixture, const b2Transform& xf, const b2Color& color, const b2Color& color2, const float linewidth)
{
	switch (fixture->GetType())
	{
	case b2Shape::e_circle:
		{
			b2CircleShape* circle = (b2CircleShape*)fixture->GetShape();

			b2Vec2 center = b2Mul(xf, circle->m_p);
			float32 radius = circle->m_radius;
			b2Vec2 axis =  b2Mul(xf.q, b2Vec2(1.0f, 0.0f));

			m_debugDraw->DrawSolidCircle2(center, radius, axis, color, color2, linewidth);
		}
		break;

	case b2Shape::e_polygon:
		{
			b2PolygonShape* poly = (b2PolygonShape*)fixture->GetShape();
			int32 vertexCount = poly->m_vertexCount;
			b2Assert(vertexCount <= b2_maxPolygonVertices);
			b2Vec2 vertices[b2_maxPolygonVertices];

			for (int32 i = 0; i < vertexCount; ++i)
			{
				vertices[i] = b2Mul(xf, poly->m_vertices[i]);
			}

			m_debugDraw->DrawSolidPolygon2(vertices, vertexCount, color, color2, linewidth);
		}
		break;
	}
	
}



void vBallpool::DrawJoint(b2Joint* joint)
{
	b2Body* bodyA = joint->GetBodyA();
	b2Body* bodyB = joint->GetBodyB();
	const b2Transform& xf1 = bodyA->GetTransform();
	const b2Transform& xf2 = bodyB->GetTransform();
	b2Vec2 x1 = xf1.p;
	b2Vec2 x2 = xf2.p;
	b2Vec2 p1 = joint->GetAnchorA();
	b2Vec2 p2 = joint->GetAnchorB();

	b2Color color(0.5f, 0.8f, 0.8f);

	switch (joint->GetType())
	{
	case e_distanceJoint:
		m_debugDraw->DrawSegment(p1, p2, color);
		break;

	case e_pulleyJoint:
		{
			b2PulleyJoint* pulley = (b2PulleyJoint*)joint;
			b2Vec2 s1 = pulley->GetGroundAnchorA();
			b2Vec2 s2 = pulley->GetGroundAnchorB();
			m_debugDraw->DrawSegment(s1, p1, color);
			m_debugDraw->DrawSegment(s2, p2, color);
			m_debugDraw->DrawSegment(s1, s2, color);
		}
		break;

	case e_mouseJoint:
		m_debugDraw->DrawSegment(p1, p2, color);
		// don't draw this
		break;

	case e_revoluteJoint:
		m_debugDraw->DrawSegment(p1+b2Vec2(0.1f,0.1f), p1, color);
		m_debugDraw->DrawSegment(p1, p2, color);
		m_debugDraw->DrawSegment(p2+b2Vec2(0.1f,0.1f), p2, color);
		break;

	default:
		m_debugDraw->DrawSegment(x1, p1, color);
		m_debugDraw->DrawSegment(p1, p2, color);
		m_debugDraw->DrawSegment(x2, p2, color);
	}
}

void vBallpool::mj_intialize(){
	// robot
	mj_head = NULL;
	mj_neck= NULL;

	mj_torso= NULL;

	mj_righthand= NULL;
	mj_rightelbow= NULL;
	mj_lefthand= NULL;
	mj_leftelbow= NULL;

	mj_rightknee= NULL;
	mj_rightfoot= NULL;
	mj_leftknee= NULL;
	mj_leftfoot= NULL;

}
void vBallpool::mj_destroy(){

}

void vBallpool::dropTitle(){
	vector<b2Body*>::iterator it = title_bodylist.begin();

	for( it; it != title_bodylist.end(); it++){
		//std::cout << (*it)->GetType() << "type\n";
		(*it)->SetType(b2_dynamicBody);
		(*it)->SetLinearVelocity(b2Vec2(RandomFloat(-6.0f, 6.0f),RandomFloat(-2.0f, 12.0f)));
		(*it)->GetFixtureList()->SetRestitution(RandomFloat(0.5f, 1.0f));
		b2Filter fl = (*it)->GetFixtureList()->GetFilterData();
		fl.groupIndex= 1 ;
		(*it)->GetFixtureList()->SetFilterData(fl);
	}

}


void vBallpool::dropCircle(){

	b2CircleShape shape;
	shape.m_radius = RandomFloat(0.5f,1.2f);
	if(shape.m_radius>1.195f){
		shape.m_radius = RandomFloat(3.0f,3.2f);
	}
	b2FixtureDef fd;
	fd.shape = &shape;
	fd.density = RandomFloat(1.0f,1.4f);

	float32 restitution[7] = {0.0f, 0.2f, 0.3f, 0.5f, 0.3f, 0.2f, 0.6f};

	//for (int32 i = 0; i < 1; ++i)
	//{
		b2BodyDef bd;

		bd.type = b2_dynamicBody;
		bd.bullet = false;
		bd.linearDamping = 0.0f;
		bd.linearVelocity = b2Vec2(RandomFloat(-2.0f, 2.0f),RandomFloat(1.0f, 4.0f));
		bd.position.Set( RandomFloat(-WW/2, WW/2), RandomFloat(30.0f, 32.0f));
		
		b2Body* body = m_world->CreateBody(&bd);
		fd.density = RandomFloat(0.1f, 1.0f);
		fd.friction = 1.0f;
		fd.restitution = RandomFloat(0.1f,0.4f);//  restitution[i%7];

		body->CreateFixture(&fd);

		//bodylist.push_back(body);
		uData* ud = new uData();
		ud->name = "Title";

		ud->fill = GetBrilliant();
		ud->stroke = b2Color(RandomFloat(0.3f, 0.7f), RandomFloat(0.3f, 0.7f), RandomFloat(0.3f, 0.7f));
		ud->strokewidth = 4.0f;
		body->SetUserData(ud);
		title_bodylist.push_back(body);
	//}

}

void vBallpool::fireworks(){
	b2Vec2 direction;
	b2Vec2 position = b2Vec2( RandomFloat(-WW, WW), RandomFloat( WH/1.5, WH*1.5) );
	float angle;

	b2Color color;
	int nBalls = 4;
	int nLoops = 4;
	color = GetBrilliant();
	for( int i=1; i <= nLoops ; i++ ){
		for( int j=0; j< nBalls *i ; j++ ){
			angle = 2 * M_PI / nBalls / i  * j;
			direction = 3.0f * i*i * b2Vec2( cos(angle) , 0.2f + sin(angle) );
			//direction = i * b2Vec2( cos(angle) , 0.2f + sin(angle) );
			dropParticle( direction, position, color );
		}
		color = b2Color( color.r*1.2f, color.g*1.2f, color.b*1.2f);
	}
	vS->playSE(string("ef_fireworks"));
}

void vBallpool::dropParticle(b2Vec2 direction, b2Vec2 position, b2Color color){

	b2CircleShape shape;
	shape.m_radius = 0.3f;

	//b2PolygonShape shape;
	//shape.SetAsBox(0.2f, 0.2f);

	b2BodyDef bd;
	b2FixtureDef fd;

	fd.shape = &shape;
	fd.filter.maskBits = 0x0000;
	fd.restitution = -2.4f;
	fd.density = 10.0f;

	bd.position.Set(position.x, position.y);
	bd.type = b2_dynamicBody;
	bd.linearDamping = 0.4f;

	b2Body* ground = m_world->CreateBody(&bd);
	ground->CreateFixture(&fd);
	ground->ApplyForce(direction, b2Vec2( position.x+10.0f, position.y+10.0f) );

	uData* ud = new uData();
	ud->name = "Target";
	ud->fill = color;
	ud->stroke = color;
	ud->strokewidth = 0.0f;
	ground->SetUserData(ud);
	fireworks_bodylist.push_back(ground);

}
void vBallpool::dropStar(){

	b2BodyDef bd;

	bd.type = b2_dynamicBody;
	bd.bullet = false;
	//bd.linearVelocity = b2Vec2(RandomFloat(-2.0f, 2.0f),RandomFloat(1.0f, 4.0f));
	bd.position.Set( RandomFloat(-WW/2, WW/2), RandomFloat(30.0f, 32.0f));

	b2Body* body = m_world->CreateBody(&bd);
	/* Pentagon */
	b2FixtureDef fd;
	fd.density = 1.0f;//RandomFloat(10.1f, 10.0f);
	fd.friction = 0.0f;
	fd.restitution = RandomFloat(0.3f,0.8f);
	

	b2PolygonShape shape;
	float s = RandomFloat(0.04f,0.13f);

	
	// Outer triangle 1
	b2Vec2 ver2[3];
	ver2[0].Set(5.26f*s,7.24f*s);
	ver2[1].Set(0,20.7f*s);
	ver2[2].Set(-5.26f*s,7.24f*s);

	fd.shape = &shape;
	shape.Set(ver2,3);
	body->CreateFixture(&fd);

	// Outer triangle 2
	ver2[0].Set(-5.26f*s,7.24f*s);
	ver2[1].Set(-19.68f*s,6.4f*s);
	ver2[2].Set(-8.52f*s,-2.77f*s);

	fd.shape = &shape;
	shape.Set(ver2,3);
	body->CreateFixture(&fd);

	// Outer triangle 3
	ver2[0].Set(-8.52f*s,-2.77f*s);
	ver2[1].Set(-12.17f*s,-16.74f*s);
	ver2[2].Set(0, -8.95f*s);

	fd.shape = &shape;
	shape.Set(ver2,3);
	body->CreateFixture(&fd);

	// Outer triangle 4
	ver2[0].Set(0, -8.95f*s);
	ver2[1].Set(12.17f*s,-16.74f*s);
	ver2[2].Set(8.52f*s,-2.77f*s);

	fd.shape = &shape;
	shape.Set(ver2,3);
	body->CreateFixture(&fd);

	// Outer triangle 5
	ver2[0].Set(8.52f*s,-2.77f*s);
	ver2[1].Set(19.68f*s,6.4f*s);
	ver2[2].Set(5.26f*s,7.24f*s);

	fd.shape = &shape;
	shape.Set(ver2,3);
	body->CreateFixture(&fd);
	
	b2Vec2 ver[10];
	ver[0].Set(5.26f*s,7.24f*s);
	//ver[1].Set(0,20.7f*s);
	ver[1].Set(-5.26f*s,7.24f*s);
	//ver[3].Set(-19.68f*s,6.4f*s);
	ver[2].Set(-8.52f*s,-2.77f*s);
	//ver[5].Set(-12.17f*s,-16.74f*s);
	ver[3].Set(0, -8.95f*s);
	//ver[7].Set(12.17f*s,-16.74f*s);
	ver[4].Set(8.52f*s,-2.77f*s);
	//ver[9].Set(19.68f*s,6.4f*s);

	fd.shape = &shape;
	shape.Set(ver,5);
	body->CreateFixture(&fd);
	
	ver[0].Set(5.26f*s,7.24f*s);
	ver[1].Set(0,20.7f*s);
	ver[2].Set(-5.26f*s,7.24f*s);
	ver[3].Set(-19.68f*s,6.4f*s);
	ver[4].Set(-8.52f*s,-2.77f*s);
	ver[5].Set(-12.17f*s,-16.74f*s);
	ver[6].Set(0, -8.95f*s);
	ver[7].Set(12.17f*s,-16.74f*s);
	ver[8].Set(8.52f*s,-2.77f*s);
	ver[9].Set(19.68f*s,6.4f*s);

	fd.shape = &shape;
	shape.Set(ver,10);
	body->CreateFixture(&fd);


	//bodylist.push_back(body);
	uData* ud = new uData();
	ud->name = "Title";
	ud->fill = GetBrilliant();
	ud->stroke = b2Color(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f));
	ud->strokewidth = 1.0f;
	body->SetUserData(ud);
	title_bodylist.push_back(body);
	
}

void vBallpool::createHanged(float y){
	/* Target You shoud hit */

	b2Body* body;
	b2CircleShape shape1;
	shape1.m_radius = 1.1f;
	b2FixtureDef fd;
	fd.shape = &shape1;
	fd.density = 0.01f;

	b2BodyDef bd;
	bd.type = b2_dynamicBody;

	bd.position.Set(y, WW-2.0f);
	body = m_world->CreateBody(&bd);
	body->CreateFixture(&shape1, 20.0f);
	//bodylist.push_back(body);
	uData* ud = new uData();
	ud->name = "Target";
	ud->fill = b2Color(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f));
	ud->stroke = b2Color(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f));
	ud->strokewidth = 4.0f;
	body->SetUserData(ud);
	blackets_bodylist.push_back(body);


	b2DistanceJointDef jd;
	b2Vec2 p1, p2, d;

	jd.frequencyHz = 4.0f;
	jd.dampingRatio = 0.5f;

	jd.bodyA = groundBody;
	jd.bodyB = body;
	jd.localAnchorA.Set(y, WW+8.0f);
	jd.localAnchorB.Set(0, 0);
	p1 = jd.bodyA->GetWorldPoint(jd.localAnchorA);
	p2 = jd.bodyB->GetWorldPoint(jd.localAnchorB);
	d = p2 - p1;
	jd.length = d.Length();
	blackets_jointlist.push_back(m_world->CreateJoint(&jd));

}

void vBallpool::createCake(int nProblem){
		destroyCake();
		nProblem = nProblem % ( MAX_PROBLEM );
		switch(nProblem){
			case 0:
				{
#include "problem1.h"
			cake_bodylist = bodylist;
				}
			break;
			case 1:
				{
#include "problem2.h"
			cake_bodylist = bodylist;
				}
			break;
			case 2:
				{
#include "problem3.h"
			cake_bodylist = bodylist;
				}
			break;
			case 3:
				{
#include "problem5.h"
			cake_bodylist = bodylist;
				}
			break;
			case 4:
				{
#include "problem6.h"
			cake_bodylist = bodylist;
				}
			break;
			case 5:
				{
#include "problem4.h"
			cake_bodylist = bodylist;
				}
			break;
		}
	
}

void vBallpool::destroyCake(){
	
		// Destroy all items
		vector<b2Body*>::iterator it = cake_bodylist.begin();
		for( it; it != cake_bodylist.end(); ){
			m_world->DestroyBody(*it);
			cake_bodylist.erase(it);
		}
}
void vBallpool::createRail(){
	
	b2PolygonShape shape;
	b2BodyDef bd;
	b2FixtureDef fd;
	{
		shape.SetAsBox(5.0f, 0.25f);

		fd.shape = &shape;
		fd.filter.groupIndex = -1;
		fd.restitution = -2.4f;

		bd.position.Set(-7.0f, 13.0f);
		bd.angle = 0.25f;

		b2Body* ground = m_world->CreateBody(&bd);
		ground->CreateFixture(&fd);

		uData* ud = new uData();
		ud->name = "Target";
		ud->fill = b2Color(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f));
		ud->stroke = b2Color(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f));
		ud->strokewidth = 4.0f;
		ground->SetUserData(ud);
		rails_bodylist.push_back(ground);
	}
	{
		shape.SetAsBox(4.5f, 0.25f);

		fd.shape = &shape;
		fd.filter.groupIndex = -1;
		fd.restitution = -2.4f;

		bd.position.Set(-13.0f, 8.0f);
		bd.angle = -0.35f;

		b2Body* ground = m_world->CreateBody(&bd);
		ground->CreateFixture(&fd);

		uData* ud = new uData();
		ud->name = "Target";
		ud->fill = b2Color(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f));
		ud->stroke = b2Color(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f));
		ud->strokewidth = 4.0f;
		ground->SetUserData(ud);
		rails_bodylist.push_back(ground);
	}

	{
		shape.SetAsBox(6.0f, 0.25f);

		fd.shape = &shape;
		fd.filter.groupIndex = -1;
		fd.restitution = -2.4f;

		bd.position.Set(-7.0f, 4.0f);
		bd.angle = 0.25f;

		b2Body* ground = m_world->CreateBody(&bd);
		ground->CreateFixture(&fd);

		uData* ud = new uData();
		ud->name = "Target";
		ud->fill = b2Color(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f));
		ud->stroke = b2Color(RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f), RandomFloat(0.0f, 1.0f));
		ud->strokewidth = 4.0f;
		ground->SetUserData(ud);
		rails_bodylist.push_back(ground);
	}


}

void vBallpool::dropAll(){
	vector<b2Body*>::iterator it = title_bodylist.begin();
	for( it; it != title_bodylist.end(); it++){
		(*it)->SetLinearVelocity(b2Vec2(RandomFloat(-1.0f, 1.0f),RandomFloat(-0.0f, 1.0f)));
		b2Fixture* fx = (*it)->GetFixtureList();
		b2Filter fl;
		for( fx ; fx!=NULL ; fx = fx->GetNext()){
			fl = fx->GetFilterData();
			fl.groupIndex= -2 ;
			fx->SetFilterData(fl);
		}
	}
}

void vBallpool::destroyOne(){
	vector<b2Body*>::iterator it = title_bodylist.begin();

	if( it != title_bodylist.end()){
		m_world->DestroyBody(*it);
		title_bodylist.erase(it);
	}
}

void vBallpool::changeGravity(float x, float y){
	m_world->SetGravity(m_world->GetGravity() + b2Vec2(x,y));
}
void vBallpool::createTitle(){
	#include "circle.h"
	title_bodylist = bodylist;
}

void vBallpool::resetGravity(){
	m_world->SetGravity(b2Vec2(GRAVITY_X, GRAVITY_Y));
}

void vBallpool::changeColors(vector<b2Body*> bodies){
	vector<b2Body*>::iterator it = bodies.begin();
	for( it; it != bodies.end(); it++){
		uData *ud = (struct uData *)((*it)->GetUserData());
		ud -> fill = b2Color(RandomFloat(0.5f, 1.0f), RandomFloat(0.5f, 1.0f), RandomFloat(0.5f, 1.0f));
	}
}
