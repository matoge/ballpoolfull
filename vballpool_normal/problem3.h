vector<b2Body*> bodylist;

//
//rect3072
//
b2BodyDef rect3072_bdef;
rect3072_bdef.type = b2_dynamicBody;
rect3072_bdef.bullet = false;
rect3072_bdef.position.Set(-2.75627952367f,9.31224823428f);
rect3072_bdef.angle=-0.0f;
rect3072_bdef.linearDamping = 0.3f;
rect3072_bdef.angularDamping =0.3f;
b2Body* rect3072_body = m_world->CreateBody(&rect3072_bdef);

b2PolygonShape rect3072_ps;
rect3072_ps.SetAsBox(2.48521551336f, 2.48521551336f);

b2FixtureDef rect3072_fdef;
rect3072_fdef.shape = &rect3072_ps;

rect3072_fdef.density = 1.0f;
rect3072_fdef.friction = 0.2f;
rect3072_fdef.restitution = 0.4f;
rect3072_fdef.filter.groupIndex = 0;

rect3072_body->CreateFixture(&rect3072_fdef);
bodylist.push_back(rect3072_body);
uData* rect3072_ud = new uData();
rect3072_ud->name = "TITLE";
rect3072_ud->fill.Set(float(0)/255,float(255)/255,float(0)/255);
rect3072_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rect3072_ud->strokewidth=3.62149692f;
rect3072_body->SetUserData(rect3072_ud);

//
//rect3073
//
b2BodyDef rect3073_bdef;
rect3073_bdef.type = b2_dynamicBody;
rect3073_bdef.bullet = false;
rect3073_bdef.position.Set(-8.28318803554f,12.5199198612f);
rect3073_bdef.angle=-0.0f;
rect3073_bdef.linearDamping = 0.3f;
rect3073_bdef.angularDamping =0.3f;
b2Body* rect3073_body = m_world->CreateBody(&rect3073_bdef);

b2PolygonShape rect3073_ps;
rect3073_ps.SetAsBox(2.48521551336f, 2.48521551336f);

b2FixtureDef rect3073_fdef;
rect3073_fdef.shape = &rect3073_ps;

rect3073_fdef.density = 1.0f;
rect3073_fdef.friction = 0.2f;
rect3073_fdef.restitution = 0.4f;
rect3073_fdef.filter.groupIndex = 0;

rect3073_body->CreateFixture(&rect3073_fdef);
bodylist.push_back(rect3073_body);
uData* rect3073_ud = new uData();
rect3073_ud->name = "TITLE";
rect3073_ud->fill.Set(float(255)/255,float(0)/255,float(0)/255);
rect3073_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rect3073_ud->strokewidth=1.20676339f;
rect3073_body->SetUserData(rect3073_ud);

//
//rect3074
//
b2BodyDef rect3074_bdef;
rect3074_bdef.type = b2_dynamicBody;
rect3074_bdef.bullet = false;
rect3074_bdef.position.Set(2.57432331711f,16.0551225905f);
rect3074_bdef.angle=-0.0f;
rect3074_bdef.linearDamping = 0.3f;
rect3074_bdef.angularDamping =0.3f;
b2Body* rect3074_body = m_world->CreateBody(&rect3074_bdef);

b2PolygonShape rect3074_ps;
rect3074_ps.SetAsBox(2.48521551336f, 2.48521551336f);

b2FixtureDef rect3074_fdef;
rect3074_fdef.shape = &rect3074_ps;

rect3074_fdef.density = 1.0f;
rect3074_fdef.friction = 0.2f;
rect3074_fdef.restitution = 0.4f;
rect3074_fdef.filter.groupIndex = 0;

rect3074_body->CreateFixture(&rect3074_fdef);
bodylist.push_back(rect3074_body);
uData* rect3074_ud = new uData();
rect3074_ud->name = "TITLE";
rect3074_ud->fill.Set(float(0)/255,float(0)/255,float(255)/255);
rect3074_ud->stroke.Set(float(0)/255,float(0)/255,float(0)/255);
rect3074_ud->strokewidth=3.62149692f;
rect3074_body->SetUserData(rect3074_ud);
